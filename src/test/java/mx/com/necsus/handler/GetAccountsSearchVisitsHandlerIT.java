package mx.com.necsus.handler;

import static io.undertow.util.Headers.CONTENT_TYPE_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.FORBIDDEN;
import static io.undertow.util.StatusCodes.OK;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.URLEncoder.encode;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static java.nio.charset.Charset.defaultCharset;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.AccountStatus.CERTIFIED;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.util.AccessCrypto.encrypt;
import static mx.com.necsus.util.AccessHeaders.X_ACCESS_TOKEN;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_JSON;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBaseGetAccountsSearchVisitsHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link GetAccountsSearchVisitsHandler}.
 */
public class GetAccountsSearchVisitsHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBaseGetAccountsSearchVisitsHandlerDataSets dataSets = new HBaseGetAccountsSearchVisitsHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.all.search.visits.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED = "LAF1234567I89";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sv : ""</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase01Works() throws Exception {

    // given
    final var request = requestBuilder(
                          MANAGER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "")))
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sv : ""</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase02Works() throws Exception {

    // given
    final var request = requestBuilder(
                          CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                "")))
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sv : Empresa</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 403</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase03Works() throws Exception {

    // given
    final var request = requestBuilder(
                           BYR_OWNER.name())
                           .uri(
                             create(
                               format(
                                 url,
                                 "Empresa")))
                           .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(FORBIDDEN, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sv : Empresa uno</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase04Works() throws Exception {

    // given
    final var request = requestBuilder(
                           MANAGER.name())
                           .uri(
                             create(
                               format(
                                 url,
                                 encode(
                                   "Empresa uno",
                                   defaultCharset()))))
                           .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sv : LAF1234567I89</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase05Works() throws Exception {

    // given
    final var request = requestBuilder(
                           MANAGER.name())
                           .uri(
                             create(
                               format(
                                 url,
                                 encode(
                                   "LAF1234567I89",
                                   defaultCharset()))))
                           .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sv : bce1dbf1-90a</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase06Works() throws Exception {

    // given
    final var request = requestBuilder(
                           MANAGER.name())
                           .uri(
                             create(
                               format(
                                 url,
                                 encode(
                                   "bce1dbf1-90a",
                                   defaultCharset()))))
                           .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param roleCode Role of the user authenticated.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String roleCode) {

    return
      requestBuilder
        .builder()
          .setHeader(
            CONTENT_TYPE_STRING,
            APPLICATION_JSON)
          .setHeader(
            X_LANG_CODE,
            DEFAULT_LANG)
          .setHeader(
            X_ACCESS_TOKEN,
            encrypt(
              requestBuilder
                .dummyAccessToken(
                  "linkomxrmacedo",
                  roleCode)
                .toString()))
        .GET();

  }

}
