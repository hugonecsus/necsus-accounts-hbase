package mx.com.necsus.handler;

import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.NOT_FOUND;
import static io.undertow.util.StatusCodes.OK;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBaseGetAccountsIdHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link GetAccountsIdHandler}.
 */
public class GetAccountsIdHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBaseGetAccountsIdHandlerDataSets dataSets = new HBaseGetAccountsIdHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Dummy account's unique identifier.
   */
  private final String DUMMY_ACCOUNT_ID = "LTA151203V38";

  /**
   * Dummy account's unique identifier encoded.
   */
  private final String DUMMY_ACCOUNT_ID_ENCODED = "TFRBMTUxMjAzVjM4";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path accountId: "TFRBMTUxMjAzVjM4"</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {
    // given
    final var request = requestBuilder(DUMMY_ACCOUNT_ID_ENCODED)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path accountId: "TFRBMTUxMjAzVjM4"</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 404</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {
    // given
    final var request = requestBuilder(DUMMY_ACCOUNT_ID_ENCODED)
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NOT_FOUND, response.statusCode());
  }


  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path accountId: ""</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase003Works() throws Exception {
    // given
    final var request = requestBuilder("")
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());
  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param accountIdEncoded Account's unique identifier encoded.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String accountIdEncoded) {

    return
      requestBuilder
        .builder()
        .uri(
          create(
            format(
              url,
              accountIdEncoded)))
        .GET();

  }

}
