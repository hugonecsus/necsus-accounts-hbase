package mx.com.necsus.handler;

import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.OK;
import static io.undertow.util.StatusCodes.UNPROCESSABLE_ENTITY;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_OFFSET;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_SIZE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBaseGetAccountsIdBuyersAssociationHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link GetAccountsIdBuyersAssociationHandler}.
 */
public class GetAccountsIdBuyersAssociationHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBaseGetAccountsIdBuyersAssociationHandlerDataSets dataSets = new HBaseGetAccountsIdBuyersAssociationHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.b.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Dummy account's unique identifier.
   */
  private final String DUMMY_ACCOUNT_ID = "LTA151203V38";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private final String BUYER_ID_DECODED = "LTA151203V40";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String BUYER_ID_DECODED2 = "LTA151203V41";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String BUYER_ID_DECODED3 = "LTA151203V42";

  /**
   * Dummy account's unique identifier encoded.
   */
  private final String DUMMY_ACCOUNT_ID_ENCODED = "TFRBMTUxMjAzVjM4";

  /**
   * Buyer association row key pattern.
   */
  public static final String BUYER_ASSOCIATION_ROW_KEY_PATTERN = "%s%s";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path encode accountId: "TFRBMTUxMjAzVjM4"</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {
    // given
    final var request = requestBuilder(
                          TRUE,
                          DUMMY_ACCOUNT_ID_ENCODED)
                            .setHeader(
                              X_PAGE_OFFSET,
                              "-1")
                            .setHeader(
                              X_PAGE_SIZE,
                              "3")
                            .setHeader(
                              X_LANG_CODE,
                              DEFAULT_LANG)
                            .build();

    // when
    dataSets
      .createDummyAccount(
        DUMMY_ACCOUNT_ID);

    final var response = client
                           .send(
                             request,
                             discarding());

    // then
    assertEquals(
      UNPROCESSABLE_ENTITY,
      response
        .statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path encode accountId: "TFRBMTUxMjAzVjM4"</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {
    // given
    final var request = requestBuilder(
                          TRUE,
                          DUMMY_ACCOUNT_ID_ENCODED)
                            .setHeader(
                              X_PAGE_OFFSET,
                              "1")
                            .setHeader(
                              X_PAGE_SIZE,
                              "251")
                            .setHeader(
                              X_LANG_CODE,
                              DEFAULT_LANG)
                            .build();

    // when
    dataSets
      .createDummyAccount(
        DUMMY_ACCOUNT_ID);

    final var response = client
                           .send(
                             request,
                             discarding());

    // then
    assertEquals(
      UNPROCESSABLE_ENTITY,
      response
        .statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path encode accountId: "TFRBMTUxMjAzVjM4"</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase003Works() throws Exception {
    // given
    final var request = requestBuilder(
                          FALSE,
                          DUMMY_ACCOUNT_ID_ENCODED)
                            .setHeader(
                              X_PAGE_OFFSET,
                              "1")
                            .setHeader(
                              X_PAGE_SIZE,
                              "3")
                            .setHeader(
                              X_LANG_CODE,
                              DEFAULT_LANG)
                            .build();

    // when
    dataSets
      .createDummyAccount(
        DUMMY_ACCOUNT_ID);

    final var response = client
                           .send(
                             request,
                             discarding());

    // then
    assertEquals(
      BAD_REQUEST,
      response
        .statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path encode accountId: "TFRBMTUxMjAzVjM4"</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase004Works() throws Exception {
    // given
    final var request = requestBuilder(
                          TRUE,
                          DUMMY_ACCOUNT_ID_ENCODED)
                            .setHeader(
                              X_PAGE_OFFSET,
                              "0")
                            .setHeader(
                              X_PAGE_SIZE,
                              "3")
                            .setHeader(
                              X_LANG_CODE,
                              DEFAULT_LANG)
                            .build();

    // when
    dataSets
      .createDummyAccount(
        DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyBuyer(
        BUYER_ID_DECODED,
        "Buyer_1");

    dataSets
      .createDummyBuyer(
        BUYER_ID_DECODED2,
        "Buyer_2");

    dataSets
      .createDummyBuyer(
        BUYER_ID_DECODED3,
        "Buyer_3");

    dataSets
      .createDummyBuyerAssociation(
        format(
          BUYER_ASSOCIATION_ROW_KEY_PATTERN,
          BUYER_ID_DECODED2,
          DUMMY_ACCOUNT_ID));

    dataSets
      .createDummyBuyerAssociation(
        format(
          BUYER_ASSOCIATION_ROW_KEY_PATTERN,
          BUYER_ID_DECODED3,
          DUMMY_ACCOUNT_ID));

    final var response = client
                          .send(
                            request,
                            discarding());

    // then
    assertEquals(
      OK,
      response
        .statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param buyerAssociation Buyer association query param.
   * @param accountIdEncoded Account's unique identifier encoded.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final Boolean buyerAssociation,
                                 final String  accountIdEncoded) {

    return
      requestBuilder
        .builder()
        .uri(
          create(
            format(
              url,
              accountIdEncoded,
              buyerAssociation)))
        .GET();

  }

}
