package mx.com.necsus.handler;

import static io.undertow.util.Headers.ACCEPT_STRING;
import static io.undertow.util.Headers.CONTENT_TYPE_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.FORBIDDEN;
import static io.undertow.util.StatusCodes.OK;
import static io.undertow.util.StatusCodes.UNPROCESSABLE_ENTITY;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.URLEncoder.encode;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static java.nio.charset.Charset.defaultCharset;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.AccountStatus.CERTIFIED;
import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.util.AccessCrypto.encrypt;
import static mx.com.necsus.util.AccessHeaders.X_ACCESS_TOKEN;
import static mx.com.necsus.util.Time.now;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_JSON;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_XLS;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_OFFSET;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_SIZE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.domain.AccessToken;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBaseGetAccountsSearchHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link GetAccountsSearchHandler}.
 */
public class GetAccountsSearchHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBaseGetAccountsSearchHandlerDataSets dataSets = new HBaseGetAccountsSearchHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.all.search.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED = "PIOE900114B11";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED_2 = "PIOE900114B12";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED_3 = "PIOE900114B13";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : ""</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "")))
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }
  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : ABC,STD_C1</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 403</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {

    // given
    final var request  = requestBuilder(
                           ACCT_OWNER.name())
                           .uri(
                             create(
                               format(
                                 url,
                                 encode(
                                   "ABC,STD_C1&s=false,ADV_C1",
                                   defaultCharset()))))
                           .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(FORBIDDEN, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : ABCS_C</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase003Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                encode(
                                  "ABCS_C",
                                  defaultCharset()))))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : Rat,STD_C1</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase004Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                encode(
                                  "|$$Rat,STD_C11",
                                  defaultCharset()))))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(UNPROCESSABLE_ENTITY, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : ABC,STD_C2</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase005Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "false,ADV_C1&s=ABC,STD_C2")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(UNPROCESSABLE_ENTITY, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : ABC,ADV_C1</p>
   *     <p>Query param s : 123,ADV_C2</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase006Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "ABC,ADV_C1&s=123STD_C2")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : ABC,STD_C1</p>
   *     <p>Query param s : 123,STD_C2</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase007Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "false,ADV_C1&s=ABC,STD_C1&s=123,STD_C2")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(UNPROCESSABLE_ENTITY, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : ABC,ADV_C1</p>
   *     <p>Query param s : 123,STD_C2</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase008Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "ABC,ADV_C1&s=123,STD_C2")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(UNPROCESSABLE_ENTITY, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : TSM,ADV_C1</p>
   *     <p>Query param s : MX,ADV_C4</p>
   *     <p>Query param s : UK,ADV_C4</p>
   *     <p>Query param s : US,ADV_C4</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase009Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "TSM,ADV_C1&s=MX,ADV_C4&s=UK,ADV_C4&s=US,ADV_C4")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : 23.01,ADV_C11</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase010Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "false,ADV_C1&s=22.02,ADV_C11")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : MX,ADV_C4</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase011Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "false,ADV_C1&s=MX,ADV_C4")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : 10101505,ADV_C3</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase012Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                encode(
                                  "false,ADV_C1&s=10101505,ADV_C3",
                                  defaultCharset()))))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : C,ADV_C1</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase013Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                encode(
                                  "C,ADV_C1",
                                  defaultCharset()))))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : 23,ADV_C7</p>
   *     <p>Query param s : 29,ADV_C8</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase014Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "false,ADV_C1&s=23,ADV_C7&s=29,ADV_C8")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : Ju,ADV_C9</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase015Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                encode(
                                  "false,ADV_C1&s=Ju,ADV_C9",
                                  defaultCharset()))))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : Maria,ADV_C9</p>
   *     <p>Query param s : Perez,ADV_C10</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase016Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "false,ADV_C1&s=Mario,ADV_C9&s=Perez,ADV_C10")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : C,ADV_C1</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase017Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .setHeader(
                            ACCEPT_STRING,
                            APPLICATION_XLS)
                          .uri(
                            create(
                              format(
                                url,
                                encode(
                                  "C,ADV_C1",
                                  defaultCharset()))))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : Lius%2CADV_C9%26s%3DPerez%2CADV_C10%26s%3DRuiz%2CADV_C11</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase018Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "false,ADV_C1%26s%3DLius%2CADV_C9%26s%3DPerez%2CADV_C10%26s%3DRuiz%2CADV_C11")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : Lius%2CADV_C9%26s%3DPerez%2CADV_C10</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase019Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "false,ADV_C1%26s%3DLius%2CADV_C9%26s%3DPerez%2CADV_C10")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param s : MX%2CADV_C5%26s%3DBCS%2CADV_C6%26s%3DMICRO%2CADV_C7</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase020Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                          .uri(
                            create(
                              format(
                                url,
                                "false,ADV_C1%26s%3DMX%2CADV_C5%26s%3DBCS%2CADV_C6%26s%3DMICRO%2CADV_C7")))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        CERTIFIED.name());

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED_2,
        CERTIFIED.name());

    dataSets
      .createSecondDummyAccount(
        ACCOUNT_ID_DECODED_3,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param  userRole User's role.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String userRole) {

    return
      requestBuilder
        .builder()
        .setHeader(X_ACCESS_TOKEN, encrypt(new AccessToken()
          .setUserId("BYR12345")
          .setRoleCode(userRole)
          .setTimeZone("America/Mexico_City")
          .setExpireAt(
            now()
              .plusMinutes(10)
              .toInstant()
              .toEpochMilli())
          .toString()))
        .setHeader(
          CONTENT_TYPE_STRING,
          APPLICATION_JSON)
        .setHeader(
          X_LANG_CODE,
          DEFAULT_LANG)
        .setHeader(
          X_PAGE_OFFSET,
          "0")
        .setHeader(
          X_PAGE_SIZE,
          "3")
        .setHeader(
          ACCEPT_STRING,
          APPLICATION_JSON)
        .GET();

  }

}
