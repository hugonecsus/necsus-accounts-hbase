package mx.com.necsus.handler;

import static io.undertow.util.Methods.PATCH_STRING;
import static io.undertow.util.StatusCodes.NOT_FOUND;
import static io.undertow.util.StatusCodes.NO_CONTENT;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.AccountStatus.DISCLAIMER;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserStatus.ENABLE;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.domain.AccountUpdateOne;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBasePatchAccountsIdCompletedDisclaimerHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link PatchAccountsIdCompletedPlanHandler}.
 */
public class PatchAccountsIdCompletedDisclaimerHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBasePatchAccountsIdCompletedDisclaimerHandlerDataSets dataSets = new HBasePatchAccountsIdCompletedDisclaimerHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.completed.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Dummy account's unique identifier.
   */
  private final String DUMMY_ACCOUNT_ID = "BI1234566830";

  /**
   * Dummy account's unique identifier encoded.
   */
  private final String DUMMY_ACCOUNT_ID_ENCODED = "QkkxMjM0NTY2ODMw";

  /**
   * Dummy certification's unique identifier.
   */
  private final String DUMMY_CERTIFICATION_ID = "059d76ee-4dad-4884-90ab-f99e3e70b027";

  /**
   * Reusable decoded user's unique identifier for the integration tests.
   */
  private static final String USER_ID_DECODED = "necsuscomdummy3";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : QkkxMjM0NTY2ODMw</p>
   *     <p>Query param completed : DISCLAIMER</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 404</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DISCLAIMER.name())
                        .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NOT_FOUND, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : QkkxMjM0NTY2ODMw</p>
   *     <p>Query param completed : DISCLAIMER</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {
    // given
    final var request = requestBuilder(
                            DISCLAIMER.name())
                          .build();

    dataSets
      .createDummyAccount(
        DUMMY_ACCOUNT_ID,
        DUMMY_CERTIFICATION_ID,
        STD.name(),
        USER_ID_DECODED);

    dataSets
      .createDummyUser(
        USER_ID_DECODED,
        ENABLE.name(),
        CONTROL_DESK.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param accountStatus Account status.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String accountStatus) {

    return
      requestBuilder
        .builder()
        .uri(
            create(
              format(
                url,
                DUMMY_ACCOUNT_ID_ENCODED,
                accountStatus)))
        .method(
          PATCH_STRING,
          ofString(
            new AccountUpdateOne()
              .toString()))
        .setHeader(
          X_LANG_CODE,
          DEFAULT_LANG);

  }

}
