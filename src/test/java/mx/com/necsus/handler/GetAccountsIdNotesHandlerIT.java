package mx.com.necsus.handler;

import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.OK;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.NotesValues.INFORMATION;
import static mx.com.necsus.domain.catalog.NotesValues.NULL;
import static mx.com.necsus.domain.catalog.NotesValues.PAYMENT;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBaseGetAccountsIdNotesHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link GetAccountsIdNotesHandlerIT}.
 */
public class GetAccountsIdNotesHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBaseGetAccountsIdNotesHandlerDataSets dataSets = new HBaseGetAccountsIdNotesHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.notes.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Dummy account's unique identifier.
   */
  private final String DUMMY_ACCOUNT_ID = "BI4256767812";

  /**
   * Dummy account's unique identifier encoded.
   */
  private final String DUMMY_ACCOUNT_ID_ENCODED = "Qkk0MjU2NzY3ODEy";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header X-Lang-Code: "ES"</p>
   *     <p>Variable path accountId: "Rk00MjU2NzY3ODk0"</p>
   *     <p>Query param notes: ""</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase01Works() throws Exception {

    // given
    final var request = requestBuilder("")
                          .build();

    // when
    dataSets
      .createDummyAccount(DUMMY_ACCOUNT_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header X-Lang-Code: "ES"</p>
   *     <p>Variable path accountId: "Rk00MjU2NzY3ODk0"</p>
   *     <p>Query param notes: NULL</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase02Works() throws Exception {

    // given
    final var request = requestBuilder(
                          NULL.name())
                          .build();

    // when
    dataSets
      .createDummyAccount(DUMMY_ACCOUNT_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header X-Lang-Code: "ES"</p>
   *     <p>Variable path accountId: "Rk00MjU2NzY3ODk0"</p>
   *     <p>Query param notes: PAYMENT</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase03Works() throws Exception {

    // given
    final var request = requestBuilder(
                          PAYMENT.name())
                          .build();

    // when
    dataSets
      .createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPaymentNotes(DUMMY_ACCOUNT_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header X-Lang-Code: "ES"</p>
   *     <p>Variable path accountId: "Rk00MjU2NzY3ODk0"</p>
   *     <p>Query param notes: INFORMATION</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase04Works() throws Exception {

    // given
    final var request = requestBuilder(
                          INFORMATION.name())
                          .build();

    // when
    dataSets
      .createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyInformationNotes(DUMMY_ACCOUNT_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * Create a builder with the common element for the requests.
   *
   * @param noteValue Flag to retrieve notes.
   *
   * @return Instance of the type {@link Builder} to build the HTTP requests.
   */
  private Builder requestBuilder(final String noteValue) {

    return
      requestBuilder
        .builder()
        .uri(
          create(
            format(
              url,
              DUMMY_ACCOUNT_ID_ENCODED,
              noteValue)))
        .setHeader(
          X_LANG_CODE,
          DEFAULT_LANG)
        .GET();

  }

}
