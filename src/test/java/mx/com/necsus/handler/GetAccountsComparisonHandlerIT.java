package mx.com.necsus.handler;

import static io.undertow.util.Headers.ACCEPT_STRING;
import static io.undertow.util.Headers.CONTENT_TYPE_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.FORBIDDEN;
import static io.undertow.util.StatusCodes.OK;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.AccountStatus.CERTIFIED;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.util.AccessCrypto.encode;
import static mx.com.necsus.util.AccessCrypto.encrypt;
import static mx.com.necsus.util.AccessHeaders.X_ACCESS_TOKEN;
import static mx.com.necsus.util.Time.now;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_JSON;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_XLS;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_OFFSET;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_SIZE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.domain.AccessToken;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBaseGetAccountsComparisonHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link GetAccountsComparisonHandler}.
 */
public class GetAccountsComparisonHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBaseGetAccountsComparisonHandlerDataSets dataSets = new HBaseGetAccountsComparisonHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.all.comparison.one.url");

  /**
   * The URL to consume for the integration tests.
   */
  private final String url_two = TEST_PROPERTIES.getInstance().getProperty("accounts.all.comparison.two.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED_ONE = "PIOE900114B13";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED_TWO = "MOOE900114B15";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sc : PIOE900114B13 </p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 403</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase01Works() throws Exception {

    // given
    final var request = requestBuilder(
                          MANAGER.name())
                        .setHeader(
                          ACCEPT_STRING,
                          APPLICATION_XLS)
                          .uri(
                            create(
                              format(
                                url,
                                encode(
                                  "PIOE900114B13"))))
                          .build();

    dataSets
      .createDummyAccountOne(
        ACCOUNT_ID_DECODED_ONE,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(FORBIDDEN, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sc : ""</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase02Works() throws Exception {

    // given
    final var request = requestBuilder(
                          MANAGER.name())
                        .setHeader(
                          ACCEPT_STRING,
                          APPLICATION_XLS)
                          .uri(
                            create(
                              format(
                                url,
                                encode(
                                  ""))))
                          .build();

    dataSets
      .createDummyAccountOne(
        ACCOUNT_ID_DECODED_ONE,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sc : PIOE900114B13,MOOE900114B15</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase03Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name())
                        .setHeader(
                          ACCEPT_STRING,
                          APPLICATION_XLS)
                          .uri(
                            create(
                              format(
                                url_two,
                                encode(
                                 "PIOE900114B13"),
                                encode(
                                 "MOOE900114B15"))))
                          .build();

    dataSets
      .createDummyAccountOne(
        ACCOUNT_ID_DECODED_ONE,
        CERTIFIED.name());

    dataSets
      .createDummyAccountTwo(
        ACCOUNT_ID_DECODED_TWO,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param  userRole User's role.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String userRole) {

    return
      requestBuilder
        .builder()
        .setHeader(X_ACCESS_TOKEN, encrypt(new AccessToken()
                                             .setUserId("BYR12345")
                                             .setRoleCode(userRole)
                                             .setTimeZone("America/Mexico_City")
                                             .setExpireAt(
                                               now()
                                                 .plusMinutes(10)
                                                 .toInstant()
                                                 .toEpochMilli())
                                             .toString()))
        .setHeader(
          CONTENT_TYPE_STRING,
          APPLICATION_JSON)
        .setHeader(
          X_LANG_CODE,
          DEFAULT_LANG)
        .setHeader(
          X_PAGE_OFFSET,
          "")
        .setHeader(
          X_PAGE_SIZE,
          "2")
        .GET();

     }

}