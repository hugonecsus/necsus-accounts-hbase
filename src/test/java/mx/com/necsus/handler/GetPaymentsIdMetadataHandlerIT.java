package mx.com.necsus.handler;

import static io.undertow.util.Headers.ACCEPT_STRING;
import static io.undertow.util.StatusCodes.NOT_FOUND;
import static io.undertow.util.StatusCodes.OK;
import static io.undertow.util.StatusCodes.UNAUTHORIZED;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.PaymentStatus.TO_CHECK;
import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.util.AccessCrypto.encrypt;
import static mx.com.necsus.util.AccessHeaders.X_ACCESS_TOKEN;
import static mx.com.necsus.util.Time.now;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_JSON;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.domain.AccessToken;
import mx.com.necsus.domain.catalog.UserRoles;
import mx.com.necsus.handler.GetPaymentsIdMetadataHandler;
import mx.com.necsus.persistence.domain.Payment;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBaseGetPaymentsIdMetadataHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link GetPaymentsIdMetadataHandler}.
 */
public class GetPaymentsIdMetadataHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBaseGetPaymentsIdMetadataHandlerDataSets dataSets = new HBaseGetPaymentsIdMetadataHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("payments.one.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Dummy account's unique identifier.
   */
  private final String DUMMY_ACCOUNT_ID = "LTB151203V01";

  /**
   * Dummy account's unique identifier encoded.
   */
  private final String DUMMY_ACCOUNT_ID_ENCODED = "TFRCMTUxMjAzVjAx";

  /**
   * Dummy payment's unique identifier.
   */
  private final Integer DUMMY_PAYMENT_ID = 0;

  /**
  * <ul>
  *   <li>
  *     <p><strong>Input</strong></p>
  *     <p>Variable path accountId: TFRCMTUxMjAzVjAx</p>
  *     <p>Variable path paymentId: 0</p>
  *   </li>
  *   <li>
  *     <p><strong>Output</strong></p>
  *     <p>HTTP Status: 200</p>
  *   </li>
  * </ul>
  *
  * @throws Exception Any unexpected exception.
  */
  @Test
  public void thatCase001Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          DUMMY_PAYMENT_ID,
                          DUMMY_ACCOUNT_ID,
                          ACCT_OWNER)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets.createDummyPayment(buildPayment());

    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
  * <ul>
  *   <li>
  *     <p><strong>Input</strong></p>
  *     <p>Variable path accountId: TFRCMTUxMjAzVjAx</p>
  *     <p>Variable path payment: 0</p>
  *   </li>
  *   <li>
  *     <p><strong>Output</strong></p>
  *     <p>HTTP Status: 401</p>
  *   </li>
  * </ul>
  *
  * @throws Exception Any unexpected exception.
  */
  @Test
  public void thatCase002Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          DUMMY_PAYMENT_ID,
                          "",
                          ACCT_OWNER)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets.createDummyPayment(buildPayment());

    final var response = client.send(request, discarding());

    // then
    assertEquals(UNAUTHORIZED, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path accountId: TFRCMTUxMjAzVjAx</p>
   *     <p>Variable path paymentId: 0</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 404</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase003Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          DUMMY_PAYMENT_ID,
                          DUMMY_ACCOUNT_ID,
                          ACCT_OWNER)
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NOT_FOUND, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path accountId: TFRCMTUxMjAzVjAx</p>
   *     <p>Variable path paymentId: 0</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 404</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase004Works() throws Exception {
    // given
    final var request = requestBuilder(
      DUMMY_ACCOUNT_ID_ENCODED,
      DUMMY_PAYMENT_ID,
      DUMMY_ACCOUNT_ID,
      ACCT_OWNER)
      .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayment(
        buildPayment()
          .setD4(true));

    final var response = client.send(request, discarding());

    // then
    assertEquals(NOT_FOUND, response.statusCode());
  }

  /**
   * Create an instance of the type {@link Payment} with <em>dummy</em> values.
   *
   * @return Instance of the type {@link Payment}.
   */
  private Payment buildPayment(){

    return
      new Payment()
        .setD0(0)
        .setD2("01/01/2020")
        .setD3("12345325")
        .setD4(false)
        .setS1(TO_CHECK.name());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param accountIdEncoded Account's unique identifier encoded.
   * @param paymentId Payment's unique identifier.
   * @param userId   User id signed.
   * @param userRole Role of the user signed.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String accountIdEncoded,
                                 final Integer paymentId,
                                 final String userId,
                                 final UserRoles userRole) {

    return
      requestBuilder
        .builder()
        .uri(
          create(
            format(
              url,
              accountIdEncoded,
              paymentId)))
        .setHeader(X_ACCESS_TOKEN, encrypt(new AccessToken()
                                             .setUserId(userId)
                                             .setRoleCode(userRole.name())
                                             .setTimeZone("America/Mexico_City")
                                             .setExpireAt(
                                               now()
                                                 .plusMinutes(10)
                                                 .toInstant()
                                                 .toEpochMilli())
                                            .toString()))
        .setHeader(ACCEPT_STRING, APPLICATION_JSON)
        .setHeader(X_LANG_CODE, DEFAULT_LANG)
        .GET();

  }

}
