package mx.com.necsus.handler;

import static io.undertow.util.Methods.PATCH_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.CONFLICT;
import static io.undertow.util.StatusCodes.NOT_FOUND;
import static io.undertow.util.StatusCodes.NO_CONTENT;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.AccountStatus.PLAN;
import static mx.com.necsus.domain.catalog.CertificationTypes.ADV;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserStatus.ENABLE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.domain.AccountUpdateOne;
import mx.com.necsus.domain.CatalogItem;
import mx.com.necsus.handler.PatchAccountsIdCompletedPlanHandler;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBasePatchAccountsIdCompletedPlanHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link PatchAccountsIdCompletedPlanHandler}.
 */
public class PatchAccountsIdCompletedPlanHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBasePatchAccountsIdCompletedPlanHandlerDataSets dataSets = new HBasePatchAccountsIdCompletedPlanHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.completed.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Reusable account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID = "UElPRTkwMDExNEw2MA==";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED = "PIOE900114L60";

  /**
   * Reusable decoded user's unique identifier for the integration tests.
   */
  private static final String USER_ID_DECODED = "necsuscomdummy1";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  UElPRTkwMDExNEw2MA==</p>
   *     <p>Query param completed :  PLAN</p>
   *     <p>Body:</p>
   *     <pre>
   *       "cD3" : "PLAN_STD_1"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 404</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {

    // given
    final var accountUpdate = new AccountUpdateOne()
                                .setD1(
                                  new CatalogItem()
                                    .setD1("PLAN_STD_1" ));

    final var request = requestBuilder(
                          PLAN.name(),
                          accountUpdate)
                        .build();

     // when
     final var response = client.send(request, discarding());

     // then
     assertEquals(NOT_FOUND, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  UElPRTkwMDExNEw2MA==</p>
   *     <p>Query param completed :  </p>
   *     <p>Body:</p>
   *     <pre>
   *       "cD3" : "PLAN_STD_1"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {

    // given
    final var accountUpdate = new AccountUpdateOne()
                                .setD1(
                                  new CatalogItem()
                                    .setD1("PLAN_STD_1" ));

    final var request = requestBuilder(
                          "",
                          accountUpdate)
                        .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  UElPRTkwMDExNEw2MA==</p>
   *     <p>Query param completed :  null</p>
   *     <p>Body:</p>
   *     <pre>
   *       "cD3" : "PLAN_STD_1"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase003Works() throws Exception {

    // given
    final var accountUpdate = new AccountUpdateOne()
                                .setD1(
                                  new CatalogItem()
                                    .setD1("PLAN_STD_1" ));

    final var request = requestBuilder(
                          null,
                          accountUpdate)
                        .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  UElPRTkwMDExNEw2MA==</p>
   *     <p>Query param completed :  PLANN</p>
   *     <p>Body:</p>
   *     <pre>
   *       "cD3" : "PLAN_STD_1"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase004Works() throws Exception {

    // given
    final var accountUpdate = new AccountUpdateOne()
                                .setD1(
                                  new CatalogItem()
                                    .setD1("PLAN_STD_1" ));

    final var request = requestBuilder(
                          "PLANN",
                          accountUpdate)
                        .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  UElPRTkwMDExNEw2MA==</p>
   *     <p>Query param completed :  PLAN</p>
   *     <p>Body:</p>
   *     <pre>
   *       "cD3" : "PLAN_STD_1"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase005Works() throws Exception {

    // given
    final var accountUpdate = new AccountUpdateOne()
                                .setD1(
                                  new CatalogItem()
                                    .setD1("PLAN_STD_1" ));

    final var request = requestBuilder(
                          PLAN.name(),
                          accountUpdate)
                        .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    dataSets
      .createDummyUser(
        USER_ID_DECODED,
        ENABLE.name(),
        CONTROL_DESK.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  UElPRTkwMDExNEw2MA==</p>
   *     <p>Query param completed :  PLAN</p>
   *     <p>Body:</p>
   *     <pre>
   *       "cD3" : "PLAN_ADV_1"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase006Works() throws Exception {

    // given
    final var accountUpdate = new AccountUpdateOne()
                                .setD1(
                                  new CatalogItem()
                                    .setD1("PLAN_ADV_1" ));

    final var request = requestBuilder(
                          PLAN.name(),
                          accountUpdate)
                        .build();

    dataSets
      .createDummyAccount4Upgrade(
        ACCOUNT_ID_DECODED,
        STD.name());

    dataSets
      .createDummyUser(
        USER_ID_DECODED,
        ENABLE.name(),
        CONTROL_DESK.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  UElPRTkwMDExNEw2MA==</p>
   *     <p>Query param completed :  PLAN</p>
   *     <p>Body:</p>
   *     <pre>
   *       "cD3" : "PLAN_STD_1"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 409</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase007Works() throws Exception {

    // given
    final var accountUpdate = new AccountUpdateOne()
                                .setD1(
                                  new CatalogItem()
                                    .setD1("PLAN_STD_1"));

    final var request = requestBuilder(
                          PLAN.name(),
                          accountUpdate)
                        .build();

    dataSets
      .createDummyAccount4Upgrade(
        ACCOUNT_ID_DECODED,
        ADV.name());

    dataSets
      .createDummyUser(
        USER_ID_DECODED,
        ENABLE.name(),
        CONTROL_DESK.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(CONFLICT, response.statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param accountStatus Account status.
   * @param accountUpdate Instance of the type {@link AccountUpdateOne} with the values to update.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String        accountStatus,
                                 final AccountUpdateOne accountUpdate) {

    return
      requestBuilder
        .builder()
        .uri(
            create(
              format(
                url,
                ACCOUNT_ID,
                accountStatus)))
        .method(
          PATCH_STRING,
          ofString(
            accountUpdate.toString()));

  }

}
