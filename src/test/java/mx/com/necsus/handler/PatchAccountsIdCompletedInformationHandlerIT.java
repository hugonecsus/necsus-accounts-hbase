package mx.com.necsus.handler;

import static io.undertow.util.Methods.PATCH_STRING;
import static io.undertow.util.StatusCodes.NO_CONTENT;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.AccountStatus.INFORMATION;
import static mx.com.necsus.domain.catalog.CertificationTypes.ADV;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserStatus.ENABLE;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.domain.AccountUpdateOne;
import mx.com.necsus.handler.PatchAccountsIdCompletedInformationHandler;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBasePatchAccountsIdCompletedInformationHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link PatchAccountsIdCompletedInformationHandler}.
 */
public class PatchAccountsIdCompletedInformationHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBasePatchAccountsIdCompletedInformationHandlerDataSets dataSets = new HBasePatchAccountsIdCompletedInformationHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.completed.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Reusable account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID = "UElPRTkwMDExNEIxMg==";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED = "PIOE900114B12";

  /**
   * Reusable decoded user's unique identifier for the integration tests.
   */
  private static final String USER_ID_DECODED = "necsuscomdummy2";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
|   *     <p>Path param accountId  : UElPRTkwMDExNEIxMg==</p>
   *     <p>Query param completed : INFORMATION</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {
    // given
    final var request = requestBuilder(
                          INFORMATION.name())
                        .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        USER_ID_DECODED,
        STD.name());

    dataSets
      .createDummyUser(
        USER_ID_DECODED,
        ENABLE.name(),
        CONTROL_DESK.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEIxMg==</p>
   *     <p>Query param completed : INFORMATION</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {
    // given
    final var request = requestBuilder(
                          INFORMATION.name())
                        .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED,
        USER_ID_DECODED,
        ADV.name());

    dataSets
      .createDummyUser(
        USER_ID_DECODED,
        ENABLE.name(),
        CONTROL_DESK.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param accountStatus Account status.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String accountStatus) {

    return
      requestBuilder
        .builder()
        .uri(
            create(
              format(
                url,
                ACCOUNT_ID,
                accountStatus)))
        .method(
          PATCH_STRING,
          ofString(
            new AccountUpdateOne()
              .toString()))
        .setHeader(
          X_LANG_CODE,
          DEFAULT_LANG);

  }

}
