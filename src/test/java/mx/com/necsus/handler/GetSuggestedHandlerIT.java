package mx.com.necsus.handler;

import static io.undertow.util.Headers.CONTENT_TYPE_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.FORBIDDEN;
import static io.undertow.util.StatusCodes.OK;
import static io.undertow.util.StatusCodes.UNPROCESSABLE_ENTITY;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.AccountStatus.CERTIFIED;
import static mx.com.necsus.domain.catalog.SuggestedValues.STD_C1;
import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_USR;
import static mx.com.necsus.util.AccessCrypto.encrypt;
import static mx.com.necsus.util.AccessHeaders.X_ACCESS_TOKEN;
import static mx.com.necsus.util.Time.now;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_JSON;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.domain.AccessToken;
import mx.com.necsus.handler.GetAccountsSuggestedHandler;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBaseGetSuggestedHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link GetAccountsSuggestedHandler}.
 */
public class GetSuggestedHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBaseGetSuggestedHandlerDataSets dataSets = new HBaseGetSuggestedHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("suggested.all.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Dummy account's unique identifier.
   */
  private final String DUMMY_ACCOUNT_ID = "BO1234566931";

  /**
   * Reusable decoded user's unique identifier for the integration tests.
   */
  private static final String USER_ID_DECODED = "necsuscomdummy12";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sg: ""</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase01Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name(),
                          "")
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sg: null</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 403</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase02Works() throws Exception {

    // given
    final var request = requestBuilder(
                          ACCT_OWNER.name(),
                          null)
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(FORBIDDEN, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sg: STD_C1</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase03Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_USR.name(),
                          STD_C1.name())
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sg: Jelly,STD_C3</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase04Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_USR.name(),
                          "Jelly,STD_C3")
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(UNPROCESSABLE_ENTITY, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sg: Relojes,STD_C1</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase05Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name(),
                          "Relojes,STD_C1")
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Query param sg: Dal,STD_C2</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase06Works() throws Exception {

    // given
    final var request = requestBuilder(
                          BYR_OWNER.name(),
                          "Dal,STD_C2")
                          .build();

    dataSets
      .createDummyAccount(
        DUMMY_ACCOUNT_ID,
        CERTIFIED.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param userRole User's role.
   * @param suggested Suggested value.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String userRole,
                                 final String suggested) {

    return
      requestBuilder
        .builder()
        .uri(
          create(
            format(
              url,
              suggested)))
        .setHeader(
          X_ACCESS_TOKEN,
          encrypt(
            new AccessToken()
              .setUserId(USER_ID_DECODED)
              .setRoleCode(userRole)
              .setTimeZone("America/Mexico_City")
              .setExpireAt(
                now()
                  .plusMinutes(10)
                  .toInstant()
                  .toEpochMilli())
              .toString()))
        .setHeader(
          CONTENT_TYPE_STRING,
          APPLICATION_JSON)
        .setHeader(
          X_LANG_CODE,
          DEFAULT_LANG)
        .GET();

     }

}
