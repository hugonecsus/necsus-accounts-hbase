package mx.com.necsus.handler;

import static io.undertow.util.Methods.PATCH_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.NOT_FOUND;
import static io.undertow.util.StatusCodes.NO_CONTENT;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpRequest.BodyPublishers.noBody;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.AccountStatus.PAYMENT;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.handler.PatchAccountsIdCompletedPlanHandler;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBasePatchAccountsIdCompletedPaymentHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link PatchAccountsIdCompletedPlanHandler}.
 */
public class PatchAccountsIdCompletedPaymentHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBasePatchAccountsIdCompletedPaymentHandlerDataSets dataSets = new HBasePatchAccountsIdCompletedPaymentHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.completed.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Reusable account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID = "UElPRTkwMDExNEw2MQ==";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED = "PIOE900114L61";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEw2MQ==</p>
   *     <p>Query param completed : PAYMENT</p>
   *     <p>No body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 404</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {

    // given
    final var request = requestBuilder()
                          .uri(
                            create(
                              format(
                                url,
                                ACCOUNT_ID,
                                PAYMENT)))
                          .method(
                            PATCH_STRING,
                            noBody())
                          .build();

     // when
     final var response = client.send(request, discarding());

     // then
     assertEquals(NOT_FOUND, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEw2MQ==</p>
   *     <p>Query param completed : </p>
   *     <p>No body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {

    // given
    final var request = requestBuilder()
                          .uri(
                            create(
                              format(
                                url,
                                ACCOUNT_ID,
                                "")))
                          .method(
                            PATCH_STRING,
                            noBody())
                          .build();

    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEw2MQ==</p>
   *     <p>Query param completed : null</p>
   *     <p>No body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase003Works() throws Exception {

    // given
    final var request = requestBuilder()
                          .uri(
                            create(
                              format(
                                url,
                                ACCOUNT_ID,
                                null)))
                          .method(
                            PATCH_STRING,
                            noBody())
                          .build();

    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEw2MQ==</p>
   *     <p>Query param completed : PAYMENTT</p>
   *     <p>No body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase004Works() throws Exception {

    // given
    final var request = requestBuilder()
                          .uri(
                            create(
                              format(
                                url,
                                ACCOUNT_ID,
                                "PAYMENTT")))
                          .method(
                            PATCH_STRING,
                            noBody())
                          .build();

    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEw2MQ==</p>
   *     <p>Query param completed : PAYMENT</p>
   *     <p>No body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase005Works() throws Exception {

    // given
    final var request = requestBuilder()
                          .uri(
                            create(
                              format(
                                url,
                                ACCOUNT_ID,
                                PAYMENT)))
                          .method(
                            PATCH_STRING,
                            noBody())
                          .build();

    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder() {

    return
      requestBuilder
        .builder();

  }

}
