package mx.com.necsus.handler;

import static com.jsoniter.JsonIterator.deserialize;
import static io.undertow.util.Headers.CONTENT_TYPE_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.FORBIDDEN;
import static io.undertow.util.StatusCodes.OK;
import static io.undertow.util.StatusCodes.UNAUTHORIZED;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static java.net.http.HttpResponse.BodyHandlers.ofString;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.AccountStatus.CERTIFIED;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_DISCLAIMER;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_INFORMATION;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_PAYMENT;
import static mx.com.necsus.domain.catalog.Stages.CERTIFICATES;
import static mx.com.necsus.domain.catalog.Stages.DISCLAIMERS_GRAY;
import static mx.com.necsus.domain.catalog.Stages.DISCLAIMERS_GREEN;
import static mx.com.necsus.domain.catalog.Stages.DISCLAIMERS_RED;
import static mx.com.necsus.domain.catalog.Stages.INFORMATIONS_RED;
import static mx.com.necsus.domain.catalog.Stages.MATCHES_GRAY;
import static mx.com.necsus.domain.catalog.Stages.PAYMENTS_GRAY;
import static mx.com.necsus.domain.catalog.Stages.PAYMENTS_GREEN;
import static mx.com.necsus.domain.catalog.Stages.PAYMENTS_RED;
import static mx.com.necsus.domain.catalog.Stages.VISITS_GRAY;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_USR;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.util.AccessCrypto.encrypt;
import static mx.com.necsus.util.AccessHeaders.X_ACCESS_TOKEN;
import static mx.com.necsus.util.Time.now;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_JSON;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;
import java.util.List;

import com.jsoniter.spi.TypeLiteral;

import mx.com.necsus.domain.AccessToken;
import mx.com.necsus.domain.AccountRetrieveAll;
import mx.com.necsus.domain.catalog.DisclaimerStatus;
import mx.com.necsus.handler.GetAccountsStageHandler;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBaseGetAccountsStageHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link GetAccountsStageHandler}.
 */
public class GetAccountsStageHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBaseGetAccountsStageHandlerDataSets dataSets = new HBaseGetAccountsStageHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.all.stage.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID = "PIAR900114L998";

  /**
   * Reusable decoded APPROVER ID for the integration tests.
   */
  private static final String APPROVER_ID = "linkomxiperez";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : PAYMENTS_GRAY</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 403</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {

    // given
    final var request = requestBuilder("1231", BYR_USR.name())
                           .uri(
                             create(
                               format(
                                 url,
                                 PAYMENTS_GRAY.name())))
                           .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(FORBIDDEN, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : ABC</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {

    // given
    final var request = requestBuilder("1231", CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                "ABC")))
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());
  }

  /**
  * <ul>
  *   <li>
  *     <p><strong>Input</strong></p>
  *     <p>Parameter st : ""</p>
  *   </li>
  * </ul>
  * <ul>
  *   <li>
  *     <p><strong>Output</strong></p>
  *     <p>HTTP Status: 400</p>
  *   </li>
  * </ul>
  *
  * @throws Exception Any unexpected exception.
  */
  @Test
  public void thatCase003Works() throws Exception {

    // given
    final var request   = requestBuilder("1231", CONTROL_DESK.name())
                            .uri(
                              create(
                                format(
                                  url,
                                  "")))
                            .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());
  }

  /**
  * <ul>
  *   <li>
  *     <p><strong>Input</strong></p>
  *     <p>Parameter st : PAYMENTS_GRAY</p>
  *   </li>
  * </ul>
  * <ul>
  *   <li>
  *     <p><strong>Output</strong></p>
  *     <p>HTTP Status: 401</p>
  *   </li>
  * </ul>
  *
  * @throws Exception Any unexpected exception.
  */
  @Test
  public void thatCase004Works() throws Exception {

    // given
    final var request = requestBuilder("1231", "")
                          .uri(
                            create(
                              format(
                                url,
                                PAYMENTS_GRAY.name())))
                          .build();

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(UNAUTHORIZED, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : PAYMENTS_GRAY</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase005Works() throws Exception {

    // given
    final var request = requestBuilder(
                            "LTA171717V17",
                            CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                PAYMENTS_GRAY.name())))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_PAYMENT.name());

    dataSets
      .createDummyPayment(ACCOUNT_ID);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : PAYMENTS_RED</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase006Works() throws Exception {

    // given
    final var request = requestBuilder(
                          "LTA171717V17",
                          CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                PAYMENTS_RED.name())))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_PAYMENT.name());

    dataSets
      .createDummyPayment(ACCOUNT_ID);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : PAYMENTS_GREEN</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase007Works() throws Exception {

    // given
    final var request = requestBuilder(
                            "LTA171717V17",
                            CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                PAYMENTS_GREEN.name())))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_PAYMENT.name());

    dataSets
      .createDummyPayment(ACCOUNT_ID);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : INFORMATIONS_GRAY</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase008Works() throws Exception {

    // given
    final var request = requestBuilder(
                            "LTA171717V17",
                            CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                INFORMATIONS_RED.name())))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_INFORMATION.name());

    dataSets
      .createDummyStatusSection(ACCOUNT_ID);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : INFORMATIONS_RED</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase009Works() throws Exception {

    // given
    final var request   = requestBuilder(
                              "LTA171717V17",
                              CONTROL_DESK.name())
                            .uri(
                              create(
                                format(
                                  url,
                                  INFORMATIONS_RED.name())))
                            .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_INFORMATION.name());

    dataSets
      .createDummyStatusSection(ACCOUNT_ID);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : MATCHES_GRAY</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase010Works() throws Exception {

    // given
    final var request = requestBuilder(
                            APPROVER_ID,
                            CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                MATCHES_GRAY.name())))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_INFORMATION.name());

    dataSets
      .createDummyStatusMatchesToCheck(ACCOUNT_ID, APPROVER_ID);

    // when
    final var response = client.send(request, ofString());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : MATCHES_GRAY</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>Size: 1</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase011Works() throws Exception {

    // given
    final var request = requestBuilder(
                            APPROVER_ID,
                            CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                MATCHES_GRAY.name())))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_INFORMATION.name());

    dataSets
      .createDummyStatusMatchesToCheck(ACCOUNT_ID, APPROVER_ID);

    // when
    final var response = client.send(request, ofString());

    // then
    assertEquals(1, deserialize(
                      response.body(),
                      new TypeLiteral<List<AccountRetrieveAll>>() {})
                    .size());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : VISITS_GRAY</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase012Works() throws Exception {

    // given
    final var request = requestBuilder(
                          APPROVER_ID,
                          CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                VISITS_GRAY.name())))
                          .build();

    dataSets
      .createDummyAccountWithVisit(
        ACCOUNT_ID,
        CHECKING_INFORMATION.name());

    // when
    final var response = client.send(request, ofString());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : DISCLAIMERS_GRAY</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase013Works() throws Exception {

    // given
    final var request = requestBuilder(
                            "LTA171717V17",
                            CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                DISCLAIMERS_GRAY.name())))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_DISCLAIMER.name());

    dataSets
      .createDummyDisclaimerStatus(
        ACCOUNT_ID,
        DisclaimerStatus
          .TO_CHECK.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : DISCLAIMERS_RED</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase014Works() throws Exception {

    // given
    final var request = requestBuilder(
                            "LTA171717V17",
                            CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                DISCLAIMERS_RED.name())))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_DISCLAIMER.name());

    dataSets
      .createDummyDisclaimerStatus(
        ACCOUNT_ID,
        DisclaimerStatus
          .TO_CHECK.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : DISCLAIMERS_GREEN</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase015Works() throws Exception {

    // given
    final var request = requestBuilder(
                            "LTA171717V17",
                            CONTROL_DESK.name())
                          .uri(
                            create(
                              format(
                                url,
                                DISCLAIMERS_GREEN.name())))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_DISCLAIMER.name());

    dataSets
      .createDummyDisclaimerStatus(
        ACCOUNT_ID,
        DisclaimerStatus
          .TO_CHECK.name());

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : CERTIFICATES</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase016Works() throws Exception {

    // given
    final var request = requestBuilder(
      "LTA171717V17",
      CONTROL_DESK.name())
      .uri(
        create(
          format(
            url,
            CERTIFICATES.name())))
      .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CERTIFIED.name());

    dataSets
      .createCertifiedStatus(
        ACCOUNT_ID);

    // when
    final var response = client.send(request, ofString());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : PAYMENTS_GRAY</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase017Works() throws Exception {

    // given
    final var request = requestBuilder(
                            "LTA171717V19",
                            MANAGER.name())
                          .uri(
                            create(
                              format(
                                url,
                                PAYMENTS_GRAY.name())))
                         .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_PAYMENT.name());

    dataSets
      .createDummyPayment(ACCOUNT_ID);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }


  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Parameter st : PAYMENTS_GRAY</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 403</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase018Works() throws Exception {

    // given
    final var request = requestBuilder(
                            "LTA171717V19",
                            BYR_USR.name())
                          .uri(
                            create(
                              format(
                                url,
                                PAYMENTS_GRAY.name())))
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID,
        CHECKING_PAYMENT.name());

    dataSets
      .createDummyPayment(ACCOUNT_ID);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(FORBIDDEN, response.statusCode());
  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param  userId User's unique identifier.
   * @param  userRole User's role.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String userId,
                                 final String userRole) {

    return
      requestBuilder
        .builder()
        .setHeader(X_ACCESS_TOKEN, encrypt(new AccessToken()
                                             .setUserId(userId)
                                             .setRoleCode(userRole)
                                             .setTimeZone("America/Mexico_City")
                                             .setExpireAt(
                                               now()
                                                 .plusMinutes(10)
                                                 .toInstant()
                                                 .toEpochMilli())
                                             .toString()))
        .setHeader(
          CONTENT_TYPE_STRING,
          APPLICATION_JSON)
        .setHeader(
          X_LANG_CODE,
          DEFAULT_LANG)
        .GET();

     }

}
