package mx.com.necsus.handler;

import static io.undertow.util.StatusCodes.OK;
import static io.undertow.util.StatusCodes.UNPROCESSABLE_ENTITY;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.handler.GetAccountsIdPaymentHandler;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBaseGetAccountsIdPaymentHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link GetAccountsIdPaymentHandler}.
 */
// TODO: restore until external service works
@Ignore
public class GetAccountsIdPaymentHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBaseGetAccountsIdPaymentHandlerDataSets dataSets = new HBaseGetAccountsIdPaymentHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.p.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Dummy account's unique identifier.
   */
  private final String DUMMY_ACCOUNT_ID = "GADG940604ET4";

  /**
   * Dummy account's unique identifier encoded.
   */
  private final String DUMMY_ACCOUNT_ID_ENCODED = "R0FERzk0MDYwNEVUNA==";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path accountId: "R0FERzk0MDYwNEVUNA=="</p>
   *     <p>Parameter p : PLAN_A</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 200</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          "PLAN_STD_1")
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(OK, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Variable path accountId: "R0FERzk0MDYwNEVUNA=="</p>
   *     <p>Parameter p : dummy</p>
   *   </li>
   * </ul>
   * <ul>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          "dummy")
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(UNPROCESSABLE_ENTITY, response.statusCode());
  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param accountIdEncoded Account's unique identifier encoded.
   * @param planCode Plan code of the account.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String accountIdEncoded,
                                 final String planCode) {

    return
      requestBuilder
        .builder()
        .uri(
          create(
            format(
              url,
              accountIdEncoded,
              planCode)))
        .GET();

  }

}
