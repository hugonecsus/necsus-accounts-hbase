package mx.com.necsus.handler;

import static io.undertow.util.Headers.CONTENT_TYPE_STRING;
import static io.undertow.util.Methods.PATCH_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.FORBIDDEN;
import static io.undertow.util.StatusCodes.NO_CONTENT;
import static io.undertow.util.StatusCodes.UNPROCESSABLE_ENTITY;
import static io.undertow.util.StatusCodes.UNSUPPORTED_MEDIA_TYPE;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpRequest.BodyPublishers.noBody;
import static java.net.http.HttpRequest.BodyPublishers.ofByteArrays;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static java.nio.charset.StandardCharsets.UTF_8;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.util.AccessCrypto.encrypt;
import static mx.com.necsus.util.AccessHeaders.X_ACCESS_TOKEN;
import static mx.com.necsus.util.Time.now;
import static mx.com.necsus.util.TraceHeaders.MULTI_PART_FORM_DATA;
import static mx.com.necsus.util.TraceHeaders.TEXT_XML;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.Builder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.necsus.domain.AccessToken;
import mx.com.necsus.domain.catalog.UserRoles;
import mx.com.necsus.handler.PatchPaymentsIdFileHandler;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBasePatchPaymentsIdFileHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link PatchPaymentsIdFileHandler}.
 */
public class PatchPaymentsIdFileHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBasePatchPaymentsIdFileHandlerDataSets dataSets = new HBasePatchPaymentsIdFileHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("payments.one.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * User's unique identifier for the CONTROL_DESK.
   */
  private static final String CONTROL_DESK_ID = "linkomxrmacedo";

  /**
   * Reusable BOUNDARY for the integration tests.
   */
  private static final String BOUNDARY = "12";

  /**
   * Reusable account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID = "TFRBMTUxMjAzVjM4";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED = "PIAR900114L70";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: multipart/form-data</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       d1: "Path.of(src/test/resources/files/dummy-txt.txt)"
   *       d2: "01/01/2020"
   *       d3: "12345325"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 403</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {
    // given
    final Map<Object, Object> data = Map.of("d1", Path.of("src/test/resources/files/dummy-txt.txt"),
                                            "d2", "01/01/2020",
                                            "d3", "12345325");

    final var request = requestBuilder(CONTROL_DESK_ID, CONTROL_DESK)
                            .uri(
                              create(
                                format(
                                  url,
                                  ACCOUNT_ID,
                                  "0")))
                            .header(
                              CONTENT_TYPE_STRING,
                              "multipart/form-data;boundary=" + BOUNDARY)
                            .method(
                              PATCH_STRING,
                              ofMimeMultipartData(data))
                            .build();
    // when
    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    final var response = client.send(request, discarding());

    // then
    assertEquals(FORBIDDEN, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: multipart/form-data</p>
   *     <p>Variable path accountId: ""</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>No Body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {
    // given
    final var request = requestBuilder(CONTROL_DESK_ID, CONTROL_DESK)
                            .uri(
                              create(
                                format(
                                  url,
                                  "",
                                  "0")))
                            .header(CONTENT_TYPE_STRING, MULTI_PART_FORM_DATA)
                            .method(
                               PATCH_STRING,
                               noBody())
                            .build();

    // when
    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: ""</p>
   *     <p>Variable path accountId: ""</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>No Body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 415</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase003Works() throws Exception {
    // given
    final var request = requestBuilder(CONTROL_DESK_ID, CONTROL_DESK)
                            .uri(
                              create(
                                format(
                                  url,
                                  ACCOUNT_ID,
                                  "0")))
                            .header(CONTENT_TYPE_STRING, "")
                            .method(
                               PATCH_STRING,
                               noBody())
                            .build();

    // when
    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    final var response = client.send(request, discarding());

    // then
    assertEquals(UNSUPPORTED_MEDIA_TYPE, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: multipart/form-data</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       d1: "Path.of(src/test/resources/files/dummy-pdf.pdf)"
   *       d2: "01/01/2020"
   *       d3: "3456732123456789"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase004Works() throws Exception {
    // given
    final Map<Object, Object> data = Map.of("d1", Path.of("src/test/resources/files/dummy-pdf.pdf"),
                                            "d2", "01/01/2020",
                                            "d3", "3456732123456789");

    final var request = requestBuilder(ACCOUNT_ID_DECODED, ACCT_OWNER)
                            .uri(
                              create(
                                  format(
                                  url,
                                  ACCOUNT_ID,
                                  "0")))
                            .header(CONTENT_TYPE_STRING,
                                "multipart/form-data;boundary=" + BOUNDARY)
                            .method(
                               PATCH_STRING,
                               ofMimeMultipartData(data))
                            .build();
    // when
    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: multipart/form-data</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: DUMMY</p>
   *     <p>No Body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase005Works() throws Exception {
    // given
    final var request = requestBuilder(CONTROL_DESK_ID, CONTROL_DESK)
                            .uri(
                              create(
                                format(
                                  url,
                                  ACCOUNT_ID,
                                  "DUMMY")))
                            .header(CONTENT_TYPE_STRING, MULTI_PART_FORM_DATA)
                            .method(
                               PATCH_STRING,
                               noBody())
                            .build();

    // when
    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: multipart/form-data</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       d1: "Path.of(src/test/resources/files/dummy-pdf.pdf)"
   *       d2: "01-01-2020"
   *       d3: "3456732123456789"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase006Works() throws Exception {
    // given
    final Map<Object, Object> data = Map.of("d1", Path.of("src/test/resources/files/dummy-pdf.pdf"),
                                            "d2", "01-01-2020",
                                            "d3", "3456732123456789");

    final var request = requestBuilder(ACCOUNT_ID_DECODED, ACCT_OWNER)
                            .uri(
                              create(
                                  format(
                                  url,
                                  ACCOUNT_ID,
                                  "0")))
                            .header(CONTENT_TYPE_STRING,
                                "multipart/form-data;boundary=" + BOUNDARY)
                            .method(
                               PATCH_STRING,
                               ofMimeMultipartData(data))
                            .build();
    // when
    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    final var response = client.send(request, discarding());

    // then
    assertEquals(UNPROCESSABLE_ENTITY, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: multipart/form-data</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       d1: "Path.of(src/test/resources/files/dummy-pdf.pdf)"
   *       d2: "01/01/2020"
   *       d3: "3456738"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase007Works() throws Exception {
    // given
    final Map<Object, Object> data = Map.of("d1", Path.of("src/test/resources/files/dummy-pdf.pdf"),
                                            "d2", "01/01/2020",
                                            "d3", "133333333434343434343434343434343434343343434343434343434343434343");

    final var request = requestBuilder(ACCOUNT_ID_DECODED, ACCT_OWNER)
                            .uri(
                              create(
                                  format(
                                  url,
                                  ACCOUNT_ID,
                                  "0")))
                            .header(CONTENT_TYPE_STRING,
                                "multipart/form-data;boundary=" + BOUNDARY)
                            .method(
                               PATCH_STRING,
                               ofMimeMultipartData(data))
                            .build();
    // when
    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    final var response = client.send(request, discarding());

    // then
    assertEquals(UNPROCESSABLE_ENTITY, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: multipart/form-data</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: ""</p>
   *     <p>No Body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase008Works() throws Exception {
    // given
    final var request = requestBuilder(CONTROL_DESK_ID, CONTROL_DESK)
                            .uri(
                              create(
                                format(
                                  url,
                                  ACCOUNT_ID,
                                  "")))
                            .header(CONTENT_TYPE_STRING, MULTI_PART_FORM_DATA)
                            .method(
                               PATCH_STRING,
                               noBody())
                            .build();

    // when
    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: multipart/form-data</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: "0"</p>
   *     <p>No Body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 403</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase009Works() throws Exception {
    // given
    final var request = requestBuilder("DUMMY", CONTROL_DESK)
                            .uri(
                              create(
                                format(
                                  url,
                                  ACCOUNT_ID,
                                  "0")))
                            .header(CONTENT_TYPE_STRING, MULTI_PART_FORM_DATA)
                            .method(
                               PATCH_STRING,
                               noBody())
                            .build();

    // when
    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    final var response = client.send(request, discarding());

    // then
    assertEquals(FORBIDDEN, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: text_xml</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: "0"</p>
   *     <p>No Body</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 415</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase010Works() throws Exception {
    // given
    final var request = requestBuilder(CONTROL_DESK_ID, CONTROL_DESK)
                            .uri(
                              create(
                                format(
                                  url,
                                  ACCOUNT_ID,
                                  "0")))
                            .header(CONTENT_TYPE_STRING, TEXT_XML)
                            .method(
                               PATCH_STRING,
                               noBody())
                            .build();

    // when
    dataSets.createDummyAccount(ACCOUNT_ID_DECODED);

    final var response = client.send(request, discarding());

    // then
    assertEquals(UNSUPPORTED_MEDIA_TYPE, response.statusCode());
  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param userId of the user signed.
   * @param userRole Role of the user signed.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String userId,
                                 final UserRoles userRole) {

    return
      requestBuilder
        .builder()
        .setHeader(X_ACCESS_TOKEN, encrypt(new AccessToken()
                                             .setUserId(userId)
                                             .setRoleCode(userRole.name())
                                             .setTimeZone("America/Mexico_City")
                                             .setExpireAt(
                                               now()
                                                 .plusMinutes(10)
                                                 .toInstant()
                                                 .toEpochMilli())
                                            .toString()));

  }

  /**
   * Method for create request body.
   *
   * @param data Data to create form body.
   *
   * @return An instance of type {@link HttpRequest}
   *
   * @throws IOException Any expect exception.
   */
  private HttpRequest.BodyPublisher ofMimeMultipartData(final Map<Object, Object> data) throws IOException {

    final List<byte[]> byteArrays = new ArrayList<>();

    final var separator = ("--" + BOUNDARY + "\r\nContent-Disposition: form-data; name=").getBytes(UTF_8);

    for (final Map.Entry<Object, Object> entry : data.entrySet()) {

      byteArrays.add(separator);

      if (entry.getValue() instanceof Path) {

        final var path = (Path) entry.getValue();
        final var mimeType = Files
                               .probeContentType(path);

        byteArrays
          .add(
            ("\"" + entry.getKey() + "\"; filename=\"" + path.getFileName()
              + "\"\r\nContent-Type: " + mimeType + "\r\n\r\n").getBytes(UTF_8));
        byteArrays
          .add(
            Files
              .readAllBytes(path));
        byteArrays
          .add("\r\n".getBytes(UTF_8));

      } else {

        byteArrays
          .add(("\"" + entry.getKey() + "\"\r\n\r\n" + entry.getValue() + "\r\n")
          .getBytes(UTF_8));

      }

    }

    byteArrays
      .add(("--" + BOUNDARY + "--").getBytes(UTF_8));

    return
      ofByteArrays(byteArrays);

  }

}
