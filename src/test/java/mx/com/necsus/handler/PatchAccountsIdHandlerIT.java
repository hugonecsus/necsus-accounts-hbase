package mx.com.necsus.handler;

import static io.undertow.util.Methods.PATCH_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.NOT_FOUND;
import static io.undertow.util.StatusCodes.NO_CONTENT;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.CompletedValues.CHECKING_PAYMENT;
import static mx.com.necsus.domain.catalog.CompletedValues.DISCLAIMER_REJECTED;
import static mx.com.necsus.domain.catalog.CompletedValues.INFORMATION_VALIDATED;
import static mx.com.necsus.domain.catalog.CompletedValues.PAYMENT_VALIDATED;
import static mx.com.necsus.domain.catalog.CompletedValues.VISIT_VALIDATED;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.domain.AccountUpdateOne;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBasePatchAccountsIdHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link PatchAccountsIdHandler}.
 */
public class PatchAccountsIdHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBasePatchAccountsIdHandlerDataSets dataSets = new HBasePatchAccountsIdHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.completed.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Reusable account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID = "UElPRTkwMDExNEEwMQ==";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED = "PIOE900114A01";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEEwMQ==</p>
   *     <p>Query param completed : CHECKING_PAYMENT</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 404</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {

    // given
    final var request = requestBuilder(CHECKING_PAYMENT.name())
                          .build();

     // when
     final var response = client.send(request, discarding());

     // then
     assertEquals(NOT_FOUND, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEEwMQ==</p>
   *     <p>Query param completed : ""</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {

    // given
    final var request = requestBuilder("")
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEEwMQ==</p>
   *     <p>Query param completed : null</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase003Works() throws Exception {

    // given
    final var request = requestBuilder(null)
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEEwMQ==</p>
   *     <p>Query param completed : PAYMENT_VALIDATED</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase004Works() throws Exception {

    // given
    final var request = requestBuilder(PAYMENT_VALIDATED.name())
                          .build();

    dataSets
      .createDummyAccount(ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEEwMQ==</p>
   *     <p>Query param completed : INFORMATION_VALIDATED</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase006Works() throws Exception {

    // given
    final var request = requestBuilder(INFORMATION_VALIDATED.name())
                          .build();

    dataSets
      .createDummyAccount(ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEEwMQ==</p>
   *     <p>Query param completed : VISIT_VALIDATED</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase007Works() throws Exception {

    // given
    final var request = requestBuilder(
                          VISIT_VALIDATED.name())
                          .build();

    dataSets
      .createDummyAccount(ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : UElPRTkwMDExNEEwMQ==</p>
   *     <p>Query param completed : DISCLAIMER_REJECTED</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase008Works() throws Exception {

    // given
    final var request = requestBuilder(
                          DISCLAIMER_REJECTED.name())
                          .build();

    dataSets
      .createDummyAccount(ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param accountStatus Account status.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String accountStatus) {

    return
      requestBuilder
        .builder()
        .uri(
            create(
              format(
                url,
                ACCOUNT_ID,
                accountStatus)))
        .method(
          PATCH_STRING,
          ofString(
            new AccountUpdateOne()
              .toString()))
        .setHeader(
          X_LANG_CODE,
          DEFAULT_LANG);

  }

}
