package mx.com.necsus.handler;

import static io.undertow.util.Methods.PATCH_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.FORBIDDEN;
import static io.undertow.util.StatusCodes.NO_CONTENT;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.BuyerAssociationStatus.TO_ASSOCIATE;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.util.AccessCrypto.encrypt;
import static mx.com.necsus.util.AccessHeaders.X_ACCESS_TOKEN;
import static mx.com.necsus.util.Time.now;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.domain.AccessToken;
import mx.com.necsus.domain.AccountUpdateOne;
import mx.com.necsus.domain.BuyerAssociationUpdateOne;
import mx.com.necsus.domain.CatalogItem;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBasePatchAccountsIdBuyerAssociationHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link PatchAccountsIdBuyerAssociationHandler}.
 */
public class PatchAccountsIdBuyerAssociationHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBasePatchAccountsIdBuyerAssociationHandlerDataSets dataSets = new HBasePatchAccountsIdBuyerAssociationHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.b.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Reusable account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID = "UElPRTkwMDExNE0zMA==";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED = "PIOE900114M30";

  /**
   * Reusable account's unique identifier for the integration tests.
   */
  private static final String BUYER_ID = "Uk9OTTkwMDExNE0zMA==";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String BUYER_ID_DECODED = "RONM900114M30";

  /**
   * Reusable user's unique identifier for the integration tests.
   */
  private static final String USER_ID = "linkomxmsantiago";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  "UElPRTkwMDExNE0zMA=="</p>
   *     <p>Query param b :  true</p>
   *     <p>Body:</p>
   *     <pre>
   *       "d0" : null
   *       "d1" : null
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 403</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {

    final var request = requestBuilder(
                          TRUE,
                          new AccountUpdateOne(),
                          BYR_OWNER
                            .name())
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client
                           .send(
                             request,
                             discarding());

    // then
    assertEquals(
      FORBIDDEN,
      response.
        statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  "UElPRTkwMDExNE0zMA=="</p>
   *     <p>Query param b :  true</p>
   *     <p>Body:</p>
   *     <pre>
   *       "d0" : null
   *       "d1" : d1: TO_ASSOCIATE
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {

    // given
    final var accountUpdate = new AccountUpdateOne()
                                .setD4(
                                  new BuyerAssociationUpdateOne()
                                    .setD1(
                                      new CatalogItem()
                                      .setD1(
                                        TO_ASSOCIATE
                                          .name())));

    final var request = requestBuilder(
                          TRUE,
                          accountUpdate,
                          ACCT_OWNER
                            .name())
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client
                           .send(
                             request,
                             discarding());

    // then
    assertEquals(
      BAD_REQUEST,
      response
        .statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  "UElPRTkwMDExNE0zMA=="</p>
   *     <p>Query param b :  true</p>
   *     <p>Body:</p>
   *     <pre>
   *       "d0" : Uk9OTTkwMDExNE0zMA==
   *       "d1" : null
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase003Works() throws Exception {

    // given
    final var accountUpdate = new AccountUpdateOne()
                                    .setD4(
                                      new BuyerAssociationUpdateOne()
                                        .setD0(
                                          BUYER_ID));

    final var request = requestBuilder(
                          TRUE,
                          accountUpdate,
                          ACCT_OWNER
                            .name())
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    dataSets
      .createDummyBuyer(
        BUYER_ID_DECODED);

    // when
    final var response = client
                           .send(
                           request,
                           discarding());

    // then
    assertEquals(
      BAD_REQUEST,
      response
        .statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  :  "UElPRTkwMDExNE0zMA=="</p>
   *     <p>Query param b :  true</p>
   *     <p>Body:</p>
   *     <pre>
   *       "d0" : Uk9OTTkwMDExNE0zMA==
   *       "d1" : d1 : TO_ASSOCIATE
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase004Works() throws Exception {

    // given
    final var accountUpdate = new AccountUpdateOne()
                                .setD4(
                                  new BuyerAssociationUpdateOne()
                                    .setD0(
                                      BUYER_ID)
                                    .setD1(
                                      new CatalogItem()
                                      .setD1(
                                        TO_ASSOCIATE.name())));

    final var request = requestBuilder(
                          TRUE,
                          accountUpdate,
                          ACCT_OWNER
                            .name())
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    dataSets
      .createDummyBuyer(
        BUYER_ID_DECODED);

    dataSets
      .createDummyUser(
        USER_ID,
        BUYER_ID_DECODED);

    // when
    final var response = client
                           .send(
                             request,
                             discarding());

    // then
    assertEquals(
      NO_CONTENT,
      response
        .statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param buyerAssociation Buyer association query param.
   * @param accountUpdate Instance of the type {@link AccountUpdateOne} with the values to update.
   * @param userRole Role of the user authenticated.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final Boolean          buyerAssociation,
                                 final AccountUpdateOne accountUpdate,
                                 final String           userRole) {

    return
      requestBuilder
        .builder()
        .setHeader(X_ACCESS_TOKEN,
          encrypt(
            new AccessToken()
              .setUserId(
                ACCOUNT_ID_DECODED)
              .setRoleCode(
                userRole)
              .setTimeZone(
                "America/Mexico_City")
              .setExpireAt(
                now()
                  .plusMinutes(10)
                  .toInstant()
                  .toEpochMilli())
              .toString()))
        .uri(
          create(
            format(
              url,
              ACCOUNT_ID,
              buyerAssociation)))
        .method(
          PATCH_STRING,
          ofString(
            accountUpdate
              .toString()));

  }

}
