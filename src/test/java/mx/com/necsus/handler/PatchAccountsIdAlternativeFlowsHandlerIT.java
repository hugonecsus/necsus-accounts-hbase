package mx.com.necsus.handler;

import static io.undertow.util.Methods.PATCH_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.NOT_FOUND;
import static io.undertow.util.StatusCodes.NO_CONTENT;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpRequest.BodyPublishers.noBody;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.AlternativeFlows.CANCEL;
import static mx.com.necsus.domain.catalog.AlternativeFlows.PLAN;
import static mx.com.necsus.domain.catalog.AlternativeFlows.RENEWAL;
import static mx.com.necsus.domain.catalog.AlternativeFlows.UPDATE;
import static mx.com.necsus.domain.catalog.AlternativeFlows.UPGRADE;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.handler.PatchAccountsIdAlternativeFlowsHandler;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBasePatchAccountIdPostCertificationFlowsHandlerDataSets;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link PatchAccountsIdAlternativeFlowsHandler}.
 */
public class PatchAccountsIdAlternativeFlowsHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBasePatchAccountIdPostCertificationFlowsHandlerDataSets dataSets = new HBasePatchAccountIdPostCertificationFlowsHandlerDataSets();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("accounts.one.f.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * Reusable account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID = "SEhSTzg4MDMxMEY5SQ==";

  /**
   * Reusable decoded account's unique identifier for the integration tests.
   */
  private static final String ACCOUNT_ID_DECODED = "HHRO880310F9I";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : SEhSTzg4MDMxMEY5SQ==</p>
   *     <p>Query param f         : null</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase01Works() throws Exception {

    // given
    final var request = requestBuilder(
                            null)
                          .build();

     // when
     final var response = client.send(request, discarding());

     // then
     assertEquals(NOT_FOUND, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : SEhSTzg4MDMxMEY5SQ==</p>
   *     <p>Query param f         : null</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase02Works() throws Exception {

    // given
    final var request = requestBuilder(
                            null)
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

     // when
     final var response = client.send(request, discarding());

     // then
     assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : SEhSTzg4MDMxMEY5SQ==</p>
   *     <p>Query param f         : UPDATE</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase03Works() throws Exception {

    // given
    final var request = requestBuilder(
                            UPDATE.name())
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

     // when
     final var response = client.send(request, discarding());

     // then
     assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : SEhSTzg4MDMxMEY5SQ==</p>
   *     <p>Query param f         : UPGRADE</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase04Works() throws Exception {

    // given
    final var request = requestBuilder(
                            UPGRADE.name())
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : SEhSTzg4MDMxMEY5SQ==</p>
   *     <p>Query param f         : CANCEL</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase05Works() throws Exception {

    // given
    final var request = requestBuilder(
                            CANCEL.name())
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : SEhSTzg4MDMxMEY5SQ==</p>
   *     <p>Query param f         : RENEWAL</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase06Works() throws Exception {

    // given
    final var request = requestBuilder(
                            RENEWAL.name())
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : SEhSTzg4MDMxMEY5SQ==</p>
   *     <p>Query param f         : PLAN</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase07Works() throws Exception {

    // given
    final var request = requestBuilder(
                            PLAN.name())
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

    // when
    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Path param accountId  : SEhSTzg4MDMxMEY5SQ==</p>
   *     <p>Query param f         : ABCD</p>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase09Works() throws Exception {

    // given
    final var request = requestBuilder(
                            "ABCD")
                          .build();

    dataSets
      .createDummyAccount(
        ACCOUNT_ID_DECODED);

     // when
     final var response = client.send(request, discarding());

     // then
     assertEquals(BAD_REQUEST, response.statusCode());

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param alternativeFlow Alternative flow.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String alternativeFlow) {

    return
      requestBuilder
        .builder()
        .uri(
            create(
              format(
                url,
                ACCOUNT_ID,
                alternativeFlow)))
        .method(
          PATCH_STRING,
          noBody())
        .setHeader(
          X_LANG_CODE,
          DEFAULT_LANG);

  }

}
