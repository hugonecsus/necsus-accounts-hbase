package mx.com.necsus.handler;

import static io.undertow.util.Headers.CONTENT_TYPE_STRING;
import static io.undertow.util.Methods.PATCH_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.FORBIDDEN;
import static io.undertow.util.StatusCodes.NOT_FOUND;
import static io.undertow.util.StatusCodes.NO_CONTENT;
import static io.undertow.util.StatusCodes.UNPROCESSABLE_ENTITY;
import static io.undertow.util.StatusCodes.UNSUPPORTED_MEDIA_TYPE;
import static java.lang.String.format;
import static java.net.URI.create;
import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static mx.com.necsus.config.TestProperties.TEST_PROPERTIES;
import static mx.com.necsus.domain.catalog.PaymentStatus.FAIL;
import static mx.com.necsus.domain.catalog.PaymentStatus.SUCCESS;
import static mx.com.necsus.domain.catalog.PaymentStatus.TO_CHECK;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.util.AccessCrypto.encrypt;
import static mx.com.necsus.util.AccessHeaders.X_ACCESS_TOKEN;
import static mx.com.necsus.util.Time.now;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_JSON;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TraceHeaders.TEXT_PLAIN_UTF8;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static org.junit.Assert.assertEquals;

import java.net.http.HttpClient;
import java.net.http.HttpRequest.Builder;

import mx.com.necsus.domain.AccessToken;
import mx.com.necsus.domain.CatalogItem;
import mx.com.necsus.domain.PaymentUpdateOne;
import mx.com.necsus.domain.catalog.UserRoles;
import mx.com.necsus.handler.PatchPaymentsIdValidationHandler;
import mx.com.necsus.rule.resource.HBaseSetup;
import mx.com.necsus.rule.watcher.HBasePatchPaymentsIdValidationHandlerDataSets;
import mx.com.necsus.rule.watcher.NotificationsSinkConsumer;
import mx.com.necsus.rule.watcher.RequestBuilder;

import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestWatcher;

/**
 * Integration test of {@link PatchPaymentsIdValidationHandler}.
 */
public class PatchPaymentsIdValidationHandlerIT {

  /**
   * {@link ExternalResource} for the HBase setup.
   */
  @ClassRule
  public static HBaseSetup HBASE_SETUP = new HBaseSetup();

  /**
   * {@link TestWatcher} that create a builder with required headers.
   */
  @Rule
  public final RequestBuilder requestBuilder = new RequestBuilder();

  /**
   * HBase data sets for the test cases.
   */
  @Rule
  public final HBasePatchPaymentsIdValidationHandlerDataSets dataSets = new HBasePatchPaymentsIdValidationHandlerDataSets();

  /**
   * NotificationsSender sink consumer.
   */
  @Rule
  public final NotificationsSinkConsumer notificationsSinkConsumer = new NotificationsSinkConsumer();

  /**
   * The URL to consume for the integration tests.
   */
  private final String url = TEST_PROPERTIES.getInstance().getProperty("payments.one.url");

  /**
   * Reusable HTTP client for the integration tests.
   */
  private final HttpClient client = newHttpClient();

  /**
   * User's unique identifier for the approver.
   */
  private static final String APPROVER_ID = "linkomxrmacedo01";

  /**
   * Dummy account's unique identifier.
   */
  private final String DUMMY_ACCOUNT_ID = "LTA151203V38";

  /**
   * Dummy account's unique identifier encoded.
   */
  private final String DUMMY_ACCOUNT_ID_ENCODED = "TFRBMTUxMjAzVjM4";

  /**
   * Reusable DUMMY STRING with 301 characters for the integration tests.
   */
  private static final String DUMMY_STRING_301 = " With this utility you generate a 16 character output based on your input of numbers and upper and lower case letters.  Random strings can be unique. Used in computing, a random string generator can also be called a random character string generator. This is an important tool if you want too generate.";

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "SUCCESS"
   *       n2: null
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase001Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          0,
                          buildPaymentUpdateOne(),
                          CONTROL_DESK)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "FAIL"
   *       n2: "Test"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase002Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          0,
                          buildPaymentUpdateOne()
                            .setS1(
                              new CatalogItem()
                                .setD1(FAIL.name()))
                            .setN2("Test"),
                          CONTROL_DESK)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "FAIL"
   *       n2: "With this utility you generate a 16 character output based on your input
   *       of numbers and upper and lower case letters.  Random strings can be unique.
   *       Used in computing, a random string generator can also be called a random
   *       character string generator. This is an important tool if you want too generate."
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase003Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          0,
                          buildPaymentUpdateOne()
                            .setS1(
                              new CatalogItem()
                                .setD1(FAIL.name()))
                            .setN2(DUMMY_STRING_301),
                          CONTROL_DESK)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: null
   *       n2: null
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase004Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          0,
                          new PaymentUpdateOne(),
                          CONTROL_DESK)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: null
   *       n2: "abc"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 400</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase005Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          0,
                          buildPaymentUpdateOne()
                            .setS1(
                              new CatalogItem())
                            .setN2("abc"),
                          CONTROL_DESK)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(BAD_REQUEST, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 3</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "SUCCESS"
   *       n2: null
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 404</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase006Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          4,
                          buildPaymentUpdateOne(),
                          CONTROL_DESK)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
  assertEquals(NOT_FOUND, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: text/plain; charset=utf-8</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "SUCCESS"
   *       n2: null
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 415</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase007Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          0,
                          buildPaymentUpdateOne(),
                          CONTROL_DESK)
                          .setHeader(
                            CONTENT_TYPE_STRING,
                            TEXT_PLAIN_UTF8)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(UNSUPPORTED_MEDIA_TYPE, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "FAIL"
   *       n2: ""
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase008Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          0,
                          buildPaymentUpdateOne()
                            .setS1(
                              new CatalogItem()
                                .setD1(FAIL.name()))
                            .setN2(""),
                          CONTROL_DESK)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(UNPROCESSABLE_ENTITY, response.statusCode());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "SUCCESS"
   *       n2: null
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>Consume only one record</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  // FIXME: redefine the test to validate a record sent.
  @Ignore
  @Test
  public void thatCase009Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          0,
                          buildPaymentUpdateOne(),
                          CONTROL_DESK)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    client.send(request, discarding());

    // then
    assertEquals(1, notificationsSinkConsumer.retrieveRecordCount());
  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "FAIL"
   *       n2: "dummy message"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase010Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          0,
                          buildPaymentUpdateOne()
                            .setS1(
                              new CatalogItem()
                                .setD1(FAIL.name()))
                            .setN2("dummy message"),
                          CONTROL_DESK)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "TO_CHECK"
   *       n2: "dummy message"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 422</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase011Works() throws Exception {
    // given
    final var request = requestBuilder(
                          DUMMY_ACCOUNT_ID_ENCODED,
                          0,
                          buildPaymentUpdateOne()
                            .setS1(
                              new CatalogItem()
                                .setD1(TO_CHECK.name()))
                            .setN2("dummy message"),
                          CONTROL_DESK)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(UNPROCESSABLE_ENTITY, response.statusCode());

  }

  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "FAIL"
   *       n2: "dummy message"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 204</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase012Works() throws Exception {
    // given
    final var request = requestBuilder(
                            DUMMY_ACCOUNT_ID_ENCODED,
                            0,
                            buildPaymentUpdateOne()
                              .setS1(
                                new CatalogItem()
                                  .setD1(FAIL.name()))
                              .setN2("dummy message"),
                            MANAGER)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    dataSets
      .createDummyPayments(
        DUMMY_ACCOUNT_ID,
        APPROVER_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(NO_CONTENT, response.statusCode());

  }


  /**
   * <ul>
   *   <li>
   *     <p><strong>Input</strong></p>
   *     <p>Header Content-Type: application/json</p>
   *     <p>Variable path accountId: TFRBMTUxMjAzVjM4</p>
   *     <p>Variable path paymentId: 0</p>
   *     <p>Body:</p>
   *     <pre>
   *       s1: "FAIL"
   *       n2: "dummy message"
   *     </pre>
   *   </li>
   *   <li>
   *     <p><strong>Output</strong></p>
   *     <p>HTTP Status: 403</p>
   *   </li>
   * </ul>
   *
   * @throws Exception Any unexpected exception.
   */
  @Test
  public void thatCase013Works() throws Exception {
    // given
    final var request = requestBuilder(
                            DUMMY_ACCOUNT_ID_ENCODED,
                            0,
                            buildPaymentUpdateOne()
                              .setS1(
                                new CatalogItem()
                                  .setD1(FAIL.name()))
                              .setN2("dummy message"),
                            BYR_OWNER)
                          .build();

    // when
    dataSets.createDummyAccount(DUMMY_ACCOUNT_ID);

    final var response = client.send(request, discarding());

    // then
    assertEquals(FORBIDDEN, response.statusCode());

  }

  /**
   * Create an instance of the type {@link PaymentUpdateOne} with <em>dummy</em> values.
   *
   * @return Instance of the type {@link PaymentUpdateOne}.
   */
  private PaymentUpdateOne buildPaymentUpdateOne(){

    return
      new PaymentUpdateOne()
        .setS1(
          new CatalogItem()
            .setD1(SUCCESS.name()));

  }

  /**
   * Create a builder to build request with headers required.
   *
   * @param accountIdEncoded Account's unique identifier encoded.
   * @param paymentId Payment's unique identifier.
   * @param paymentUpdateOne Instance of type {@link PaymentUpdateOne} with the payload to update.
   * @param userRole Role unique identifier.
   *
   * @return Instance of the type {@link Builder} to build HTTP requests.
   */
  private Builder requestBuilder(final String           accountIdEncoded,
                                 final Integer          paymentId,
                                 final PaymentUpdateOne paymentUpdateOne,
                                 final UserRoles        userRole) {

    return
      requestBuilder
        .builder()
        .uri(
          create(
            format(
              url,
              accountIdEncoded,
              paymentId)))
        .setHeader(X_ACCESS_TOKEN, encrypt(new AccessToken()
                                             .setUserId(APPROVER_ID)
                                             .setRoleCode(userRole.name())
                                             .setTimeZone("America/Mexico_City")
                                             .setExpireAt(
                                               now()
                                                 .plusMinutes(10)
                                                 .toInstant()
                                                 .toEpochMilli())
                                             .toString()))
        .setHeader(CONTENT_TYPE_STRING, APPLICATION_JSON)
        .setHeader(X_LANG_CODE, DEFAULT_LANG)
        .method(
          PATCH_STRING,
          ofString(
            paymentUpdateOne.toString()));

  }

}
