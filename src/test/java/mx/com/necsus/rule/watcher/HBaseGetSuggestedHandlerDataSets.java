package mx.com.necsus.rule.watcher;

import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D5;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetSuggestedHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Explicit default constructor.
   */
  public HBaseGetSuggestedHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create dummy accounts.
   *
   * @param accountId Dummy account's unique identifier.
   * @param accountStatus Dummy account's status.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId,
                                 final String accountStatus) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = putAccount(
                        accountId,
                        accountStatus,
                        "059d76ee-4dad-4884-90ab-f99e3e70b017",
                        STD.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D1,
        "Dalton");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D4,
        "GHDB031498");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D5,
        "570349");

      table
        .put(put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
