package mx.com.necsus.rule.watcher;

import static java.util.UUID.randomUUID;
import static java.util.stream.IntStream.rangeClosed;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_PAYMENT;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.PaymentStatus.TO_CHECK;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D1;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D2;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D3;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D5;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_S1;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_S2;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBasePatchPaymentsIdValidationHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Explicit default constructor.
   */
  public HBasePatchPaymentsIdValidationHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create a dummy account.
   *
   * @param accountId Account's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      table.put(
             putAccount(
               accountId,
               CHECKING_PAYMENT.name(),
               randomUUID().toString(),
               STD.name()));

    }

  }

  /**
   * Create dummy payments.
   *
   * @param accountId Account's unique identifier.
   * @param approverId Approver's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyPayments(final String accountId,
                                  final String approverId) throws Exception {

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        PY_D1,
        approverId);

      rangeClosed(0, 3)
        .forEach(
          (index) -> {

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameOneIndex(
                PY_D2,
                index),
              "01/01/2020");

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameOneIndex(
                PY_D3,
                index),
              "12345325");

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameOneIndex(
                PY_D4,
                index),
              false);

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameOneIndex(
                PY_D5,
                index),
              true);

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameOneIndex(
                PY_S1,
                index),
              TO_CHECK.name());

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameOneIndex(
                PY_S2,
                index),
              nowEpochMilli());

          });

      table.put(put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void starting(final Description description) {

    accountId = null;

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table.delete(new Delete(toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
