package mx.com.necsus.rule.watcher;

import static java.util.stream.IntStream.range;
import static mx.com.necsus.domain.catalog.AccountStatus.CERTIFIED;
import static mx.com.necsus.domain.catalog.CertificationTypes.ADV;
import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D2;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D2;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N1;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N2;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N3;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N4;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameWithPattern;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_N1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_N2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_N3;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_N4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B2_N1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B2_N2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B2_N3;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B2_N4;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetAccountsIdNotesHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Explicit default constructor.
   */
  public HBaseGetAccountsIdNotesHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create a dummy account.
   *
   * @param accountId  Dummy account's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D2,
        CERTIFIED.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D1,
        nowEpochMilli());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D2,
        ADV.name());

      table.put(put);

    }

  }

  /**
   * Create dummy notes of the payment.
   *
   * @param accountId  Dummy account's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyPaymentNotes(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      range(0, 5)
        .forEach(
          (index) -> {

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameOneIndex(
                PY_D4,
                index),
              false);

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameOneIndex(
                PY_N1,
                index),
              2);

            final var now = nowEpochMilli();

            range(0, 2)
              .forEach(
                (indexNote) -> {

                  putValue(
                    put,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    qualifierNameWithPattern(
                      PY_N2,
                      index,
                      indexNote),
                    "test message of the payment");

                  putValue(
                    put,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    qualifierNameWithPattern(
                      PY_N3,
                      index,
                      indexNote),
                    now);

                  putValue(
                    put,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    qualifierNameWithPattern(
                      PY_N4,
                      index,
                      indexNote),
                    ACCT_OWNER.name());

                });

          });

      table.put(put);

    }

  }

  /**
   * Create dummy notes of the information.
   *
   * @param accountId  Dummy account's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyInformationNotes(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_N1,
        2);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B2_N1,
        2);

      final var now = nowEpochMilli();

      range(0, 2)
        .forEach(
          (index) -> {

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameWithPattern(
                S1_B1_N2,
                index),
              "test message of the information of block 1");

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameWithPattern(
                S1_B1_N3,
                index),
              now);

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameWithPattern(
                S1_B1_N4,
                index),
              ACCT_OWNER.name());

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameWithPattern(
                S1_B2_N2,
                index),
              "test message of the information of block 2");

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameWithPattern(
                S1_B2_N3,
                index),
              now);

            putValue(
              put,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              qualifierNameWithPattern(
                S1_B2_N4,
                index),
              CONTROL_DESK.name());

          });

      table.put(put);

    }


  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void starting(final Description description) {

    accountId = null;

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table.delete(new Delete(toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
