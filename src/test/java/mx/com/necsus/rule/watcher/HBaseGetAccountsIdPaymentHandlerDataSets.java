package mx.com.necsus.rule.watcher;

import static java.util.UUID.randomUUID;
import static mx.com.necsus.domain.catalog.AccountStatus.PAYMENT;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D2;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D1;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D2;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D6;
import static mx.com.necsus.persistence.SignUpQualifiers.SIGNUP_D13;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetAccountsIdPaymentHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Explicit default constructor.
   */
  public HBaseGetAccountsIdPaymentHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }
  /**
   * Create a dummy account.
   *
   * @param accountId Dummy account's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D2,
        PAYMENT.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D1,
        randomUUID().toString());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D2,
        "STD");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D3,
        "PLAN_3_7");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D2,
        "DUMMY");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D6,
        "DUMMY");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        SIGNUP_D13,
        "ggarcia@linko.mx");

      table.put(put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void starting(final Description description) {

    accountId = null;

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table.delete(new Delete(toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
