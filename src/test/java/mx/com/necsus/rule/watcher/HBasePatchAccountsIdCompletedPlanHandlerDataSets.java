package mx.com.necsus.rule.watcher;

import static mx.com.necsus.domain.catalog.AccountStatus.PLAN;
import static mx.com.necsus.domain.catalog.ControlDeskGrants.GRANT_PAYMENT;
import static mx.com.necsus.domain.catalog.Origins.LOCAL;
import static mx.com.necsus.domain.catalog.PostCertificationStatus.UPGRADE_INITIATED;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D8;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D9;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D2;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.UserQualifiers.USER_D18;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.UsersTable.USERS_TABLE_NAME;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putUser;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import mx.com.necsus.domain.catalog.CertificationTypes;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBasePatchAccountsIdCompletedPlanHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * User's unique identifier to clean.
   */
  private String userId;

  /**
   * Dummy certification's unique identifier.
   */
  private final String DUMMY_CERTIFICATION_ID = "059d76ee-4dad-4884-90ab-f99e3e70b087";

  /**
   * Explicit default constructor.
   */
  public HBasePatchAccountsIdCompletedPlanHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Set the account's unique identifier.
   *
   * @param accountId String with the account's unique identifier.
   */
  public void setAccountId(final String accountId) {
    this.accountId = accountId;
  }

  /**
   * Create a dummy account.
   *
   * @param accountId  Dummy account's unique identifier.
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = putAccount(
                        accountId,
                        PLAN.name(),
                        null,
                        null);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D1,
        nowEpochMilli());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D2,
        LOCAL.name());

      table.put(put);

    }

  }

  /**
   * Create a dummy account for an upgrade.
   *
   * @param accountId Dummy account's unique identifier.
   * @param certificationType One of {@link CertificationTypes}.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount4Upgrade(final String accountId,
                                         final String certificationType) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = putAccount(
                        accountId,
                        PLAN.name(),
                        DUMMY_CERTIFICATION_ID,
                        certificationType);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D1,
        nowEpochMilli());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D3,
        "PLAN_ADV_1");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D8,
        UPGRADE_INITIATED.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D9,
        nowEpochMilli());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D2,
        LOCAL.name());

      table.put(put);

    }

  }

  /**
   * Create a dummy user.
   *
   * @param userId Dummy user's unique identifier.
   * @param userStatus User's status code.
   * @param userRole User's role code.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyUser(final String userId,
                              final String userStatus,
                              final String userRole) throws Exception {

    this.userId = userId;

    try (final var table = connection.getTable(USERS_TABLE_NAME)) {

      final var put = putUser(
        userId,
        userStatus,
        userRole);

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D18,
        GRANT_PAYMENT.name());

      table.put(put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void starting(final Description description) {

    accountId = null;

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table.delete(new Delete(toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

    if (null != userId) {

      try (final var table = connection.getTable(USERS_TABLE_NAME)) {

        table.delete(new Delete(toBytes(userId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
