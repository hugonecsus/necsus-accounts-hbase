package mx.com.necsus.rule.watcher;

import static mx.com.necsus.domain.catalog.AccountStatus.INFORMATION;
import static mx.com.necsus.domain.catalog.BuyerStatus.ENABLE;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.BuyersTable.BUYERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.BuyersTable.BUYERS_TABLE_NAME;
import static mx.com.necsus.persistence.BuyerQualifiers.BUYER_D1;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.UserQualifiers.USER_D16;
import static mx.com.necsus.persistence.UserQualifiers.USER_D2;
import static mx.com.necsus.persistence.UserQualifiers.USER_D4;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.UsersTable.USERS_TABLE_NAME;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import mx.com.necsus.util.Strings;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBasePatchAccountsIdBuyerAssociationHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Buyer's unique identifier to clean.
   */
  private String buyerId;

  /**
   * User's unique identifier to clean.
   */
  private String userId;


  /**
   * Explicit default constructor.
   */
  public HBasePatchAccountsIdBuyerAssociationHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create dummy accounts.
   *
   * @param accountId Dummy account's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection
                            .getTable(
                              ACCOUNTS_TABLE_NAME)) {

      final var put = putAccount(
                        accountId,
                        INFORMATION.name(),
                        "059d76ee-4dad-4884-90ab-f99e3e70b017",
                        STD.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D1,
        nowEpochMilli());

      table.put(put);

    }

  }

  /**
   * Create dummy buyers.
   *
   * @param buyerId Dummy buyer's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyBuyer(final String buyerId) throws Exception {

    this.buyerId = buyerId;

    try (final var table = connection
                             .getTable(
                               BUYERS_TABLE_NAME)) {

      final var put = newPut(buyerId);

      putValue(
        put,
        BUYERS_COLUMN_FAMILY_DEFAULT,
        BUYER_D1,
        Strings.EMPTY_STRING);

      table.put(put);

    }

  }

  /**
   * Create dummy buyers.
   *
   * @param userId Dummy user's unique identifier.
   * @param buyerId Dummy buyer's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyUser(final String userId,
                              final String buyerId) throws Exception {

    this.userId = userId;

    try (final var table = connection
                             .getTable(
                               USERS_TABLE_NAME)) {

      final var put = newPut(userId);

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D2,
        ENABLE.name());

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D4,
        BYR_OWNER.name());

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D16,
        buyerId);

      table
        .put(
          put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void starting(final Description description) {

    accountId = null;

    buyerId = null;

    userId = null;

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection
                               .getTable(
                                 ACCOUNTS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

    if (null != buyerId) {

      try (final var table = connection
                               .getTable(
                                 BUYERS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(buyerId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

    if (null != userId) {

      try (final var table = connection.getTable(USERS_TABLE_NAME)) {

        table.delete(new Delete(toBytes(userId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
