package mx.com.necsus.rule.watcher;

import static mx.com.necsus.domain.catalog.AccountStatus.DISCLAIMER;
import static mx.com.necsus.domain.catalog.ControlDeskGrants.GRANT_DISCLAIMER;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D25;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.UserQualifiers.USER_D18;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.UsersTable.USERS_TABLE_NAME;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putUser;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBasePatchAccountsIdCompletedDisclaimerHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * User's unique identifier to clean.
   */
  private String userId;

  /**
   * Explicit default constructor.
   */
  public HBasePatchAccountsIdCompletedDisclaimerHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {
      exc.printStackTrace();
    }

  }

  /**
   * Create a dummy account.
   *
   * @param accountId  Dummy account's unique identifier.
   * @param certificationId Dummy certification's unique identifier.
   * @param certificationType The certification type of the account.
   * @param approverId Dummy approver's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId,
                                 final String certificationId,
                                 final String certificationType,
                                 final String approverId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = putAccount(
                        accountId,
                        DISCLAIMER.name(),
                        certificationId,
                        certificationType);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D1,
        nowEpochMilli());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D25,
        approverId);

      table.put(put);

    }

  }

  /**
   * Create a dummy user.
   *
   * @param userId Dummy user's unique identifier.
   * @param userStatus User's status code.
   * @param userRole User's role code.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyUser(final String userId,
                              final String userStatus,
                              final String userRole) throws Exception {

    this.userId = userId;

    try (final var table = connection.getTable(USERS_TABLE_NAME)) {

      final var put = putUser(
        userId,
        userStatus,
        userRole);

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D18,
        GRANT_DISCLAIMER.name());

      table.put(put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void starting(final Description description) {

    accountId = null;

    userId = null;

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table.delete(new Delete(toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw new RuntimeException(exc);

      }

    }

    if (null != userId) {

      try (final var table = connection.getTable(USERS_TABLE_NAME)) {

        table.delete(new Delete(toBytes(userId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw new RuntimeException(exc);

      }

    }

  }

}
