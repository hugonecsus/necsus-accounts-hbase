package mx.com.necsus.rule.watcher;

import static java.lang.String.format;
import static java.lang.Thread.sleep;
import static java.time.Duration.ofMillis;
import static java.util.Arrays.asList;
import static org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG;

import java.util.Properties;
import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the <em>consumer</em> of the topic <strong>notifications</strong>.
 */
public class NotificationsSinkConsumer extends TestWatcher {

  /**
   * Logger for the test rule.
   */
  private static final Logger LOGGER = Logger.getLogger(NotificationsSinkConsumer.class.getName());

  /**
   * Time out to poll the topic.
   */
  private static final int POLL_TIMEOUT = 200;

  /**
   * Consumer's wait time.
   */
  private static final int CONSUMER_WAIT_TIME = 300;

  /**
   * Apache Kafka {@link Consumer} for the topic.
   */
  private Consumer<String, String> consumer;

  /**
   * Explicit default constructor.
   */
  public NotificationsSinkConsumer() {

    final Properties props = new Properties();

    try (final var inputStream = NotificationsSinkConsumer
                                   .class
                                   .getResourceAsStream("/conf/test.properties")) {

      props.load(inputStream);

      consumer = new KafkaConsumer<>(createKafkaConsumerProps(props));

      consumer
        .subscribe(
          asList(
            props
              .getProperty(
                "consumer.topic")));

      LOGGER.info(
        format(
          "'%s' initialized",
          consumer));

    } catch (final Exception exc) {

      LOGGER.severe("Can not create NotificationsSinkConsumer instance");

      exc.printStackTrace();

    }

  }

  /**
   * Retrieve the number of records in the topic.
   *
   * @return Record count.
   */
  public int retrieveRecordCount() {

    return
      consumer
        .poll(
          ofMillis(
            POLL_TIMEOUT))
        .count();

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void starting(final Description description) {

    if (description
          .getMethodName()
          .equalsIgnoreCase("thatCase009Works")) {

      LOGGER.info(
        format(
          "Consumed '%s'",
          consumer
            .poll(
              ofMillis(
                POLL_TIMEOUT))
            .count()));

      try {

        sleep(CONSUMER_WAIT_TIME);

      } catch (final Exception exc) {

        LOGGER.severe(
          format(
              "'%s'",
              exc.toString()));

      }

    }

  }

  /**
   * Create the consumer {@link Properties} with the configuration.
   *
   * @param props Instance of the type {@link Properties} with the values in <em>test.properties</em>.
   *
   * @return properties Instance of the type {@link Properties} used for the configuration of the consumer.
   */
  private static Properties createKafkaConsumerProps(final Properties props) {

    final var properties = new Properties();

    properties.put(BOOTSTRAP_SERVERS_CONFIG        , props.getProperty("kafka.bootstrap.servers"));
    properties.put(GROUP_ID_CONFIG                 , props.getProperty("consumer.groupid"));
    properties.put(ENABLE_AUTO_COMMIT_CONFIG       , "true");
    properties.put(AUTO_OFFSET_RESET_CONFIG        , "earliest");
    properties.put(MAX_POLL_INTERVAL_MS_CONFIG     , "600000");
    properties.put(KEY_DESERIALIZER_CLASS_CONFIG   , StringDeserializer.class.getName());
    properties.put(VALUE_DESERIALIZER_CLASS_CONFIG , StringDeserializer.class.getName());

    return properties;

  }

}
