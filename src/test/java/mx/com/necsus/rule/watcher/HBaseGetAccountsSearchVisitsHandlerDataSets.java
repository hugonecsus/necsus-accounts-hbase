package mx.com.necsus.rule.watcher;

import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.VisitStatus.UNCONFIRMED_ON_TIME;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D1;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D3;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D39;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D4;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D6;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetAccountsSearchVisitsHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Size of the visit's list.
   */
  private static final Integer SIZE = 1;

  /**
   * Index for qualifier name
   */
  private static final Integer INDEX = 0;

  /**
   * Explicit default constructor.
   */
  public HBaseGetAccountsSearchVisitsHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create dummy accounts.
   *
   * @param accountId Dummy account's unique identifier.
   * @param accountStatus Dummy account's status.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId,
                                 final String accountStatus) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = putAccount(
                        accountId,
                        accountStatus,
                        "059d76ft-4add-4664-90ab-f67e3e70b789",
                        STD.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D1,
        "Empresa uno");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D4,
        accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        VISIT_D1,
        SIZE);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          VISIT_D3,
          INDEX),
        "bce1dbf1-90a2-405b-a7f6-cfca6059bc2d");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          VISIT_D4,
          INDEX),
        UNCONFIRMED_ON_TIME.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          VISIT_D6,
          INDEX),
        2);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          VISIT_D39,
          INDEX),
        nowEpochMilli());

      table.put(put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
