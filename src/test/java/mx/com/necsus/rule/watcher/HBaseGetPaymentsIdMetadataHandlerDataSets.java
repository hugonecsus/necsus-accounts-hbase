package mx.com.necsus.rule.watcher;

import static java.util.UUID.randomUUID;
import static mx.com.necsus.domain.catalog.AccountStatus.PAYMENT;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D2;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D3;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D5;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_S1;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import mx.com.necsus.persistence.domain.Payment;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetPaymentsIdMetadataHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Explicit default constructor.
   */
  public HBaseGetPaymentsIdMetadataHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create dummy accounts.
   *
   * @param accountId Dummy account's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      table.put(
             putAccount(
               accountId,
               PAYMENT.name(),
               randomUUID().toString(),
               STD.name()));

    }

  }

  /**
   * Create dummy payment.
   *
   * @param payment Instance of the type {@link Payment} with the data to insert.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyPayment(final Payment payment) throws Exception {

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      final var paymentId = payment.getD0();

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_S1,
          paymentId),
        payment.getS1());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D2,
          paymentId),
        payment.getD2());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D3,
          paymentId),
        payment.getD3());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D4,
          paymentId),
        payment.getD4());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D5,
          paymentId),
        true);

      table.put(put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
