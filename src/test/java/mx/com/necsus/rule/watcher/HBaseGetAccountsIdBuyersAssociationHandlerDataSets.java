package mx.com.necsus.rule.watcher;

import static java.util.UUID.randomUUID;
import static mx.com.necsus.domain.catalog.AccountStatus.INFORMATION;
import static mx.com.necsus.domain.catalog.BuyerStatus.ENABLE;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.BuyerAssociationQualifiers.BUYER_ASSOCIATION_D1;
import static mx.com.necsus.persistence.BuyerAssociationQualifiers.BUYER_ASSOCIATION_D2;
import static mx.com.necsus.persistence.BuyerQualifiers.BUYER_D1;
import static mx.com.necsus.persistence.BuyerQualifiers.BUYER_D15;
import static mx.com.necsus.persistence.BuyerQualifiers.BUYER_D2;
import static mx.com.necsus.persistence.BuyersTable.BUYERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.BuyersTable.BUYERS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import java.util.ArrayList;
import java.util.List;

import mx.com.necsus.domain.catalog.BuyerAssociationStatus;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetAccountsIdBuyersAssociationHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * {@link List} of buyer's unique identifier to clean.
   */
  private List<String> buyersId;

  /**
   * {@link List} of buyer association's unique identifier to clean.
   */
  private List<String> buyersAssociationId;

  /**
   * Dummy account certification plan code.
   */
  private final String DUMMY_CERTIFICATION_PLAN_CODE = "PLAN_ADV_5";

  /**
   * Explicit default constructor.
   */
  public HBaseGetAccountsIdBuyersAssociationHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create a dummy account.
   *
   * @param accountId Account's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var put = putAccount(
                        accountId,
                        INFORMATION.name(),
                        randomUUID().toString(),
                        STD.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D1,
        nowEpochMilli());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D3,
        DUMMY_CERTIFICATION_PLAN_CODE);

      table.put(put);

    }

  }

  /**
   * Create dummy buyers.
   *
   * @param buyerId Dummy buyer's unique identifier.
   * @param businessName Dummy buyer's business name.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyBuyer(final String buyerId,
                               final String businessName) throws Exception {

    buyersId
      .add(
        buyerId);

      try (final var table = connection
                               .getTable(
                                 BUYERS_TABLE_NAME)) {

        final var put = newPut(
                          buyerId);

        putValue(
          put,
          BUYERS_COLUMN_FAMILY_DEFAULT,
          BUYER_D1,
          businessName);

        putValue(
          put,
          BUYERS_COLUMN_FAMILY_DEFAULT,
          BUYER_D2,
          buyerId);

        putValue(
          put,
          BUYERS_COLUMN_FAMILY_DEFAULT,
          BUYER_D15,
          ENABLE
            .name());

        table
          .put(
            put);

      }

    }

  /**
   * Create dummy buyer association.
   *
   * @param buyerAssociationId Dummy buyer association's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyBuyerAssociation(final String buyerAssociationId) throws Exception{

    buyersAssociationId
      .add(
        buyerAssociationId);

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               BUYERS_TABLE_NAME)) {

      final var put = newPut(
                        buyerAssociationId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        BUYER_ASSOCIATION_D1,
        BuyerAssociationStatus
          .ASSOCIATED
          .name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        BUYER_ASSOCIATION_D2,
        nowEpochMilli());

      table
        .put(
          put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void starting(final Description description) {

    accountId = null;

    buyersId = new ArrayList<>();

    buyersAssociationId = new ArrayList<>();

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection
                               .getTable(
                                 ACCOUNTS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(
                accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

    buyersId
      .forEach(
        (buyerId) -> {

          try (final var table = connection
                                   .getTable(
                                     BUYERS_TABLE_NAME)) {

            table
              .delete(
                new Delete(
                  toBytes(
                    buyerId)));

          } catch (final Exception exc) {

            exc.printStackTrace();

          }

        });

    buyersAssociationId
      .forEach(
        (buyerAssociationId) -> {

          try (final var table = connection
                                   .getTable(
                                     BUYERS_TABLE_NAME)) {

            table
              .delete(
                new Delete(
                  toBytes(
                    buyerAssociationId)));

          } catch (final Exception exc) {

            exc.printStackTrace();

          }

        });

  }

}
