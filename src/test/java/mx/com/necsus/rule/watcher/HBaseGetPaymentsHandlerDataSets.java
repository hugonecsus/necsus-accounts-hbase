package mx.com.necsus.rule.watcher;

import static java.util.UUID.randomUUID;
import static mx.com.necsus.domain.catalog.AccountStatus.PAYMENT;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D2;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D3;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D5;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N1;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N2;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N3;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_S1;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameTwoIndex;
import static mx.com.necsus.persistence.TypesHandler.FIRST_ELEMENT_INDEX;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import mx.com.necsus.persistence.domain.Payment;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetPaymentsHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Explicit default constructor.
   */
  public HBaseGetPaymentsHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create dummy accounts.
   *
   * @param accountId Dummy account's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = putAccount(
                        accountId,
                        PAYMENT.name(),
                        randomUUID().toString(),
                        STD.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D1,
        nowEpochMilli());

      table.put(put);

    }

  }

  /**
   * Create dummy payment.
   *
   * @param payment Instance of the type {@link Payment} with the data to insert.
   * @param noteMessage The note message of the payment.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyPayment(final Payment payment,
                                 final String noteMessage) throws Exception {

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      final var paymentId = payment.getD0();

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_S1,
          paymentId),
        payment.getS1());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D2,
          paymentId),
        payment.getD2());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D3,
          paymentId),
        payment.getD3());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D4,
          paymentId),
        payment.getD4());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D5,
          paymentId),
        true);

      if (null != noteMessage) {

        putValue(
          put,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameOneIndex(
            PY_N1,
            paymentId),
          1);

        putValue(
          put,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameTwoIndex(
            PY_N2,
            paymentId,
            FIRST_ELEMENT_INDEX),
          noteMessage);

        putValue(
          put,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameTwoIndex(
            PY_N3,
            paymentId,
            FIRST_ELEMENT_INDEX),
          nowEpochMilli());

        putValue(
          put,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameTwoIndex(
            PY_N4,
            paymentId,
            FIRST_ELEMENT_INDEX),
          ACCT_OWNER.name());

      }

      table.put(put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
