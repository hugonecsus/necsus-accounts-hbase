package mx.com.necsus.rule.watcher;

import static java.util.UUID.randomUUID;
import static mx.com.necsus.domain.catalog.AccountStatus.INFORMATION;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetAccountsIdHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Dummy account certification plan code.
   */
  private final String DUMMY_CERTIFICATION_PLAN_CODE = "PLAN_ADV_5";

  /**
   * Explicit default constructor.
   */
  public HBaseGetAccountsIdHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create a dummy account.
   *
   * @param accountId Account's unique identifier.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = putAccount(
                        accountId,
                        INFORMATION.name(),
                        randomUUID().toString(),
                        STD.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D1,
        nowEpochMilli());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D3,
        DUMMY_CERTIFICATION_PLAN_CODE);

      table
        .put(
          put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void starting(final Description description) {

    accountId = null;

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table.delete(new Delete(toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
