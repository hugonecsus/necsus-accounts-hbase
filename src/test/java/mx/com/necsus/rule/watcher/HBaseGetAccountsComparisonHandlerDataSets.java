package mx.com.necsus.rule.watcher;

import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D10;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D11;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D12;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D13;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D14;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D15;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D16;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D17;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D18;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D19;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D4;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D5;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D9;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D1;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D2;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D3;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D6;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D7;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D9;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B2_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D12;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D3;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D5;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D6;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D7;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D8;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D9;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D10;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D11;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D9;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B1_D1;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D1;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D2;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D3;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D4;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D7;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B3_D2;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B3_D5;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B3_D6;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_D2;
import static mx.com.necsus.persistence.Section6Qualifiers.S6_B1_D3;
import static mx.com.necsus.persistence.Section6Qualifiers.S6_B2_D3;
import static mx.com.necsus.persistence.Section6Qualifiers.S6_B3_D3;
import static mx.com.necsus.persistence.TypesHandler.FIRST_ELEMENT_INDEX;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.UserQualifiers.USER_D10;
import static mx.com.necsus.persistence.UserQualifiers.USER_D12;
import static mx.com.necsus.persistence.UserQualifiers.USER_D5;
import static mx.com.necsus.persistence.UserQualifiers.USER_D6;
import static mx.com.necsus.persistence.UserQualifiers.USER_D7;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.UsersTable.USERS_TABLE_NAME;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetAccountsComparisonHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountIdOne;

  /**
   * Account's unique identifier to clean.
   */
  private String accountIdTwo;

  /**
   * Explicit default constructor.
   */
  public HBaseGetAccountsComparisonHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create dummy accounts.
   *
   * @param accountId Dummy account's unique identifier.
   * @param accountStatus Dummy account's status.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccountOne(final String accountId,
                                    final String accountStatus) throws Exception {

    this.accountIdOne = accountId;

    try (final var accountsTable = connection
                                     .getTable(
                                       ACCOUNTS_TABLE_NAME);
         final var usersTable = connection
                                  .getTable(
                                    USERS_TABLE_NAME)) {

      final var accountPut = putAccount(
                               accountId,
                               accountStatus,
                               "059d76ee-4dad-4884-90ab-f99e3e70b017",
                               STD.name());

      final var userPut = newPut(accountId);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D4,
        "MYTTER09018e5t");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D4,
        "1614664800000");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D5,
        "1651467600000");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D6,
        "NATURAL");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B2_D1,
        "MX");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D9,
        "www.facebook.com.mx");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D3,
        "CIUDAD DE MEXICO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D6,
        "MEXICO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D4,
        "GUSTAVO A MADERO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D5,
        "29633");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D12,
        "stret");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D7,
        "alces");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D9,
        "1");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D8,
        "15");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D5,
        "sebastian");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D6,
        "solis");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D7,
        "aguilar");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D10,
        "5526950827");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D12,
        "agular@gmail.com");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B1_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B2_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B3_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        MT_D1,
        1);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D2,
          0),
        "s1b1");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D3,
          0),
        "SFP");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D3,
        "PLAN_STD_1");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D9,
        80);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D10,
        91);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D11,
        10);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D12,
        80);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D13,
        70);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D14,
        50);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D15,
        76);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D16,
        20);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D17,
        98);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D18,
        45);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D19,
        100);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D1,
        "ABC");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D2,
        "LOCAL");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D7,
        "111110");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D9,
          0),
        "MX");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D10,
          0),
        "GRO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D11,
          0),
        "10101505");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S4_B1_D1,
        "Juan Antonio");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_B1_D1,
        2);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D2,
          0),
        2022);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D7,
          0),
        26.00);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D2,
          1),
        2021);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D7,
          1),
        30.00);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_D2,
        "MEDIUM");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D3,
          FIRST_ELEMENT_INDEX),
        431);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameOneIndex(
            S5_B1_D4,
            FIRST_ELEMENT_INDEX),
        51);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_B3_D2,
        1);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B3_D5,
          FIRST_ELEMENT_INDEX),
        "maria de lourdes");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B3_D6,
          FIRST_ELEMENT_INDEX),
        "perez");

      accountsTable.put(accountPut);

      usersTable.put(userPut);

    }

  }

  /**
   * Create dummy accounts.
   *
   * @param accountIdTwo Dummy account's unique identifier.
   * @param accountStatus Dummy account's status.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccountTwo(final String accountIdTwo,
                                    final String accountStatus) throws Exception {

    this.accountIdTwo = accountIdTwo;

    try (final var accountsTable = connection
                                     .getTable(
                                       ACCOUNTS_TABLE_NAME);
         final var usersTable = connection
                                  .getTable(
                                    USERS_TABLE_NAME)) {

      final var accountPut = putAccount(
                               accountIdTwo,
                               accountStatus,
                               "059d76ee-4dad-4884-90ab-f99e3e70b017",
                               STD.name());

      final var userPut = newPut(accountIdTwo);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D4,
        "MYTTER09018e5t");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D4,
        "1614664800000");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D5,
        "1651467600000");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D6,
        "NATURAL");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B2_D1,
        "MX");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D9,
        "www.facebook.com.mx");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D3,
        "CIUDAD DE MEXICO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D6,
        "MEXICO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D4,
        "GUSTAVO A MADERO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D5,
        "29633");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D12,
        "stret");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D7,
        "alces");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D9,
        "1");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D8,
        "15");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D5,
        "sebastian");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D6,
        "solis");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D7,
        "aguilar");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D10,
        "5526950827");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D12,
        "agular@gmail.com");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B1_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B2_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B3_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        MT_D1,
        1);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D2,
          0),
        "s1b1");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D3,
          0),
        "SFP");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D3,
        "PLAN_STD_1");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D9,
        8.01F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D10,
        9.02F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D11,
        10.03F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D12,
        8.04F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D13,
        8.05F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D14,
        8.06F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D15,
        8.07F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D16,
        8.08F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D17,
        8.09F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D18,
        8.11F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D19,
        100.00F);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D1,
        "ABC");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D2,
        "LOCAL");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D7,
        "111110");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D9,
          0),
        "MX");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D10,
          0),
        "GRO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D11,
          0),
        "10101505");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S4_B1_D1,
        "Juan Antonio");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_B1_D1,
        2);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D2,
          0),
        2022);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D7,
          0),
        26.00);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D2,
          1),
        2021);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D7,
          1),
        30.00);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_D2,
        "MEDIUM");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D3,
          FIRST_ELEMENT_INDEX),
        431);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameOneIndex(
            S5_B1_D4,
            FIRST_ELEMENT_INDEX),
        51);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_B3_D2,
        1);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B3_D5,
          FIRST_ELEMENT_INDEX),
        "maria de lourdes");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B3_D6,
          FIRST_ELEMENT_INDEX),
        "perez");

      accountsTable.put(accountPut);

      usersTable.put(userPut);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountIdOne) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountIdOne)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

      try (final var table = connection.getTable(USERS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountIdOne)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

    if (null != accountIdTwo) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountIdTwo)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

      try (final var table = connection.getTable(USERS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountIdTwo)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

   }

  }

}
