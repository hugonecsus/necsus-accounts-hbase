package mx.com.necsus.rule.watcher;

import static java.util.UUID.randomUUID;
import static mx.com.necsus.domain.catalog.CertificationMatchesStatus.MATCHES_TO_CHECK;
import static mx.com.necsus.domain.catalog.CertificationMatchesStatus.MATCHES_VALIDATED;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.Origins.LOCAL;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D19;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D2;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D22;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D24;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D25;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D26;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D32;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D4;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D5;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D6;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D8;
import static mx.com.necsus.persistence.DisclaimerQualifiers.DISCLAIMER_S1;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D1;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D5;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_S1;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D5;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D7;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B4_D2;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D1;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D3;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import mx.com.necsus.domain.catalog.CertificationMatchesStatus;
import mx.com.necsus.domain.catalog.InformationStatus;
import mx.com.necsus.domain.catalog.PaymentStatus;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetAccountsStageHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Explicit default constructor.
   */
  public HBaseGetAccountsStageHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create a dummy account.
   *
   * @param accountId Dummy account unique identifier to create the account.
   * @param accountStatus Account's status.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId,
                                 final String accountStatus) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put =  putAccount(
                         accountId,
                         accountStatus,
                         "059d76ee-4dad-4884-90ab-f99e3e70b017",
                         STD.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D1,
        "It's just a proof S.A. de C.V.");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D2,
        "LOCAL");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D4,
        "PIAR900114V86");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D5,
        "PIAR900114V86");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D7,
        "22");

      table.put(put);

    }

  }

  /**
   * Create a dummy payment.
   *
   * @param accountId Dummy account unique identifier to create the payments.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyPayment(final String accountId) throws Exception {

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        PY_D1,
        "LTA171717V17");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D4,
          0),
        false);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D5,
          0),
        true);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_S1,
          0),
        PaymentStatus.SUCCESS.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D4,
          1),
        false);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D5,
          1),
        true);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_S1,
          1),
        PaymentStatus.TO_CHECK.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D4,
          2),
        false);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D5,
          2),
        true);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_S1,
          2),
        PaymentStatus.FAIL.name());

      table.put(put);

    }

  }

  /**
   * Create a dummy status section.
   *
   * @param accountId Dummy account unique identifier to create sections.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyStatusSection(final String accountId) throws Exception {

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D25,
        "LTA171717V17");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D32,
        InformationStatus.TO_CHECK.name());

      table.put(put);

    }

  }

  /**
   * Create a dummy status matches to check.
   *
   * @param accountId Dummy account unique identifier to create sections.
   * @param approverId Dummy approver unique identifier to create sections.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyStatusMatchesToCheck(final String accountId,
                                              final String approverId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D8,
        approverId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D6,
        MATCHES_TO_CHECK.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D1,
        "Linko S.A.");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B4_D2,
        "MX");

      table.put(put);

    }

  }

  /**
   * Create a dummy status matches monitoring.
   *
   * @param accountId Dummy account unique identifier to create sections.
   * @param approverId Dummy approver unique identifier to create sections.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyStatusMatchesMonitoring(final String accountId,
                                                 final String approverId) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D8,
        approverId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D6,
        CertificationMatchesStatus.MATCHES_VALIDATED.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D1,
        "Linko S.A.");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B4_D2,
        "MX");

      table.put(put);

    }

  }

  /**
   * Create a dummy account with visit.
   *
   * @param accountId Dummy account unique identifier to create the account.
   * @param accountStatus Account's status.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccountWithVisit(final String accountId,
                                          final String accountStatus) throws Exception {

    this.accountId = accountId;

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put =  putAccount(
                         accountId,
                         accountStatus,
                         "059d56ee-8dad-4884-90ab-f99e3e70b014",
                         STD.name());

      final var size = 1;

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D1,
        "Linko");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D2,
        LOCAL.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D4,
        accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D7,
        "111110");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D22,
        MATCHES_VALIDATED.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D24,
        "linkomxiperez");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        VISIT_D1,
        size);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          VISIT_D3,
          size - 1),
        randomUUID()
          .toString());

      table.put(put);

    }

  }

  /**
   * Create a dummy disclaimer status.
   *
   * @param accountId Dummy account unique identifier to create the disclaimer status.
   * @param disclaimerStatus The disclaimer status.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyDisclaimerStatus(final String accountId,
                                          final String disclaimerStatus) throws Exception {

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D26,
        "LTA171717V17");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        DISCLAIMER_S1,
        disclaimerStatus);

      table.put(put);

    }

  }

  /**
   * Create a dummy certified status.
   *
   * @param accountId Dummy account unique identifier to create the certified status.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createCertifiedStatus(final String accountId) throws Exception {

    try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D2,
        "STD");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D3,
        "PLAN_STD_1");

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D4,
        1637193600000L);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D5,
        1668729600000L);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D1,
        1668729600000L);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D19,
        8.5);

      table.put(put);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void starting(final Description description) {

    accountId = null;

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table.delete(new Delete(toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
