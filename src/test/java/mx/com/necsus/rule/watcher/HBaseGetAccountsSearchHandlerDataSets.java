package mx.com.necsus.rule.watcher;

import static java.lang.Boolean.TRUE;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D11;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D12;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D13;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D8;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D10;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D11;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D12;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D13;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D14;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D15;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D16;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D17;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D18;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D19;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D4;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D5;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D9;
import static mx.com.necsus.persistence.DisclaimerQualifiers.DISCLAIMER_S2;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D1;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D2;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D3;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D4;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D6;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D7;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D9;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B2_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D12;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D3;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D5;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D6;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D7;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D8;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D9;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D1;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D10;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D11;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D13;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D9;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B1_D1;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D1;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D2;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D3;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D4;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D7;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B3_D5;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B3_D6;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_D2;
import static mx.com.necsus.persistence.Section6Qualifiers.S6_B1_D3;
import static mx.com.necsus.persistence.Section6Qualifiers.S6_B2_D3;
import static mx.com.necsus.persistence.Section6Qualifiers.S6_B3_D3;
import static mx.com.necsus.persistence.SignUpQualifiers.SIGNUP_D1;
import static mx.com.necsus.persistence.TypesHandler.FIRST_ELEMENT_INDEX;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.UserQualifiers.USER_D10;
import static mx.com.necsus.persistence.UserQualifiers.USER_D12;
import static mx.com.necsus.persistence.UserQualifiers.USER_D5;
import static mx.com.necsus.persistence.UserQualifiers.USER_D6;
import static mx.com.necsus.persistence.UserQualifiers.USER_D7;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.UsersTable.USERS_TABLE_NAME;
import static mx.com.necsus.rule.watcher.HBaseCommonsDataSets.putAccount;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Test rule for the data sets required in the methods of the <em>test cases</em>.
 */
public class HBaseGetAccountsSearchHandlerDataSets extends TestWatcher {

  /**
   * HBase connection to execute the operations.
   */
  private Connection connection;

  /**
   * Account's unique identifier to clean.
   */
  private String accountId;

  /**
   * Explicit default constructor.
   */
  public HBaseGetAccountsSearchHandlerDataSets() {

    try {

      connection = H_BASE.getInstance();

    } catch (final Exception exc) {

      exc.printStackTrace();

    }

  }

  /**
   * Create dummy accounts.
   *
   * @param accountId Dummy account's unique identifier.
   * @param accountStatus Dummy account's status.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createSecondDummyAccount(final String accountId,
                                       final String accountStatus) throws Exception {

    this.accountId = accountId;

    try (final var accountsTable = connection
                                     .getTable(
                                       ACCOUNTS_TABLE_NAME);
                                       final var usersTable = connection
                                                                .getTable(
                                                                  USERS_TABLE_NAME)) {

      final var accountPut = putAccount(
                               accountId,
                               accountStatus,
                               "059d76ee-4dad-4884-90ab-f99e3e70b017",
                               STD.name());

      final var userPut = newPut(accountId);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        SIGNUP_D1,
        "Manuel");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        "s1b1fb3".getBytes(),
        "MICRO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D11,
        TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D13,
        TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D4,
        "MYTTER09018e5t");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D4,
        "1614664800000");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D5,
        "1651467600000");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D6,
        "NATURAL");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B2_D1,
        "MX");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D9,
        "www.facebook.com.mx");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D3,
        "CIUDAD DE MEXICO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D6,
        "MEXICO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D4,
        "GUSTAVO A MADERO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D5,
        "29633");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D12,
        "stret");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D7,
        "alces");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D9,
        "1");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D8,
        "15");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D5,
        "sebastian");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D6,
        "solis");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D7,
        "aguilar");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D10,
        "5526950827");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D12,
        "agular@gmail.com");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B1_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B2_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B3_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        MT_D1,
        2);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D2,
          0),
        "s1b1");
      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D4,
          0),
        "CREATED");
      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D2,
          1),
        "S4B5");
      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D4,
          1),
        "DISCARDED");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D3,
          0),
        "SFP");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D3,
        "PLAN_STD_1");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D9,
        10);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D10,
        20);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D11,
        30);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D12,
        40);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D13,
        50);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D14,
        60);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D15,
        70);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D16,
        80);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D17,
        90);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D18,
        100);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D19,
        110);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D1,
        "ABC");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D2,
        "LOCAL");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D7,
        "111110");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D9,
          0),
        "MX");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S3_B1_D1,
        1);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D10,
          0),
        "BCS");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D11,
          0),
        "10101505");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D13,
          0),
        false);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S4_B1_D1,
        "Juan Antonio");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_B1_D1,
        2);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D2,
          0),
        2022);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D7,
          0),
        26.00);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D2,
          1),
        2021);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D7,
          1),
        30.00);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_D2,
        "MICRO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D3,
          FIRST_ELEMENT_INDEX),
        431);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D4,
          FIRST_ELEMENT_INDEX),
        51);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_B1_D1,
        2);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B3_D5,
          FIRST_ELEMENT_INDEX),
        "maria de lourdes");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B3_D6,
          FIRST_ELEMENT_INDEX),
        "perez");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D8,
        "UPGRADE_INITIATED");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D12,
        "1614664800000");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        DISCLAIMER_S2,
        "1606802400000");

      accountsTable.put(accountPut);

      usersTable.put(userPut);

    }

  }

  /**
   * Create dummy accounts.
   *
   * @param accountId Dummy account's unique identifier.
   * @param accountStatus Dummy account's status.
   *
   * @throws Exception Any unexpected exception.
   */
  public void createDummyAccount(final String accountId,
                                 final String accountStatus) throws Exception {

    this.accountId = accountId;

    try (final var accountsTable = connection
                                     .getTable(
                                       ACCOUNTS_TABLE_NAME);
         final var usersTable = connection
                                  .getTable(
                                    USERS_TABLE_NAME)) {

      final var accountPut = putAccount(
                               accountId,
                               accountStatus,
                               "059d76ee-4dad-4884-90ab-f99e3e70b017",
                               STD.name());

      final var userPut = newPut(accountId);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D11,
        TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D13,
        TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D4,
        "MYTTER09018e5t");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D4,
        "1614664800000");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D5,
        "1651467600000");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D6,
        "LEGAL");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B2_D1,
        "MX");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D9,
        "www.facebook.com.mx");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D3,
        "CIUDAD DE MEXICO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D6,
        "MEXICO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D4,
        "GUSTAVO A MADERO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D5,
        "29633");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D12,
        "stret");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D7,
        "alces");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D9,
        "1");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B3_D8,
        "15");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D5,
        "sebastian");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D6,
        "solis");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D7,
        "aguilar");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D10,
        "5526950827");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        "s1b1fb3".getBytes(),
        "MICRO");

      putValue(
        userPut,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D12,
        "agular@gmail.com");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B1_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B2_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_B3_D3,
        Boolean.TRUE);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        MT_D1,
        2);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D2,
          0),
        "s1b1");
      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D4,
          0),
        "CREATED");
      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D2,
          1),
        "S4B5");
      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
        MT_D4,
          1),
        "DISCARDED");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          MT_D3,
          0),
        "SFP");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D3,
        "PLAN_STD_1");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D9,
        10);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D10,
        20);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D11,
        30);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D12,
        40);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D13,
        50);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D14,
        60);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D15,
        70);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D16,
        80);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D17,
        90);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D18,
        100);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D19,
        110);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D1,
        "ABC");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D2,
        "LOCAL");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S1_B1_D7,
        "111110");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D9,
          0),
        "MX");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S3_B1_D1,
        1);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D10,
          0),
        "GRO");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D11,
          0),
        "10101505");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S3_B1_D13,
          0),
        false);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S4_B1_D1,
        "Juan Antonio");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_B1_D1,
        2);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D2,
          0),
        2022);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D7,
          0),
        26.00);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D2,
          1),
        2021);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D7,
          1),
        30.00);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_D2,
        "MEDIUM");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B1_D3,
          FIRST_ELEMENT_INDEX),
        431);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameOneIndex(
            S5_B1_D4,
            FIRST_ELEMENT_INDEX),
          51);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_B1_D1,
        2);

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B3_D5,
          FIRST_ELEMENT_INDEX),
        "maria de lourdes");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          S5_B3_D6,
          FIRST_ELEMENT_INDEX),
        "perez");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D8,
        "UPGRADE_INITIATED");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D12,
        "1614664800000");

      putValue(
        accountPut,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        DISCLAIMER_S2,
        "1606802400000");

      accountsTable.put(accountPut);

      usersTable.put(userPut);

    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void finished(final Description description) {

    if (null != accountId) {

      try (final var table = connection.getTable(ACCOUNTS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

      try (final var table = connection.getTable(USERS_TABLE_NAME)) {

        table
          .delete(
            new Delete(
              toBytes(accountId)));

      } catch (final Exception exc) {

        exc.printStackTrace();

        throw
          new RuntimeException(exc);

      }

    }

  }

}
