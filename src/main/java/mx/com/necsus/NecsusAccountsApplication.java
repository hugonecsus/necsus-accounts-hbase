package mx.com.necsus;

import static io.undertow.Handlers.routing;
import static io.undertow.util.Methods.GET;
import static io.undertow.util.Methods.PATCH;
import static mx.com.necsus.config.ApplicationProperties.APPLICATION_PROPERTIES;
import static mx.com.necsus.config.JasperReportsTemplates.JASPER_REPORTS_TEMPLATES;
import static mx.com.necsus.config.KafkaProducerStringString.KAFKA_PRODUCER_STRING_STRING;
import static mx.com.necsus.persistence.CatalogsDb.CATALOGS_DB;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.NecsusDb.NECSUS_DB;
import static mx.com.necsus.util.AccountsStrings.ACCOUNTS_PATCH_ONE_PATH;
import static mx.com.necsus.util.AccountsStrings.ACCOUNTS_PAYMENTS_PATCH_ONE_PATH;
import static mx.com.necsus.util.AccountsStrings.ACCOUNTS_PAYMENTS_RETRIEVE_ALL_PATH;
import static mx.com.necsus.util.AccountsStrings.ACCOUNTS_PAYMENTS_RETRIEVE_ONE_PATH;
import static mx.com.necsus.util.AccountsStrings.ACCOUNTS_RETRIEVE_ALL_PATH;
import static mx.com.necsus.util.AccountsStrings.ACCOUNTS_RETRIEVE_ONE_PATH;
import static mx.com.necsus.util.UndertowApplication.buildUndertow;
import static mx.com.necsus.util.UndertowApplication.logApplicationStarted;
import static org.apache.logging.log4j.LogManager.getLogger;

import io.undertow.Handlers;
import io.undertow.server.HttpHandler;
import mx.com.necsus.handler.AccessHandler;
import mx.com.necsus.handler.GetAccountsHandler;
import mx.com.necsus.handler.GetAccountsIdHandler;
import mx.com.necsus.handler.GetPaymentsHandler;
import mx.com.necsus.handler.GetPaymentsIdHandler;
import mx.com.necsus.handler.PatchAccountsIdHandler;
import mx.com.necsus.handler.PatchPaymentsIdHandler;
import mx.com.necsus.handler.TraceHandler;

import org.apache.logging.log4j.Logger;

/**
 * Application class for the <strong>Accounts</strong> <em>microservice</em>.
 */
public final class NecsusAccountsApplication {

  /**
   * Logger for the main.
   */
  private static final Logger LOGGER = getLogger(NecsusAccountsApplication.class);

  /**
   * Main of the <strong>Accounts</strong> <em>microservice</em>.
   *
   * @param args Array of {@link String} with the arguments for the application.
   *
   * @throws Exception Any unexpected exception.
   */
  public static void main(final String[] args) throws Exception {

    APPLICATION_PROPERTIES.logInitialized();

    JASPER_REPORTS_TEMPLATES.logInitialized();

    H_BASE.logInitialized();

    CATALOGS_DB.logInitialized();

    NECSUS_DB.logInitialized();

    KAFKA_PRODUCER_STRING_STRING.logInitialized();

    buildUndertow(
      routes())
      .start();

    logApplicationStarted(
      LOGGER,
      NecsusAccountsApplication
        .class
        .getSimpleName());

  }

  /**
   * Private explicit default constructor for security.
   */
  private NecsusAccountsApplication() {
  }

  /**
   * Definition of the routes to dispatch.
   *
   * @return An instance of {@link HttpHandler} from {@link Handlers} {@code routing} with the routing.
   */
  private static HttpHandler routes() {

    return
      routing()
        .add(
          GET,
          ACCOUNTS_RETRIEVE_ALL_PATH,
          new TraceHandler(
            new AccessHandler(
              new GetAccountsHandler())))
        .add(
          GET,
          ACCOUNTS_RETRIEVE_ONE_PATH,
          new TraceHandler(
            new AccessHandler(
              new GetAccountsIdHandler())))
        .add(
          PATCH,
          ACCOUNTS_PATCH_ONE_PATH,
          new TraceHandler(
            new AccessHandler(
              new PatchAccountsIdHandler())))
        .add(
          GET,
          ACCOUNTS_PAYMENTS_RETRIEVE_ALL_PATH,
          new TraceHandler(
            new AccessHandler(
              new GetPaymentsHandler())))
        .add(
          GET,
          ACCOUNTS_PAYMENTS_RETRIEVE_ONE_PATH,
          new TraceHandler(
            new AccessHandler(
              new GetPaymentsIdHandler())))
        .add(
          PATCH,
          ACCOUNTS_PAYMENTS_PATCH_ONE_PATH,
          new TraceHandler(
            new AccessHandler(
              new PatchPaymentsIdHandler())));

  }

}
