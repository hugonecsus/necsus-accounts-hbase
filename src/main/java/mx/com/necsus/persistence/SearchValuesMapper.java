package mx.com.necsus.persistence;

import static java.util.Arrays.stream;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toUnmodifiableMap;
import static mx.com.necsus.domain.catalog.SearchValues.NULL;
import static mx.com.necsus.domain.catalog.SearchValues.values;
import static mx.com.necsus.util.RequestBody.isBlankCatalogItem;

import java.util.Map;

import mx.com.necsus.domain.CatalogItem;
import mx.com.necsus.domain.catalog.SearchValues;

/**
 * <em>Mapper</em> for the <strong>search values</strong>.
 */
public final class SearchValuesMapper {

  /**
   * Fixed {@link Map} with the search values.
   */
  private static final Map<String, SearchValues> VALUES = stream(values())
                                                            .collect(
                                                              toUnmodifiableMap(
                                                                SearchValues::name,
                                                                identity()));

  /**
   * Private explicit default constructor for security.
   */
  private SearchValuesMapper() {
  }

  /**
   * Find the <strong>code</strong> in {@link Map} {@link #VALUES}.
   *
   * @param code Unique identifier to search.
   *
   * @return The instance of {@link SearchValues} that matches, {@link SearchValues#NULL} in other case.
   */
  public static SearchValues findSearchValue(final String code) {

    return
      null == code
        ? NULL
        : ofNullable(
            VALUES.get(code))
          .orElse(
            NULL);

  }

  /**
   * Find the <strong>d1</strong> of the {@link CatalogItem} in {@link Map} {@link #VALUES}.
   *
   * @param catalogItem Instance of the type {@link CatalogItem} with the code.
   *
   * @return The instance of {@link SearchValues} that matches, {@link SearchValues#NULL} in other case.
   */
  public static SearchValues findSearchValue(final CatalogItem catalogItem) {

    if (isBlankCatalogItem(catalogItem)) {

      return NULL;

    }

    return findSearchValue(catalogItem.getD1());

  }

}
