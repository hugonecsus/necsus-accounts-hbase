package mx.com.necsus.persistence;

import static java.util.Arrays.stream;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toUnmodifiableMap;
import static mx.com.necsus.domain.catalog.Stages.NULL;
import static mx.com.necsus.domain.catalog.Stages.values;

import java.util.Map;

import mx.com.necsus.domain.catalog.Stages;

/**
 * <em>Mapper</em> for the <strong>stages</strong>.
 */
public final class StagesMapper {

  /**
   * Fixed {@link Map} with the checking values.
   */
  private static final Map<String, Stages> STAGES = stream(
                                                        values())
                                                      .collect(
                                                        toUnmodifiableMap(
                                                          Stages::name,
                                                          identity()));

  /**
   * Private explicit default constructor for security.
   */
  private StagesMapper() {
  }

  /**
   * Search the <strong>code</strong> in {@link Map} {@link #STAGES}.
   *
   * @param code Unique identifier to search.
   *
   * @return The instance of {@link Stages} that matches, {@link Stages#NULL} in other case.
   */
  public static Stages findStage(final String code) {

    return
      null == code
        ? NULL
        : ofNullable(
            STAGES.get(code))
          .orElse(
            NULL);

  }

}
