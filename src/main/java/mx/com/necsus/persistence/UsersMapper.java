package mx.com.necsus.persistence;

import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_USR;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.TypesHandler.getString;
import static mx.com.necsus.persistence.TypesHandler.newGet;
import static mx.com.necsus.persistence.UserQualifiers.USER_D16;
import static mx.com.necsus.persistence.UserQualifiers.USER_D5;
import static mx.com.necsus.persistence.UserQualifiers.USER_D6;
import static mx.com.necsus.persistence.UserQualifiers.USER_D7;
import static mx.com.necsus.persistence.UserQualifiers.USER_PREFIX;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.UsersTable.USERS_TABLE_NAME;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.filter.ColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.MultipleColumnPrefixFilter;

/**
 * Persistence layer for the <strong>Account</strong>.
 */
public class UsersMapper {

  /**
   * Column prefixes to retrieve in {@link #retrieveAssignedControlDesk}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_ASSIGNED_CONTROL_DESK = new MultipleColumnPrefixFilter(
                                                                                       new byte[][] {
                                                                                         USER_D5,
                                                                                         USER_D6,
                                                                                         USER_D7});

  /**
   * Private explicit default constructor for security.
   */
  private UsersMapper() {
  }

  /**
   * Retrieve the initial values for an user by the unique identifier.
   *
   * @param userId User's unique identifier.
   * @param roleCode Role code.
   *
   * @return The buyer RFC string.
   */
  public static String retrieveBuyerRfcByUserId(final String userId,
                                                final String roleCode) {

    if (BYR_OWNER != findUserRole(roleCode) &&
        BYR_USR != findUserRole(roleCode)) {

      return userId;

    }

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               USERS_TABLE_NAME)) {

      final var result = table
                            .get(
                              newGet(userId)
                                .addFamily(
                                  USERS_COLUMN_FAMILY_DEFAULT)
                                .setFilter(
                                  new ColumnPrefixFilter(
                                    USER_PREFIX)));

      return
        getString(
          result,
          USERS_COLUMN_FAMILY_DEFAULT,
          USER_D16);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the information of the control desk assigned to an account.
   *
   * @param userId User's unique identifier.
   *
   * @return An instance of the type {@link Result} with the information.
   */
  public static Result retrieveAssignedControlDesk(final String userId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               USERS_TABLE_NAME)) {

      return
        table
          .get(
            newGet(userId)
              .addFamily(
                USERS_COLUMN_FAMILY_DEFAULT)
              .setFilter(
                PREFIXES_4_ASSIGNED_CONTROL_DESK));


    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

}
