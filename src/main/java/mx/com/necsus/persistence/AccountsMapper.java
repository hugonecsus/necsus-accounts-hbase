package mx.com.necsus.persistence;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.time.Duration.between;
import static java.time.Instant.ofEpochMilli;
import static java.time.ZoneOffset.UTC;
import static java.time.ZonedDateTime.ofInstant;
import static java.util.Collections.emptyList;
import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsLast;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static java.util.stream.IntStream.rangeClosed;
import static mx.com.necsus.domain.catalog.AccountStatus.CERTIFIED;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_DISCLAIMER;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_INFORMATION;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_PAYMENT;
import static mx.com.necsus.domain.catalog.AccountStatus.DISCLAIMER;
import static mx.com.necsus.domain.catalog.AccountStatus.DISCLAIMER_REJECTED;
import static mx.com.necsus.domain.catalog.AccountStatus.INFORMATION_VALIDATED;
import static mx.com.necsus.domain.catalog.CatalogCodes.CERTIFICATION_TYPES;
import static mx.com.necsus.domain.catalog.CatalogCodes.COUNTRIES;
import static mx.com.necsus.domain.catalog.CatalogCodes.COUNTRY_CALLING_CODES;
import static mx.com.necsus.domain.catalog.CatalogCodes.ECONOMIC_ACTIVITIES;
import static mx.com.necsus.domain.catalog.CatalogCodes.INVITED_BY;
import static mx.com.necsus.domain.catalog.CatalogCodes.LEGAL_ENTITIES;
import static mx.com.necsus.domain.catalog.CatalogCodes.ORIGINS;
import static mx.com.necsus.domain.catalog.CertificationStatus.ACTIVE;
import static mx.com.necsus.domain.catalog.CertificationStatus.INACTIVE;
import static mx.com.necsus.domain.catalog.CertificationStatus.UPDATE;
import static mx.com.necsus.domain.catalog.CertificationStatus.UPGRADE;
import static mx.com.necsus.domain.catalog.CertificationTypes.ADV;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.FileCodes.S3B4FA;
import static mx.com.necsus.domain.catalog.InformationStatus.ADJUSTMENT;
import static mx.com.necsus.domain.catalog.InformationStatus.FAIL;
import static mx.com.necsus.domain.catalog.InformationStatus.NULL;
import static mx.com.necsus.domain.catalog.InformationStatus.SUCCESS;
import static mx.com.necsus.domain.catalog.PaymentStatus.TO_CHECK;
import static mx.com.necsus.domain.catalog.PostCertificationStatus.UPDATE_CANCELED;
import static mx.com.necsus.domain.catalog.PostCertificationStatus.UPGRADE_CANCELED;
import static mx.com.necsus.domain.catalog.Stages.DISCLAIMERS_GRAY;
import static mx.com.necsus.domain.catalog.Stages.DISCLAIMERS_GREEN;
import static mx.com.necsus.domain.catalog.Stages.DISCLAIMERS_RED;
import static mx.com.necsus.domain.catalog.Stages.INFORMATIONS_GRAY;
import static mx.com.necsus.domain.catalog.Stages.INFORMATIONS_GREEN;
import static mx.com.necsus.domain.catalog.Stages.INFORMATIONS_RED;
import static mx.com.necsus.domain.catalog.Stages.INFORMATIONS_YELLOW;
import static mx.com.necsus.domain.catalog.Stages.MATCHES_GRAY;
import static mx.com.necsus.domain.catalog.Stages.MATCHES_GREEN;
import static mx.com.necsus.domain.catalog.Stages.MATCHES_YELLOW;
import static mx.com.necsus.domain.catalog.Stages.PAYMENTS_GRAY;
import static mx.com.necsus.domain.catalog.Stages.PAYMENTS_GREEN;
import static mx.com.necsus.domain.catalog.Stages.PAYMENTS_RED;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D10;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D11;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D13;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D14;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D15;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D2;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D3;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D6;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D7;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D8;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D9;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_PREFIX;
import static mx.com.necsus.persistence.AccountStatusMapper.findAccountStatus;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.BuyersAssociationMapper.hasAnyAssociation2Buyers;
import static mx.com.necsus.persistence.CatalogsDb.CATALOGS_DB;
import static mx.com.necsus.persistence.CertificationMatchesStatusMapper.findCertificationMatchesStatus;
import static mx.com.necsus.persistence.CertificationQualifiers.BLOCKS_STATUS;
import static mx.com.necsus.persistence.CertificationQualifiers.BLOCKS_STATUS_DATE;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D1;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D19;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D2;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D22;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D23;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D25;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D26;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D30;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D31;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D32;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D33;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D35;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D4;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D5;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D6;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D7;
import static mx.com.necsus.persistence.CertificationQualifiers.SECTIONS_STATUS;
import static mx.com.necsus.persistence.CertificationQualifiers.SECTIONS_STATUS_DATE;
import static mx.com.necsus.persistence.CertificationStatusMapper.findCertificationStatus;
import static mx.com.necsus.persistence.CertificationTypesMapper.findCertificationType;
import static mx.com.necsus.persistence.DisclaimerQualifiers.DISCLAIMER_N1;
import static mx.com.necsus.persistence.DisclaimerQualifiers.DISCLAIMER_N2;
import static mx.com.necsus.persistence.DisclaimerQualifiers.DISCLAIMER_N3;
import static mx.com.necsus.persistence.DisclaimerQualifiers.DISCLAIMER_N4;
import static mx.com.necsus.persistence.DisclaimerQualifiers.DISCLAIMER_S1;
import static mx.com.necsus.persistence.Filters.createSingleColumnValueFilter;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.InformationStatusMapper.findInformationStatus;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_N1;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_N2;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_N3;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_N4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D2;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_S1;
import static mx.com.necsus.persistence.PaymentStatusMapper.findPaymentStatus;
import static mx.com.necsus.persistence.PaymentsMapper.isNotDeletedAndConfirmed;
import static mx.com.necsus.persistence.PaymentsMapper.retrieveAllPayments;
import static mx.com.necsus.persistence.PaymentsMapper.retrievePaymentNotes;
import static mx.com.necsus.persistence.PostCertificationStatusMapper.findPostCertificationStatus;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameWithPattern;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D5;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D6;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D7;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B4_D2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_S1_B2;
import static mx.com.necsus.persistence.Section2Qualifiers.S2_D1;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D1;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D11;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D12;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D13;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D2;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D3;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D5;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D7;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D8;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B4_D1;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B4_D10;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B4_D2;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B4_D3;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B4_D4;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B4_D5;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B4_D6;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B4_D7;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B4_D8;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B4_D9;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_D1;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_S1_B3;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D1;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D2;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D21;
import static mx.com.necsus.persistence.Section6Qualifiers.S6_D1;
import static mx.com.necsus.persistence.Section8Qualifiers.S8_D1;
import static mx.com.necsus.persistence.SignUpQualifiers.SIGNUP_D1;
import static mx.com.necsus.persistence.SignUpQualifiers.SIGNUP_D2;
import static mx.com.necsus.persistence.SignUpQualifiers.SIGNUP_D3;
import static mx.com.necsus.persistence.SignUpQualifiers.SIGNUP_D4;
import static mx.com.necsus.persistence.SignUpQualifiers.SIGNUP_D5;
import static mx.com.necsus.persistence.SignUpQualifiers.SIGNUP_PREFIX;
import static mx.com.necsus.persistence.StagesMapper.findStage;
import static mx.com.necsus.persistence.TypesHandler.EMPTY_1N_SIZE;
import static mx.com.necsus.persistence.TypesHandler.FIRST_ELEMENT_INDEX;
import static mx.com.necsus.persistence.TypesHandler.get1nSize;
import static mx.com.necsus.persistence.TypesHandler.getBoolean;
import static mx.com.necsus.persistence.TypesHandler.getCatalogItem;
import static mx.com.necsus.persistence.TypesHandler.getDateString;
import static mx.com.necsus.persistence.TypesHandler.getDateTimeString;
import static mx.com.necsus.persistence.TypesHandler.getFloat;
import static mx.com.necsus.persistence.TypesHandler.getInteger;
import static mx.com.necsus.persistence.TypesHandler.getLong;
import static mx.com.necsus.persistence.TypesHandler.getString;
import static mx.com.necsus.persistence.TypesHandler.getStringOrEmpty;
import static mx.com.necsus.persistence.TypesHandler.getZonedDateTime;
import static mx.com.necsus.persistence.TypesHandler.intStreamRange;
import static mx.com.necsus.persistence.TypesHandler.newGet;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.TypesHandler.streamParallel4ResultScanner;
import static mx.com.necsus.persistence.UserQualifiers.USER_D10;
import static mx.com.necsus.persistence.UserQualifiers.USER_D11;
import static mx.com.necsus.persistence.UserQualifiers.USER_D12;
import static mx.com.necsus.persistence.UserQualifiers.USER_D5;
import static mx.com.necsus.persistence.UserQualifiers.USER_D6;
import static mx.com.necsus.persistence.UserQualifiers.USER_D7;
import static mx.com.necsus.persistence.UserQualifiers.USER_D8;
import static mx.com.necsus.persistence.UserQualifiers.USER_D9;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.persistence.UsersMapper.retrieveAssignedControlDesk;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.UsersTable.USERS_TABLE_NAME;
import static mx.com.necsus.util.AccessCrypto.encode;
import static mx.com.necsus.util.Accounts.isLatestPlan;
import static mx.com.necsus.util.AccountsCommons.assignControlDesk4Information;
import static mx.com.necsus.util.AccountsCommons.deleteFile;
import static mx.com.necsus.util.AccountsCommons.isAccountInPostCertificationStatus;
import static mx.com.necsus.util.AccountsCommons.retrievePostCertificationStatus;
import static mx.com.necsus.util.AccountsCommons.retrieveVersion4PostCertificationStatus;
import static mx.com.necsus.util.AccountsStrings.LOG_DELETE_B1_AND_B4_FROM_S3_FOR;
import static mx.com.necsus.util.Catalogs.retrieveCatalogItemCodeInLowerCase;
import static mx.com.necsus.util.Catalogs.retrieveCatalogMap;
import static mx.com.necsus.util.Catalogs.retrieveSectionsAndBlocksCatalog;
import static mx.com.necsus.util.Catalogs.retrieveSpecificSectionsAndBlocksCatalog;
import static mx.com.necsus.util.CertificationsCommons.isNotDeletedListItem;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static mx.com.necsus.util.RequestBody.isNotBlankCatalogItem;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.Strings.EMPTY_STRING;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static org.apache.hadoop.hbase.CompareOperator.EQUAL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ALL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ONE;
import static org.apache.logging.log4j.LogManager.getLogger;

import java.util.List;

import mx.com.necsus.domain.Section3;
import mx.com.necsus.domain.catalog.VisitStatus;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.MultipleColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.logging.log4j.Logger;

import mx.com.necsus.domain.AccountOwnerUpdateOne;
import mx.com.necsus.domain.AccountRetrieveAll;
import mx.com.necsus.domain.AccountRetrieveOne;
import mx.com.necsus.domain.AccountUpdateOne;
import mx.com.necsus.domain.CatalogItem;
import mx.com.necsus.domain.Note;
import mx.com.necsus.domain.PaymentRetrieveAll;
import mx.com.necsus.domain.catalog.AccountStatus;
import mx.com.necsus.domain.catalog.DisclaimerStatus;
import mx.com.necsus.domain.catalog.InformationStatus;
import mx.com.necsus.domain.catalog.PaymentStatus;
import mx.com.necsus.domain.catalog.Stages;
import mx.com.necsus.domain.catalog.UserRoles;
import mx.com.necsus.persistence.domain.Account;

/**
 * Persistence layer for the <strong>Account</strong>.
 */
public final class AccountsMapper {

  /**
   * Logger for the mapper.
   */
  private static final Logger LOGGER = getLogger(AccountsMapper.class);

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to retrieve in {@link #retrieveAllInformationNotes}
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_INFORMATION_NOTES = new MultipleColumnPrefixFilter(
                                                                                   new byte[][] {
                                                                                     CERTIFICATION_D2,
                                                                                     "s1b1n".getBytes(),
                                                                                     "s1b2n".getBytes(),
                                                                                     "s1b3n".getBytes(),
                                                                                     "s1b4n".getBytes(),
                                                                                     "s1b5n".getBytes(),
                                                                                     "s2b1n".getBytes(),
                                                                                     "s2b2n".getBytes(),
                                                                                     "s3b1n".getBytes(),
                                                                                     "s3b2n".getBytes(),
                                                                                     "s3b3n".getBytes(),
                                                                                     "s4b1n".getBytes(),
                                                                                     "s4b2n".getBytes(),
                                                                                     "s4b3n".getBytes(),
                                                                                     "s4b4n".getBytes(),
                                                                                     "s4b5n".getBytes(),
                                                                                     "s4b6n".getBytes(),
                                                                                     "s4b7n".getBytes(),
                                                                                     "s5b1n".getBytes(),
                                                                                     "s5b2n".getBytes(),
                                                                                     "s5b3n".getBytes(),
                                                                                     "s6b1n".getBytes(),
                                                                                     "s6b2n".getBytes(),
                                                                                     "s6b3n".getBytes(),
                                                                                     "s7b1n".getBytes(),
                                                                                     "s7b2n".getBytes(),
                                                                                     "s7b3n".getBytes(),
                                                                                     "s7b4n".getBytes(),
                                                                                     "s7b5n".getBytes(),
                                                                                     "s8b1n".getBytes()});

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to retrieve in {@link #retrieveByAccountId}
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_RETRIEVE_BY_ACCOUNT_ID = new MultipleColumnPrefixFilter(
                                                                                        new byte[][] {
                                                                                          ACCOUNT_D10,
                                                                                          ACCOUNT_D13,
                                                                                          ACCOUNT_D14,
                                                                                          ACCOUNT_D15,
                                                                                          ACCOUNT_D2,
                                                                                          ACCOUNT_D6,
                                                                                          ACCOUNT_D7,
                                                                                          ACCOUNT_D8,
                                                                                          CERTIFICATION_D1,
                                                                                          CERTIFICATION_D2,
                                                                                          CERTIFICATION_D3,
                                                                                          CERTIFICATION_D6,
                                                                                          CERTIFICATION_D30,
                                                                                          CERTIFICATION_D32,
                                                                                          SIGNUP_D2,
                                                                                          S1_B1_D2,
                                                                                          S3_B1_D1});

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to retrieve in {@link #retrieveOriginCode}
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_RETRIEVE_ORIGIN_CODE = new MultipleColumnPrefixFilter(
                                                                                      new byte[][] {
                                                                                        ACCOUNT_D7,
                                                                                        ACCOUNT_D8,
                                                                                        SIGNUP_D2,
                                                                                        S1_B1_D2});

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to retrieve in {@link #retrieveAccounts4InformationStage}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_CHECKING_INFORMATION = new MultipleColumnPrefixFilter(
                                                                                      new byte[][] {
                                                                                        ACCOUNT_D1,
                                                                                        ACCOUNT_D2,
                                                                                        ACCOUNT_D7,
                                                                                        ACCOUNT_D8,
                                                                                        CERTIFICATION_D1,
                                                                                        CERTIFICATION_D2,
                                                                                        CERTIFICATION_D3,
                                                                                        CERTIFICATION_D4,
                                                                                        CERTIFICATION_D5,
                                                                                        CERTIFICATION_D19,
                                                                                        CERTIFICATION_D25,
                                                                                        CERTIFICATION_D30,
                                                                                        CERTIFICATION_D32,
                                                                                        S1_B1_D1,
                                                                                        S1_B1_D2,
                                                                                        S1_B1_D4,
                                                                                        S1_B1_D5,
                                                                                        S1_B1_D6,
                                                                                        S1_B1_D7,
                                                                                        S1_B4_D2});

  /**
   * Column prefixes to retrieve in {@link #retrieveAccounts4DisclaimerStage}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_CHECKING_DISCLAIMER = new MultipleColumnPrefixFilter(
                                                                                      new byte[][] {
                                                                                        ACCOUNT_D1,
                                                                                        ACCOUNT_D2,
                                                                                        ACCOUNT_D7,
                                                                                        ACCOUNT_D8,
                                                                                        CERTIFICATION_D1,
                                                                                        CERTIFICATION_D2,
                                                                                        CERTIFICATION_D3,
                                                                                        CERTIFICATION_D4,
                                                                                        CERTIFICATION_D5,
                                                                                        CERTIFICATION_D19,
                                                                                        CERTIFICATION_D26,
                                                                                        CERTIFICATION_D30,
                                                                                        DISCLAIMER_S1,
                                                                                        S1_B1_D1,
                                                                                        S1_B1_D2,
                                                                                        S1_B1_D4,
                                                                                        S1_B1_D5,
                                                                                        S1_B1_D7,
                                                                                        S1_B4_D2});

  /**
   * Column prefixes to retrieve in {@link #retrieveAccounts4CertifiedStage}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_CERTIFIED = new MultipleColumnPrefixFilter(
                                                                                      new byte[][] {
                                                                                        ACCOUNT_D1,
                                                                                        ACCOUNT_D11,
                                                                                        ACCOUNT_D2,
                                                                                        CERTIFICATION_D1,
                                                                                        CERTIFICATION_D2,
                                                                                        CERTIFICATION_D3,
                                                                                        CERTIFICATION_D4,
                                                                                        CERTIFICATION_D5,
                                                                                        CERTIFICATION_D19,
                                                                                        CERTIFICATION_D30,
                                                                                        S1_B1_D1,
                                                                                        S1_B1_D2,
                                                                                        S1_B1_D4,
                                                                                        S1_B1_D5,
                                                                                        S1_B1_D7,
                                                                                        S1_B4_D2});

  /**
   * {@link FilterList} for the {@link AccountStatus} to retrieve in {@link #retrieveAccounts4InformationStage}
   */
  private static final FilterList FILTER_ACCOUNT_STATUS_4_CHECKING_INFORMATION = new FilterList(
                                                                                    MUST_PASS_ONE,
                                                                                    createSingleColumnValueFilter(
                                                                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                      ACCOUNT_D2,
                                                                                      EQUAL,
                                                                                      CHECKING_INFORMATION.name()),
                                                                                    createSingleColumnValueFilter(
                                                                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                      ACCOUNT_D2,
                                                                                      EQUAL,
                                                                                      INFORMATION_VALIDATED.name()),
                                                                                    createSingleColumnValueFilter(
                                                                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                      ACCOUNT_D2,
                                                                                      EQUAL,
                                                                                      DISCLAIMER.name()),
                                                                                    createSingleColumnValueFilter(
                                                                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                      ACCOUNT_D2,
                                                                                      EQUAL,
                                                                                      DISCLAIMER_REJECTED.name()),
                                                                                    createSingleColumnValueFilter(
                                                                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                      ACCOUNT_D2,
                                                                                      EQUAL,
                                                                                      CHECKING_DISCLAIMER.name()),
                                                                                    createSingleColumnValueFilter(
                                                                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                      ACCOUNT_D2,
                                                                                      EQUAL,
                                                                                      CERTIFIED.name()));

  /**
   * {@link FilterList} for the {@link AccountStatus} to retrieve in {@link #retrieveAccounts4DisclaimerStage}
   */
  private static final FilterList FILTER_ACCOUNT_STATUS_4_CHECKING_DISCLAIMER = new FilterList(
                                                                                  MUST_PASS_ONE,
                                                                                  createSingleColumnValueFilter(
                                                                                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                    ACCOUNT_D2,
                                                                                    EQUAL,
                                                                                    DISCLAIMER_REJECTED.name()),
                                                                                  createSingleColumnValueFilter(
                                                                                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                    ACCOUNT_D2,
                                                                                    EQUAL,
                                                                                    CHECKING_DISCLAIMER.name()),
                                                                                  createSingleColumnValueFilter(
                                                                                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                    ACCOUNT_D2,
                                                                                    EQUAL,
                                                                                    CERTIFIED.name()));

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to fill new certification.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_2_FILL_NEW_CERTIFICATION = new MultipleColumnPrefixFilter(
                                                                                        new byte[][] {
                                                                                          SIGNUP_PREFIX,
                                                                                          ACCOUNT_PREFIX});

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to update the contact person.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_2_UPDATE_CONTACT_PERSON = new MultipleColumnPrefixFilter(
                                                                                        new byte[][] {
                                                                                          USER_D5,
                                                                                          USER_D6,
                                                                                          USER_D7,
                                                                                          USER_D8,
                                                                                          USER_D9,
                                                                                          USER_D10,
                                                                                          USER_D11,
                                                                                          USER_D12});

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to retrieve in {@link #retrieveAccount4Completed}
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_RETRIEVE_ACCOUNT_4_COMPLETED =
                                                                                        new MultipleColumnPrefixFilter(
                                                                                          new byte[][] {
                                                                                            ACCOUNT_D1,
                                                                                            ACCOUNT_D2,
                                                                                            ACCOUNT_D3,
                                                                                            ACCOUNT_D7,
                                                                                            ACCOUNT_D8,
                                                                                            CERTIFICATION_D2,
                                                                                            CERTIFICATION_D3,
                                                                                            "pyd".getBytes(),
                                                                                            "pys1".getBytes(),
                                                                                            S1_B1_D6,
                                                                                            S3_B1_D1,
                                                                                            S3_B4_D1,
                                                                                            S5_B1_D1,
                                                                                            "s5b1d2".getBytes(),
                                                                                            "s5b1d21".getBytes()});

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to retrieve in {@link #retrieveAccount4Completed}
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_PRIVACY_ACCOUNT = new MultipleColumnPrefixFilter(
                                                                                new byte[][] {
                                                                                  ACCOUNT_D13,
                                                                                  ACCOUNT_D14});

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to retrieve in {@link #retrieveAllPaymentNotes}
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_PAYMENT_NOTE = new MultipleColumnPrefixFilter(
                                                                              new byte[][] {
                                                                                "pyd".getBytes(),
                                                                                "pyn".getBytes()});

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to retrieve in {@link #retrieveAllMatchesNotes}
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_MATCHES_NOTE = new MultipleColumnPrefixFilter(
                                                                              new byte[][] {
                                                                                "mtn".getBytes()});

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to retrieve in {@link #retrieveAllDisclaimerNotes}
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_DISCLAIMER_NOTE = new MultipleColumnPrefixFilter(
                                                                                 new byte[][] {
                                                                                   "dmn".getBytes()});

  /**
   * {@link MultipleColumnPrefixFilter} with the prefixes to retrieve in {@link #retrieveAccount4CompletedInformation}
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_RETRIEVE_ACCOUNT_4_COMPLETED_INFORMATION
                                                                                      = new MultipleColumnPrefixFilter(
                                                                                          new byte[][] {
                                                                                            ACCOUNT_D7,
                                                                                            ACCOUNT_D8,
                                                                                            CERTIFICATION_D2,
                                                                                            S1_S1_B2,
                                                                                            S3_S1_B3,});

  /**
   * {@link SingleColumnValueFilter} for the {@link AccountQualifiers#ACCOUNT_D11} to retrieve in {@link #retrieveAccounts4CertifiedStage}
   */
  private static final SingleColumnValueFilter FILTER_4_IS_SEARCHABLE = createSingleColumnValueFilter(
                                                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                          ACCOUNT_D11,
                                                                          EQUAL,
                                                                          TRUE);

  /**
   * Index for the first element of a {@link List}.
   */
  private static final int FIVE_ELEMENT_INDEX = 4;

  /**
   * Block note size pattern.
   */
  private static final String SX_BX_N1_PATTERN = "%s%sn1";

  /**
   * Block note message pattern.
   */
  private static final String SX_BX_N2_XX_PATTERN = "%s%sn2%02d";

  /**
   * Note creation date pattern.
   */
  private static final String SX_BX_N3_XX_PATTERN = "%s%sn3%02d";

  /**
   * Note creation user role pattern.
   */
  private static final String SX_BX_N4_XX_PATTERN = "%s%sn4%02d";

  /**
   * Day elapsed zero.
   */
  private static final int ZERO_DAY = 0;

  /**
   * Private explicit default constructor for security.
   */
  private AccountsMapper() {
  }

  /**
   * Search an account by unique identifier.
   *
   * @param accountId Account's unique identifier.
   *
   * @return <strong>true</strong> if the account does not exists, <strong>false</strong> in other case.
   */
  public static boolean isNotExistingAccount(final String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var rowKey = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .addColumn(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                 ACCOUNT_D1))
                           .getRow();

      LOGGER.info(
        "Account id found '{}'",
        null == rowKey
          ? null
          : Bytes
              .toString(
                rowKey));

      return
        null == rowKey;

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve an account by unique identifier.
   *
   * @param lang Language used in the application.
   * @param roleCode  Role of the user signed.
   * @param accountId Account's unique identifier.
   *
   * @return An instance of the type {@link AccountRetrieveOne} with the values found.
   */
  public static AccountRetrieveOne retrieveByAccountId(final String lang,
                                                       final String roleCode,
                                                       final String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 PREFIXES_4_RETRIEVE_BY_ACCOUNT_ID));

      final var version = retrieveVersion4PostCertificationStatus(
                            result);

      final var certificationId = getString(
                                    result,
                                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                    CERTIFICATION_D1);

      final var section3 = new Section3();

      Section3Mapper
        .retrieveBlock1(
          result,
          section3,
          DEFAULT_LANG);

      final var originCode = isNotBlankString(
                               getStringOrEmpty(
                                 result,
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                 S1_B1_D2,
                                 version))
                               ? getString(
                                   result,
                                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                   S1_B1_D2,
                                   version)
                               : getStringOrEmpty(
                                   result,
                                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                   SIGNUP_D2);

      final var accountRetrieved = new AccountRetrieveOne()
                                      .setD1(
                                        getCatalogItem(
                                          result,
                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                          ACCOUNT_D2))
                                      .setD2(
                                        certificationId)
                                      .setD3(
                                        getCatalogItem(
                                          result,
                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                          CERTIFICATION_D2,
                                          version))
                                      .setD4(
                                        getCatalogItem(
                                          result,
                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                          CERTIFICATION_D3,
                                          version))
                                      .setD6(
                                        getBoolean(
                                          result,
                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                          ACCOUNT_D6))
                                      .setD7(
                                        getCatalogItem(
                                          result,
                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                          CERTIFICATION_D30))
                                      .setD8(
                                        getCatalogItem(
                                          result,
                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                          ACCOUNT_D14,
                                          INVITED_BY,
                                          lang))
                                      .setD9(
                                        getBoolean(
                                          result,
                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                          ACCOUNT_D13))
                                      .setD15(
                                        isChangePlanAvailable(
                                          result))
                                      .setD16(
                                        findStageByAccountId(
                                          accountId,
                                          roleCode,
                                          getCatalogItem(
                                            result,
                                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                            ACCOUNT_D2,
                                            version),
                                          getCatalogItem(
                                            result,
                                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                            CERTIFICATION_D32,
                                            version)))
                                      .setD17(
                                        findMatchesStageByStatus(
                                          getCatalogItem(
                                            result,
                                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                            ACCOUNT_D2,
                                            version),
                                          getCatalogItem(
                                            result,
                                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                            CERTIFICATION_D6,
                                            version)))
                                      .setD19(
                                        section3
                                          .getB1D1()
                                          .size())
                                      .setD20(
                                        hasAnyAssociation2Buyers(
                                            accountId))
                                      .setD22(
                                        getBoolean(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        ACCOUNT_D10))
                                      .setD23(
                                        new CatalogItem()
                                          .setD1(originCode));

      if (null != certificationId) {

        accountRetrieved
          .setD10(
            retrieveAccountOwnerByAccountId(
              accountId,
              lang));

      }

      return accountRetrieved;

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve a account owner by unique identifier.
   *
   * @param accountId Account's unique identifier.
   * @param lang Language used in the application.
   *
   * @return An instance of the type {@link AccountOwnerUpdateOne} with the values found.
   */
  public static AccountOwnerUpdateOne retrieveAccountOwnerByAccountId(final String accountId,
                                                                      final String lang) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               USERS_TABLE_NAME)) {

      final var result = table
                            .get(
                              newGet(
                                  accountId)
                                .addFamily(
                                  USERS_COLUMN_FAMILY_DEFAULT)
                                .setFilter(
                                  PREFIXES_2_UPDATE_CONTACT_PERSON));

      return
        new AccountOwnerUpdateOne()
          .setD1(
            getString(
              result,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D5))
          .setD2(
            getString(
              result,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D6))
          .setD3(
            getString(
              result,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D7))
          .setD4(
            getString(
              result,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D8))
          .setD5(
            getCatalogItem(
              result,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D9,
              COUNTRY_CALLING_CODES,
              lang))
          .setD6(
            getString(
              result,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D10))
          .setD7(
            getString(
              result,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D11))
          .setD8(
            getString(
              result,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D12));

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the origin code of the account.
   *
   * @param accountId Account's unique identifier.
   *
   * @return The origin code of the account.
   */
  public static String retrieveOriginCode(final String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                               accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 PREFIXES_4_RETRIEVE_ORIGIN_CODE));

      final var version = retrieveVersion4PostCertificationStatus(result);

      return isNotBlankString(
                 getStringOrEmpty(
                   result,
                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                   S1_B1_D2,
                   version))
               ? getString(
                   result,
                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                   S1_B1_D2,
                   version)
               : getStringOrEmpty(
                   result,
                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                   SIGNUP_D2);

    } catch (final Exception exc) {

      throw new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the certification type's unique identifier for the plan.
   *
   * @param plan Instance of the type {@link CatalogItem} with the plan's unique identifier.
   *
   * @return Certification type's unique identifier.
   */
  public static String retrieveCertificationTypeByPlan(final CatalogItem plan) {

    try (final var sqlSession = CATALOGS_DB
                                  .getInstance()
                                  .openSession(
                                    true)) {

      return
        sqlSession
          .getMapper(
            CatalogsStaticItemsMapper.class)
          .retrieveCertificationTypeByPlanCode(
            plan.getD1());

    }

  }

  /**
   * Retrieve the values required for complete information.
   *
   *  @param accountId Account's unique identifier.
   *
   * @return Instance of the type {@link Account} with the values in HBase for the account.
   */
  public static Account retrieveAccount4CompletedInformation(final String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 PREFIXES_4_RETRIEVE_ACCOUNT_4_COMPLETED_INFORMATION));

      final var version = retrieveVersion4PostCertificationStatus(
                            result);

      return
        new Account()
          .setPostCertificationVersion(
            getLong(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              ACCOUNT_D7))
          .setPostCertificationStatus(
            getString(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              ACCOUNT_D8))
          .setCurrentCertificationType(
            getString(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D2))
          .setS3S1B3(
            getString(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S3_S1_B3,
              version))
          .setS1S1B2(
            getString(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_S1_B2,
              version));

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Update the <strong>account</strong> for the completed step.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param account Instance of the type {@link Account} with the data to put.
   */
  public static void updateAccount4Completed(final Put     put,
                                             final Account account) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D2,
        account
          .getStatusCode());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D3,
        account
          .getStatusDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D6,
        account
          .getCertificationCongrats());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D7,
        account
          .getPostCertificationVersion());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D11,
        account
          .getIsSearchable());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D4,
        account
          .getCertificationCreationDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D5,
        account
          .getExpireDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D6,
        account
          .getMatchesStatusCode());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D7,
        account
          .getMatchesStatusDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D22,
        account
          .getVisitStatusCode());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D23,
        account
          .getVisitStatusDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D30,
        account
          .getCertificationStatusCode());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D31,
        account
          .getCertificationStatusDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D35,
        account
          .getEvaluationDate());

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Update the privacy <strong>status</strong> of the account.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param hasPublicScope Boolean scope of private or public scope.
   * @param accountUpdate Instance of the type {@link AccountUpdateOne} with the data to put.
   */
  public static void updateAccountPrivacy(final Put     put,
                                          final Boolean hasPublicScope,
                                          final AccountUpdateOne accountUpdate) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D13,
        hasPublicScope);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D14,
        accountUpdate
          .getD2());

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Update the detail of the {@link UserRoles#ACCT_OWNER}.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param accountOwnerUpdated Instance of the type {@link AccountUpdateOne} with the data to put.
   */
  public static void updateAccountOwner(final Put                   put,
                                        final AccountOwnerUpdateOne accountOwnerUpdated) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               USERS_TABLE_NAME)) {

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D5,
        accountOwnerUpdated.getD1());

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D6,
        accountOwnerUpdated.getD2());

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D7,
        accountOwnerUpdated.getD3());

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D8,
        accountOwnerUpdated.getD4());

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D9,
        accountOwnerUpdated.getD5());

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D10,
        accountOwnerUpdated.getD6());

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D11,
        accountOwnerUpdated.getD7());

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Update the email for an {@link UserRoles#ACCT_OWNER}.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param email New email string for the new account owner.
   */
  public static void updateEmailAccountOwner(final Put     put,
                                             final String email) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               USERS_TABLE_NAME)) {

      putValue(
        put,
        USERS_COLUMN_FAMILY_DEFAULT,
        USER_D12,
        email);

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the values required for complete plan.
   *
   * @param lang Language used in the application.
   * @param accountId Account's unique identifier.
   *
   * @return Instance of the type {@link Account} with the values in HBase for the account.
   */
  public static AccountRetrieveOne retrieveAccountPrivacy(final String lang,
                                                          final String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                          .get(
                            newGet(
                              accountId)
                              .addFamily(
                                ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                              .setFilter(
                                PREFIXES_4_PRIVACY_ACCOUNT));

      return
        new AccountRetrieveOne()
          .setD8(
            getCatalogItem(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              ACCOUNT_D14,
              INVITED_BY,
              lang))
          .setD9(
            getBoolean(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              ACCOUNT_D13));

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the email for an {@link UserRoles#ACCT_OWNER}.
   *
   * @param accountId Account's unique identifier.
   *
   * @return Instance string Email of the current account.
   */
  public static String retrieveEmailAccountOwner(final String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               USERS_TABLE_NAME)) {

      final var result = table
                            .get(
                              newGet(
                                accountId)
                                .addFamily(
                                  USERS_COLUMN_FAMILY_DEFAULT)
                                .setFilter(
                                  new MultipleColumnPrefixFilter(
                                    new byte[][] { USER_D12 })));

      return
        getString(
          result,
          USERS_COLUMN_FAMILY_DEFAULT,
          USER_D12);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the values required for complete plan.
   *
   * @param accountId Account's unique identifier.
   *
   * @return Instance of the type {@link Account} with the values in HBase for the account.
   */
  public static Account retrieveAccount4Completed(final String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 PREFIXES_4_RETRIEVE_ACCOUNT_4_COMPLETED));

      final var version = retrieveVersion4PostCertificationStatus(
                            result);

      return
        new Account()
          .setStatusCode(
            getString(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              ACCOUNT_D2))
          .setSignUpDate(
            getZonedDateTime(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              ACCOUNT_D1))
          .setPostCertificationVersion(
            getLong(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              ACCOUNT_D7))
          .setPostCertificationStatus(
            getString(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              ACCOUNT_D8))
          .setCurrentCertificationType(
            getString(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D2))
          .setCertificationPlan(
            getCatalogItem(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D3,
              version))
          .setLegalEntityCode(
            getString(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B1_D6))
          .setArticleSize(
            get1nSize(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S3_B1_D1))
          .setFiscalYearsSize(
            get1nSize(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S5_B1_D1))
          .setLastFiscalYear(
            retrieveLastFiscalYear(
              result))
          .setPaymentCaptureDate(
            CHECKING_PAYMENT == findAccountStatus(
                                  getString(
                                    result,
                                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                    ACCOUNT_D2))
              ? getString(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    PY_D2,
                    retrieveLastPaymentId(
                      result)))
              : null)
          .setAccreditationSize(
            get1nSize(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S3_B4_D1));

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Execute the {@link Put} to complete the <strong>plan</strong>.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param account Instance of the type {@link Account} with the certification data to put.
   */
  public static void updateAccount4CompletePlan(final Put     put,
                                                final Account account) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D2,
        account
          .getStatusCode());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D3,
        account
          .getStatusDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D2,
        account
          .getPostCertificationVersion(),
        account
          .getCurrentCertificationType());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D3,
        account
          .getPostCertificationVersion(),
        account
          .getCertificationPlan());

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Execute the {@link Put} to complete the <strong>payment validated</strong>.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param account Instance of the type {@link Account} with the certification data to put.
   */
  public static void updateAccount4CompletePaymentValidated(final Put     put,
                                                            final Account account) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D2,
        account
          .getStatusCode());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D3,
        account
          .getStatusDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D15,
        account
          .getIsLatestPlan());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D1,
        account
          .getPostCertificationVersion(),
        account
          .getCertificationId());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D5,
        account
          .getPostCertificationVersion(),
        account
          .getExpireDate());

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Execute the {@link Put} to complete the <strong>information</strong>.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param account Instance of the type {@link Account} with the certification data to put.
   */
  public static void completeInformation(final Put     put,
                                         final Account account) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D2,
        account.getStatusCode());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D3,
        account.getStatusDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D32,
        account.getInformationStatusCode());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D33,
        account.getInformationStatusDate());

      if (!isAccountInPostCertificationStatus(
            account
              .getPostCertificationStatus())) {

        retrieveSectionsAndBlocksCatalog(
          account.getRowKey(),
          DEFAULT_LANG)
          .forEach(
            (section) -> {

              final var sectionCode = section
                                        .getD1()
                                        .toLowerCase();

              putValue(
                put,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                qualifierNameWithPattern(
                  SECTIONS_STATUS,
                  sectionCode),
                account.getInformationStatusCode());

              putValue(
                put,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                qualifierNameWithPattern(
                  SECTIONS_STATUS_DATE,
                  sectionCode),
                account.getInformationStatusDate());

              section
                .getBlocks()
                .forEach(
                  (block) -> {

                    final var blockCode = retrieveCatalogItemCodeInLowerCase(
                                            block);

                    putValue(
                      put,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      qualifierNameWithPattern(
                        BLOCKS_STATUS,
                        sectionCode,
                        blockCode),
                      account.getInformationStatusCode());

                    putValue(
                      put,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      qualifierNameWithPattern(
                        BLOCKS_STATUS_DATE,
                        sectionCode,
                        blockCode),
                      account.getInformationStatusDate());

                  });

            });

        retrieveSpecificSectionsAndBlocksCatalog(
            account.getRowKey(),
            DEFAULT_LANG)
          .forEach(
            (section) -> {

              final var sectionCode = section
                                        .getD1()
                                        .toLowerCase();

              putValue(
                put,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                qualifierNameWithPattern(
                  SECTIONS_STATUS,
                  sectionCode),
                account.getInformationStatusCode());

              putValue(
                put,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                qualifierNameWithPattern(
                  SECTIONS_STATUS_DATE,
                  sectionCode),
                account.getInformationStatusDate());

              section
                .getBlocks()
                .forEach(
                  (block) -> {

                    final var blockCode = retrieveCatalogItemCodeInLowerCase(
                                            block);

                    putValue(
                      put,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      qualifierNameWithPattern(
                        BLOCKS_STATUS,
                        sectionCode,
                        blockCode),
                      account.getInformationStatusCode());

                    putValue(
                      put,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      qualifierNameWithPattern(
                        BLOCKS_STATUS_DATE,
                        sectionCode,
                        blockCode),
                      account.getInformationStatusDate());

                  });

            });

      }

      assignControlDesk4Information(
        put,
        account.getRowKey());

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve account rows by checking information.
   *
   * @param approverId Approver's unique identifier.
   * @param stage One of {@link Stages}.
   * @param timeZone Session's time zone.
   * @param lang The language to retrieve catalog item.
   * @param roleCode Role unique identifier.
   *
   * @return A list of the type {@link AccountRetrieveAll}.
   */
  public static List<AccountRetrieveAll> retrieveAccounts4InformationStage(final String approverId,
                                                                           final String stage,
                                                                           final String timeZone,
                                                                           final String lang,
                                                                           final String roleCode) {

    var informationStatus = NULL;

    switch (findStage(stage)) {

      case INFORMATIONS_GRAY:

        informationStatus = InformationStatus.TO_CHECK;

        break ;

      case INFORMATIONS_RED:

        informationStatus = FAIL;

        break ;

      case INFORMATIONS_YELLOW:

        informationStatus = ADJUSTMENT;

        break ;

      case INFORMATIONS_GREEN:

        informationStatus = SUCCESS;

        break ;

      default:

        return emptyList();

    }

    final var filterList = new FilterList(
                             MUST_PASS_ALL,
                             PREFIXES_4_CHECKING_INFORMATION,
                             FILTER_ACCOUNT_STATUS_4_CHECKING_INFORMATION,
                             createSingleColumnValueFilter(
                               ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                               CERTIFICATION_D32,
                               EQUAL,
                               informationStatus.name()));

      if (CONTROL_DESK == findUserRole(roleCode)) {

        filterList
          .addFilter(
            createSingleColumnValueFilter(
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D25,
              EQUAL,
              approverId));

      }

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME);
         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           filterList)
                                         .readAllVersions())) {

      return
        retrieveAccounts4InformationStage(
          resultScanner,
          timeZone,
          lang,
          roleCode);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve account rows by checking disclaimer.
   *
   * @param approverId Approver's unique identifier.
   * @param stage One of {@link Stages}.
   * @param timeZone Session's time zone.
   * @param lang The language to retrieve catalog item.
   * @param roleCode Role unique identifier.
   *
   * @return A list of the type {@link AccountRetrieveAll}.
   */
  public static List<AccountRetrieveAll> retrieveAccounts4DisclaimerStage(final String approverId,
                                                                          final String stage,
                                                                          final String timeZone,
                                                                          final String lang,
                                                                          final String roleCode) {

    var disclaimerStatus = DisclaimerStatus.NULL;

    switch (findStage(stage)) {

      case DISCLAIMERS_GRAY:

        disclaimerStatus = DisclaimerStatus.TO_CHECK;

        break ;

      case DISCLAIMERS_RED:

        disclaimerStatus = DisclaimerStatus.FAIL;

        break ;

      case DISCLAIMERS_GREEN:

        disclaimerStatus = DisclaimerStatus.SUCCESS;

        break ;

      default:

        return emptyList();

    }

    final var filterList = new FilterList(
                             MUST_PASS_ALL,
                             PREFIXES_4_CHECKING_DISCLAIMER,
                             FILTER_ACCOUNT_STATUS_4_CHECKING_DISCLAIMER,
                             createSingleColumnValueFilter(
                               ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                               DISCLAIMER_S1,
                               EQUAL,
                               disclaimerStatus.name()));

    if (CONTROL_DESK == findUserRole(roleCode)) {

      filterList
        .addFilter(
          createSingleColumnValueFilter(
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            CERTIFICATION_D26,
            EQUAL,
            approverId));

    }

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME);
         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           filterList)
                                         .readAllVersions())) {

      return
        retrieveAccounts4DisclaimerStage(
          resultScanner,
          timeZone,
          lang,
          roleCode);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve account rows by certified stage.
   *
   * @param timeZone Session's time zone.
   * @param lang The language to retrieve catalog item.
   *
   * @return A list of the type {@link AccountRetrieveAll}.
   */
  public static List<AccountRetrieveAll> retrieveAccounts4CertifiedStage(final String timeZone,
                                                                         final String lang) {

    try (final var table = H_BASE
                            .getInstance()
                            .getTable(
                              ACCOUNTS_TABLE_NAME);
                               final var resultScanner = table
                                 .getScanner(
                                   new Scan()
                                     .addFamily(
                                       ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                     .setFilter(
                                       new FilterList(
                                         MUST_PASS_ALL,
                                         PREFIXES_4_CERTIFIED,
                                         FILTER_4_IS_SEARCHABLE)))) {

      return
        retrieveAccounts4CertifiedStage(
          resultScanner,
          timeZone,
          lang);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve all notes of the payments.
   *
   * @param accountId Account's unique identifier.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return A list of the type {@link Note}
   */
  public static List<Note> retrieveAllPaymentNotes(final String accountId,
                                                   final String timeZone,
                                                   final String lang) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                               accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 PREFIXES_4_PAYMENT_NOTE));

      return
        range(
            FIRST_ELEMENT_INDEX,
            5)
          .filter(
            index -> isNotDeletedListItem(
                       result,
                       ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                       PY_D4,
                       index))
          .mapToObj(
            index -> retrievePaymentNotes(
                       result,
                       index,
                       timeZone,
                       lang))
          .flatMap(
            List::stream)
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve all notes of the information.
   *
   * @param accountId Account's unique identifier.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return A list of the type {@link Note}
   */
  public static List<Note> retrieveAllInformationNotes(final String accountId,
                                                       final String timeZone,
                                                       final String lang) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                               accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 PREFIXES_4_INFORMATION_NOTES));

      return
        retrieveSectionsAndBlocksCatalog(
            accountId,
            DEFAULT_LANG)
          .stream()
          .map(
            (section) -> {

              final var sectionCode = section
                                        .getD1()
                                        .toLowerCase();

              return
                section
                  .getBlocks()
                  .stream()
                  .map(
                    (block) -> {

                      final var blockCode = block
                                              .getD1()
                                              .toLowerCase();

                      return
                        intStreamRange(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            qualifierNameWithPattern(
                              SX_BX_N1_PATTERN,
                              sectionCode,
                              blockCode))
                          .parallel()
                          .mapToObj(
                            index -> new Note()
                                       .setD2(
                                         getString(
                                           result,
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                           qualifierNameWithPattern(
                                             SX_BX_N2_XX_PATTERN,
                                             sectionCode,
                                             blockCode,
                                             index)))
                                       .setD3(
                                         getDateTimeString(
                                           result,
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                           qualifierNameWithPattern(
                                             SX_BX_N3_XX_PATTERN,
                                             sectionCode,
                                             blockCode,
                                             index),
                                           timeZone,
                                           lang))
                                       .setD4(
                                         getCatalogItem(
                                           result,
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                           qualifierNameWithPattern(
                                             SX_BX_N4_XX_PATTERN,
                                             sectionCode,
                                             blockCode,
                                             index))))
                          .collect(
                            toList());

                    })
                  .flatMap(
                    List::stream)
                  .collect(
                    toList());

            })
          .flatMap(
            List::stream)
          .sorted(
            nullsLast(
              comparing(Note::getD3)
                .reversed()))
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve all notes of the matches.
   *
   * @param accountId Account's unique identifier.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return A list of the type {@link Note}
   */
  public static List<Note> retrieveAllMatchesNotes(final String accountId,
                                                   final String timeZone,
                                                   final String lang) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 PREFIXES_4_MATCHES_NOTE));

      return
        intStreamRange(
            result,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            MT_N1)
          .parallel()
          .mapToObj(
            index -> new Note()
                       .setD2(
                         getString(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           qualifierNameOneIndex(
                             MT_N2,
                             index)))
                       .setD3(
                         getDateTimeString(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           qualifierNameOneIndex(
                             MT_N3,
                             index),
                           timeZone,
                           lang))
                       .setD4(
                         getCatalogItem(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           qualifierNameOneIndex(
                             MT_N4,
                             index))))
          .sorted(
            nullsLast(
              comparing(Note::getD3)
                .reversed()))
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve all notes of the disclaimer.
   *
   * @param accountId Account's unique identifier.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return A list of the type {@link Note}
   */
  public static List<Note> retrieveAllDisclaimerNotes(final String accountId,
                                                      final String timeZone,
                                                      final String lang) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 PREFIXES_4_DISCLAIMER_NOTE));

      return
        intStreamRange(
            result,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            DISCLAIMER_N1)
          .parallel()
          .mapToObj(
            index -> new Note()
                       .setD2(
                         getString(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           qualifierNameOneIndex(
                             DISCLAIMER_N2,
                             index)))
                       .setD3(
                         getDateTimeString(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           qualifierNameOneIndex(
                             DISCLAIMER_N3,
                             index),
                           timeZone,
                           lang))
                       .setD4(
                         getCatalogItem(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           qualifierNameOneIndex(
                             DISCLAIMER_N4,
                             index))))
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Update the post certification status for an account.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param account Instance of the type {@link Account} with the data to put.
   */
  public static void updatePostCertificationStatus(final Put     put,
                                                   final Account account) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D2,
        account.getStatusCode());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D3,
        account.getStatusDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D7,
        account.getPostCertificationVersion());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D8,
        account.getPostCertificationStatus());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D9,
        account.getPostCertificationDate());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D30,
        account.getCertificationStatusCode());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D31,
        account.getCertificationStatusDate());

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the post certification status to cancel.
   *
   * @param accountId Account's unique identifier.
   *
   * @return {@link String} with the post certification status for the account.
   */
  public static String retrievePostCertificationStatus2Cancel(final String accountId) {

    switch (findPostCertificationStatus(
              retrievePostCertificationStatus(
                accountId))) {

      case UPDATE_INITIATED:

        return
          UPDATE_CANCELED.name();

      case UPGRADE_INITIATED:

        return
          UPGRADE_CANCELED.name();

      default:

        return
          null;

    }

  }

  /**
   * Retrieve the certification status.
   *
   * @param accountId Account's unique identifier.
   *
   * @return {@link String} with the certification status for the account.
   */
  public static String retrieveCertificationStatus(final String accountId) {


    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .addColumn(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                 CERTIFICATION_D5));

      final var expireDate = getZonedDateTime(
                               result,
                               ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                               CERTIFICATION_D5);

      if (null == expireDate) {

        return null;

      }

    final var daysElapsed = (int )between(
                                    ofInstant(
                                      ofEpochMilli(
                                        nowEpochMilli()),
                                      UTC),
                                    expireDate)
                                    .toDays();

      if (daysElapsed > ZERO_DAY) {

        return ACTIVE.name();

      } else {

        return INACTIVE.name();

      }

    } catch (final Exception exc) {

      throw new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the sign up data to fill the corresponding sections.
   *
   * @param put Instance of the type {@link Put} to which the value will add.
   * @param accountId Account's unique identifier.
   */
  public static void fillSections4NewCertification(final Put    put,
                                                   final String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 PREFIXES_2_FILL_NEW_CERTIFICATION));

      fillSection1(
        put,
        result);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S2_D1,
        TRUE);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Reset mission complete and visit status.
   *
   * @param put Instance of the type {@link Put} to which the value will add.
   * @param account Instance of the type {@link Account} with values from HBase.
   */
  public static void resetMissionCompleteAndVisitStatus(final Put     put,
                                                        final Account account) {

    final var currentCertificationType = findCertificationType(
                                           account
                                             .getCurrentCertificationType());

    final var updateCertificationType = findCertificationType(
                                          account
                                            .getUpdateCertificationType());

    final var now = nowEpochMilli();

    if (STD == currentCertificationType &&
        ADV == updateCertificationType){

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S6_D1,
        account
          .getPostCertificationVersion(),
        FALSE);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S8_D1,
        account
          .getPostCertificationVersion(),
        FALSE);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D22,
        VisitStatus.NULL.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D23,
        now);

    }

    if ((ADV == currentCertificationType &&
         STD == updateCertificationType) ||
        (ADV == currentCertificationType &&
         ADV == updateCertificationType)) {

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D22,
        VisitStatus.NULL.name());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        CERTIFICATION_D23,
        now);

    }

  }

  /**
   * Set the deleted flag to delete block 1 and block 2 from section 3.
   *
   * @param accountId Account's unique identifier.
   * @param account Instance of the type {@link Account} with values from HBase.
   * @param put Instance of the type {@link Put} to which the value will add.
   */
  public static void deleteB1AndB4FromS3(final String accountId,
                                         final Account account,
                                         final Put     put) {

    LOGGER.info(
      LOG_DELETE_B1_AND_B4_FROM_S3_FOR,
      accountId);

    putValue(
      put,
      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
      S3_D1,
      account
        .getPostCertificationVersion(),
      FALSE);

    putValue(
      put,
      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
      S3_B1_D1,
      account
        .getPostCertificationVersion(),
      EMPTY_1N_SIZE);

    rangeClosed(
        FIRST_ELEMENT_INDEX,
        account
          .getArticleSize())
      .forEach(
        (index) -> {

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B1_D2,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B1_D3,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B1_D5,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B1_D7,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B1_D8,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B1_D11,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B1_D12,
              index),
            account
              .getPostCertificationVersion(),
            FALSE);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B1_D13,
              index),
            account
              .getPostCertificationVersion(),
            TRUE);

        });

    putValue(
      put,
      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
      S3_B4_D1,
      account
        .getPostCertificationVersion(),
      EMPTY_1N_SIZE);

    rangeClosed(
        FIRST_ELEMENT_INDEX,
        account
          .getAccreditationSize())
      .forEach(
        (index) -> {

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B4_D2,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B4_D3,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B4_D4,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B4_D5,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B4_D6,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B4_D7,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B4_D8,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B4_D9,
              index),
            account
              .getPostCertificationVersion(),
            EMPTY_STRING);

          putValue(
            put,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            qualifierNameOneIndex(
              S3_B4_D10,
              index),
            account
              .getPostCertificationVersion(),
            FALSE);

          deleteFile(
            put,
            S3B4FA.name(),
            index,
            account
              .getPostCertificationVersion());

        });

  }

  /**
   * Retrieve accounts with multiple qualifiers from an HBase table.
   *
   * @param resultScanner The current connection result to extract multiple values.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   * @param roleCode Role unique identifier.
   *
   * @return A list of the type {@link AccountRetrieveAll}
   */
  private static List<AccountRetrieveAll> retrieveAccounts4InformationStage(final ResultScanner resultScanner,
                                                                            final String        timeZone,
                                                                            final String        lang,
                                                                            final String        roleCode) {

    final var certificationTypes = retrieveCatalogMap(
                                     CERTIFICATION_TYPES,
                                     lang);

    final var origins = retrieveCatalogMap(
                          ORIGINS,
                          lang);

    final var economicActivities = retrieveCatalogMap(
                                     ECONOMIC_ACTIVITIES,
                                     lang);

    final var countries = retrieveCatalogMap(
                            COUNTRIES,
                            lang);

    final var legalEntities = retrieveCatalogMap(
                                LEGAL_ENTITIES,
                                lang);

    return
      streamParallel4ResultScanner(
          resultScanner)
        .map(
          (result) -> {

            final var version = retrieveVersion4PostCertificationStatus(
                                  result);

            final var account = new AccountRetrieveAll()
                                  .setD0(
                                    encode(
                                      Bytes.toString(
                                        result.getRow())))
                                  .setD1(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      ACCOUNT_D2))
                                  .setD2(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D1))
                                  .setD3(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D2,
                                      version,
                                      certificationTypes))
                                  .setD4(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D3))
                                  .setD5(
                                    getDateString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D4,
                                      timeZone,
                                      lang))
                                  .setD6(
                                    getDateString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D5,
                                      timeZone,
                                      lang))
                                  .setD17(
                                    getFloat(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D19))
                                  .setD18(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D1,
                                      version))
                                  .setD19(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D2,
                                      origins))
                                  .setD20(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D4))
                                  .setD21(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D5))
                                  .setD22(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D7,
                                      economicActivities))
                                  .setD23(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B4_D2,
                                      countries))
                                  .setD34(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D6,
                                      legalEntities))
                                  .setD35(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D30))
                                  .setD36(
                                    getDateString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      ACCOUNT_D1,
                                      timeZone,
                                      lang));

            if (MANAGER == findUserRole(roleCode) &&
                isNotBlankString(
                  getString(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  CERTIFICATION_D25))) {

              final var userResult = retrieveAssignedControlDesk(
                                       getString(
                                         result,
                                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                         CERTIFICATION_D25));

              if (null != userResult.getRow()) {

                account
                  .setD37(
                    getString(
                      userResult,
                      USERS_COLUMN_FAMILY_DEFAULT,
                      USER_D5))
                  .setD38(
                    getString(
                      userResult,
                      USERS_COLUMN_FAMILY_DEFAULT,
                      USER_D6))
                  .setD39(
                    getString(
                      userResult,
                      USERS_COLUMN_FAMILY_DEFAULT,
                      USER_D7));

              }

            }

            return
              account;

          })
        .collect(
          toList());

  }

  /**
   * Retrieve accounts with multiple qualifiers from an HBase table.
   *
   * @param resultScanner The current connection result to extract multiple values.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   * @param roleCode Role unique identifier.
   *
   * @return A list of the type {@link AccountRetrieveAll}
   */
  private static List<AccountRetrieveAll> retrieveAccounts4DisclaimerStage(final ResultScanner resultScanner,
                                                                           final String        timeZone,
                                                                           final String        lang,
                                                                           final String        roleCode) {

    final var certificationTypes = retrieveCatalogMap(
                                     CERTIFICATION_TYPES,
                                     lang);

    final var origins = retrieveCatalogMap(
                          ORIGINS,
                          lang);

    final var economicActivities = retrieveCatalogMap(
                                     ECONOMIC_ACTIVITIES,
                                     lang);

    final var countries = retrieveCatalogMap(
                            COUNTRIES,
                            lang);

    return
      streamParallel4ResultScanner(
          resultScanner)
        .map(
          (result) -> {

            final var version = retrieveVersion4PostCertificationStatus(
                                  result);

            final var account = new AccountRetrieveAll()
                                  .setD0(
                                    encode(
                                      Bytes.toString(
                                        result.getRow())))
                                  .setD1(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      ACCOUNT_D2))
                                  .setD2(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D1))
                                  .setD3(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D2,
                                      version,
                                      certificationTypes))
                                  .setD4(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D3))
                                  .setD5(
                                    getDateString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D4,
                                      timeZone,
                                      lang))
                                  .setD6(
                                    getDateString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D5,
                                      timeZone,
                                      lang))
                                  .setD17(
                                    getFloat(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D19))
                                  .setD18(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D1,
                                      version))
                                  .setD19(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D2,
                                      origins))
                                  .setD20(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D4))
                                  .setD21(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D5))
                                  .setD22(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D7,
                                      economicActivities))
                                  .setD23(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B4_D2,
                                      countries))
                                  .setD35(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D30))
                                  .setD36(
                                    getDateString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      ACCOUNT_D1,
                                      timeZone,
                                      lang));

            if (MANAGER == findUserRole(roleCode) &&
                isNotBlankString(
                  getString(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  CERTIFICATION_D26))) {

              final var userResult = retrieveAssignedControlDesk(
                                       getString(
                                         result,
                                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                         CERTIFICATION_D26));

              if (null != userResult.getRow()) {

                account
                  .setD37(
                    getString(
                      userResult,
                      USERS_COLUMN_FAMILY_DEFAULT,
                      USER_D5))
                  .setD38(
                    getString(
                      userResult,
                      USERS_COLUMN_FAMILY_DEFAULT,
                      USER_D6))
                  .setD39(
                    getString(
                      userResult,
                      USERS_COLUMN_FAMILY_DEFAULT,
                      USER_D7));

              }

            }

            return
              account;

          })
        .sorted(
          reverseOrder(
            comparing(
              accountRetrieveAll -> isNotBlankCatalogItem(
                                        accountRetrieveAll
                                          .getD35())
                                      ? accountRetrieveAll
                                          .getD35()
                                          .getD1()
                                      : EMPTY_STRING)))
        .collect(
          toList());

  }

  /**
   * Retrieve accounts with multiple qualifiers from an HBase table for certified status.
   *
   * @param resultScanner The current connection result to extract multiple values.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return A list of the type {@link AccountRetrieveAll}
   */
  private static List<AccountRetrieveAll> retrieveAccounts4CertifiedStage(final ResultScanner resultScanner,
                                                                          final String        timeZone,
                                                                          final String        lang) {

    final var certificationTypes = retrieveCatalogMap(
                                    CERTIFICATION_TYPES,
                                    lang);

    final var origins = retrieveCatalogMap(
                          ORIGINS,
                          lang);

    final var economicActivities = retrieveCatalogMap(
                                    ECONOMIC_ACTIVITIES,
                                    lang);

    final var countries = retrieveCatalogMap(
                            COUNTRIES,
                            lang);

    return
      streamParallel4ResultScanner(resultScanner)
        .map(
          result -> new AccountRetrieveAll()
                      .setD0(
                        encode(
                          Bytes.toString(
                            result.getRow())))
                      .setD1(
                        getCatalogItem(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          ACCOUNT_D2))
                      .setD2(
                        getString(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          CERTIFICATION_D1))
                      .setD3(
                        getCatalogItem(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          CERTIFICATION_D2,
                          certificationTypes))
                      .setD4(
                        getCatalogItem(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          CERTIFICATION_D3))
                      .setD5(
                        getDateString(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          CERTIFICATION_D4,
                          timeZone,
                          lang))
                      .setD6(
                        getDateString(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          CERTIFICATION_D5,
                          timeZone,
                          lang))
                      .setD17(
                        getFloat(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          CERTIFICATION_D19))
                      .setD18(
                        getString(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          S1_B1_D1))
                      .setD19(
                        getCatalogItem(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          S1_B1_D2,
                          origins))
                      .setD20(
                        getString(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          S1_B1_D4))
                      .setD21(
                        getString(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          S1_B1_D5))
                      .setD22(
                        getCatalogItem(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          S1_B1_D7,
                          economicActivities))
                      .setD23(
                        getCatalogItem(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          S1_B4_D2,
                          countries))
                      .setD35(
                        getCatalogItem(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          CERTIFICATION_D30))
                      .setD36(
                        getDateString(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          ACCOUNT_D1,
                          timeZone,
                          lang)))
        .filter(
          account -> null != account.getD5())
        .sorted(
          comparing(
            AccountRetrieveAll::getD5))
        .collect(
          toList());

  }

  /**
   * Fill the {@link Put} with the values for the <strong>section 1</strong> from the <strong>sign up</strong>.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param result Instance of the type {@link Result} with the values from the sign up.
   */
  private static void fillSection1(final Put    put,
                                   final Result result) {

    putValue(
      put,
      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
      S1_B1_D1,
      getString(
        result,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        SIGNUP_D1));

    putValue(
      put,
      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
      S1_B1_D2,
      getString(
        result,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        SIGNUP_D2));

    putValue(
      put,
      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
      S1_B1_D4,
      getString(
        result,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        SIGNUP_D3));

    putValue(
      put,
      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
      S1_B1_D5,
      getString(
        result,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        SIGNUP_D4));

    putValue(
      put,
      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
      S1_B1_D6,
      getString(
        result,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        SIGNUP_D5));

  }

  /**
   * Retrieve the last fiscal year.
   *
   * @param result Instance of the type {@link Result} with the qualifiers.
   *
   * @return {@link Integer} with the last fiscal year.
   */
  private static Integer retrieveLastFiscalYear(final Result result) {

    return
    intStreamRange(
        result,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_B1_D1)
      .filter(
        index -> isNotDeletedListItem(
                   result,
                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                   S5_B1_D21,
                   index))
      .map(
        index -> getInteger(
                   result,
                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                   qualifierNameOneIndex(
                     S5_B1_D2,
                     index)))
      .max()
      .orElse(
        EMPTY_1N_SIZE);

  }

  /**
   * Retrieve the current stage based on the accountId
   *
   * @param accountId Account's unique identifier.
   * @param roleCode  Role of the user signed.
   * @param status Instance of the type {@link CatalogItem} with the values from the current status account.
   * @param informationStatus Instance of the type {@link CatalogItem} from the current information status.
   *
   * @return The current stage as String value of the account.
   */
  public static String findStageByAccountId(final String  accountId,
                                            final String roleCode,
                                            final CatalogItem status,
                                            final CatalogItem informationStatus) {

    switch (findAccountStatus(status.getD1())) {

      case PAYMENT:
      case CHECKING_PAYMENT:

        return
          retrieveAllPayments(accountId,
                              roleCode)
            .stream()
            .map(PaymentRetrieveAll::getS1)
            .map(CatalogItem::getD1)
            .filter(s -> TO_CHECK.name().equalsIgnoreCase(s))
            .findFirst()
            .map(s -> PAYMENTS_GRAY.name())
            .orElse(PAYMENTS_RED.name());

      case PAYMENT_VALIDATED:
      case INFORMATION:

        return PAYMENTS_GREEN.name();

      case CHECKING_INFORMATION:

        if (null == informationStatus) {

          return INFORMATIONS_GRAY.name();

        }

        switch (findInformationStatus(
                  informationStatus
                    .getD1())) {

          case FAIL:

            return INFORMATIONS_RED.name();

          case ADJUSTMENT:

            return INFORMATIONS_YELLOW.name();

          default:

        }

        return INFORMATIONS_GRAY.name();

      case INFORMATION_VALIDATED:

        return INFORMATIONS_GREEN.name();

      case CHECKING_DISCLAIMER:

        return DISCLAIMERS_GRAY.name();

      case DISCLAIMER_REJECTED:

        return DISCLAIMERS_RED.name();

      case CERTIFIED:

        return DISCLAIMERS_GREEN.name();

      case SIGN_UP:
      case PLAN:
      case DISCLAIMER:
      default:

        return NULL.name();

    }

  }

  /**
   * Retrieve the current matches stage based on the accountId
   *
   * @param status Instance of the type {@link CatalogItem} with the values from the current status account.
   * @param matchesStatus Instance of the type {@link CatalogItem} with the values from the current matches status.
   *
   * @return The current stage as String value of the matches process.
   */
  public static String findMatchesStageByStatus(final CatalogItem status,
                                                final CatalogItem matchesStatus) {

    if (!CHECKING_INFORMATION.equals(
          findAccountStatus(
            status.getD1())) ||
        null == matchesStatus ||
        null == matchesStatus.getD1()) {

      return NULL.name();

    }

    switch (findCertificationMatchesStatus(
              matchesStatus
                .getD1())) {

      case MATCHES_TO_CHECK:

        return MATCHES_GRAY.name();

      case MATCHES_ADJUSTMENTS:

        return MATCHES_YELLOW.name();

      default:

    }

    return MATCHES_GREEN.name();

  }

  /**
   * Retrieve if the change plan is available.
   *
   * @param result Instance of the type {@link Result} with the values from the sign up.
   *
   * @return Returns <strong>false</strong> if the conditions apply, <strong>true</strong> in other case.
   */
  private static Boolean isChangePlanAvailable(final Result result) {

    final var isLatestPlan = getBoolean(
                               result,
                               ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                               ACCOUNT_D15);

    final var certificationStatus = findCertificationStatus(
                                      getString(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        CERTIFICATION_D30));

    final var certificationPlan = getCatalogItem(
                                    result,
                                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                    CERTIFICATION_D3);

    if (UPDATE == certificationStatus ||
        UPGRADE == certificationStatus ||
        STD == findCertificationType(
                 getString(
                   result,
                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                   CERTIFICATION_D2))) {

      return false;

    }

    if (null == isLatestPlan &&
        null != certificationPlan) {

      return
        !isLatestPlan(
          certificationPlan);

    }

    return
      null != isLatestPlan &&
      !isLatestPlan;

  }

  /**
   * Retrieve the last payment id.
   *
   * @param result Instance of the type {@link Result} with the qualifiers and values.
   *
   * @return The payment capture date.
   */
  private static Integer retrieveLastPaymentId(final Result result) {

    return
      rangeClosed(
          FIRST_ELEMENT_INDEX,
          FIVE_ELEMENT_INDEX)
        .filter(
          index -> PaymentStatus.SUCCESS == findPaymentStatus(
                                              getString(
                                                result,
                                                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                qualifierNameOneIndex(
                                                  PY_S1,
                                                  index))) &&
                    isNotDeletedAndConfirmed(
                      result,
                      index))
        .max()
        .orElse(
          EMPTY_1N_SIZE);

  }

}
