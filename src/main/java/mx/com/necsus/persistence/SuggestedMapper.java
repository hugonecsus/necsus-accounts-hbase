package mx.com.necsus.persistence;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D11;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D13;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D14;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.Filters.createSingleColumnValueFilter;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.SearchesMapper.PUNCTUATION_MARKS_REGEX;
import static mx.com.necsus.persistence.SearchesMapper.hasKeyword;
import static mx.com.necsus.persistence.SearchesMapper.isExistingBuyerAssociation;
import static mx.com.necsus.persistence.SearchesMapper.retrieveProductsCatalog;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D5;
import static mx.com.necsus.persistence.SuggestedValuesMapper.findSuggestedValue;
import static mx.com.necsus.persistence.TypesHandler.getBoolean;
import static mx.com.necsus.persistence.TypesHandler.getString;
import static mx.com.necsus.persistence.TypesHandler.streamParallel4ResultScanner;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.Strings.EMPTY_STRING;
import static org.apache.hadoop.hbase.CompareOperator.EQUAL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ALL;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.MultipleColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

import mx.com.necsus.domain.CatalogItem;

/**
 * Persistence layer for the <strong>Suggested</strong>.
 */
public final class SuggestedMapper {

  /**
   * {@link SingleColumnValueFilter} for the {@link AccountQualifiers#ACCOUNT_D11} to retrieve in {@link #retrieveSuggested}
   */
  private static final SingleColumnValueFilter FILTER_4_IS_SEARCHABLE = createSingleColumnValueFilter(
                                                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                          ACCOUNT_D11,
                                                                          EQUAL,
                                                                          TRUE);

  /**
   * Column prefixes to retrieve in {@link #retrieveSuggested4StdC2}
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_STD_C2 = new MultipleColumnPrefixFilter(
                                                                        new byte[][] {
                                                                          ACCOUNT_D11,
                                                                          ACCOUNT_D13,
                                                                          ACCOUNT_D14,
                                                                          S1_B1_D1,
                                                                          S1_B1_D4,
                                                                          S1_B1_D5});


  /**
   * Private explicit default constructor for security.
   */
  private SuggestedMapper() {
  }

  /**
   * Retrieve the suggested list.
   *
   * @param keyword The keyword to search suggested.
   * @param criteria The criteria to identify the type of search.
   * @param lang The language to retrieve catalog item.
   * @param buyerId Buyer's unique identifier.
   *
   * @return A list of the type {@link String}.
   */
  public static List<CatalogItem> retrieveSuggested(final String keyword,
                                                    final String criteria,
                                                    final String buyerId,
                                                    final String lang) {

    switch (findSuggestedValue(criteria)) {

      case STD_C1:

        return
          retrieveProductsCatalog(
            keyword,
            lang);

      case STD_C2:

        return
          retrieveSuggested4StdC2(
            keyword
              .toLowerCase(),
            buyerId);

      default:

        return
          emptyList();

    }


  }

  /**
   * Retrieve suggested for business name, RFC and TIN.
   *
   * @param keyword The keyword to search suggested.
   * @param buyerId Buyer's unique identifier.
   *
   * @return A list of the type {@link CatalogItem}.
   */
  private static List<CatalogItem> retrieveSuggested4StdC2(final String keyword,
                                                           final String buyerId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      try (final var resultScanner = table
                                       .getScanner(
                                         new Scan()
                                           .addFamily(
                                             ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                           .setFilter(
                                             new FilterList(
                                               MUST_PASS_ALL,
                                               FILTER_4_IS_SEARCHABLE,
                                               PREFIXES_4_STD_C2)))) {

        return
          streamParallel4ResultScanner(resultScanner)
            .filter(
              (result) -> {

                final var isAccountEligible = new AtomicBoolean(TRUE);

                final var businessName = getString(
                                           result,
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                           S1_B1_D1);

                final var rfc = getString(
                                  result,
                                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                  S1_B1_D4);

                final var tin = getString(
                                  result,
                                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                  S1_B1_D5);

                if (hasKeyword(
                      isNotBlankString(businessName)
                        ? businessName
                            .replaceAll(
                              PUNCTUATION_MARKS_REGEX,
                              EMPTY_STRING)
                        : businessName,
                      keyword) ||
                    hasKeyword(
                      isNotBlankString(rfc)
                        ? rfc
                            .replaceAll(
                              PUNCTUATION_MARKS_REGEX,
                              EMPTY_STRING)
                        : rfc,
                      keyword) ||
                    hasKeyword(
                      isNotBlankString(tin)
                        ? tin
                            .replaceAll(
                              PUNCTUATION_MARKS_REGEX,
                              EMPTY_STRING)
                        : tin,
                      keyword)) {

                  isAccountEligible
                    .compareAndSet(
                      TRUE,
                      isAccountReachable(
                        result,
                        buyerId));

                } else {

                  isAccountEligible
                    .compareAndSet(
                      TRUE,
                      FALSE);

                }

                return
                  isAccountEligible.get();

              })
            .map(
              result -> new CatalogItem()
                          .setD1(
                            Bytes
                              .toString(
                                result
                                  .getRow()))
                          .setD2(
                            getString(
                              result,
                              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                              S1_B1_D1)))
            .collect(
              toList());

      }

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Validate if the account is reachable.
   *
   * @param result Instance of the type {@link Result} with the qualifiers.
   * @param buyerId Buyer's unique identifier.arched.
   *
   * @return Returns <strong>true</strong> if the account is reachable, otherwise <strong>false</strong>.
   */
  private static boolean isAccountReachable(final Result result,
                                            final String buyerId) {

    if (FALSE
          .equals(
            getBoolean(
              result,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              ACCOUNT_D13))) {

      return
        isExistingBuyerAssociation(
          Bytes
            .toString(
              result
                .getRow()),
          buyerId);

    }

    return TRUE;

  }

}
