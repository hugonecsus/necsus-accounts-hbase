package mx.com.necsus.persistence;

import static java.lang.Boolean.FALSE;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static mx.com.necsus.domain.catalog.BuyerAssociationStatus.ASSOCIATED;
import static mx.com.necsus.domain.catalog.BuyerAssociationStatus.TO_ASSOCIATE;
import static mx.com.necsus.domain.catalog.BuyerStatus.ENABLE;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.BuyerAssociationQualifiers.BUYER_ASSOCIATION_D1;
import static mx.com.necsus.persistence.BuyerAssociationQualifiers.BUYER_ASSOCIATION_D2;
import static mx.com.necsus.persistence.BuyerQualifiers.BUYER_D1;
import static mx.com.necsus.persistence.BuyerQualifiers.BUYER_D15;
import static mx.com.necsus.persistence.BuyerQualifiers.BUYER_D2;
import static mx.com.necsus.persistence.BuyersTable.BUYERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.BuyersTable.BUYERS_TABLE_NAME;
import static mx.com.necsus.persistence.Filters.createSingleColumnValueFilter;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.TypesHandler.getDateString;
import static mx.com.necsus.persistence.TypesHandler.getString;
import static mx.com.necsus.persistence.TypesHandler.newGet;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.TypesHandler.stream4ResultScanner;
import static mx.com.necsus.persistence.TypesHandler.streamParallel4ResultScanner;
import static mx.com.necsus.persistence.UserQualifiers.USER_D16;
import static mx.com.necsus.persistence.UserQualifiers.USER_D2;
import static mx.com.necsus.persistence.UserQualifiers.USER_D4;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.UsersTable.USERS_TABLE_NAME;
import static mx.com.necsus.util.AccessCrypto.decode;
import static mx.com.necsus.util.AccountsStrings.LOG_BUYER_ASSOCIATION_ID_FOUND;
import static mx.com.necsus.util.AccountsStrings.LOG_BUYER_ID_FOUND;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.CompareOperator.EQUAL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ALL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ONE;
import static org.apache.logging.log4j.LogManager.getLogger;

import java.util.List;

import mx.com.necsus.domain.BuyerAssociationUpdateOne;
import mx.com.necsus.persistence.domain.Buyer;
import mx.com.necsus.persistence.domain.BuyerAssociation;

import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.MultipleColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.RegexStringComparator;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.logging.log4j.Logger;

/**
 * Persistence layer for the <strong>buyers associated</strong>.
 */
public final class BuyersAssociationMapper {

  /**
   * Logger for the mapper.
   */
  private static final Logger LOGGER = getLogger(BuyersAssociationMapper.class);

  /**
   * Column prefixes to retrieve in {@link #retrieveBuyers}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_BUYERS = new MultipleColumnPrefixFilter(
                                                                        new byte[][] {
                                                                          BUYER_D1,
                                                                          BUYER_D15,
                                                                          BUYER_D2});

  /**
   * Column prefixes to retrieve in {@link #retrieveAssociations}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_BUYERS_ASSOCIATION = new MultipleColumnPrefixFilter(
                                                                                    new byte[][] {
                                                                                      BUYER_ASSOCIATION_D1,
                                                                                      BUYER_ASSOCIATION_D2});

  /**
   * {@link SingleColumnValueFilter} for the {@link BuyerQualifiers#BUYER_D15} to retrieve in {@link #retrieveBuyers}
   */
  private static final SingleColumnValueFilter FILTER_4_IS_BUYER_ENABLE = createSingleColumnValueFilter(
                                                                            BUYERS_COLUMN_FAMILY_DEFAULT,
                                                                            BUYER_D15,
                                                                            EQUAL,
                                                                            ENABLE.name());

  /**
   * Column prefixes to retrieve in {@link #retrieveUsersAssociated4Buyer}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_USERS_ASSOCIATED_BUYER = new MultipleColumnPrefixFilter(
                                                                                        new byte[][] {
                                                                                          USER_D2,
                                                                                          USER_D4,
                                                                                          USER_D16});

  /**
   * {@link SingleColumnValueFilter} for the {@link UserQualifiers#USER_D2} to retrieve
   * in {@link #retrieveUsersAssociated4Buyer}
   */
  private static final SingleColumnValueFilter FILTER_4_USER_STATUS = createSingleColumnValueFilter(
                                                                        USERS_COLUMN_FAMILY_DEFAULT,
                                                                        USER_D2,
                                                                        EQUAL,
                                                                        ENABLE.name());

  /**
   * {@link SingleColumnValueFilter} for the {@link UserQualifiers#USER_D4} to retrieve
   * in {@link #retrieveUsersAssociated4Buyer}
   */
  private static final SingleColumnValueFilter FILTER_4_USER_ROLE = createSingleColumnValueFilter(
                                                                      USERS_COLUMN_FAMILY_DEFAULT,
                                                                      USER_D4,
                                                                      EQUAL,
                                                                      BYR_OWNER.name());

  /**
   * {@link FilterList} for the for the {@link BuyerAssociationQualifiers#BUYER_ASSOCIATION_D1} to retrieve
   * in {@link #isExistingBuyerAssociation}
   */
  private static final FilterList FILTER_4_BUYER_ASSOCIATION_STATUS = new FilterList(
                                                                        MUST_PASS_ONE,
                                                                        createSingleColumnValueFilter(
                                                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                          BUYER_ASSOCIATION_D1,
                                                                          EQUAL,
                                                                          ASSOCIATED.name()),
                                                                        createSingleColumnValueFilter(
                                                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                          BUYER_ASSOCIATION_D1,
                                                                          EQUAL,
                                                                          TO_ASSOCIATE.name()));

  /**
   * Buyer association row key pattern.
   */
  private static final String BUYER_ASSOCIATION_ROW_KEY_PATTERN = "%s%s";

  /**
   * Private explicit default constructor for security.
   */
  private BuyersAssociationMapper() {
  }

  /**
   * Search a buyer by unique identifier.
   *
   * @param buyerId Buyer's unique identifier.
   *
   * @return <strong>true</strong> if the buyer does not exist, <strong>false</strong> in other case.
   */
  public static boolean isNotExistingBuyer(final String buyerId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               BUYERS_TABLE_NAME)) {

      final var rowKey = table
                           .get(
                             newGet(
                               buyerId))
                           .getRow();

      LOGGER.info(
        LOG_BUYER_ID_FOUND,
        null == rowKey
          ? null
          : Bytes
          .toString(
            rowKey));

      return
        null == rowKey;

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve all buyers.
   *
   * @return A list of the type {@link Buyer}.
   */
  public static List<Buyer> retrieveBuyers() {

    try (final var table = H_BASE
                            .getInstance()
                            .getTable(
                              BUYERS_TABLE_NAME);

         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           BUYERS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           new FilterList(
                                             MUST_PASS_ALL,
                                             PREFIXES_4_BUYERS,
                                             FILTER_4_IS_BUYER_ENABLE)))) {

      return
        stream4ResultScanner(
          resultScanner)
          .map(
            result -> new Buyer()
                        .setRowKey(
                          Bytes
                            .toString(
                              result.getRow()))
                        .setBusinessName(
                          getString(
                            result,
                            BUYERS_COLUMN_FAMILY_DEFAULT,
                            BUYER_D1)))
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve all buyers' association.
   *
   * @param accountId Account's unique identifier.
   * @param lang Language used in the application.
   *
   * @return A list of the type {@link BuyerAssociation}.
   */
  public static List<BuyerAssociation> retrieveAssociations(final String accountId,
                                                            final String lang) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               BUYERS_TABLE_NAME);

         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           BUYERS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           new FilterList(
                                             MUST_PASS_ALL,
                                             PREFIXES_4_BUYERS_ASSOCIATION,
                                             new RowFilter(
                                               EQUAL,
                                               new RegexStringComparator(
                                                 accountId)))))) {

      return
        stream4ResultScanner(
          resultScanner)
          .map(
            result -> new BuyerAssociation()
                        .setRowKey(
                          Bytes
                            .toString(
                              result.getRow()))
                        .setAssociationStatusCode(
                          getString(
                            result,
                            BUYERS_COLUMN_FAMILY_DEFAULT,
                            BUYER_ASSOCIATION_D1))
                        .setAssociationDate(
                          getDateString(
                            result,
                            BUYERS_COLUMN_FAMILY_DEFAULT,
                            BUYER_ASSOCIATION_D2,
                            lang)))
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Update the <strong>status</strong> of the association.
   *
   * @param accountId Account's unique identifier.
   * @param buyerAssociationUpdate Instance of the type {@link BuyerAssociationUpdateOne} with the data to put.
   */
  public static void updateBuyerAssociation(final String accountId,
                                            final BuyerAssociationUpdateOne buyerAssociationUpdate) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               BUYERS_TABLE_NAME)) {

      final var put = newPut(
                        format(
                          BUYER_ASSOCIATION_ROW_KEY_PATTERN,
                          decode(
                            buyerAssociationUpdate
                              .getD0()),
                          accountId));

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        BUYER_ASSOCIATION_D1,
        buyerAssociationUpdate
          .getD1()
            .getD1());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        BUYER_ASSOCIATION_D2,
        nowEpochMilli());

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Valide if the account has any association to buyers.
   *
   * @param accountId Account's unique identifier.
   *
   * @return <strong>true</strong> if the account has any association to buyers, <strong>false</strong> in other case.
   */
  public static Boolean hasAnyAssociation2Buyers (String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               BUYERS_TABLE_NAME);
         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           BUYERS_COLUMN_FAMILY_DEFAULT)
                                         .addColumn(
                                           BUYERS_COLUMN_FAMILY_DEFAULT,
                                           BUYER_ASSOCIATION_D1)
                                         .setFilter(
                                           new FilterList(
                                             MUST_PASS_ALL,
                                             FILTER_4_BUYER_ASSOCIATION_STATUS,
                                             new RowFilter(
                                               EQUAL,
                                               new RegexStringComparator(
                                                 accountId)))))) {

      if (null == resultScanner) {

        return FALSE;

      }

      final var associations = streamParallel4ResultScanner(
                                   resultScanner)
                                 .map(
                                   result -> getString(
                                               result,
                                               BUYERS_COLUMN_FAMILY_DEFAULT,
                                               BUYER_ASSOCIATION_D1))
                                 .collect(
                                   toList());

      return
        !associations.isEmpty();

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve one buyer's association.
   *
   * @param buyerId Buyer's unique identifier.
   *
   * @return A list of the type {@link String}.
   */
  public static List<String> retrieveUsersAssociated4Buyer(final String buyerId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               USERS_TABLE_NAME);

         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           USERS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           new FilterList(
                                             MUST_PASS_ALL,
                                             PREFIXES_4_USERS_ASSOCIATED_BUYER,
                                             FILTER_4_USER_STATUS,
                                             FILTER_4_USER_ROLE,
                                             createSingleColumnValueFilter(
                                               USERS_COLUMN_FAMILY_DEFAULT,
                                               USER_D16,
                                               EQUAL,
                                               buyerId))))) {

      return
        streamParallel4ResultScanner(
          resultScanner)
          .map(
            result -> Bytes
                        .toString(
                          result
                            .getRow()))
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Search a buyer association by unique identifier.
   *
   * @param accountId Account's unique identifier.
   * @param buyerId Buyer's unique identifier.
   *
   * @return <strong>true</strong> if the buyer association exist, <strong>false</strong> in other case.
   */
  private static boolean isExistingBuyerAssociation(final String accountId,
                                                    final String buyerId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               BUYERS_TABLE_NAME)) {

      final var rowKey = table
                           .get(
                             newGet(
                               format(
                                 BUYER_ASSOCIATION_ROW_KEY_PATTERN,
                                 buyerId,
                                 accountId))
                               .addFamily(
                                 BUYERS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 new FilterList(
                                   MUST_PASS_ALL,
                                   PREFIXES_4_BUYERS_ASSOCIATION,
                                   FILTER_4_BUYER_ASSOCIATION_STATUS)))
                           .getRow();

      final var buyerAssociationId = Bytes
                                       .toString(
                                         rowKey);

      LOGGER.info(
        LOG_BUYER_ASSOCIATION_ID_FOUND,
        null == rowKey
          ? null
          : buyerAssociationId);

      return
        null != rowKey;

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

}
