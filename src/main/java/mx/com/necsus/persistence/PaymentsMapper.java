package mx.com.necsus.persistence;

import static java.lang.Boolean.TRUE;
import static java.util.Collections.emptyList;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.rangeClosed;
import static mx.com.necsus.domain.catalog.AccountStatus.CERTIFIED;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_DISCLAIMER;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_INFORMATION;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_PAYMENT;
import static mx.com.necsus.domain.catalog.AccountStatus.DISCLAIMER;
import static mx.com.necsus.domain.catalog.AccountStatus.DISCLAIMER_REJECTED;
import static mx.com.necsus.domain.catalog.AccountStatus.INFORMATION;
import static mx.com.necsus.domain.catalog.AccountStatus.INFORMATION_VALIDATED;
import static mx.com.necsus.domain.catalog.AccountStatus.PAYMENT;
import static mx.com.necsus.domain.catalog.AccountStatus.PAYMENT_VALIDATED;
import static mx.com.necsus.domain.catalog.CatalogCodes.CERTIFICATION_TYPES;
import static mx.com.necsus.domain.catalog.CatalogCodes.ORIGINS;
import static mx.com.necsus.domain.catalog.PaymentStatus.FAIL;
import static mx.com.necsus.domain.catalog.PaymentStatus.NULL;
import static mx.com.necsus.domain.catalog.PaymentStatus.SUCCESS;
import static mx.com.necsus.domain.catalog.PaymentStatus.TO_CHECK;
import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D2;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D3;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D19;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D2;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D30;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D4;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D5;
import static mx.com.necsus.persistence.Filters.createSingleColumnValueFilter;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.PaymentQualifiers.PAYMENT_PREFIX;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D1;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D2;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D3;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D5;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_F1;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_F2;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_F3;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_F4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N1;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N2;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N3;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_N4;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_S1;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_S2;
import static mx.com.necsus.persistence.PaymentStatusMapper.findPaymentStatus;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameTwoIndex;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D5;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D7;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B4_D2;
import static mx.com.necsus.persistence.StagesMapper.findStage;
import static mx.com.necsus.persistence.TypesHandler.FIRST_ELEMENT_INDEX;
import static mx.com.necsus.persistence.TypesHandler.get1nSize;
import static mx.com.necsus.persistence.TypesHandler.getBoolean;
import static mx.com.necsus.persistence.TypesHandler.getCatalogItem;
import static mx.com.necsus.persistence.TypesHandler.getDateString;
import static mx.com.necsus.persistence.TypesHandler.getDateTimeString;
import static mx.com.necsus.persistence.TypesHandler.getLong;
import static mx.com.necsus.persistence.TypesHandler.getString;
import static mx.com.necsus.persistence.TypesHandler.intStreamRange;
import static mx.com.necsus.persistence.TypesHandler.newGet;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.TypesHandler.streamParallel4ResultScanner;
import static mx.com.necsus.persistence.UserQualifiers.USER_D5;
import static mx.com.necsus.persistence.UserQualifiers.USER_D6;
import static mx.com.necsus.persistence.UserQualifiers.USER_D7;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.persistence.UsersMapper.retrieveAssignedControlDesk;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.util.AccessCrypto.encode;
import static mx.com.necsus.util.Catalogs.retrieveCatalogMap;
import static mx.com.necsus.util.RequestBody.isNotBlankCollection;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static org.apache.hadoop.hbase.CompareOperator.EQUAL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ALL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ONE;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;
import static org.apache.logging.log4j.LogManager.getLogger;

import java.util.List;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.ColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.MultipleColumnPrefixFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.logging.log4j.Logger;

import mx.com.necsus.domain.AccountRetrieveAll;
import mx.com.necsus.domain.Note;
import mx.com.necsus.domain.PaymentRetrieveAll;
import mx.com.necsus.domain.PaymentUpdateOne;
import mx.com.necsus.domain.catalog.AccountStatus;
import mx.com.necsus.domain.catalog.Stages;
import mx.com.necsus.persistence.domain.Payment;

/**
 * Persistence layer for the <strong>Payment</strong>.
 */
public final class PaymentsMapper {

  /**
   * Logger for the mapper.
   */
  private static final Logger LOGGER = getLogger(PaymentsMapper.class);

  /**
   * Column prefixes to retrieve in {@link #retrieveAccounts4PaymentStage}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_CHECKING_PAYMENT = new MultipleColumnPrefixFilter(
                                                                                  new byte[][] {
                                                                                    ACCOUNT_D1,
                                                                                    ACCOUNT_D2,
                                                                                    CERTIFICATION_D2,
                                                                                    CERTIFICATION_D4,
                                                                                    CERTIFICATION_D5,
                                                                                    CERTIFICATION_D19,
                                                                                    CERTIFICATION_D30,
                                                                                    S1_B1_D1,
                                                                                    S1_B1_D2,
                                                                                    S1_B1_D4,
                                                                                    S1_B1_D5,
                                                                                    S1_B1_D7,
                                                                                    S1_B4_D2,
                                                                                    "pyd".getBytes(),
                                                                                    "pys".getBytes()});

  /**
   * Column prefixes to retrieve in {@link #updatePaymentValidation}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_PAYMENT = new MultipleColumnPrefixFilter(
                                                                         new byte[][] {
                                                                           "pyd".getBytes(),
                                                                           "pyn1".getBytes(),
                                                                           "pys1".getBytes()});

  /**
   * {@link FilterList} for the {@link AccountStatus} to retrieve in {@link #retrieveAccounts4PaymentStage}
   */
  private static final FilterList FILTER_ACCOUNT_STATUS_4_CHECKING_PAYMENT = new FilterList(
                                                                               MUST_PASS_ONE,
                                                                               createSingleColumnValueFilter(
                                                                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                 ACCOUNT_D2,
                                                                                 EQUAL,
                                                                                 PAYMENT.name()),
                                                                               createSingleColumnValueFilter(
                                                                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                 ACCOUNT_D2,
                                                                                 EQUAL,
                                                                                 CHECKING_PAYMENT.name()),
                                                                               createSingleColumnValueFilter(
                                                                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                 ACCOUNT_D2,
                                                                                 EQUAL,
                                                                                 PAYMENT_VALIDATED.name()),
                                                                               createSingleColumnValueFilter(
                                                                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                 ACCOUNT_D2,
                                                                                 EQUAL,
                                                                                 INFORMATION.name()),
                                                                               createSingleColumnValueFilter(
                                                                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                 ACCOUNT_D2,
                                                                                 EQUAL,
                                                                                 CHECKING_INFORMATION.name()),
                                                                               createSingleColumnValueFilter(
                                                                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                 ACCOUNT_D2,
                                                                                 EQUAL,
                                                                                 INFORMATION_VALIDATED.name()),
                                                                               createSingleColumnValueFilter(
                                                                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                 ACCOUNT_D2,
                                                                                 EQUAL,
                                                                                 DISCLAIMER.name()),
                                                                               createSingleColumnValueFilter(
                                                                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                 ACCOUNT_D2,
                                                                                 EQUAL,
                                                                                 DISCLAIMER_REJECTED.name()),
                                                                               createSingleColumnValueFilter(
                                                                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                 ACCOUNT_D2,
                                                                                 EQUAL,
                                                                                 CHECKING_DISCLAIMER.name()),
                                                                               createSingleColumnValueFilter(
                                                                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                 ACCOUNT_D2,
                                                                                 EQUAL,
                                                                                 CERTIFIED.name()));

  /**
   * Private explicit default constructor for security.
   */
  private PaymentsMapper() {
  }

  /**
   * Retrieve the <em>metadata</em> of the payment with unique identifier <strong>paymentId</strong>.
   *
   * @param accountId Account's unique identifier.
   * @param paymentId Payment's unique identifier.
   *
   * @return Instance of the type {@link Payment} with the <em>metadata</em> found or <strong>null</strong>
   * if the payment was <strong>deleted</strong>.
   */
  public static Payment retrievePaymentFileMetadata(final String accountId,
                                                    final Integer paymentId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .addColumn(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                 qualifierNameOneIndex(
                                   PY_D4,
                                   paymentId))
                               .addColumn(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                 qualifierNameOneIndex(
                                   PY_F2,
                                   paymentId))
                               .addColumn(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                 qualifierNameOneIndex(
                                   PY_F3,
                                   paymentId))
                               .addColumn(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                 qualifierNameOneIndex(
                                   PY_S1,
                                   paymentId)));

      return
        isDeleted(
            result,
            paymentId)
          ? null
          : new Payment()
              .setF2(
                getString(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    PY_F2,
                    paymentId)))
              .setF3(
                getLong(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    PY_F3,
                    paymentId)))
              .setS1(
                getString(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    PY_S1,
                    paymentId)));

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the <em>file content</em> of the payment with unique identifier <strong>paymentId</strong>.
   *
   * @param accountId Account's unique identifier.
   * @param paymentId Payment's unique identifier.
   *
   * @return Instance of the type {@link Payment} with the <em>file content</em> found or <strong>null</strong>
   * if the payment was <strong>deleted</strong>.
   */
  public static Payment retrievePaymentFileContent(final String accountId,
                                                   final Integer paymentId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .addColumn(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                 qualifierNameOneIndex(
                                   PY_D4,
                                   paymentId))
                               .addColumn(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                 qualifierNameOneIndex(
                                   PY_F1,
                                   paymentId))
                               .addColumn(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                 qualifierNameOneIndex(
                                   PY_F2,
                                   paymentId)));

      return
        isDeleted(
            result,
            paymentId)
          ? null
          : new Payment()
              .setF1(
                result.getValue(
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    PY_F1,
                    paymentId)))
              .setF2(
                getString(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    PY_F2,
                    paymentId)));

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve all payments that <strong>was not deletes</strong>.
   *
   * @param accountId Account's unique identifier.
   * @param roleCode  Role of the user signed.
   * @param timeZone  Session's time zone.
   * @param lang      The language to retrieve catalog item.
   *
   * @return A list of the type {@link PaymentRetrieveAll}.
   */
  public static List<PaymentRetrieveAll> retrieveAllPayments(final String accountId,
                                                             final String roleCode,
                                                             final String timeZone,
                                                             final String lang) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table.get(
                                newGet(accountId)
                                  .addFamily(ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                  .setFilter(
                                    new ColumnPrefixFilter(PAYMENT_PREFIX)));

      return
        rangeClosed(0, 4)
          .parallel()
          .filter(
            index -> {

              if (ACCT_OWNER == findUserRole(roleCode)) {

                return
                  !isDeleted(
                    result,
                    index);

              }

              if (CONTROL_DESK == findUserRole(roleCode) ||
                  MANAGER == findUserRole(roleCode)) {

               return
                 isNotDeletedAndConfirmed(
                   result,
                   index);

              }

              return false;

          })
          .mapToObj(
            index -> new PaymentRetrieveAll()
                       .setD0(index)
                       .setD2(
                         getString(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           qualifierNameOneIndex(
                             PY_D2,
                             index)))
                       .setD3(
                         getString(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           qualifierNameOneIndex(
                             PY_D3,
                             index)))
                       .setS1(
                         getCatalogItem(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           qualifierNameOneIndex(
                             PY_S1,
                             index)))
                       .setN(
                         retrievePaymentNotes(
                           result,
                           index,
                           timeZone,
                           lang)))
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve all payments that <strong>was not deletes</strong>.
   *
   * @param accountId Account's unique identifier.
   * @param roleCode  Role of the user signed.
   *
   * @return A list of the type {@link PaymentRetrieveAll}.
   */
  public static List<PaymentRetrieveAll> retrieveAllPayments(final String accountId,
                                                             final String roleCode) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table.get(
        newGet(accountId)
          .addFamily(ACCOUNTS_COLUMN_FAMILY_DEFAULT)
          .setFilter(
            new ColumnPrefixFilter(PAYMENT_PREFIX)));

      return
        rangeClosed(0, 4)
          .parallel()
          .filter(
            index -> {

              if (ACCT_OWNER == findUserRole(roleCode)) {

                return
                  !isDeleted(
                    result,
                    index);

              }

              if (CONTROL_DESK == findUserRole(roleCode) ||
                MANAGER == findUserRole(roleCode)) {

                return
                  isNotDeletedAndConfirmed(
                    result,
                    index);

              }

              return false;

            })
          .mapToObj(
            index -> new PaymentRetrieveAll()
              .setD0(index)
              .setD2(
                getString(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    PY_D2,
                    index)))
              .setD3(
                getString(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    PY_D3,
                    index)))
              .setS1(
                getCatalogItem(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    PY_S1,
                    index))))
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Update the file of the payment with unique identifier <strong>paymentId</strong>.
   *
   * @param accountId Account's unique identifier.
   * @param payment   File content and metadata to update.
   */
  public static void updatePaymentFile(final String accountId,
                                       final Payment payment) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D2,
          payment.getD0()),
        payment.getD2());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D3,
          payment.getD0()),
        payment.getD3());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D4,
          payment.getD0()),
        payment.getD4());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_D5,
          payment.getD0()),
        payment.getD5());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_F1,
          payment.getD0()),
        payment.getF1());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_F2,
          payment.getD0()),
        payment.getF2());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_F3,
          payment.getD0()),
        payment.getF3());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_F4,
          payment.getD0()),
        payment.getF4());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_S1,
          payment.getD0()),
        payment.getS1());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_S2,
          payment.getD0()),
        payment.getS2());

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Search a payment by unique identifier.
   *
   * @param accountId Account's unique identifier.
   * @param paymentId Payment's unique identifier.
   *
   * @return <strong>true</strong> if the payment does not exists, <strong>false</strong> in other case.
   */
  public static boolean isNotExistingPayment(final String accountId,
                                             final Integer paymentId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      return
        isDeleted(
          table
            .get(
              newGet(
                  accountId)
                .addFamily(
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                .addColumn(
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    PY_D4,
                    paymentId))),
          paymentId);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the user's unique identifier of the approver associated to the payments.
   *
   * @param accountId Account's unique identifier.
   *
   * @return {@link String} The approver's unique identifier.
   */
  public static String retrievePaymentsApproverId(final String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table.get(
                                newGet(accountId)
                                  .addFamily(
                                    ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                  .addColumn(
                                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                    PY_D1));

      return
        getString(
          result,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          PY_D1);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Update the status of the payment with unique identifier <strong>paymentId</strong>.
   *
   * @param accountId Account's unique identifier.
   * @param paymentId Payment's unique identifier.
   * @param payment   Metadata of the payment to update.
   * @param roleCode  Role of the user signed.
   */
  public static void updatePaymentValidation(final String accountId,
                                             final Integer paymentId,
                                             final PaymentUpdateOne payment,
                                             final String roleCode) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var put = newPut(accountId);

      final var now = nowEpochMilli();

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_S1,
          paymentId),
        payment.getS1());

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        qualifierNameOneIndex(
          PY_S2,
          paymentId),
        now);

      final var result = table.get(
                                newGet(accountId)
                                  .addFamily(
                                    ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                  .setFilter(
                                    PREFIXES_4_PAYMENT));

      if (null != payment.getN2()) {

        final var size = get1nSize(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           qualifierNameOneIndex(
                             PY_N1,
                             paymentId));

        putValue(
          put,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameOneIndex(
            PY_N1,
            paymentId),
          size + 1);

        putValue(
          put,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameTwoIndex(
            PY_N2,
            paymentId,
            size),
          payment.getN2());

        putValue(
          put,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameTwoIndex(
            PY_N3,
            paymentId,
            size),
          now);

        putValue(
          put,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameTwoIndex(
            PY_N4,
            paymentId,
            size),
          roleCode);

      }

      if (isAnyPaymentRejected(
            result,
            paymentId,
            payment
              .getS1()
              .getD1())) {

        putValue(
          put,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          ACCOUNT_D2,
          PAYMENT.name());

        putValue(
          put,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          ACCOUNT_D3,
          now);

      }

      table.put(put);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve account rows by checking payment.
   *
   * @param approverId Approver's unique identifier.
   * @param stage One of {@link Stages}.
   * @param timeZone Session's time zone.
   * @param language The language to retrieve catalog item.
   * @param roleCode Role unique identifier.
   *
   * @return A list of the type {@link AccountRetrieveAll}.
   */
  public static List<AccountRetrieveAll> retrieveAccounts4PaymentStage(final String approverId,
                                                                       final String stage,
                                                                       final String timeZone,
                                                                       final String language,
                                                                       final String roleCode) {

    var paymentStatus = NULL;

    switch (findStage(stage)) {

      case PAYMENTS_GRAY:

        paymentStatus = TO_CHECK;

        break ;

      case PAYMENTS_RED:

        paymentStatus = FAIL;

        break ;

      case PAYMENTS_GREEN:

        paymentStatus = SUCCESS;

        break ;

      default:

        return emptyList();

    }

    final var filterList = new FilterList(
                             MUST_PASS_ALL,
                             PREFIXES_4_CHECKING_PAYMENT,
                             FILTER_ACCOUNT_STATUS_4_CHECKING_PAYMENT);

    if (CONTROL_DESK == findUserRole(roleCode)) {

      filterList
        .addFilter(
          createSingleColumnValueFilter(
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            PY_D1,
            EQUAL,
            approverId));

    }

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME);
         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           filterList))) {

      return
        retrieveAccounts4StagePayment(
          resultScanner,
          paymentStatus.name(),
          timeZone,
          language,
          roleCode);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Method to add columns of confirm payments.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param accountId Account's unique identifier.
   */
  public static void confirmPayments(final Put    put,
                                     final String accountId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 new ColumnPrefixFilter(
                                   "pyd4"
                                     .getBytes())));

      rangeClosed(
          FIRST_ELEMENT_INDEX,
          4)
        .filter(
          index -> !isDeleted(
                     result,
                     index))
        .forEach(
          index -> putValue(
                     put,
                     ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                     qualifierNameOneIndex(
                       PY_D5,
                       index),
                     TRUE));

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Validate if the payment <strong>is deleted </strong>.
   *
   * @param result Instance of the type {@link Result} with the values of the qualifiers.
   * @param paymentId Payment's unique identifier.
   *
   * @return Returns <strong>true</strong> if the qualifier value is <em>null</em> or the value as such.
   */
  public static boolean isDeleted(final Result  result,
                                  final Integer paymentId) {

    final var deleted = getBoolean(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          qualifierNameOneIndex(
                            PY_D4,
                            paymentId));

    return
      null == deleted
        ? true
        : deleted;

  }

  /**
   * Retrieve the notes of a payment
   *
   * @param result Instance of the type {@link Result} with the values of the qualifiers.
   * @param paymentId Payment's unique identifier.
   * @param timeZone Session's time zone.
   * @param lang The language to retrieve catalog item.
   *
   * @return Returns the notes of a payment.
   */
  public static List<Note> retrievePaymentNotes(final Result  result,
                                                final Integer paymentId,
                                                final String  timeZone,
                                                final String  lang) {

    return
      intStreamRange(
          result,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          qualifierNameOneIndex(
            PY_N1,
            paymentId))
        .parallel()
        .mapToObj(
          index -> new Note()
                     .setD2(
                       getString(
                         result,
                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                         qualifierNameTwoIndex(
                           PY_N2,
                           paymentId,
                           index)))
                     .setD3(
                       getDateTimeString(
                         result,
                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                         qualifierNameTwoIndex(
                           PY_N3,
                           paymentId,
                           index),
                         timeZone,
                         lang))
                     .setD4(
                       getCatalogItem(
                         result,
                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                         qualifierNameTwoIndex(
                           PY_N4,
                           paymentId,
                           index))))
        .collect(
          toList());

  }

  /**
   * Delete all payments.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   */
  public static void deleteAllPayments(final Put put) {

    rangeClosed(
        FIRST_ELEMENT_INDEX,
        4)
      .forEach(
        index -> putValue(
                   put,
                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                   qualifierNameOneIndex(
                     PY_D4,
                     index),
                   toBytes(
                     true)));

  }

  /**
   * Validate if the payment <strong>is not deleted and confirmed</strong>.
   *
   * @param result  Instance of the type {@link Result} with the values of the qualifiers.
   * @param paymentId Payment's unique identifier.
   *
   * @return Returns <strong>true</strong> if the qualifier value is <em>null</em> or the value as such.
   */
  public static boolean isNotDeletedAndConfirmed(final Result  result,
                                                 final Integer paymentId) {

    final var deleted = getBoolean(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          qualifierNameOneIndex(
                            PY_D4,
                            paymentId));

    final var confirmed = getBoolean(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            qualifierNameOneIndex(
                              PY_D5,
                              paymentId));

    return
      !(null != deleted && deleted) &&
      (null != confirmed && confirmed);

  }

  /**
   * Retrieve accounts with multiple qualifiers from an HBase table.
   *
   * @param resultScanner The current connection result to extract multiple values.
   * @param stage Account stage regarding payment.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   * @param roleCode Role unique identifier.
   *
   * @return A list of the type {@link AccountRetrieveAll}
   */
  private static List<AccountRetrieveAll> retrieveAccounts4StagePayment(final ResultScanner resultScanner,
                                                                        final String        stage,
                                                                        final String        timeZone,
                                                                        final String        lang,
                                                                        final String        roleCode) {

    if (null == resultScanner) {

      return emptyList();

    }

    final var origins = retrieveCatalogMap(
                          ORIGINS,
                          lang);

    final var certificationTypes = retrieveCatalogMap(
                                     CERTIFICATION_TYPES,
                                     lang);

    return
      streamParallel4ResultScanner(
          resultScanner)
        .filter(
          result -> isStageRequired(
                      result,
                      stage))
        .map(
          result -> {

            final var account = new AccountRetrieveAll()
                                  .setD0(
                                    encode(
                                      Bytes.toString(
                                        result.getRow())))
                                  .setD1(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      ACCOUNT_D2))
                                  .setD3(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D2,
                                      certificationTypes))
                                  .setD4(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D3))
                                  .setD18(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D1))
                                  .setD19(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D2,
                                      origins))
                                  .setD20(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D4))
                                  .setD21(
                                    getString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D5))
                                  .setD35(
                                    getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      CERTIFICATION_D30))
                                  .setD36(
                                    getDateString(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      ACCOUNT_D1,
                                      timeZone,
                                      lang));

            if (MANAGER == findUserRole(roleCode) &&
                isNotBlankString(
                  getString(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  PY_D1))) {

              final var userResult = retrieveAssignedControlDesk(
                                       getString(
                                         result,
                                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                         PY_D1));

              if (null != userResult.getRow()) {

                account
                  .setD37(
                    getString(
                      userResult,
                      USERS_COLUMN_FAMILY_DEFAULT,
                      USER_D5))
                  .setD38(
                    getString(
                      userResult,
                      USERS_COLUMN_FAMILY_DEFAULT,
                      USER_D6))
                  .setD39(
                    getString(
                      userResult,
                      USERS_COLUMN_FAMILY_DEFAULT,
                      USER_D7));

              }

            }

            return
              account;

          })
        .collect(
          toList());

  }

  /**
   * Method to validated if the stage is the required.
   *
   * @param result Instance of the type {@link Result} with the values of the qualifiers.
   * @param stage  The stage required.
   *
   * @return Returns <strong>true</strong> if the value of the stage is equal to the map.
   */
  private static Boolean isStageRequired(final Result result,
                                         final String stage) {

    if (LOGGER.isDebugEnabled()) {

     LOGGER.debug(
       "Account '{}'",
       Bytes
         .toString(
           result
             .getRow()));

    }

    final var stagesMap = rangeClosed(
                              FIRST_ELEMENT_INDEX,
                              4)
                            .filter(
                              index -> isNotDeletedAndConfirmed(
                                         result,
                                         index))
                            .mapToObj(
                              index -> getString(
                                         result,
                                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                         qualifierNameOneIndex(
                                           PY_S1,
                                           index)))
                            .collect(
                              groupingBy(
                                identity()));

    if (null == stagesMap) {

      return false;

    }

    final var stageEnum = findPaymentStatus(stage);

    if (null != stagesMap.get(TO_CHECK.name())) {

      return
        TO_CHECK == stageEnum;

    }

    if (null != stagesMap.get(FAIL.name())) {

      return
        FAIL == stageEnum;

    }

    if (null != stagesMap.get(SUCCESS.name())) {

        return
          SUCCESS == stageEnum;

    }

    return false;

  }

  /**
   * Method to validate if any payment is rejected.
   *
   * @param result Instance of the type {@link Result} with the values of the qualifiers.
   * @param paymentId Payment's unique identifier of the current payment.
   * @param paymentStatus The payment status of the current payment.
   *
   * @return Returns <strong>true</strong> if any of the payment is rejected.
   */
  private static boolean isAnyPaymentRejected(final Result result,
                                              final Integer paymentId,
                                              final String paymentStatus) {

    final var paymentStatusList = rangeClosed(
                                      FIRST_ELEMENT_INDEX,
                                      4)
                                    .filter(
                                      index -> isNotDeletedAndConfirmed(
                                                 result,
                                                 index))
                                    .filter(
                                      index -> index == paymentId
                                                 ? SUCCESS != findPaymentStatus(paymentStatus)
                                                 : SUCCESS != findPaymentStatus(
                                                                getString(
                                                                  result,
                                                                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                  qualifierNameOneIndex(
                                                                    PY_S1,
                                                                    index))))
                                    .mapToObj(
                                      index -> index == paymentId
                                                 ? paymentStatus
                                                 : getString(
                                                     result,
                                                     ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                     qualifierNameOneIndex(
                                                       PY_S1,
                                                       index)))
                                    .collect(
                                      toList());

    return
      isNotBlankCollection(
        paymentStatusList) &&
      paymentStatusList
        .stream()
        .allMatch(
          status -> FAIL == findPaymentStatus(status));

  }

}
