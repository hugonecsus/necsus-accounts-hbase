package mx.com.necsus.persistence;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static mx.com.necsus.domain.catalog.CatalogCodes.CERTIFICATION_TYPES;
import static mx.com.necsus.domain.catalog.CatalogCodes.COUNTRIES;
import static mx.com.necsus.domain.catalog.CatalogCodes.ECONOMIC_ACTIVITIES;
import static mx.com.necsus.domain.catalog.CatalogCodes.ORIGINS;
import static mx.com.necsus.domain.catalog.CertificationVisitStatus.NULL;
import static mx.com.necsus.domain.catalog.CertificationVisitStatus.VISIT_CREATED;
import static mx.com.necsus.domain.catalog.CertificationVisitStatus.VISIT_CREATED_FAIL;
import static mx.com.necsus.domain.catalog.CertificationVisitStatus.VISIT_TERMINATED;
import static mx.com.necsus.domain.catalog.CertificationVisitStatus.VISIT_TO_CREATE;
import static mx.com.necsus.domain.catalog.CertificationVisitStatus.VISIT_VALIDATED;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D1;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D2;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D1;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D19;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D2;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D22;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D24;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D30;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D4;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D5;
import static mx.com.necsus.persistence.Filters.createSingleColumnValueFilter;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D5;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D7;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B4_D2;
import static mx.com.necsus.persistence.StagesMapper.findStage;
import static mx.com.necsus.persistence.TypesHandler.getCatalogItem;
import static mx.com.necsus.persistence.TypesHandler.getDateString;
import static mx.com.necsus.persistence.TypesHandler.getFloat;
import static mx.com.necsus.persistence.TypesHandler.getString;
import static mx.com.necsus.persistence.TypesHandler.stream4ResultScanner;
import static mx.com.necsus.persistence.UserQualifiers.USER_D5;
import static mx.com.necsus.persistence.UserQualifiers.USER_D6;
import static mx.com.necsus.persistence.UserQualifiers.USER_D7;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.persistence.UsersMapper.retrieveAssignedControlDesk;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D1;
import static mx.com.necsus.util.AccessCrypto.encode;
import static mx.com.necsus.util.Catalogs.retrieveCatalogMap;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static org.apache.hadoop.hbase.CompareOperator.EQUAL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ALL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ONE;

import java.util.List;

import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.MultipleColumnPrefixFilter;
import org.apache.hadoop.hbase.util.Bytes;

import mx.com.necsus.domain.AccountRetrieveAll;
import mx.com.necsus.domain.catalog.Stages;

/**
 * Persistence layer for the <strong>Visit</strong>.
 */
public final class VisitsMapper {

  /**
   * Prefixes to retrieve accounts.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_CHECKING_INFORMATION = new MultipleColumnPrefixFilter(
                                                                                      new byte[][] {
                                                                                        ACCOUNT_D1,
                                                                                        ACCOUNT_D2,
                                                                                        CERTIFICATION_D1,
                                                                                        CERTIFICATION_D2,
                                                                                        CERTIFICATION_D3,
                                                                                        CERTIFICATION_D4,
                                                                                        CERTIFICATION_D5,
                                                                                        CERTIFICATION_D19,
                                                                                        CERTIFICATION_D24,
                                                                                        CERTIFICATION_D30,
                                                                                        S1_B1_D1,
                                                                                        S1_B1_D2,
                                                                                        S1_B1_D4,
                                                                                        S1_B1_D5,
                                                                                        S1_B1_D7,
                                                                                        S1_B4_D2,
                                                                                        VISIT_D1});

  /**
   * {@link FilterList} for the visit status  to retrieve in {@link #retrieveAccounts4VisitStage}
   */
  private static final FilterList FILTER_4_VISIT_STATUS = new FilterList(
                                                            MUST_PASS_ONE,
                                                            createSingleColumnValueFilter(
                                                              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                              CERTIFICATION_D22,
                                                              EQUAL,
                                                              VISIT_TO_CREATE.name()),
                                                            createSingleColumnValueFilter(
                                                              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                              CERTIFICATION_D22,
                                                              EQUAL,
                                                              VISIT_CREATED.name()));

  /**
   * Private explicit default constructor for security.
   */
  private VisitsMapper() {
  }

  /**
   * Retrieve a {@link List} of the type {@link AccountRetrieveAll} with the accounts that has at least one visit.
   *
   * @param approverId Approver's unique identifier.
   * @param stage One of {@link Stages}.
   * @param timeZone Session's time zone.
   * @param lang Language of the application.
   * @param roleCode Role unique identifier.
   *
   * @return An instance of the type {@link List} with the {@link AccountRetrieveAll}.
   */
  public static List<AccountRetrieveAll> retrieveAccounts4VisitStage(final String approverId,
                                                                     final String stage,
                                                                     final String timeZone,
                                                                     final String lang,
                                                                     final String roleCode) {

    var visitStatus = NULL;

    switch (findStage(stage)) {

      case VISITS_GRAY:

        visitStatus = VISIT_CREATED;

        break;

      case VISITS_RED:

        visitStatus = VISIT_CREATED_FAIL;

        break;

      case VISITS_YELLOW:

        visitStatus = VISIT_TERMINATED;

        break;

      case VISITS_GREEN:

        visitStatus = VISIT_VALIDATED;

        break;

      default:

        return emptyList();

    }

    final var filterList = new FilterList(
                             MUST_PASS_ALL,
                             PREFIXES_4_CHECKING_INFORMATION,
                             VISIT_CREATED == visitStatus
                               ? FILTER_4_VISIT_STATUS
                               : createSingleColumnValueFilter(
                                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                   CERTIFICATION_D22,
                                   EQUAL,
                                   visitStatus.name()));

    if (CONTROL_DESK == findUserRole(roleCode)) {

      filterList
        .addFilter(
          createSingleColumnValueFilter(
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            CERTIFICATION_D24,
            EQUAL,
            approverId));

    }

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME);
         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           filterList))) {

      final var origins = retrieveCatalogMap(
                            ORIGINS,
                            lang);

      final var certificationTypes = retrieveCatalogMap(
                                       CERTIFICATION_TYPES,
                                       lang);

      final var economicActivities = retrieveCatalogMap(
                                       ECONOMIC_ACTIVITIES,
                                       lang);

      final var countries = retrieveCatalogMap(
                              COUNTRIES,
                              lang);

      return
        stream4ResultScanner(resultScanner)
          .map(
            result -> {

              final var account = new AccountRetrieveAll()
                                    .setD0(
                                      encode(
                                        Bytes
                                          .toString(
                                            result
                                              .getRow())))
                                    .setD1(
                                      getCatalogItem(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        ACCOUNT_D2))
                                    .setD2(
                                      getString(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        CERTIFICATION_D1))
                                    .setD3(
                                      getCatalogItem(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        CERTIFICATION_D2,
                                        certificationTypes))
                                    .setD4(
                                      getCatalogItem(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        CERTIFICATION_D3))
                                    .setD5(
                                      getDateString(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        CERTIFICATION_D4,
                                        timeZone,
                                        lang))
                                    .setD6(
                                      getDateString(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        CERTIFICATION_D5,
                                        timeZone,
                                        lang))
                                    .setD17(
                                      getFloat(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        CERTIFICATION_D19))
                                    .setD18(
                                      getString(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        S1_B1_D1))
                                    .setD19(
                                      getCatalogItem(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        S1_B1_D2,
                                        origins))
                                    .setD20(
                                      getString(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        S1_B1_D4))
                                    .setD21(
                                      getString(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        S1_B1_D5))
                                    .setD22(
                                      getCatalogItem(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        S1_B1_D7,
                                        economicActivities))
                                    .setD23(
                                      getCatalogItem(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        S1_B4_D2,
                                        countries))
                                    .setD35(
                                      getCatalogItem(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        CERTIFICATION_D30))
                                    .setD36(
                                      getDateString(
                                        result,
                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                        ACCOUNT_D1,
                                        timeZone,
                                        lang));

              if (MANAGER == findUserRole(roleCode) &&
                  isNotBlankString(
                    getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D24))) {

                final var userResult = retrieveAssignedControlDesk(
                                         getString(
                                           result,
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                           CERTIFICATION_D24));

                if (null != userResult.getRow()) {

                  account
                    .setD37(
                      getString(
                        userResult,
                        USERS_COLUMN_FAMILY_DEFAULT,
                        USER_D5))
                    .setD38(
                      getString(
                        userResult,
                        USERS_COLUMN_FAMILY_DEFAULT,
                        USER_D6))
                    .setD39(
                      getString(
                        userResult,
                        USERS_COLUMN_FAMILY_DEFAULT,
                        USER_D7));

                }

              }

              return
                account;

            })
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

}
