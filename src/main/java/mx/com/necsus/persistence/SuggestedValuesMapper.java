package mx.com.necsus.persistence;

import static java.util.Arrays.stream;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toUnmodifiableMap;
import static mx.com.necsus.domain.catalog.SuggestedValues.NULL;
import static mx.com.necsus.domain.catalog.SuggestedValues.values;

import java.util.Map;

import mx.com.necsus.domain.catalog.SuggestedValues;

/**
 * <em>Mapper</em> for the <strong>suggested values</strong>.
 */
public final class SuggestedValuesMapper {

  /**
   * Fixed {@link Map} with the suggested values.
   */
  private static final Map<String, SuggestedValues> VALUES = stream(values())
                                                               .collect(
                                                                 toUnmodifiableMap(
                                                                   SuggestedValues::name,
                                                                   identity()));

  /**
   * Private explicit default constructor for security.
   */
  private SuggestedValuesMapper() {
  }

  /**
   * Find the <strong>code</strong> in {@link Map} {@link #VALUES}.
   *
   * @param code Unique identifier to suggested.
   *
   * @return The instance of {@link SuggestedValues} that matches, {@link SuggestedValues#NULL} in other case.
   */
  public static SuggestedValues findSuggestedValue(final String code) {

    return
      null == code
        ? NULL
        : ofNullable(
            VALUES.get(code))
          .orElse(
            NULL);

  }

}
