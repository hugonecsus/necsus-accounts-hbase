package mx.com.necsus.persistence.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for a buyer.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class BuyerAssociation {

  /**
   * Buyer's unique identifier.
   *
   * @param rowKey Buyer's unique identifier.
   * @return Buyer's unique identifier.
   */
  private String rowKey;

  /**
   * Buyer association's date.
   *
   * @param associationDate Buyer association's date.
   * @return Buyer association's date.
   */
  private String associationDate;

  /**
   * Buyer association status code.
   *
   * @param associationStatusCode Buyer association status code.
   * @return Buyer association status code.
   */
  private String associationStatusCode;

  /**
   * Explicit default constructor
   */
  public BuyerAssociation() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
