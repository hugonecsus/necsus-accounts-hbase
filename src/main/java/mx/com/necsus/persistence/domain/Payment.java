package mx.com.necsus.persistence.domain;

import static com.jsoniter.output.JsonStream.serialize;

import com.jsoniter.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for a payment file.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class Payment {

  /**
   * Payment's unique identifier.
   *
   * @param d0 Payment's unique identifier.
   * @return Payment's unique identifier.
   */
  private Integer d0;

  /**
   * Payment's related approver identifier.
   *
   * @param d1 Payment related approver identifier.
   * @return Payment related approver identifier.
   */
  private String d1;

  /**
   * Payment capture date.
   *
   * @param d2 Payment capture date.
   * @return Payment capture date.
   */
  private String d2;

  /**
   * Payment's capture reference.
   *
   * @param d3 Payment capture reference.
   * @return Payment capture reference.
   */
  private String d3;

  /**
   * Payment's deleted flag.
   *
   * @param d4 Payment's deleted flag.
   * @return Payment's deleted flag.
   */
  private Boolean d4;

  /**
   * Payment's confirmed flag.
   *
   * @param d5 Payment's confirmed flag.
   * @return Payment's confirmed flag.
   */
  private Boolean d5;

  /**
   * Payment's file fileContent.
   *
   * @param f1 Payment's file fileContent.
   * @return Payment's file fileContent.
   */
  @JsonIgnore
  private byte[] f1;

  /**
   * Payment's file extension.
   *
   * @param f2 Payment's file extension
   * @return Payment's file extension
   */
  private String f2;

  /**
   * Payment's file size.
   *
   * @param f3 Payment's file size.
   * @return Payment's file size.
   */
  private Long f3;

  /**
   * Payment's file upload date.
   *
   * @param f4 Payment's file upload date.
   * @return Payment's file upload date.
   */
  private long f4;

  /**
   * Payment's status code.
   *
   * @param s1 Payment's status code.
   * @return Payment's status code.
   */
  private String s1;

  /**
   * Payment's status date.
   *
   * @param s2 Payment's status date.
   * @return Payment's status date.
   */
  private long s2;

  /**
   * Explicit default constructor
   */
  public Payment() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
