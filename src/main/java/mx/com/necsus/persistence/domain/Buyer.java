package mx.com.necsus.persistence.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for a buyer.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class Buyer {

  /**
   * Buyer's unique identifier.
   *
   * @param rowKey Buyer's unique identifier.
   * @return Buyer's unique identifier.
   */
  private String rowKey;

  /**
   * Buyer business name.
   *
   * @param businessName Buyer business name.
   * @return Buyer business name.
   */
  private String businessName;

  /**
   * Explicit default constructor
   */
  public Buyer() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
