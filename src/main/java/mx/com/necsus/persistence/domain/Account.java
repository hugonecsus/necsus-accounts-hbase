package mx.com.necsus.persistence.domain;

import static com.jsoniter.output.JsonStream.serialize;

import java.time.ZonedDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import mx.com.necsus.domain.CatalogItem;

/**
 * Domain for an account.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class Account {

  /**
   * Account's unique identifier.
   *
   * @param rowKey Account's unique identifier.
   * @return Account's unique identifier.
   */
  private String rowKey;

  /**
   * Account's status code.
   *
   * @param statusCode Account's status code.
   * @return Account's status code.
   */
  private String statusCode;

  /**
   * Account's status code date.
   *
   * @param statusDate Account's status code date.
   * @return Account's status code date.
   */
  private Long statusDate;

  /**
   * Account's sign up date time.
   *
   * @param signUpDate Account's sign up date time.
   * @return Account's sign up date times.
   */
  private ZonedDateTime signUpDate;

  /**
   * Account's expire date.
   *
   * @param expireDate Account's expire date.
   * @return Account's expire date.
   */
  private Long expireDate;

  /**
   * Certification's unique identifier.
   *
   * @param certificationId Certification's unique identifier.
   * @return Certification's unique identifier.
   */
  private String certificationId;

  /**
   * Current certification's type code.
   *
   * @param currentCertificationType Current certification's type code.
   * @return Current certification's type code.
   */
  private String currentCertificationType;

  /**
   * Update certification's type code.
   *
   * @param updateCertificationType Update certification's type code.
   * @return Update certification's type code.
   */
  private String updateCertificationType;

  /**
   * Certification's plan code.
   *
   * @param certificationPlan Certification's plan code.
   * @return Certification's plan code.
   */
  private CatalogItem certificationPlan;

  /**
   * Certification's creation date.
   *
   * @param certificationCreationDate Certification's creation date.
   * @return Certification's creation date.
   */
  private Long certificationCreationDate;

  /**
   * Visit's status code.
   *
   * @param visitStatusCode Visit's status code.
   * @return Visit's status code.
   */
  private String visitStatusCode;

  /**
   * Visit's status code date.
   *
   * @param visitStatusDate Visit's status code date.
   * @return Visit's status code date.
   */
  private Long visitStatusDate;

  /**
   * Matches status code.
   *
   * @param matchesStatusCode Matches status code.
   * @return Matches status code.
   */
  private String matchesStatusCode;

  /**
   * Matches status code date.
   *
   * @param matchesStatusDate Matches status code date.
   * @return Matches status code date.
   */
  private Long matchesStatusDate;

  /**
   * Certification congrats.
   *
   * @param certificationCongrats Certification congrats.
   * @return Certification congrats.
   */
  private Boolean certificationCongrats;

  /**
   * Account post certification version.
   *
   * @param postCertificationVersion Account post certification version.
   * @return Account post certification version.
   */
  private Long postCertificationVersion;

  /**
   * Account post certification status.
   *
   * @param postCertificationStatus Account post certification status.
   * @return Account post certification status.
   */
  private String postCertificationStatus;

  /**
   * Account post certification date.
   *
   * @param postCertificationDate Account post certification date.
   * @return Account post certification date.
   */
  private Long postCertificationDate;

  /**
   * Is searchable.
   *
   * @param isSearchable Is searchable.
   * @return Is searchable.
   */
  private Boolean isSearchable;

  /**
   * Certification's status.
   *
   * @param certificationStatusCode Certification's status.
   * @return Certification's status.
   */
  private String certificationStatusCode;

  /**
   * Certification status date.
   *
   * @param certificationStatusDate Certification status date.
   * @return Certification status date.
   */
  private Long certificationStatusDate;

  /**
   * Information's status code.
   *
   * @param informationStatusCode Information's status code.
   * @return Information's status code.
   */
  private String informationStatusCode;

  /**
   * Information status date.
   *
   * @param informationStatusDate Information status date.
   * @return Information status date.
   */
  private Long informationStatusDate;

  /**
   * Legal entity code.
   *
   * @param legalEntityCode Legal entity code.
   * @return Legal entity code.
   */
  private String legalEntityCode;

  /**
   * Article size.
   *
   * @param articleSize Article size.
   * @return Article size.
   */
  private Integer articleSize;

  /**
   * Fiscal years size.
   *
   * @param fiscalYearsSize Fiscal years size.
   * @return Fiscal years size.
   */
  private Integer fiscalYearsSize;

  /**
   * Last fiscal year.
   *
   * @param lastFiscalYear Last fiscal year.
   * @return Last fiscal year.
   */
  private Integer lastFiscalYear;

  /**
   * Is the latest plan.
   *
   * @param isLatestPlan Is the latest plan.
   * @return Is the latest plan.
   */
  private Boolean isLatestPlan;

  /**
   * Last payment capture date.
   *
   * @param paymentCaptureDate Last payment capture date.
   * @return Last payment capture date.
   */
  private String paymentCaptureDate;

  /**
   * Section 3 block 3 status code.
   *
   * @param s3S1B3 Section 3 block 3 status code.
   * @return Section 3 block 3 status code.
   */
  private String s3S1B3;

  /**
   * Accreditation size.
   *
   * @param accreditationSize Accreditation size.
   * @return Accreditation size.
   */
  private Integer accreditationSize;

  /**
   * Section 1 block 2 status code.
   *
   * @param s1S1B2 Section 1 block 2 status code.
   * @return Section 1 block 2 status code.
   */
  private String s1S1B2;

  /**
   * Information evaluation date.
   *
   * @param evaluationDate Information evaluation date.
   * @return Information evaluation date.
   */
  private Long evaluationDate;

  /**
   * Explicit default constructor
   */
  public Account() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
