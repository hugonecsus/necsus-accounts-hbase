package mx.com.necsus.persistence.domain;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for retrieve information excel comparison.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class ComparisonAccounts {

  /**
   * Account one.
   *
   * @param accountOne Account one.
   * @return Account one.
   */
  private AccountMetadata accountOne;

  /**
   * Account two.
   *
   * @param accountTwo Account two.
   * @return Account two.
   */
  private AccountMetadata accountTwo;

  /**
   * Account three.
   *
   * @param accountThree Account three.
   * @return Account three.
   */
  private AccountMetadata accountThree;

  /**
   * Account List.
   *
   * @param accountList Account List.
   * @return Account List.
   */
  private List<AccountMetadata> accountList;

  /**
   * Matches counter.
   *
   * @param d2 Matches counter.
   * @return Matches counter.
   */
  private Integer d2;

}