package mx.com.necsus.persistence.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for retrieve information excel.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class AccountMetadata {

  /**
   * Business name.
   *
   * @param businessName Business name.
   * @return Business name.
   */
  private String businessName;

  /**
   * RFC.
   *
   * @param rfc RFC.
   * @return RFC.
   */
  private String rfc;

  /**
   * Origin Code.
   *
   * @param origin Origin Code.
   * @return Origin Code.
   */
  private String origin;

  /**
   * Certification type code.
   *
   * @param certificationType Certification type code.
   * @return Certification type code.
   */
  private String certificationType;

  /**
   * Certification status.
   *
   * @param certificationStatus Certification status.
   * @return Certification status.
   */
  private String certificationStatus;

  /**
   * Certification date.
   *
   * @param certificationDate Certification date.
   * @return Certification date.
   */
  private String certificationDate;

  /**
   * Last validation.
   *
   * @param lastValidation Last validation.
   * @return Last validation.
   */
  private String lastValidation;

  /**
   * Certification expire date.
   *
   * @param certificationExpirateDate Certification expire date.
   * @return Certification expire date.
   */
  private String certificationExpirateDate;

  /**
   * Legal entity.
   *
   * @param legalEntity Legal entity.
   * @return Legal entity.
   */
  private String legalEntity;

  /**
   * Country.
   *
   * @param country Country.
   * @return Country.
   */
  private String country;

  /**
   * Total employees the company has.
   *
   * @param sizeCompany Total employees the company has.
   * @return Total employees the company has.
   */
  private String sizeCompany;

  /**
   * Economic activity.
   *
   * @param economicActivity Economic activity.
   * @return Economic activity.
   */
  private String economicActivity;

  /**
   * Web site.
   *
   * @param webSite Web site.
   * @return Web site.
   */
  private String webSite;

  /**
   * Legal direction state.
   *
   * @param state Legal direction state..
   * @return Legal direction state..
   */
  private String state;

  /**
   * Legal direction city.
   *
   * @param city Legal direction city.
   * @return Legal direction city.
   */
  private String city;

  /**
   * Legal direction municipality.
   *
   * @param municipality Legal direction municipality.
   * @return Legal direction municipality.
   */
  private String municipality;

  /**
   * Legal direction district.
   *
   * @param district Legal direction district.
   * @return Legal direction district.
   */
  private String district;

  /**
   * Legal direction street Name.
   *
   * @param stretName Legal direction street Name.
   * @return Legal direction street Name.
   */
  private String stretName;

  /**
   * Main contact Name.
   *
   * @param name Main contact Name.
   * @return Main contact Name.
   */
  private String name;

  /**
   * Main contact First surname.
   *
   * @param firstSurname Main contact First surname.
   * @return Main contact First surname..
   */
  private String firstSurname;

  /**
   * Main contact Second surname.
   *
   * @param secondSurname Main contact  Second surname.
   * @return Main contact  Second surname.
   */
  private String secondSurname;

  /**
   * Main contact Mobile phone.
   *
   * @param mobilePhone Main contact Mobile phone.
   * @return Main contact Mobile phone.
   */
  private String mobilePhone;

  /**
   *  Main contact Email.
   *
   * @param email Main contact Email.
   * @return Main contact Email.
   */
  private String email;

  /**
   * Certification score legal total.
   *
   * @param recursosHumanos Certification score legal total.
   * @return Certification score legal total.
   */
  private Integer recursosHumanos;

  /**
   * Certification score total.
   *
   * @param scoreTotal Certification score total.
   * @return Certification score total.
   */
  private Float scoreTotal;

  /**
   * Certification score legal total.
   *
   * @param scoreLegal Certification score legal total.
   * @return Certification score legal total.
   */
  private Float scoreLegal;

  /**
   * Certification score fiscal total.
   *
   * @param scoreFiscal Certification score fiscal total.
   * @return Certification score fiscal total.
   */
  private Float scoreFiscal;

  /**
   * Certification score ethics and integrity total.
   *
   * @param scoreEthicsIntegrity Certification score ethics and integrity total.
   * @return Certification score ethics and integrity total.
   */
  private Float scoreEthicsIntegrity;

  /**
   * Certification score financial total.
   *
   * @param scoreFinancial Certification score financial total.
   * @return Certification score financial total.
   */
  private Float scoreFinancial;

  /**
   * Certification score commercial total.
   *
   * @param scoreCommercial Certification score commercial total.
   * @return Certification score commercial total.
   */
  private Float scoreCommercial;

  /**
   * Certification score quality total.
   *
   * @param scoreQuality Certification score quality total.
   * @return Certification score quality total.
   */
  private Float scoreQuality;

  /**
   * Certification score health total.
   *
   * @param scoreHealth Certification score health total.
   * @return Certification score health total.
   */
  private Float scoreHealth;

  /**
   * Certification score environment total.
   *
   * @param scoreEnvironment Certification score environment total.
   * @return Certification score environment total.
   */
  private Float scoreEnvironment;

  /**
   * Certification score operation total.
   *
   * @param scoreOperation Certification score operation total.
   * @return Certification score operation total.
   */
  private Float scoreOperation;

  /**
   * Certification score economic performance total.
   *
   * @param scoreEconomic Certification score economic performance total.
   * @return Certification score economic performance total.
   */
  private Float scoreEconomic;

  /**
   * Has quality certificate.
   *
   * @param certificateQuality Has quality certificate.
   * @return Has quality certificate.
   */
  private Boolean certificateQuality;

  /**
   * Has health and safety certificate.
   *
   * @param certificateHealthSafety Has health and safety certificate
   * @return Has health and safety certificate
   */
  private Boolean certificateHealthSafety;

  /**
   * Has environmental certificate.
   *
   * @param certificateEnvironmental Has environmental certificate.
   * @return Has environmental certificate.
   */
  private Boolean certificateEnvironmental;

  /**
   * Has observation of lists.
   *
   * @param observationList Has observation of lists.
   * @return Has observation of lists.
   */
  private Integer observationList;

  /**
   * Disabled SFP.
   *
   * @param disabledSfp Has observation of lists.
   * @return Has observation of lists.
   */
  private Integer disabledSfp;

  /**
   * REPSE certificate status.
   *
   * @param repseCertificateStatus REPSE certificate status.
   * @return REPSE certificate status.
   */
  private String repseCertificateStatus;

  /**
   * Explicit default constructor
   */
  public AccountMetadata() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}