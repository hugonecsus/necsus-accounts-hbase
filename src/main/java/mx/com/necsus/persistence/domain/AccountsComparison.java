package mx.com.necsus.persistence.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for <strong>accounts comparison</strong>.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class AccountsComparison {

  /**
   * Account one.
   *
   * @param accountOne Account one.
   * @return Account one.
   */
  private AccountMetadata accountOne;

  /**
   * Account two.
   *
   * @param accountTwo Account two.
   * @return Account two.
   */
  private AccountMetadata accountTwo;

  /**
   * Account three.
   *
   * @param accountThree Account three.
   * @return Account three.
   */
  private AccountMetadata accountThree;

  /**
   * Explicit default constructor
   */
  public AccountsComparison() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
