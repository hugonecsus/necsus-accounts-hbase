package mx.com.necsus.persistence;

import static java.util.Arrays.stream;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toUnmodifiableMap;
import static mx.com.necsus.domain.catalog.MessagesValues.NULL;
import static mx.com.necsus.domain.catalog.MessagesValues.values;

import java.util.Map;

import mx.com.necsus.domain.catalog.MessagesValues;

/**
 * <em>Mapper</em> for the <strong>messages values</strong>.
 */
public final class MessagesValuesMapper {

  /**
   * Fixed {@link Map} with the messages values.
   */
  private static final Map<String, MessagesValues> VALUES = stream(values())
                                                              .collect(toUnmodifiableMap(
                                                                         MessagesValues::name,
                                                                         identity()));

  /**
   * Private explicit default constructor for security.
   */
  private MessagesValuesMapper() {
  }

  /**
   * Search the <strong>code</strong> in {@link Map} {@link #VALUES}.
   *
   * @param code Unique identifier to search.
   *
   * @return The instance of {@link MessagesValues} that matches, {@link MessagesValues#NULL} in other case.
   */
  public static MessagesValues findMessageValue(final String code) {

    return
      null == code
        ? NULL
        : ofNullable(
            VALUES.get(code))
          .orElse(
            NULL);

  }

}
