package mx.com.necsus.persistence;

import static java.util.Arrays.stream;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toUnmodifiableMap;
import static mx.com.necsus.domain.catalog.NotesValues.NULL;
import static mx.com.necsus.domain.catalog.NotesValues.values;

import java.util.Map;

import mx.com.necsus.domain.catalog.NotesValues;

/**
 * <em>Mapper</em> for the <strong>note values</strong>.
 */
public final class NotesValuesMapper {

  /**
   * Fixed {@link Map} with the note values.
   */
  private static final Map<String, NotesValues> VALUES = stream(values())
                                                              .collect(toUnmodifiableMap(
                                                                         NotesValues::name,
                                                                         identity()));

  /**
   * Private explicit default constructor for security.
   */
  private NotesValuesMapper() {
  }

  /**
   * Search the <strong>code</strong> in {@link Map} {@link #VALUES}.
   *
   * @param code Unique identifier to search.
   *
   * @return The instance of {@link NotesValues} that matches, {@link NotesValues#NULL} in other case.
   */
  public static NotesValues findNoteValue(final String code) {

    return
      null == code
        ? NULL
        : ofNullable(
            VALUES.get(code))
          .orElse(
            NULL);

  }

}
