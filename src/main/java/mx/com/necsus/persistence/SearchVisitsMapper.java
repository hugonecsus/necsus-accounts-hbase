package mx.com.necsus.persistence;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static mx.com.necsus.domain.catalog.CatalogCodes.CERTIFICATION_TYPES;
import static mx.com.necsus.domain.catalog.CatalogCodes.VISIT_STATUS;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D2;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D7;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D8;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D2;
import static mx.com.necsus.persistence.Filters.createSingleColumnValueFilter;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D5;
import static mx.com.necsus.persistence.TypesHandler.EMPTY_1N_SIZE;
import static mx.com.necsus.persistence.TypesHandler.get1nSize;
import static mx.com.necsus.persistence.TypesHandler.getCatalogItem;
import static mx.com.necsus.persistence.TypesHandler.getDateTimeString;
import static mx.com.necsus.persistence.TypesHandler.getInteger;
import static mx.com.necsus.persistence.TypesHandler.getString;
import static mx.com.necsus.persistence.TypesHandler.stream4ResultScanner;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D1;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D3;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D39;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D4;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_D6;
import static mx.com.necsus.persistence.VisitQualifiers.VISIT_PREFIX;
import static mx.com.necsus.util.AccessCrypto.encode;
import static mx.com.necsus.util.AccountsCommons.retrieveVersion4PostCertificationStatus;
import static mx.com.necsus.util.Catalogs.retrieveCatalogMap;
import static org.apache.hadoop.hbase.CompareOperator.EQUAL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ALL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ONE;

import java.util.List;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.ColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.MultiRowRangeFilter;
import org.apache.hadoop.hbase.filter.MultipleColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.filter.ValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

import mx.com.necsus.domain.AccountRetrieveAll;

/**
 * Persistence layer for the <strong>search visits</strong>.
 */
public final class SearchVisitsMapper {

  /**
   * Prefix for the pattern <strong>vid3</strong>.
   */
  private static final byte[] VI_D3_PREFIX = "vid3".getBytes();

  /**
   * Value for none ranges for the {@link ValueFilter}.
   */
  private static final int ZERO_RANGES = 0;

  /**
   * Value for a single range for the {@link ValueFilter}
   */
  private static final int ONE_RANGE = 1;

  /**
   * Prefixes for search.
   */
  private static final byte[][] SEARCH_PREFIXES = new byte[][] {
                                                    ACCOUNT_D2,
                                                    ACCOUNT_D7,
                                                    ACCOUNT_D8,
                                                    CERTIFICATION_D2,
                                                    S1_B1_D1,
                                                    S1_B1_D4,
                                                    S1_B1_D5,
                                                    VISIT_PREFIX
                                                  };

  /**
   * Retrieves an instance of the type {@link List} with the accounts that matches with the search keyword.
   *
   * @param searchKeyword String with the keyword to search.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return A list of the type {@link AccountRetrieveAll} with the accounts found.
   */
  public static List<AccountRetrieveAll> retrieveAccounts4SearchVisits(final String searchKeyword,
                                                                       final String timeZone,
                                                                       final String lang) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME)) {

      try (final var resultScanner = table
                                       .getScanner(
                                         new Scan()
                                           .addFamily(
                                             ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                           .setFilter(
                                             new FilterList(
                                               MUST_PASS_ALL,
                                               new MultipleColumnPrefixFilter(
                                                 SEARCH_PREFIXES),
                                               createFilter4Search(
                                                 table,
                                                 searchKeyword)))
                                           .readAllVersions())) {

        return
          retrieveAccountsFound(
            resultScanner,
            timeZone,
            lang);

      }

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the accounts found.
   *
   * @param resultScanner Instance of the type {@link ResultScanner} with the row keys.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return A list of the type {@link AccountRetrieveAll} with the accounts found.
   */
  private static List<AccountRetrieveAll> retrieveAccountsFound(final ResultScanner resultScanner,
                                                                final String timeZone,
                                                                final String lang) {

    if (null == resultScanner) {

      return emptyList();

    }

    final var certificationTypes = retrieveCatalogMap(
                                     CERTIFICATION_TYPES,
                                     lang);

    final var visitStatus = retrieveCatalogMap(
                              VISIT_STATUS,
                              lang);

    return
      stream4ResultScanner(resultScanner)
        .filter(
          result -> hasFolio(result))
        .map(
          result -> {

            final var size = get1nSize(
                               result,
                               ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                               VISIT_D1);

            final var version = retrieveVersion4PostCertificationStatus(
                                  result);

            return
              new AccountRetrieveAll()
                .setD0(
                  encode(
                    Bytes.toString(
                      result.getRow())))
                .setD1(
                  getCatalogItem(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    ACCOUNT_D2))
                .setD3(
                  getCatalogItem(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D2,
                    version,
                    certificationTypes))
                .setD18(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B1_D1))
                .setD20(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B1_D4))
                .setD21(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B1_D5))
                .setD30(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    qualifierNameOneIndex(
                      VISIT_D3,
                      size - 1)))
                .setD31(
                  getCatalogItem(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    qualifierNameOneIndex(
                      VISIT_D4,
                      size - 1),
                    visitStatus))
                .setD32(
                  getInteger(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    qualifierNameOneIndex(
                      VISIT_D6,
                      size - 1)))
                .setD33(
                  getDateTimeString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    qualifierNameOneIndex(
                      VISIT_D39,
                      size - 1),
                    timeZone,
                    lang));

          })
        .collect(
          toList());

  }

  /**
   * Creates a {@link FilterList} with the filters to use for the search.
   *
   * @param table The current instance of {@link Table}.
   * @param searchKeyword String with the keyword for the search.
   *
   * @return An instance of the type {@link FilterList} with the filters.
   */
  private static FilterList createFilter4Search(final Table table,
                                                final String searchKeyword) {

    final var filterList4Search = new FilterList(MUST_PASS_ONE);

    final var range = createFilterByFolio(
                        table,
                        searchKeyword);

    switch (range.length) {

      case ZERO_RANGES:

        filterList4Search
          .addFilter(
            createSingleColumnValueFilter(
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B1_D4,
              EQUAL,
              new SubstringComparator(
                searchKeyword)));

        filterList4Search
          .addFilter(
            createSingleColumnValueFilter(
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B1_D5,
              EQUAL,
              new SubstringComparator(
                searchKeyword)));

        filterList4Search
          .addFilter(
            createSingleColumnValueFilter(
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B1_D1,
              EQUAL,
              new SubstringComparator(
                searchKeyword)));

        break;

      case ONE_RANGE:

        filterList4Search
          .addFilter(
            new MultiRowRangeFilter(
              range));

        break;

      default:

        filterList4Search
          .addFilter(
            createSingleColumnValueFilter(
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B1_D4,
              EQUAL,
              new SubstringComparator(
                searchKeyword)));

        filterList4Search
          .addFilter(
            createSingleColumnValueFilter(
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B1_D5,
              EQUAL,
              new SubstringComparator(
                searchKeyword)));

        filterList4Search
          .addFilter(
            createSingleColumnValueFilter(
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B1_D1,
              EQUAL,
              new SubstringComparator(
                searchKeyword)));

        filterList4Search
          .addFilter(
            new MultiRowRangeFilter(
              range));

        break;

    }

    return filterList4Search;

  }

  /**
   * Perform a {@link Scan} with {@link ValueFilter} to found matches by folio.
   *
   * @param table The current instance of {@link Table}.
   * @param searchKeyword String with the keyword for the search.
   *
   * @return An array with the columns for the range.
   */
  private static byte[][] createFilterByFolio(final Table table,
                                              final String searchKeyword) {

    try (final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           new FilterList(
                                             MUST_PASS_ALL,
                                             new ColumnPrefixFilter(
                                               VI_D3_PREFIX),
                                             new ValueFilter(
                                               EQUAL,
                                               new SubstringComparator(
                                                 searchKeyword)))))) {

      return
        stream4ResultScanner(resultScanner)
          .map(
            Result::getRow)
          .toArray(
            byte[][]::new);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Validate if the current item has folio.
   *
   * @param result Instance of the type {@link Result} with the qualifiers.
   *
   * @return Returns <strong>true</strong> if the qualifier has value otherwise <strong>false</strong>.
   */
  private static boolean hasFolio(final Result result) {

    final var size = get1nSize(
                       result,
                       ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                       VISIT_D1);

    if (EMPTY_1N_SIZE != size) {

      return
        null != getString(
                  result,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  qualifierNameOneIndex(
                    VISIT_D3,
                    size - 1));

    }

    return false;

  }

}
