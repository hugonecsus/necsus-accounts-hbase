package mx.com.necsus.persistence;

import static java.util.Arrays.stream;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toUnmodifiableMap;
import static mx.com.necsus.domain.catalog.CompletedValues.NULL;
import static mx.com.necsus.domain.catalog.CompletedValues.values;
import static mx.com.necsus.util.RequestBody.isBlankCatalogItem;

import java.util.Map;

import mx.com.necsus.domain.CatalogItem;
import mx.com.necsus.domain.catalog.CompletedValues;

/**
 * <em>Mapper</em> for the <strong>completed values</strong>.
 */
public final class CompletedValuesMapper {

  /**
   * Fixed {@link Map} with the completed values.
   */
  private static final Map<String, CompletedValues> VALUES = stream(
                                                                 values())
                                                               .collect(
                                                                 toUnmodifiableMap(
                                                                   CompletedValues::name,
                                                                   identity()));

  /**
   * Private explicit default constructor for security.
   */
  private CompletedValuesMapper() {
  }

  /**
   * Search the <strong>code</strong> in {@link Map} {@link #VALUES}.
   *
   * @param code Unique identifier to search.
   *
   * @return The instance of {@link CompletedValues} that matches, {@link CompletedValues#NULL} in other case.
   */
  public static CompletedValues findCompletedValue(final String code) {

    return
      null == code
        ? NULL
        : ofNullable(
            VALUES.get(code))
          .orElse(
            NULL);

  }

  /**
   * Search the <strong>d1</strong> of the {@link CatalogItem} in {@link Map} {@link #VALUES}.
   *
   * @param catalogItem Instance of the type {@link CatalogItem} with the code.
   *
   * @return The instance of {@link CompletedValues} that matches, {@link CompletedValues#NULL} in other case.
   */
  public static CompletedValues findCompletedValue(final CatalogItem catalogItem) {

    if (isBlankCatalogItem(catalogItem)) {

      return NULL;

    }

    return
      findCompletedValue(
        catalogItem.getD1());

  }

}
