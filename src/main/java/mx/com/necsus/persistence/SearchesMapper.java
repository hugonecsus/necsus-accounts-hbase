package mx.com.necsus.persistence;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.parseBoolean;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static mx.com.necsus.domain.catalog.BlacklistCodes.SFP;
import static mx.com.necsus.domain.catalog.BuyerAssociationStatus.ASSOCIATED;
import static mx.com.necsus.domain.catalog.CatalogCodes.CERTIFICATION_TYPES;
import static mx.com.necsus.domain.catalog.CatalogCodes.COMPANY_SIZE;
import static mx.com.necsus.domain.catalog.CatalogCodes.COUNTRIES;
import static mx.com.necsus.domain.catalog.CatalogCodes.ECONOMIC_ACTIVITIES;
import static mx.com.necsus.domain.catalog.CatalogCodes.LEGAL_ENTITIES;
import static mx.com.necsus.domain.catalog.CatalogCodes.ORIGINS;
import static mx.com.necsus.domain.catalog.CatalogCodes.PRODUCTS_AND_SERVICES;
import static mx.com.necsus.domain.catalog.CatalogCodes.REPSE_STATUS;
import static mx.com.necsus.domain.catalog.CatalogCodes.ZIP_CODES;
import static mx.com.necsus.domain.catalog.MatchesStatus.CONFIRMED;
import static mx.com.necsus.domain.catalog.Origins.LOCAL;
import static mx.com.necsus.domain.catalog.PostCertificationStatus.UPDATE_FINALIZED;
import static mx.com.necsus.domain.catalog.SearchValues.ADV_C1;
import static mx.com.necsus.domain.catalog.SearchValues.STD_C1;
import static mx.com.necsus.domain.catalog.SearchValues.STD_C2;
import static mx.com.necsus.domain.catalog.SubjectCodes.S1B1;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D11;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D12;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D13;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D2;
import static mx.com.necsus.persistence.AccountQualifiers.ACCOUNT_D8;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_TABLE_NAME;
import static mx.com.necsus.persistence.BuyerAssociationQualifiers.BUYER_ASSOCIATION_D1;
import static mx.com.necsus.persistence.BuyersTable.BUYERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.BuyersTable.BUYERS_TABLE_NAME;
import static mx.com.necsus.persistence.CatalogsDb.CATALOGS_DB;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D1;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D10;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D11;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D12;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D13;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D14;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D15;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D16;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D17;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D18;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D19;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D2;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D3;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D30;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D34;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D4;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D5;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D9;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_PREFIX;
import static mx.com.necsus.persistence.DisclaimerQualifiers.DISCLAIMER_S2;
import static mx.com.necsus.persistence.Filters.createSingleColumnValueFilter;
import static mx.com.necsus.persistence.HBase.H_BASE;
import static mx.com.necsus.persistence.LegalEntitiesMapper.findLegalEntity;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D1;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D2;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D3;
import static mx.com.necsus.persistence.MatchesQualifiers.MT_D4;
import static mx.com.necsus.persistence.MatchesStatusMapper.findMatchesStatus;
import static mx.com.necsus.persistence.OriginsMapper.findOrigin;
import static mx.com.necsus.persistence.PostCertificationStatusMapper.findPostCertificationStatus;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.SearchValuesMapper.findSearchValue;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D5;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D6;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D7;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B1_D9;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B2_D1;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D12;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D3;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D4;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D5;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D6;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D7;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D8;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B3_D9;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B4_D2;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_B5_D5;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D1;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D10;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D11;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D13;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_B1_D9;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D1;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D3;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D4;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_D2;
import static mx.com.necsus.persistence.Section6Qualifiers.S6_B1_D3;
import static mx.com.necsus.persistence.Section6Qualifiers.S6_B2_D3;
import static mx.com.necsus.persistence.Section6Qualifiers.S6_B3_D3;
import static mx.com.necsus.persistence.TypesHandler.FIRST_ELEMENT_INDEX;
import static mx.com.necsus.persistence.TypesHandler.get1nSize;
import static mx.com.necsus.persistence.TypesHandler.getBoolean;
import static mx.com.necsus.persistence.TypesHandler.getCatalogItem;
import static mx.com.necsus.persistence.TypesHandler.getCatalogItemList;
import static mx.com.necsus.persistence.TypesHandler.getDateString;
import static mx.com.necsus.persistence.TypesHandler.getFloat;
import static mx.com.necsus.persistence.TypesHandler.getLong;
import static mx.com.necsus.persistence.TypesHandler.getString;
import static mx.com.necsus.persistence.TypesHandler.getStringOrEmpty;
import static mx.com.necsus.persistence.TypesHandler.intStreamRange;
import static mx.com.necsus.persistence.TypesHandler.newGet;
import static mx.com.necsus.persistence.TypesHandler.streamParallel4ResultScanner;
import static mx.com.necsus.persistence.UserQualifiers.USER_D10;
import static mx.com.necsus.persistence.UserQualifiers.USER_D11;
import static mx.com.necsus.persistence.UserQualifiers.USER_D12;
import static mx.com.necsus.persistence.UserQualifiers.USER_D5;
import static mx.com.necsus.persistence.UserQualifiers.USER_D6;
import static mx.com.necsus.persistence.UserQualifiers.USER_D7;
import static mx.com.necsus.persistence.UserQualifiers.USER_D8;
import static mx.com.necsus.persistence.UserQualifiers.USER_D9;
import static mx.com.necsus.persistence.UsersMapper.retrieveBuyerRfcByUserId;
import static mx.com.necsus.persistence.UsersTable.USERS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.UsersTable.USERS_TABLE_NAME;
import static mx.com.necsus.util.AccessCrypto.encode;
import static mx.com.necsus.util.AccountsHeaders.X_TOTAL_SIZE_HTTPSTRING;
import static mx.com.necsus.util.AccountsStrings.LOG_BUYER_ASSOCIATION_ID_FOUND;
import static mx.com.necsus.util.AccountsStrings.LOG_MATCHES_FOR_SUBJECTS_RETRIEVED;
import static mx.com.necsus.util.AccountsStrings.LOG_SIZE_PRODUCTS_FOUND;
import static mx.com.necsus.util.Catalogs.retrieveCatalogItemDescriptionByCode;
import static mx.com.necsus.util.Catalogs.retrieveCatalogMap;
import static mx.com.necsus.util.Catalogs.retrieveSubjectsCatalog;
import static mx.com.necsus.util.CertificationsCommons.isNotDeletedListItem;
import static mx.com.necsus.util.RequestBody.isBlankCollection;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static mx.com.necsus.util.RequestBody.isNotBlankCatalogItem;
import static mx.com.necsus.util.RequestBody.isNotBlankCollection;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.Searches.anyMemberIsEligible;
import static mx.com.necsus.util.Searches.isNameIndicated;
import static mx.com.necsus.util.Strings.EMPTY_STRING;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_OFFSET;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_SIZE;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TIME_ZONE;
import static mx.com.necsus.util.TraceHeaders.X_USER_ID;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static org.apache.hadoop.hbase.CompareOperator.EQUAL;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.MUST_PASS_ALL;
import static org.apache.logging.log4j.LogManager.getLogger;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.AccountRetrieveAll;
import mx.com.necsus.domain.CatalogItem;
import mx.com.necsus.persistence.domain.AccountMetadata;
import mx.com.necsus.persistence.domain.CatalogFilter;
import mx.com.necsus.persistence.domain.ComparisonAccounts;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.MultipleColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.logging.log4j.Logger;

/**
 * Persistence layer for the <strong>Search</strong>.
 */
public final class SearchesMapper {

  /**
   * Regular expression for punctuation marks.
   */
  public static final String PUNCTUATION_MARKS_REGEX = "[,.:;]*";

  /**
   * Logger for the mapper.
   */
  private static final Logger LOGGER = getLogger(SearchesMapper.class);

  /**
   * Column prefixes to retrieve in {@link #retrieveAccounts4AdvSearch}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_ADV_SEARCH = new MultipleColumnPrefixFilter(
                                                                            new byte[][] {
                                                                              ACCOUNT_D11,
                                                                              ACCOUNT_D13,
                                                                              ACCOUNT_D2,
                                                                              CERTIFICATION_PREFIX,
                                                                              S1_B1_D1,
                                                                              S1_B1_D2,
                                                                              S1_B1_D4,
                                                                              S1_B1_D5,
                                                                              S1_B1_D7,
                                                                              S1_B1_D6,
                                                                              S1_B2_D1,
                                                                              S1_B4_D2,
                                                                              "s3b1d".getBytes(),
                                                                              "s4b1d".getBytes(),
                                                                              "s4b2d".getBytes(),
                                                                              "s4b3d".getBytes(),
                                                                              "s4b4d".getBytes(),
                                                                              "s4b5d".getBytes(),
                                                                              "s4b7d".getBytes(),
                                                                              "s5b3d".getBytes(),
                                                                              S5_D2,
                                                                              S6_B1_D3,
                                                                              S6_B2_D3,
                                                                              S6_B3_D3});

  /**
   * Column prefixes to retrieve in {@link #retrieveAccounts4StdSearch}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_STD_SEARCH = new MultipleColumnPrefixFilter(
                                                                            new byte[][] {
                                                                              ACCOUNT_D11,
                                                                              ACCOUNT_D13,
                                                                              ACCOUNT_D2,
                                                                              CERTIFICATION_PREFIX,
                                                                              S1_B1_D1,
                                                                              S1_B1_D2,
                                                                              S1_B1_D4,
                                                                              S1_B1_D5,
                                                                              S1_B1_D6,
                                                                              S1_B1_D7,
                                                                              S1_B2_D1,
                                                                              S1_B4_D2,
                                                                              "s3b1d".getBytes(),
                                                                              S6_B1_D3,
                                                                              S6_B2_D3,
                                                                              S6_B3_D3});

  /**
   * Column prefixes to retrieve in {@link #retrieveAccounts4StdSearch2Xls}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_STD_SEARCH_2_XLS = new MultipleColumnPrefixFilter(
                                                                                  new byte[][] {
                                                                                    ACCOUNT_D11,
                                                                                    ACCOUNT_D12,
                                                                                    ACCOUNT_D13,
                                                                                    ACCOUNT_D2,
                                                                                    ACCOUNT_D8,
                                                                                    CERTIFICATION_PREFIX,
                                                                                    DISCLAIMER_S2,
                                                                                    MT_D1,
                                                                                    "mtd2".getBytes(),
                                                                                    "mtd3".getBytes(),
                                                                                    "mtd4".getBytes(),
                                                                                    S1_B1_D1,
                                                                                    S1_B1_D2,
                                                                                    S1_B1_D4,
                                                                                    S1_B1_D5,
                                                                                    S1_B1_D6,
                                                                                    S1_B1_D7,
                                                                                    S1_B1_D9,
                                                                                    S1_B2_D1,
                                                                                    S1_B3_D3,
                                                                                    S1_B3_D4,
                                                                                    S1_B3_D5,
                                                                                    S1_B3_D6,
                                                                                    S1_B3_D7,
                                                                                    S1_B3_D8,
                                                                                    S1_B3_D9,
                                                                                    S1_B3_D12,
                                                                                    S1_B5_D5,
                                                                                    "s3b1d".getBytes(),
                                                                                    S5_B1_D1,
                                                                                    "s5b1d3".getBytes(),
                                                                                    "s5b1d4".getBytes(),
                                                                                    S5_D2,
                                                                                    S6_B1_D3,
                                                                                    S6_B2_D3,
                                                                                    S6_B3_D3});

  /**
   * Column prefixes to retrieve in {@link #retrieveAccounts4AdvSearch2Xls}.
   */
  private static final MultipleColumnPrefixFilter PREFIXES_4_ADV_SEARCH_2_XLS = new MultipleColumnPrefixFilter(
                                                                                  new byte[][] {
                                                                                    ACCOUNT_D11,
                                                                                    ACCOUNT_D12,
                                                                                    ACCOUNT_D13,
                                                                                    ACCOUNT_D2,
                                                                                    ACCOUNT_D8,
                                                                                    CERTIFICATION_PREFIX,
                                                                                    DISCLAIMER_S2,
                                                                                    MT_D1,
                                                                                    "mtd2".getBytes(),
                                                                                    "mtd3".getBytes(),
                                                                                    "mtd4".getBytes(),
                                                                                    S1_B1_D1,
                                                                                    S1_B1_D2,
                                                                                    S1_B1_D4,
                                                                                    S1_B1_D5,
                                                                                    S1_B1_D6,
                                                                                    S1_B1_D7,
                                                                                    S1_B1_D9,
                                                                                    S1_B2_D1,
                                                                                    S1_B3_D3,
                                                                                    S1_B3_D4,
                                                                                    S1_B3_D5,
                                                                                    S1_B3_D6,
                                                                                    S1_B3_D7,
                                                                                    S1_B3_D8,
                                                                                    S1_B3_D9,
                                                                                    S1_B3_D12,
                                                                                    S1_B5_D5,
                                                                                    "s3b1d".getBytes(),
                                                                                    "s4b1d".getBytes(),
                                                                                    "s4b2d".getBytes(),
                                                                                    "s4b3d".getBytes(),
                                                                                    "s4b4d".getBytes(),
                                                                                    "s4b5d".getBytes(),
                                                                                    "s4b7d".getBytes(),
                                                                                    S5_B1_D1,
                                                                                    "s5b1d3".getBytes(),
                                                                                    "s5b1d4".getBytes(),
                                                                                    "s5b3d".getBytes(),
                                                                                    S5_D2,
                                                                                    S6_B1_D3,
                                                                                    S6_B2_D3,
                                                                                    S6_B3_D3});

  /**
   * {@link MultipleColumnPrefixFilter} for the user values.
   */
  private static final MultipleColumnPrefixFilter USER_PREFIXES = new MultipleColumnPrefixFilter(
                                                                    new byte[][] {
                                                                      USER_D5,
                                                                      USER_D6,
                                                                      USER_D7,
                                                                      USER_D8,
                                                                      USER_D9,
                                                                      USER_D10,
                                                                      USER_D11,
                                                                      USER_D12});

  /**
   * {@link SingleColumnValueFilter} for the {@link AccountQualifiers#ACCOUNT_D11} to retrieve in {@link #retrieveAccounts4Search}
   */
  private static final SingleColumnValueFilter FILTER_4_IS_SEARCHABLE = createSingleColumnValueFilter(
                                                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                          ACCOUNT_D11,
                                                                          EQUAL,
                                                                          TRUE);

  /**
   * {@link SingleColumnValueFilter} for the for the {@link BuyerAssociationQualifiers#BUYER_ASSOCIATION_D1} to retrieve
   * in {@link #isExistingBuyerAssociation}
   */
  private static final SingleColumnValueFilter FILTER_4_BUYER_ASSOCIATION_STATUS = createSingleColumnValueFilter(
                                                                                     ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                                                     BUYER_ASSOCIATION_D1,
                                                                                     EQUAL,
                                                                                     ASSOCIATED.name());

  /**
   * Size two of the collect.
   */
  private static final int SIZE_TWO = 2;

  /**
   * No date value.
   */
  private static final long NO_DATE = 0;

  /**
   * Pattern for the <strong> Legal direction</strong>.
   */
  private static final String LEGAL_DIRECTION_PATTERN = "%s, %s, %s";

  /**
   * Buyer association row key pattern.
   */
  private static final String BUYER_ASSOCIATION_ROW_KEY_PATTERN = "%s%s";

  /**
   * Private explicit default constructor for security.
   */
  private SearchesMapper() {
  }

  /**
   * Retrieve account rows by search.
   *
   * @param exchange Instance of the type {@link HttpServerExchange}.
   * @param searchCriteria The search criteria and keyword to search matches.
   *
   * @return A list of the type {@link AccountRetrieveAll}.
   */
  public static List<AccountRetrieveAll> retrieveAccounts4Search(final HttpServerExchange       exchange,
                                                                 final Map<String,List<String>> searchCriteria) {

    final var buyerId =  retrieveBuyerRfcByUserId(
                           getHeader(
                             exchange,
                             X_USER_ID),
                           getHeader(
                             exchange,
                             X_ROLE_CODE));

    List<AccountRetrieveAll> accountList = null;

    if (isNotBlankCollection(searchCriteria.get(STD_C1.name())) ||
        isNotBlankCollection(searchCriteria.get(STD_C2.name()))) {

      accountList = retrieveAccounts4StdSearch(
                      exchange,
                      buyerId,
                      searchCriteria);

    } else {

      accountList = retrieveAccounts4AdvSearch(
                      exchange,
                      buyerId,
                      searchCriteria);

    }

    exchange
      .getResponseHeaders()
      .put(
        X_TOTAL_SIZE_HTTPSTRING,
        accountList.size());

    return
      accountList;

  }

  /**
   * Retrieve accounts metadata from search.
   *
   * @param exchange Instance of the type {@link HttpServerExchange}.
   * @param searchCriteria The search criteria and keyword to search matches.
   *
   * @return A list of the type {@link AccountMetadata}.
   */
  public static List<AccountMetadata> retrieveAccountsMetadata(final HttpServerExchange       exchange,
                                                               final Map<String,List<String>> searchCriteria) {

    final var timeZone = getHeader(
                           exchange,
                           X_TIME_ZONE);

    final var lang = getHeader(
                       exchange,
                       X_LANG_CODE);

    final var buyerId =  retrieveBuyerRfcByUserId(
                           getHeader(
                             exchange,
                             X_USER_ID),
                           getHeader(
                             exchange,
                             X_ROLE_CODE));

    if (isNotBlankCollection(
          searchCriteria
            .get(
              STD_C1.name())) ||
        isNotBlankCollection(
          searchCriteria
            .get(
              STD_C2.name()))) {

      return
        retrieveAccounts4StdSearch2Xls(
          timeZone,
          lang,
          buyerId,
          searchCriteria);

    } else {

      return
        retrieveAccounts4AdvSearch2Xls(
          timeZone,
          lang,
          buyerId,
          searchCriteria);

    }

  }

  /**
   * Retrieve the products catalog by description and language.
   *
   * @param description The description to find products.
   * @param lang Language used in the application.
   *
   * @return {@link List} of {@link CatalogItem} with the products.
   */
  public static List<CatalogItem> retrieveProductsCatalog(final String description,
                                                          final String lang) {

    try (final var sqlSession = CATALOGS_DB
                                  .getInstance()
                                  .openSession(
                                    TRUE)) {

      final var products = sqlSession
                             .getMapper(
                               CatalogsStaticItemsMapper.class)
                             .searchProductsAndServicesByDescription(
                               new CatalogFilter()
                                 .setDescription(
                                   description)
                                 .setLang(
                                   lang));

      LOGGER.info(
        LOG_SIZE_PRODUCTS_FOUND,
        products.size());

      return
        products;

    }

  }

  /**
   * Method to sum the direct employees with subcontracted employees.
   *
   * @param result The current connection result to extract multiple values.
   *
   * @return Total number of employees working in the company.
   */
  public static Integer sumEmployees(final Result result) {

    return
      intStreamRange(
          result,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          S5_B1_D1)
        .map(
          index -> get1nSize(
                     result,
                     ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                     qualifierNameOneIndex(
                       S5_B1_D3,
                       index))
                   +
                   get1nSize(
                     result,
                     ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                     qualifierNameOneIndex(
                       S5_B1_D4,
                       index)))
        .sum();

  }

  /**
   * Validate if the value has the keyword.
   *
   * @param value The value obtained from a qualifier.
   * @param keyword The keyword to validate.
   *
   * @return Returns <strong>true</strong> if the value has the keyword otherwise <strong>false</strong>.
   */
  public static boolean hasKeyword(final String value,
                                   final String keyword) {

    return
      isNotBlankString(value) &&
      value
        .toLowerCase()
        .contains(
          keyword.toLowerCase());

  }

  /**
   * Retrieve the information of each of the accounts
   *
   * @param accountId Account's unique identifier.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return Instance of the type {@link AccountMetadata}.
   */
  public static AccountMetadata retrieveAccount4XlsMetadata(final String accountId,
                                                            final String timeZone,
                                                            final String lang){

    try (final var accountsTable = H_BASE
                                     .getInstance()
                                     .getTable(
                                       ACCOUNTS_TABLE_NAME);
         final var usersTable = H_BASE
                                  .getInstance()
                                  .getTable(
                                    USERS_TABLE_NAME)) {

      final var accountsResult = accountsTable
                                   .get(
                                     newGet(
                                       accountId)
                                       .addFamily(
                                         ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                       .setFilter(
                                         PREFIXES_4_ADV_SEARCH_2_XLS));

      final var usersResult = usersTable
                                .get(
                                  newGet(
                                    accountId)
                                    .addFamily(
                                      USERS_COLUMN_FAMILY_DEFAULT)
                                    .setFilter(
                                      USER_PREFIXES));

      final var originCode = getCatalogItem(
                               accountsResult,
                               ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                               S1_B1_D2,
                               ORIGINS,
                               lang);

      return
        new AccountMetadata()
          .setBusinessName(
            getString(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B1_D1))
          .setRfc(
            getString(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B1_D4))
          .setOrigin(
            retrieveValue(
              originCode))
          .setCertificationType(
            retrieveValue(
              getCatalogItem(
                accountsResult,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                CERTIFICATION_D2,
                CERTIFICATION_TYPES,
                lang)))
          .setCertificationStatus(
            getString(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D30))
          .setCertificationDate(
            getDateString(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D4,
              timeZone,
              lang))
          .setLastValidation(
            retrieveLastValidation(
              accountsResult,
              timeZone,
              lang))
          .setCertificationExpirateDate(
            getDateString(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D5,
              timeZone,
              lang))
          .setLegalEntity(
            retrieveValue(
              getCatalogItem(
                accountsResult,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                S1_B1_D6,
                LEGAL_ENTITIES,
                lang)))
          .setCountry(
            retrieveValue(
              getCatalogItem(
                accountsResult,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                S1_B2_D1,
                COUNTRIES,
                lang)))
          .setSizeCompany(
            retrieveValue(
              getCatalogItem(
                accountsResult,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                S5_D2,
                COMPANY_SIZE,
                lang)))
          .setEconomicActivity(
            retrieveValue(
              getCatalogItem(
                accountsResult,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                S1_B1_D7,
                ECONOMIC_ACTIVITIES,
                lang)))
          .setWebSite(
            getString(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B1_D9))
          .setState(
            getString(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B3_D3))
          .setCity(
            getString(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B3_D6))
          .setMunicipality(
            getString(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S1_B3_D4))
          .setDistrict(
            LOCAL == findOrigin(originCode)
              ? retrieveValue(
                  getCatalogItem(
                    accountsResult,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B3_D5,
                    ZIP_CODES,
                    lang))
              : getString(
                  accountsResult,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  S1_B3_D12))
          .setStretName(
            format(
              LEGAL_DIRECTION_PATTERN,
              getStringOrEmpty(
                accountsResult,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                S1_B3_D7),
              getStringOrEmpty(
                accountsResult,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                S1_B3_D9),
              getStringOrEmpty(
                accountsResult,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                S1_B3_D8)))
          .setName(
            getString(
              usersResult,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D5))
          .setFirstSurname(
            getString(
              usersResult,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D6))
          .setSecondSurname(
            getStringOrEmpty(
              usersResult,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D7))
          .setMobilePhone(
            getString(
              usersResult,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D10))
          .setEmail(
            getString(
              usersResult,
              USERS_COLUMN_FAMILY_DEFAULT,
              USER_D12))
          .setRecursosHumanos(
            sumEmployees(
              accountsResult))
          .setScoreTotal(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D19))
          .setScoreLegal(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D12))
          .setScoreFiscal(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D18))
          .setScoreEthicsIntegrity(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D11))
          .setScoreFinancial(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D16))
          .setScoreCommercial(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D10))
          .setScoreQuality(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D17))
          .setScoreHealth(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D15))
          .setScoreEnvironment(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D14))
          .setScoreOperation(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D9))
          .setScoreEconomic(
            getFloat(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              CERTIFICATION_D13))
          .setCertificateQuality(
            getBoolean(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S6_B1_D3))
          .setCertificateHealthSafety(
            getBoolean(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S6_B2_D3))
          .setCertificateEnvironmental(
            getBoolean(
              accountsResult,
              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
              S6_B3_D3))
          .setObservationList(
            retrieve4Matches(
              accountsResult,
              accountId,
              lang))
          .setDisabledSfp(
            retrieveMatches4Blacklists(
              accountsResult))
          .setRepseCertificateStatus(
            TRUE
              .equals(
                getBoolean(
                  accountsResult,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  S1_B5_D5))
              ? retrieveCatalogItemDescriptionByCode(
                  REPSE_STATUS,
                  getString(
                    accountsResult,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D34),
                  lang)
              : null);

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Search a buyer association by unique identifier.
   *
   * @param accountId Account's unique identifier.
   * @param buyerId Buyer's unique identifier.
   *
   * @return <strong>true</strong> if the buyer association exist, <strong>false</strong> in other case.
   */
  public static boolean isExistingBuyerAssociation(final String accountId,
                                                   final String buyerId) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               BUYERS_TABLE_NAME)) {

      final var rowKey = table
                           .get(
                             newGet(
                               format(
                                 BUYER_ASSOCIATION_ROW_KEY_PATTERN,
                                 buyerId,
                                 accountId))
                               .addFamily(
                                 BUYERS_COLUMN_FAMILY_DEFAULT)
                               .addColumn(
                                 BUYERS_COLUMN_FAMILY_DEFAULT,
                                 BUYER_ASSOCIATION_D1)
                               .setFilter(
                                 FILTER_4_BUYER_ASSOCIATION_STATUS))
                           .getRow();

      final var buyerAssociationId = Bytes
                                       .toString(
                                         rowKey);

      LOGGER.info(
        LOG_BUYER_ASSOCIATION_ID_FOUND,
        null == rowKey
          ? null
          : buyerAssociationId);

      return
        null != rowKey;

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve account rows for the standard search.
   *
   * @param exchange Instance of the type {@link HttpServerExchange}.
   * @param buyerId BuyerId's unique identifier.
   * @param searchCriteria Instance of the type {@link Map} with the search criteria.
   *
   * @return A list of the type {@link AccountRetrieveAll}.
   */
  private static List<AccountRetrieveAll> retrieveAccounts4StdSearch(final HttpServerExchange       exchange,
                                                                     final String                   buyerId,
                                                                     final Map<String,List<String>> searchCriteria) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(ACCOUNTS_TABLE_NAME);
         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           new FilterList(
                                             MUST_PASS_ALL,
                                             PREFIXES_4_STD_SEARCH,
                                             FILTER_4_IS_SEARCHABLE)))) {

      if (null == resultScanner) {

        return emptyList();

      }

      final var timeZone = getHeader(
                             exchange,
                             X_TIME_ZONE);

      final var lang = getHeader(
                         exchange,
                         X_LANG_CODE);

      final var pageOffset = parseInt(
                               getHeader(
                                 exchange,
                                 X_PAGE_OFFSET));

      final var pageSize = parseInt(
                             getHeader(
                               exchange,
                               X_PAGE_SIZE));

      final var origins = retrieveCatalogMap(
                            ORIGINS,
                            lang);

      final var certificationTypes = retrieveCatalogMap(
                                       CERTIFICATION_TYPES,
                                       lang);

      final var economicActivities = retrieveCatalogMap(
                                       ECONOMIC_ACTIVITIES,
                                       lang);

      final var countries = retrieveCatalogMap(
                              COUNTRIES,
                              lang);

      final var companySize = retrieveCatalogMap(
                                COMPANY_SIZE,
                                lang);

      return
        streamParallel4ResultScanner(
          resultScanner)
          .filter(
            result -> isAccountEligible4StdSearch(
                        result,
                        lang,
                        buyerId,
                        searchCriteria))
          .skip(
            (long) pageOffset * pageSize)
          .limit(
            pageSize)
          .map(
            result -> new AccountRetrieveAll()
                        .setD0(
                          encode(
                            Bytes
                              .toString(
                                result
                                  .getRow())))
                        .setD1(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            ACCOUNT_D2))
                        .setD2(
                          getString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D1))
                        .setD3(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D2,
                            certificationTypes))
                        .setD5(
                          getDateString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D4,
                            timeZone,
                            lang))
                        .setD6(
                          getDateString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D5,
                            timeZone,
                            lang))
                        .setD7(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D9))
                        .setD8(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D10))
                        .setD9(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D11))
                        .setD10(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D12))
                        .setD11(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D13))
                        .setD12(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D14))
                        .setD13(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D15))
                        .setD14(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D16))
                        .setD15(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D17))
                        .setD16(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D18))
                        .setD17(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D19))
                        .setD18(
                          getString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B1_D1))
                        .setD19(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B1_D2,
                            origins))
                        .setD20(
                          getString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B1_D4))
                        .setD21(
                          getString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B1_D5))
                        .setD22(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B1_D7,
                            economicActivities))
                        .setD23(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B2_D1,
                            countries))
                        .setD24(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S5_D2,
                            companySize))
                        .setD25(
                          sumEmployees(
                            result))
                        .setD26(
                          getBoolean(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S6_B1_D3))
                        .setD27(
                          getBoolean(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S6_B2_D3))
                        .setD28(
                          getBoolean(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S6_B3_D3))
                        .setD35(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D30)))
          .collect(
            toList());

    }  catch (final Exception exc) {

    throw
      new PersistenceException(exc);

  }

  }

  /**
   * Retrieve account rows for the advanced search.
   *
   * @param exchange Instance of the type {@link HttpServerExchange}.
   * @param buyerId BuyerId's unique identifier.
   * @param searchCriteria Instance of the type {@link Map} with the search criteria.
   *
   * @return A list of the type {@link AccountRetrieveAll}.
   */
  private static List<AccountRetrieveAll> retrieveAccounts4AdvSearch(final HttpServerExchange       exchange,
                                                                     final String                   buyerId,
                                                                     final Map<String,List<String>> searchCriteria) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME);
         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           new FilterList(
                                             MUST_PASS_ALL,
                                             PREFIXES_4_ADV_SEARCH,
                                             FILTER_4_IS_SEARCHABLE)))) {

      if (null == resultScanner) {

        return emptyList();

      }

      final var timeZone = getHeader(
                             exchange,
                             X_TIME_ZONE);

      final var lang = getHeader(
                         exchange,
                         X_LANG_CODE);

      final var pageOffset = parseInt(
                               getHeader(
                                 exchange,
                                 X_PAGE_OFFSET));

      final var pageSize = parseInt(
                             getHeader(
                               exchange,
                               X_PAGE_SIZE));

      final var origins = retrieveCatalogMap(
                            ORIGINS,
                            lang);

      final var certificationTypes = retrieveCatalogMap(
                                       CERTIFICATION_TYPES,
                                       lang);

      final var economicActivities = retrieveCatalogMap(
                                       ECONOMIC_ACTIVITIES,
                                       lang);

      final var countries = retrieveCatalogMap(
                              COUNTRIES,
                              lang);

      final var companySize = retrieveCatalogMap(
                                COMPANY_SIZE,
                                lang);

      return
        streamParallel4ResultScanner(
          resultScanner)
          .filter(
            result -> isAccountEligible4AdvSearch(
                        result,
                        buyerId,
                        searchCriteria))
          .skip(
            (long) pageOffset * pageSize)
          .limit(
            pageSize)
          .map(
            result -> new AccountRetrieveAll()
                        .setD0(
                          encode(
                            Bytes
                              .toString(
                                result
                                  .getRow())))
                        .setD1(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            ACCOUNT_D2))
                        .setD2(
                          getString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D1))
                        .setD3(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D2,
                            certificationTypes))
                        .setD5(
                          getDateString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D4,
                            timeZone,
                            lang))
                        .setD6(
                          getDateString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D5,
                            timeZone,
                            lang))
                        .setD7(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D9))
                        .setD8(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D10))
                        .setD9(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D11))
                        .setD10(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D12))
                        .setD11(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D13))
                        .setD12(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D14))
                        .setD13(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D15))
                        .setD14(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D16))
                        .setD15(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D17))
                        .setD16(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D18))
                        .setD17(
                          getFloat(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D19))
                        .setD18(
                          getString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B1_D1))
                        .setD19(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B1_D2,
                            origins))
                        .setD20(
                          getString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B1_D4))
                        .setD21(
                          getString(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B1_D5))
                        .setD22(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B1_D7,
                            economicActivities))
                        .setD23(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S1_B2_D1,
                            countries))
                        .setD24(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S5_D2,
                            companySize))
                        .setD25(
                          sumEmployees(
                            result))
                        .setD26(
                          getBoolean(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S6_B1_D3))
                        .setD27(
                          getBoolean(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S6_B2_D3))
                        .setD28(
                          getBoolean(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            S6_B3_D3))
                        .setD35(
                          getCatalogItem(
                            result,
                            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                            CERTIFICATION_D30)))
          .collect(
            toList());

    } catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve account rows for the standard search to xls metadata.
   *
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   * @param buyerId BuyerId's unique identifier.
   * @param searchCriteria Instance of the type {@link Map} with the search criteria.
   *
   * @return A list of the type {@link AccountMetadata}.
   */
  private static List<AccountMetadata> retrieveAccounts4StdSearch2Xls(final String timeZone,
                                                                      final String lang,
                                                                      final String buyerId,
                                                                      final Map<String,List<String>> searchCriteria) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME);
         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           new FilterList(
                                             MUST_PASS_ALL,
                                             PREFIXES_4_STD_SEARCH_2_XLS,
                                             FILTER_4_IS_SEARCHABLE)))) {

      if (null == resultScanner) {

        return emptyList();

      }

      final var origins = retrieveCatalogMap(
                            ORIGINS,
                            lang);

      final var certificationTypes = retrieveCatalogMap(
                                       CERTIFICATION_TYPES,
                                       lang);

      final var legalEntities = retrieveCatalogMap(
                                  LEGAL_ENTITIES,
                                  lang);

      final var countries = retrieveCatalogMap(
                              COUNTRIES,
                              lang);

      final var companySize = retrieveCatalogMap(
                                COMPANY_SIZE,
                                lang);

      final var economicActivities = retrieveCatalogMap(
                                       ECONOMIC_ACTIVITIES,
                                       lang);

      return
        streamParallel4ResultScanner(
          resultScanner)
          .filter(
            result -> isAccountEligible4StdSearch(
                        result,
                        lang,
                        buyerId,
                        searchCriteria))
          .map(
            (result) -> {

              final var accountMetadata = new AccountMetadata();

              final var originCode = getCatalogItem(
                                      result,
                                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                      S1_B1_D2,
                                      origins);

              final var accountId = Bytes
                                      .toString(
                                        result
                                          .getRow());

              accountMetadata
                .setBusinessName(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B1_D1))
                .setRfc(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B1_D4))
                .setOrigin(
                  retrieveValue(
                    originCode))
                .setCertificationType(
                  retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      CERTIFICATION_D2,
                      certificationTypes)))
                .setCertificationStatus(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D30))
                .setCertificationDate(
                  getDateString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D4,
                    timeZone,
                    lang))
                .setLastValidation(
                  retrieveLastValidation(
                    result,
                    timeZone,
                    lang))
                .setCertificationExpirateDate(
                  getDateString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D5,
                    timeZone,
                    lang))
                .setLegalEntity(
                  retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B1_D6,
                      legalEntities)))
                .setCountry(
                  retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B2_D1,
                      countries)))
                .setSizeCompany(
                  retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S5_D2,
                      companySize)))
                .setEconomicActivity(
                  retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B1_D7,
                      economicActivities)))
                .setWebSite(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B1_D9))
                .setState(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B3_D3))
                .setCity(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B3_D6))
                .setMunicipality(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B3_D4))
                .setDistrict(
                  LOCAL == findOrigin(originCode)
                    ? retrieveValue(
                        getCatalogItem(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          S1_B3_D5,
                          ZIP_CODES,
                          lang))
                    : getString(
                        result,
                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                        S1_B3_D12))
                .setStretName(
                  format(
                    LEGAL_DIRECTION_PATTERN,
                    getStringOrEmpty(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B3_D7),
                    getStringOrEmpty(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B3_D9),
                    getStringOrEmpty(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B3_D8)))
                .setRecursosHumanos(
                  sumEmployees(
                    result))
                .setScoreTotal(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D19))
                .setScoreLegal(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D12))
                .setScoreFiscal(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D18))
                .setScoreEthicsIntegrity(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D11))
                .setScoreFinancial(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D16))
                .setScoreCommercial(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D10))
                .setScoreQuality(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D17))
                .setScoreHealth(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D15))
                .setScoreEnvironment(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D14))
                .setScoreOperation(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D9))
                .setScoreEconomic(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D13))
                .setCertificateQuality(
                  getBoolean(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S6_B1_D3))
                .setCertificateHealthSafety(
                  getBoolean(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S6_B2_D3))
                .setCertificateEnvironmental(
                  getBoolean(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S6_B3_D3))
                .setObservationList(
                  retrieve4Matches(
                    result,
                    accountId,
                    lang))
                .setDisabledSfp(
                  retrieveMatches4Blacklists(
                    result))
                .setRepseCertificateStatus(
                  TRUE
                    .equals(
                      getBoolean(
                        result,
                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                        S1_B5_D5))
                    ? retrieveCatalogItemDescriptionByCode(
                        REPSE_STATUS,
                        getString(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          CERTIFICATION_D34),
                        lang)
                    : null);

          retrieveUserValues4XlsMetadata(
            accountId,
            accountMetadata);

              return
                accountMetadata;

            })
          .collect(
            toList());

    }  catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve account rows for the advanced search to xls metadata.
   *
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   * @param buyerId BuyerId's unique identifier.
   * @param searchCriteria Instance of the type {@link Map} with the search criteria.
   *
   * @return A list of the type {@link AccountMetadata}.
   */
  private static List<AccountMetadata> retrieveAccounts4AdvSearch2Xls(final String timeZone,
                                                                      final String lang,
                                                                      final String buyerId,
                                                                      final Map<String,List<String>> searchCriteria) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               ACCOUNTS_TABLE_NAME);
         final var resultScanner = table
                                     .getScanner(
                                       new Scan()
                                         .addFamily(
                                           ACCOUNTS_COLUMN_FAMILY_DEFAULT)
                                         .setFilter(
                                           new FilterList(
                                             MUST_PASS_ALL,
                                             PREFIXES_4_ADV_SEARCH_2_XLS,
                                             FILTER_4_IS_SEARCHABLE)))) {

      if (null == resultScanner) {

        return emptyList();

      }

      final var origins = retrieveCatalogMap(
                            ORIGINS,
                            lang);

      final var certificationTypes = retrieveCatalogMap(
                                       CERTIFICATION_TYPES,
                                       lang);

      final var legalEntities = retrieveCatalogMap(
                                  LEGAL_ENTITIES,
                                  lang);

      final var countries = retrieveCatalogMap(
                              COUNTRIES,
                              lang);

      final var companySize = retrieveCatalogMap(
                                COMPANY_SIZE,
                                lang);

      final var economicActivities = retrieveCatalogMap(
                                       ECONOMIC_ACTIVITIES,
                                       lang);

      return
        streamParallel4ResultScanner(
          resultScanner)
          .filter(
            result -> isAccountEligible4AdvSearch(
                        result,
                        buyerId,
                        searchCriteria))
          .map(
            (result) -> {

              final var accountMetadata = new AccountMetadata();

              final var originCode = getCatalogItem(
                                       result,
                                       ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                       S1_B1_D2,
                                       origins);

              final var accountId = Bytes
                                      .toString(
                                        result
                                          .getRow());

              accountMetadata
                .setBusinessName(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B1_D1))
                .setRfc(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B1_D4))
                .setOrigin(
                  retrieveValue(
                    originCode))
                .setCertificationType(
                  retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      CERTIFICATION_D2,
                      certificationTypes)))
                .setCertificationStatus(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D30))
                .setCertificationDate(
                  getDateString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D4,
                    timeZone,
                    lang))
                .setLastValidation(
                  retrieveLastValidation(
                    result,
                    timeZone,
                    lang))
                .setCertificationExpirateDate(
                  getDateString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D5,
                    timeZone,
                    lang))
                .setLegalEntity(
                  retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B1_D6,
                      legalEntities)))
                .setCountry(
                  retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B2_D1,
                      countries)))
                .setSizeCompany(
                  retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S5_D2,
                      companySize)))
                .setEconomicActivity(
                  retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B1_D7,
                      economicActivities)))
                .setWebSite(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B1_D9))
                .setState(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B3_D3))
                .setCity(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B3_D6))
                .setMunicipality(
                  getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B3_D4))
                .setDistrict(
                  LOCAL == findOrigin(originCode)
                    ? retrieveValue(
                    getCatalogItem(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B3_D5,
                      ZIP_CODES,
                      lang))
                    : getString(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S1_B3_D12))
                .setStretName(
                  format(
                    LEGAL_DIRECTION_PATTERN,
                    getStringOrEmpty(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B3_D7),
                    getStringOrEmpty(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B3_D9),
                    getStringOrEmpty(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_B3_D8)))
                .setRecursosHumanos(
                  sumEmployees(
                    result))
                .setScoreTotal(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D19))
                .setScoreLegal(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D12))
                .setScoreFiscal(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D18))
                .setScoreEthicsIntegrity(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D11))
                .setScoreFinancial(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D16))
                .setScoreCommercial(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D10))
                .setScoreQuality(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D17))
                .setScoreHealth(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D15))
                .setScoreEnvironment(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D14))
                .setScoreOperation(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D9))
                .setScoreEconomic(
                  getFloat(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D13))
                .setCertificateQuality(
                  getBoolean(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S6_B1_D3))
                .setCertificateHealthSafety(
                  getBoolean(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S6_B2_D3))
                .setCertificateEnvironmental(
                  getBoolean(
                    result,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S6_B3_D3))
                .setObservationList(
                  retrieve4Matches(
                    result,
                    accountId,
                    lang))
                .setDisabledSfp(
                  retrieveMatches4Blacklists(
                    result))
                .setRepseCertificateStatus(
                  TRUE
                    .equals(
                      getBoolean(
                        result,
                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                        S1_B5_D5))
                    ? retrieveCatalogItemDescriptionByCode(
                        REPSE_STATUS,
                        getString(
                          result,
                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                          CERTIFICATION_D34),
                        lang)
                    : null);

              retrieveUserValues4XlsMetadata(
                accountId,
                accountMetadata);

              return
                accountMetadata;

            })
          .collect(
            toList());


    }  catch (final Exception exc) {

      throw
        new PersistenceException(exc);

    }

  }

  /**
   * Retrieve the user values for xls metadata.
   *
   * @param accountId Account's unique identifier.
   * @param accountMetadata An instance of type {@link AccountMetadata} with values.
   */
  private static void retrieveUserValues4XlsMetadata(final String          accountId,
                                                     final AccountMetadata accountMetadata) {

    try (final var table = H_BASE
                             .getInstance()
                             .getTable(
                               USERS_TABLE_NAME)) {

      final var result = table
                           .get(
                             newGet(
                                 accountId)
                               .addFamily(
                                 USERS_COLUMN_FAMILY_DEFAULT)
                               .setFilter(
                                 USER_PREFIXES));

      accountMetadata
        .setName(
          getString(
            result,
            USERS_COLUMN_FAMILY_DEFAULT,
            USER_D5))
        .setFirstSurname(
          getString(
            result,
            USERS_COLUMN_FAMILY_DEFAULT,
            USER_D6))
        .setSecondSurname(
          getStringOrEmpty(
            result,
            USERS_COLUMN_FAMILY_DEFAULT,
            USER_D7))
        .setMobilePhone(
          getString(
            result,
            USERS_COLUMN_FAMILY_DEFAULT,
            USER_D10))
        .setEmail(
          getString(
            result,
            USERS_COLUMN_FAMILY_DEFAULT,
            USER_D12));

    } catch (final Exception exc) {

    throw
      new PersistenceException(exc);

    }

  }

  /**
   * Validate if the account is eligible for the standard search.
   *
   * @param result Instance of the type {@link Result} with the value to get.
   * @param lang Language used in the application.
   * @param buyerId Buyer's unique identifier.
   * @param searchCriteria Instance of the type {@link Map} with search criteria.
   *
   * @return Returns <strong>true</strong> if the account is eligible for the standard search,
   * <strong>false</strong> in other case.
   */
  private static boolean isAccountEligible4StdSearch(final Result                   result,
                                                     final String                   lang,
                                                     final String                   buyerId,
                                                     final Map<String,List<String>> searchCriteria) {

    final var isAccountEligible = new AtomicBoolean(TRUE);

    searchCriteria
      .entrySet()
      .stream()
      .parallel()
      .forEach(
        (element) -> {

          final var firstKeyword = element
                                     .getValue()
                                     .get(
                                       FIRST_ELEMENT_INDEX);

          switch (findSearchValue(
                    element
                      .getKey())) {

            case STD_C1:

              if (isBlankString(
                    retrieveCatalogItemDescriptionByCode(
                      PRODUCTS_AND_SERVICES,
                      firstKeyword,
                      lang))) {

                final var products = retrieveProductsCatalog(
                                       firstKeyword,
                                       lang);

                if (isNotBlankCollection(
                      products)) {

                  isAccountEligible
                    .compareAndSet(
                      TRUE,
                      areProductsMatches(
                        result,
                        products
                          .stream()
                          .parallel()
                          .map(
                            CatalogItem::getD1)
                          .collect(
                            toList())));

                } else {

                  isAccountEligible
                    .compareAndSet(
                      TRUE,
                      FALSE);

                }

              } else {

              isAccountEligible
                .compareAndSet(
                  TRUE,
                  areProductsMatches(
                    result,
                    element
                      .getValue()));

              }

              break;

            case STD_C2:

              final var businessName = getString(
                                         result,
                                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                         S1_B1_D1);

              final var rfcOrTin =  Bytes
                                      .toString(
                                        result
                                          .getRow());

              if (hasKeyword(
                    isNotBlankString(businessName)
                      ? businessName
                          .replaceAll(
                            PUNCTUATION_MARKS_REGEX,
                            EMPTY_STRING)
                      : businessName,
                    firstKeyword) ||
                  hasKeyword(
                    isNotBlankString(rfcOrTin)
                      ? rfcOrTin
                          .replaceAll(
                            PUNCTUATION_MARKS_REGEX,
                            EMPTY_STRING)
                      : rfcOrTin,
                    firstKeyword)) {

                isAccountEligible
                  .compareAndSet(
                    TRUE,
                    TRUE);

              }  else {

                isAccountEligible
                  .compareAndSet(
                    TRUE,
                    FALSE);

              }

              break;

          }

        });

    if (isAccountEligible.get()) {

      isAccountEligible
        .compareAndSet(
          TRUE,
          isAccountReachable(
            result,
            buyerId));

    }

    return
      isAccountEligible.get();

  }

  /**
   * Validate if the account is eligible for the advanced search.
   *
   * @param result Instance of the type {@link Result} with the value to get.
   * @param buyerId Buyer's unique identifier.
   * @param searchCriteria Instance of the type {@link Map} with search criteria.
   *
   * @return Returns <strong>true</strong> if the account is eligible for the advanced search,
   * <strong>false</strong> in other case.
   */
  private static boolean isAccountEligible4AdvSearch(final Result                   result,
                                                     final String                   buyerId,
                                                     final Map<String,List<String>> searchCriteria) {

    final var isAccountEligible = new AtomicBoolean(TRUE);

    if (isNameIndicated(searchCriteria)) {

      isAccountEligible
        .compareAndSet(
          TRUE,
          anyMemberIsEligible(
            searchCriteria,
            result));

    } else {

      searchCriteria
        .entrySet()
        .stream()
        .parallel()
        .forEach(
          (element) -> {

            final var firstKeyword = element
                                       .getValue()
                                       .get(
                                         FIRST_ELEMENT_INDEX);

            switch (findSearchValue(
                      element
                        .getKey())) {

              case ADV_C2:

                if (SIZE_TWO != element
                                  .getValue()
                                  .size()) {

                  isAccountEligible
                    .compareAndSet(
                      TRUE,
                      FALSE);

                } else {

                  final var initialDate = element
                                            .getValue()
                                            .stream()
                                            .mapToLong(
                                              Long::parseLong)
                                            .min()
                                            .orElse(
                                              NO_DATE);

                  final var endDate = element
                                        .getValue()
                                        .stream()
                                        .mapToLong(
                                          Long::parseLong)
                                        .max()
                                        .orElse(
                                          NO_DATE);

                  final var certificationCreationDate = getLong(
                                                          result,
                                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                          CERTIFICATION_D4);

                  if (null == certificationCreationDate) {

                    isAccountEligible
                      .compareAndSet(
                        TRUE,
                        FALSE);

                  } else {

                    isAccountEligible
                      .compareAndSet(
                        TRUE,
                        (certificationCreationDate >= initialDate &&
                         certificationCreationDate <= endDate));

                  }

                }

                break;

              case ADV_C3:

                if (SIZE_TWO != element
                                  .getValue()
                                  .size()) {

                  isAccountEligible
                    .compareAndSet(
                      TRUE,
                      FALSE);

                } else {

                  final var initialDateAdvC3 = element
                                                 .getValue()
                                                 .stream()
                                                 .mapToLong(
                                                   Long::parseLong)
                                                 .min()
                                                 .orElse(
                                                   NO_DATE);

                  final var endDateAdvC3 = element
                                             .getValue()
                                             .stream()
                                             .mapToLong(
                                               Long::parseLong)
                                             .max()
                                             .orElse(
                                               NO_DATE);

                  final var certificationExpireDate = getLong(
                                                        result,
                                                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                        CERTIFICATION_D5);

                  if (null == certificationExpireDate) {

                    isAccountEligible
                      .compareAndSet(
                        TRUE,
                        FALSE);

                  } else {

                    isAccountEligible
                      .compareAndSet(
                        TRUE,
                        (certificationExpireDate >= initialDateAdvC3 &&
                         certificationExpireDate <= endDateAdvC3));

                  }

                }

                break;

              case ADV_C4:

                isAccountEligible
                  .compareAndSet(
                    TRUE,
                    areProductsMatches(
                      result,
                      element
                        .getValue()));

                break;

              case ADV_C5:

                final var countries = intStreamRange(
                                          result,
                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                          S3_B1_D1)
                                        .parallel()
                                        .filter(
                                          index -> isNotDeletedListItem(
                                                     result,
                                                     ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                     S3_B1_D13,
                                                     index))
                                        .mapToObj(
                                          index -> getCatalogItemList(
                                                       result,
                                                       ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                       qualifierNameOneIndex(
                                                         S3_B1_D9,
                                                         index))
                                                     .stream()
                                                     .map(
                                                       CatalogItem::getD1)
                                                     .collect(
                                                       toList()))
                                        .flatMap(
                                          List::stream)
                                        .collect(
                                          toList());

                isAccountEligible
                  .compareAndSet(
                    TRUE,
                    isNotBlankCollection(countries) &&
                    countries
                      .stream()
                      .parallel()
                      .anyMatch(
                        country -> element
                          .getValue()
                          .stream()
                          .parallel()
                          .anyMatch(
                            country::equalsIgnoreCase)));

                break;

              case ADV_C6:

                final var states = intStreamRange(
                                       result,
                                       ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                       S3_B1_D1)
                                     .parallel()
                                     .filter(
                                       index -> isNotDeletedListItem(
                                                  result,
                                                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                  S3_B1_D13,
                                                  index))
                                     .mapToObj(
                                       index -> getCatalogItemList(
                                                    result,
                                                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                                    qualifierNameOneIndex(
                                                      S3_B1_D10,
                                                      index))
                                                  .stream()
                                                  .map(
                                                    CatalogItem::getD1)
                                                  .collect(
                                                    toList()))
                                    .flatMap(
                                      List::stream)
                                    .collect(
                                      toList());

                isAccountEligible
                  .compareAndSet(
                    TRUE,
                    isNotBlankCollection(states) &&
                    states
                      .stream()
                      .parallel()
                      .anyMatch(
                        state -> element
                          .getValue()
                          .stream()
                          .parallel()
                          .anyMatch(
                            state::equalsIgnoreCase)));

                break;

              case ADV_C7:

                if (isBlankCollection(
                      element
                        .getValue())) {

                  isAccountEligible
                    .compareAndSet(
                      TRUE,
                      FALSE);

                }

                final var companySize = getString(
                                          result,
                                          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                          S5_D2);

                if (isBlankString(companySize)) {

                  isAccountEligible
                    .compareAndSet(
                      TRUE,
                      FALSE);

                } else {

                  isAccountEligible
                    .compareAndSet(
                      TRUE,
                      element
                        .getValue()
                        .stream()
                        .parallel()
                        .anyMatch(
                          company -> element
                                       .getValue()
                                       .stream()
                                       .parallel()
                                       .anyMatch(
                                         companySize::equalsIgnoreCase)));

                }

                break;

              case ADV_C8:

                if (isBlankString(firstKeyword)) {

                  isAccountEligible
                    .compareAndSet(
                      TRUE,
                      FALSE);

                }

                isAccountEligible
                  .compareAndSet(
                    TRUE,
                    findLegalEntity(
                      firstKeyword) ==
                    findLegalEntity(
                      getString(
                        result,
                        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                        S1_B1_D6)));

                break;

            }

          });

    }

    if (isAccountEligible.get()) {

      if (parseBoolean(
            searchCriteria
              .get(
                ADV_C1.name())
              .get(
                FIRST_ELEMENT_INDEX))) {

        isAccountEligible
          .compareAndSet(
            TRUE,
            isAccountReachable(
              result,
              buyerId));

      } else {

        isAccountEligible
          .compareAndSet(
            TRUE,
            isExistingBuyerAssociation(
              Bytes
                .toString(
                  result
                    .getRow()),
              buyerId));

      }

    }

    return
      isAccountEligible.get();

  }

  /**
   * Validate if any product matches.
   *
   * @param result Instance of the type {@link Result} with the qualifiers.
   * @param productCodes A {@link List} with the product codes searched.
   *
   * @return Returns <strong>true</strong> if any product match otherwise <strong>false</strong>.
   */
  private static boolean areProductsMatches(final Result result,
                                            final List<String> productCodes) {

    if (isBlankCollection(productCodes)) {

      return FALSE;

    }

    final var products = intStreamRange(
                             result,
                             ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                             S3_B1_D1)
                            .parallel()
                            .filter(
                              index -> isNotDeletedListItem(
                                         result,
                                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                         S3_B1_D13,
                                         index))
                            .mapToObj(
                              index -> getString(
                                         result,
                                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                                         qualifierNameOneIndex(
                                           S3_B1_D11,
                                           index)))
                            .collect(
                              toList());

    return
      isNotBlankCollection(products) &&
      products
        .stream()
        .parallel()
        .anyMatch(
          product -> productCodes
                       .stream()
                       .parallel()
                       .anyMatch(
                         product::equalsIgnoreCase));

  }

  /**
   * Validate if the account is reachable.
   *
   * @param result Instance of the type {@link Result} with the qualifiers.
   * @param buyerId Buyer's unique identifier.
   *
   * @return Returns <strong>true</strong> if the account is reachable, otherwise <strong>false</strong>.
   */
  private static boolean isAccountReachable(final Result result,
                                            final String buyerId) {

    final var isReachable = getBoolean(
                              result,
                              ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                              ACCOUNT_D13);

    if (FALSE
         .equals(
           isReachable)) {

      return
        isExistingBuyerAssociation(
          Bytes
            .toString(
              result
                .getRow()),
          buyerId);

    }

    return TRUE;

  }

  /**
   * Method that defines the date of the last validation.
   *
   * @param result Instance of the type {@link Result} with the qualifiers.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return Instance of the type {@link String}.
   */
  private static String retrieveLastValidation(final Result result,
                                               final String timeZone,
                                               final String lang) {

    if(UPDATE_FINALIZED == findPostCertificationStatus(
                             getString(
                               result,
                               ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                               ACCOUNT_D8))) {

     return
       getDateString(
        result,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        ACCOUNT_D12,
        timeZone,
        lang);

    } else {

      return
        getDateString(
          result,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          DISCLAIMER_S2,
          timeZone,
          lang);

    }

  }

  /**
   * Retrieve value for a {@link CatalogItem}.
   *
   * @param catalogItem Instance of the type {@link CatalogItem}.
   *
   * @return Instance of the type {@link String}.
   */
  private static String retrieveValue(final CatalogItem catalogItem) {

    return
      isNotBlankCatalogItem(catalogItem)
        ? catalogItem.getD2()
        : EMPTY_STRING;

  }

  /**
   * Retrieve the payload of the matches required to the blacklists.
   *
   * @param result The current connection result to extract multiple values.
   *
   * @return Instance of the type {@link Integer}.
   */
  private static Integer retrieveMatches4Blacklists(final Result result) {

    return
      (int) intStreamRange(
                result,
                ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                MT_D1)
              .filter(
                index -> S1B1
                           .qualifierPattern()
                           .equalsIgnoreCase(
                             getString(
                               result,
                               ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                               qualifierNameOneIndex(
                                 MT_D2,
                                 index))) &&

                         SFP
                           .toString()
                           .equalsIgnoreCase(
                             getString(
                               result,
                               ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                               qualifierNameOneIndex(
                                 MT_D3,
                                 index))))
              .count();

  }

  /**
   * Retrieve the matches of status code.
   *
   * @param result The current connection result to extract multiple values.
   * @param accountId Account's unique identifier.
   * @param lang Language used in the application.
   *
   * @return Instance of the type {@link Integer}.
   */
  private static Integer retrieve4Matches(final Result result,
                                          final String accountId,
                                          final String lang) {

    final var matchesSummaries = retrieveMatches4SubjectsSummaries(result);

    LOGGER.info(
      LOG_MATCHES_FOR_SUBJECTS_RETRIEVED,
      accountId,
      matchesSummaries);

    final var subjectsCatalog = retrieveSubjectsCatalog(
                                  accountId,
                                  lang);

    return
      subjectsCatalog
        .stream()
        .map(
          item -> new ComparisonAccounts()
                    .setD2(
                      (int) matchesSummaries
                              .stream()
                              .filter(
                                matchesSubjectSummary -> item
                                                           .getD1()
                                                           .equalsIgnoreCase(
                                                             matchesSubjectSummary
                                                               .getD1()) &&
                                                         CONFIRMED == findMatchesStatus(
                                                                        matchesSubjectSummary
                                                                          .getD2()))
                              .count()))
        .collect(
          toList())
        .stream()
        .mapToInt(
          ComparisonAccounts::getD2)
        .sum();

  }

  /**
   * Retrieve the payload of the matches required to the subjects summaries.
   *
   * @param result The current connection result to extract multiple values.
   *
   * @return List of the type {@link CatalogItem} with the payload.
   */
  private static List<CatalogItem> retrieveMatches4SubjectsSummaries(final Result result) {

    return
      intStreamRange(
          result,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          MT_D1)
        .mapToObj(
          index -> new CatalogItem()
                     .setD1(
                       getString(
                         result,
                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                         qualifierNameOneIndex(
                           MT_D2,
                           index)))
                     .setD2(
                       getString(
                         result,
                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                         qualifierNameOneIndex(
                           MT_D4,
                           index))))
        .collect(
          toList());

  }

}
