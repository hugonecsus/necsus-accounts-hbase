package mx.com.necsus.config;

import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNTS_COMPARISON_EN;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNTS_COMPARISON_ES;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNTS_LIST_EN;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNTS_LIST_ES;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNT_SUBREPORT_EN;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNT_SUBREPORT_ES;
import static mx.com.necsus.util.JasperReportsCommons.createMapWithReportTemplatesCompiled;
import static mx.com.necsus.util.Strings.LOG_INITIALIZE;
import static mx.com.necsus.util.Strings.LOG_INITIALIZED;
import static org.apache.logging.log4j.LogManager.getLogger;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import mx.com.necsus.domain.catalog.ReportTemplates;
import mx.com.necsus.util.Strings;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.logging.log4j.Logger;

/**
 * Singleton for the {@link JasperReport} compiled templates.
 */
public enum JasperReportsTemplates {

  /**
   * Instance of the <em>Singleton</em>.
   */
  JASPER_REPORTS_TEMPLATES;

  /**
   * Logger for the singleton.
   */
  private final Logger logger = getLogger(JasperReportsTemplates.class);

  /**
   * Size for an empty {@link Map}.
   */
  private static final int EMPTY_MAP_SIZE = 0;

  /**
   * Listing of {@link ReportTemplates} to be compiled.
   */
  private final List<ReportTemplates> REPORT_TEMPLATES_LIST = List
                                                                .of(
                                                                  ACCOUNTS_LIST_ES,
                                                                  ACCOUNTS_LIST_EN,
                                                                  ACCOUNT_SUBREPORT_ES,
                                                                  ACCOUNT_SUBREPORT_EN,
                                                                  ACCOUNTS_COMPARISON_ES,
                                                                  ACCOUNTS_COMPARISON_EN);

  /**
   * {@link Map} with the paths for the compiled templates by {@link ReportTemplates} name.
   *
   * @return {@link Map} with the paths for the compiled templates by {@link ReportTemplates} name.
   */
  @Getter
  private final Map<ReportTemplates, String> instance = initialize();

  /**
   * Log the {@link Strings#LOG_INITIALIZED}.
   */
  public void logInitialized() {

    logger.info(
      LOG_INITIALIZED,
      JasperReportsTemplates
        .class
        .getSimpleName());

  }
  /**
   * Initialize {@link #instance}.
   *
   * @return {@link Map} with the paths for the compiled templates by {@link ReportTemplates} name.
   */
  private Map<ReportTemplates, String> initialize() {

    if (logger.isDebugEnabled()) {

      logger.debug(
        LOG_INITIALIZE,
        JasperReportsTemplates
          .class
          .getSimpleName());

    }

    final var instanceValue = createMapWithReportTemplatesCompiled(REPORT_TEMPLATES_LIST);

    if (isInitialized(instanceValue)) {

      return instanceValue;

    } else {

      throw
        new SingletonException(ApplicationProperties.class);

    }

  }

  /**
   * Validate if singleton was initialized successfully.
   *
   * @param instanceValue {@link Map} with the key/values from {@link #REPORT_TEMPLATES_LIST}.
   *
   * @return Returns <em>true</em> if all validations was success, <em>false</em> in other case.
   */
  private boolean isInitialized(final Map<ReportTemplates, String> instanceValue) {

    return EMPTY_MAP_SIZE != instanceValue.size();

  }

}
