package mx.com.necsus.util;

import static mx.com.necsus.domain.catalog.CatalogCodes.INVITED_BY;
import static mx.com.necsus.domain.catalog.InvitedBy.NONE;
import static mx.com.necsus.persistence.BuyerAssociationStatusMapper.findBuyerAssociationStatus;
import static mx.com.necsus.persistence.BuyersAssociationMapper.isNotExistingBuyer;
import static mx.com.necsus.persistence.TypesHandler.JOIN_DELIMITER;
import static mx.com.necsus.util.Catalogs.isNotExistingCatalogItemCode;
import static mx.com.necsus.util.Payments.isNotExistingPlan;
import static mx.com.necsus.util.RequestBody.hasNotMatches;
import static mx.com.necsus.util.RequestBody.isBlankCatalogItem;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static org.apache.logging.log4j.LogManager.getLogger;
import static mx.com.necsus.util.AccessCrypto.decode;

import org.apache.logging.log4j.Logger;
import mx.com.necsus.domain.AccountUpdateOne;
import mx.com.necsus.domain.Notice;
import mx.com.necsus.domain.BuyerAssociationUpdateOne;
import mx.com.necsus.domain.catalog.BuyerAssociationStatus;

/**
 * Helper final class with the validations of the <strong>syntax</strong> and <strong>semantic</strong> for the
 * request body content.
 */
public final class AccountsRequestBody {

  /**
   * Logger for the helper.
   */
  private static final Logger LOGGER = getLogger(AccountsRequestBody.class);

  /**
   * Regular expression for the plan code.
   */
  private static final String PLAN_CODE_REGEX = "^[a-zA-Z0-9_]{1,60}$";

  /**
   * Regular expression for the email.
   */
  private static final String EMAIL_REGEX = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

  /**
   * Regular expression for the rfc.
   */
  private static final String RFC_REGEX = "^[a-zA-Z0-9]{12,13}$";

  /**
   * Private explicit default constructor for security.
   */
  private AccountsRequestBody() {
  }

  /**
   * Syntax validations for the content of <strong>accountUpdate</strong>.
   *
   * @param accountUpdate Instance of the type {@link AccountUpdateOne}.
   *
   * @return Return <strong>true</strong> if all syntax validations are passed.
   */
  public static boolean isInvalidAccountUpdateSyntax(final AccountUpdateOne accountUpdate) {

    if (!isBlankCatalogItem(accountUpdate.getD1()) &&
        !accountUpdate
          .getD1()
          .getD1()
          .matches(PLAN_CODE_REGEX)) {

      LOGGER.info("Invalid d1");

      return true;

    }

    return false;

  }

  /**
   * Semantic validations for the content of <strong>accountUpdate</strong>.
   *
   * @param accountId Account's unique identifier.
   * @param accountUpdate Instance of the type {@link AccountUpdateOne}.
   *
   * @return Return <strong>true</strong> if all semantic validations are passed.
   */
  public static boolean isInvalidAccountUpdateSemantic(final String accountId,
                                                       final AccountUpdateOne accountUpdate) {

    if (!isBlankCatalogItem(
          accountUpdate.getD1()) &&
        isNotExistingPlan(
          accountId,
          accountUpdate
            .getD1()
            .getD1())) {

      LOGGER.info("Not existing d1");

      return true;

    }

    return false;

  }

  /**
   * Semantic validations for the content of <strong>accountUpdate</strong>.
   *
   * @param accountUpdate Instance of the type {@link AccountUpdateOne}.
   *
   * @return Return <strong>true</strong> if all semantic validations are passed.
   */
  public static boolean isInvalidAccountPrivacySyntax(final AccountUpdateOne accountUpdate){

    if (accountUpdate == null) {

      return true;

    }

    if (!isBlankCatalogItem(
      accountUpdate.getD2()) &&
      isNotExistingCatalogItemCode(
        INVITED_BY,
        accountUpdate
          .getD2()
          .getD1())) {

      LOGGER.info("Not existing d2");

      return true;

    }

    if (!isBlankCatalogItem(
      accountUpdate.getD2()) &&
      NONE.name().equals(accountUpdate
        .getD2()
        .getD1())) {

      LOGGER.info("d2 could not be NONE for a private scope");

      return true;

    }

    return false;

  }

  /**
   * Semantic validations for the content of <strong>accountUpdate</strong>.
   *
   * @param accountUpdate Instance of the type {@link AccountUpdateOne}.
   *
   * @return Return <strong>true</strong> if all semantic validations are passed.
   */
  public static boolean isInvalidAccountUpdated(final AccountUpdateOne accountUpdate) {

    if (null == accountUpdate.getD3()) {

      LOGGER.info("Not existing d3");

      return true;

    }

    if (null != accountUpdate.getD3().getD8() &&
      !accountUpdate
        .getD3()
        .getD8()
        .trim()
        .matches(EMAIL_REGEX)) {

      LOGGER.info("Not existing or is wrong d8");

      return true;

    }

    return false;

  }

  /**
   * Semantic validations for the content of <strong>accountUpdate</strong>.
   *
   * @param notice Instance of the type {@link Notice}.
   *
   * @return Return <strong>true</strong> if all semantic validations are passed.
   */
  public static boolean isInvalidNotice(final Notice notice) {

    if (null == notice.getAccountId()) {

      LOGGER.info("Not existing accountId");

      return true;

    }

    if (null == notice
      .getAccountId()
      .split(JOIN_DELIMITER)[0]) {

      LOGGER.info("Not existing account Id");

      return true;

    }

    if (null == notice
      .getAccountId()
      .split(JOIN_DELIMITER)[1]) {

      LOGGER.info("Not existing Email");

      return true;

    }

    return false;

  }

  /**
   * Syntax validations for the content of <strong>buyerAssociationUpdateOne</strong>.
   *
   * @param buyerAssociationUpdateOne Instance of the type {@link BuyerAssociationUpdateOne}.
   *
   * @return Return <strong>true</strong> if all syntax validations are passed.
   */
  public static boolean isInvalidAccountAssociationUpdateSyntax(final BuyerAssociationUpdateOne buyerAssociationUpdateOne) {

    if (null == buyerAssociationUpdateOne) {

       LOGGER.info("Null d4");

       return true;

    }

    if (isBlankString(
         buyerAssociationUpdateOne
           .getD0(),
         "Require d0") ||
         isBlankCatalogItem(
           buyerAssociationUpdateOne
             .getD1(),
         "Require d1")) {

      return true;

    }

    return
      hasNotMatches(
        decode(
          buyerAssociationUpdateOne
            .getD0()),
        RFC_REGEX,
        "Invalid d0");

  }

  /**
   * Semantic validations for the content of <strong>associationUpdateOne</strong>.
   *
   * @param associationUpdateOne Instance of the type {@link BuyerAssociationUpdateOne}.
   *
   * @return Return <strong>true</strong> if all semantic validations are passed.
   */
  public static boolean isInvalidAccountAssociationUpdateSemantic(final BuyerAssociationUpdateOne associationUpdateOne) {

    if (BuyerAssociationStatus.NULL ==
         findBuyerAssociationStatus(
           associationUpdateOne
             .getD1())) {

       LOGGER.info("Not existing status");

       return true;

    }

    if (BuyerAssociationStatus.ASSOCIATED ==
         findBuyerAssociationStatus(
           associationUpdateOne
             .getD1())) {

       LOGGER.info("Invalid status");

       return true;

    }

    return
      isNotExistingBuyer(
        decode(
          associationUpdateOne
            .getD0()));

  }

}
