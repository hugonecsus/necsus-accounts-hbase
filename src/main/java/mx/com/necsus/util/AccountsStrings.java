package mx.com.necsus.util;

import mx.com.necsus.NecsusAccountsApplication;

/**
 * String constants for {@link NecsusAccountsApplication}.
 */
public final class AccountsStrings {

  /**
   * The path for <strong>accounts-retrieveAll</strong> operative.
   */
  public static final String ACCOUNTS_RETRIEVE_ALL_PATH = "/api/v1/accounts";

  /**
   * The path for <strong>accounts-retrieveOne</strong> operative.
   */
  public static final String ACCOUNTS_RETRIEVE_ONE_PATH = "/api/v1/accounts/{accountId}";

  /**
   * The path for <strong>accounts-patchOne</strong> operative.
   */
  public static final String ACCOUNTS_PATCH_ONE_PATH = "/api/v1/accounts/{accountId}";

  /**
   * The path for <strong>accounts-payments-retrieveAll</strong> operative.
   */
  public static final String ACCOUNTS_PAYMENTS_RETRIEVE_ALL_PATH = "/api/v1/accounts/{accountId}/payments";

  /**
   * The path for <strong>accounts-payments-retrieveOne</strong> operative.
   */
  public static final String ACCOUNTS_PAYMENTS_RETRIEVE_ONE_PATH = "/api/v1/accounts/{accountId}/payments/{paymentId}";

  /**
   * The path for <strong>accounts-payments-patchOne</strong> operative.
   */
  public static final String ACCOUNTS_PAYMENTS_PATCH_ONE_PATH = "/api/v1/accounts/{accountId}/payments/{paymentId}";

  /**
   * Log for <em>Size of products found '{}'</em>
   */
  public static final String LOG_SIZE_PRODUCTS_FOUND = "Size of products found '{}'";

  /**
   * Log for <em>request valuef of buyers associated handler get'</em>
   */
  public static final String LOG_REQUEST_BUYERS_ASSOCIATED_HANDLER_GET = "{} {} '{}' {} '{}' {} '{}' {} '{}' {} '{}' {} '{}'";

  /**
   * Log for <em>request valuef of buyers associated handler patch'</em>
   */
  public static final String LOG_REQUEST_BUYERS_ASSOCIATED_HANDLER_PATCH = "{} {} '{}' {} '{}' {} '{}' {} '{}'";

  /**
   * Log for <em>delete all articles for '{}'</em>
   */
  public static final String LOG_DELETE_B1_AND_B4_FROM_S3_FOR = "Delete block 1 and block2 from section 3 for '{}'";

  /**
   * Log for <em>account retrieved '{}'</em>
   */
  public static final String LOG_ACCOUNT_RETRIEVED = "Account retrieved '{}'";

  /**
   * Log for <em>complete {} '{}'</em>
   */
  public static final String LOG_COMPLETE = "Complete {} '{}'";

  /**
   * Log for <em>retrieve all accounts by matches.</em>
   */
  public static final String LOG_GET_ACCOUNTS_SEARCH_HANDLER = "{} {} '{}' {} '{}' {} '{}' {} '{}' {} '{}' {} '{}' {} '{}' {} '{}'";

  /**
   * Log for <em>size of accounts found'{}'</em>
   */
  public static final String LOG_SIZE_ACCOUNTS_FOUND = "Found '{}' accounts";

  /**
   * Log for <em>matches for subjects retrieved '{}' '{}'</em>
   */
  public static final String LOG_MATCHES_FOR_SUBJECTS_RETRIEVED = "Matches for subjects retrieved '{}' '{}'";

  /**
   * Log for <em>Buyer association id found '{}'</em>
   */
  public static final String LOG_BUYER_ASSOCIATION_ID_FOUND = "Buyer association id found '{}'";

  /**
   * Log for <em>Buyer id found '{}'</em>
   */
  public static final String LOG_BUYER_ID_FOUND = "Buyer id found '{}'";

  /**
   * Private explicit default constructor for security.
   */
  private AccountsStrings() {
    // Lombok @NoArgsConstructor does not support javadoc.
  }

}
