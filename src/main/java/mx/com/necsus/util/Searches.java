package mx.com.necsus.util;

import static mx.com.necsus.domain.catalog.SearchValues.ADV_C10;
import static mx.com.necsus.domain.catalog.SearchValues.ADV_C11;
import static mx.com.necsus.domain.catalog.SearchValues.ADV_C9;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B1_D1;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B1_D2;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B1_D3;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B2_D1;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B2_D2;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B2_D3;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B3_D1;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B3_D2;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B3_D3;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B4_D1;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B4_D2;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B4_D3;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B5_D1;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B5_D2;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B5_D3;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B7_D1;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B7_D10;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B7_D2;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B7_D3;
import static mx.com.necsus.persistence.Section4Qualifiers.S4_B7_D4;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B3_D11;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B3_D2;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B3_D5;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B3_D6;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B3_D7;
import static mx.com.necsus.persistence.TypesHandler.EMPTY_1N_SIZE;
import static mx.com.necsus.persistence.TypesHandler.FIRST_ELEMENT_INDEX;
import static mx.com.necsus.persistence.TypesHandler.get1nSize;
import static mx.com.necsus.persistence.TypesHandler.getString;
import static mx.com.necsus.persistence.TypesHandler.intStreamRange;
import static mx.com.necsus.util.CertificationsCommons.isNotDeletedListItem;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static mx.com.necsus.util.RequestBody.isNotBlankCollection;
import static mx.com.necsus.util.Strings.EMPTY_STRING;

import java.util.List;
import java.util.Map;

import org.apache.hadoop.hbase.client.Result;

/**
 * Helper class for operations with the account.
 */
public final class Searches {

  /**
   * Private explicit default constructor for security.
   */
  private Searches() {
  }

  /**
   * Validate if the name is indicated for searches.
   *
   * @param searchCriteria The search criteria and keyword map.
   *
   * @return Returns <strong>false</strong> if the cases matches, <strong>true</strong> in any other case.
   */
  public static boolean isNameIndicated(final Map<String, List<String>> searchCriteria) {

    return
      isNotBlankCollection(
        searchCriteria
          .get(ADV_C9.name())) ||
    isNotBlankCollection(
      searchCriteria
        .get(ADV_C10.name())) ||
    isNotBlankCollection(
      searchCriteria
        .get(ADV_C11.name()));

  }

  /**
   * Validate if the full name is the required for searches.
   *
   * @param searchCriteria The search criteria and keyword map.
   * @param result Instance of the type {@link Result} with the qualifiers.
   *
   * @return Returns <strong>false</strong> if the cases matches, <strong>true</strong> in any other case.
   */
  public static boolean anyMemberIsEligible(final Map<String, List<String>> searchCriteria,
                                            final Result                    result) {

    final var name = isNotBlankCollection(
                         searchCriteria
                           .get(ADV_C9.name()))
                       ? searchCriteria
                           .get(ADV_C9.name())
                           .get(FIRST_ELEMENT_INDEX)
                      : EMPTY_STRING;

    final var firstSurname = isNotBlankCollection(
                                 searchCriteria
                                   .get(ADV_C10.name()))
                               ? searchCriteria
                                   .get(ADV_C10.name())
                                   .get(FIRST_ELEMENT_INDEX)
                               : EMPTY_STRING;

    final var secondSurname = isNotBlankCollection(
                                  searchCriteria
                                    .get(ADV_C11.name()))
                                ? searchCriteria
                                    .get(ADV_C11.name())
                                    .get(FIRST_ELEMENT_INDEX)
                                : EMPTY_STRING;

    if (hasKeyword(
          result,
          S4_B1_D1,
          name) &&
        hasKeyword(
          result,
          S4_B1_D2,
          firstSurname) &&
         hasKeyword(
          result,
          S4_B1_D3,
          secondSurname)) {

      return true;

    }

    if (hasKeyword(
          result,
          S4_B2_D1,
          name) &&
        hasKeyword(
          result,
          S4_B2_D2,
          firstSurname) &&
        hasKeyword(
          result,
          S4_B2_D3,
          secondSurname)) {

      return true;

    }

    if (hasKeyword(
          result,
          S4_B3_D1,
          name) &&
        hasKeyword(
          result,
          S4_B3_D2,
          firstSurname) &&
        hasKeyword(
          result,
          S4_B3_D3,
          secondSurname)) {

      return true;

    }

    if (hasKeyword(
          result,
          S4_B4_D1,
          name) &&
        hasKeyword(
          result,
          S4_B4_D2,
          firstSurname) &&
        hasKeyword(
          result,
          S4_B4_D3,
          secondSurname)) {

      return true;

    }

    if (hasKeyword(
          result,
          S4_B5_D1,
          name) &&
        hasKeyword(
          result,
          S4_B5_D2,
          firstSurname) &&
        hasKeyword(
          result,
          S4_B5_D3,
          secondSurname)) {

      return true;

    }

    if (EMPTY_1N_SIZE != get1nSize(
                           result,
                           ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                           S4_B7_D1) &&
        intStreamRange(
            result,
            ACCOUNTS_COLUMN_FAMILY_DEFAULT,
            S4_B7_D1)
          .parallel()
          .filter(
            index -> isNotDeletedListItem(
                       result,
                       ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                       S4_B7_D10,
                       index))
          .anyMatch(
            index -> hasKeyword(
                       result,
                       qualifierNameOneIndex(
                         S4_B7_D2,
                         index),
                       name) &&
                     hasKeyword(
                       result,
                       qualifierNameOneIndex(
                         S4_B7_D3,
                         index),
                      firstSurname) &&
                     hasKeyword(
                       result,
                       qualifierNameOneIndex(
                         S4_B7_D4,
                         index),
                       secondSurname))) {

      return true;

    }

    return
      EMPTY_1N_SIZE != get1nSize(
                         result,
                         ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                         S5_B3_D2) &&
      intStreamRange(
          result,
          ACCOUNTS_COLUMN_FAMILY_DEFAULT,
          S5_B3_D2)
        .parallel()
        .filter(
          index -> isNotDeletedListItem(
                     result,
                     ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                     S5_B3_D11,
                     index))
        .anyMatch(
          index -> hasKeyword(
                     result,
                     qualifierNameOneIndex(
                       S5_B3_D5,
                       index),
                     name) &&
                   hasKeyword(
                     result,
                     qualifierNameOneIndex(
                       S5_B3_D6,
                       index),
                     firstSurname) &&
                   hasKeyword(
                     result,
                     qualifierNameOneIndex(
                       S5_B3_D7,
                       index),
                     secondSurname));

  }

  /**
   * Validate if the value has the keyword.
   *
   * @param result Instance of the type {@link Result} with the qualifiers.
   * @param qualifier The qualifier to retrieve value.
   * @param keyword The keyword to validate.
   *
   * @return Returns <strong>true</strong> if the value has the keyword otherwise <strong>false</strong>.
   */
  public static boolean hasKeyword(final Result result,
                                   final byte[] qualifier,
                                   final String keyword) {

    if (isBlankString(keyword)) {

      return true;

    }

    final var value = getString(
                      result,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      qualifier);

    if (isBlankString(value)) {

      return false;

    }

    return
      value
        .toLowerCase()
        .contains(
          keyword
            .toLowerCase());

  }

}
