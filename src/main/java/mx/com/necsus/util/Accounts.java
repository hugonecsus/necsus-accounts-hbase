package mx.com.necsus.util;

import static io.undertow.util.StatusCodes.CONFLICT;
import static java.lang.String.format;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static mx.com.necsus.config.JasperReportsTemplates.JASPER_REPORTS_TEMPLATES;
import static mx.com.necsus.domain.catalog.BuyerAssociationStatus.UNASSOCIATED;
import static mx.com.necsus.domain.catalog.CatalogCodes.BUYER_ASSOCIATION_STATUS;
import static mx.com.necsus.domain.catalog.CertificationTypes.ADV;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.NoticeTypes.BUYER_ASSOCIATION_UPDATING;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNT_SUBREPORT_EN;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNT_SUBREPORT_ES;
import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.persistence.AccountsMapper.retrieveCertificationTypeByPlan;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.BuyerAssociationStatusMapper.findBuyerAssociationStatus;
import static mx.com.necsus.persistence.BuyersAssociationMapper.retrieveAssociations;
import static mx.com.necsus.persistence.BuyersAssociationMapper.retrieveBuyers;
import static mx.com.necsus.persistence.BuyersAssociationMapper.retrieveUsersAssociated4Buyer;
import static mx.com.necsus.persistence.CertificationTypesMapper.findCertificationType;
import static mx.com.necsus.persistence.LanguagesMapper.findLanguage;
import static mx.com.necsus.persistence.QualifiersNames.qualifierNameOneIndex;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D1;
import static mx.com.necsus.persistence.Section5Qualifiers.S5_B1_D2;
import static mx.com.necsus.persistence.TypesHandler.FIRST_ELEMENT_INDEX;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.util.AccessCrypto.decode;
import static mx.com.necsus.util.AccessCrypto.encode;
import static mx.com.necsus.util.AccountsCommons.YEARS_TO_REDUCE_AFTER_LIMIT;
import static mx.com.necsus.util.AccountsCommons.YEARS_TO_REDUCE_BEFORE_LIMIT;
import static mx.com.necsus.util.AccountsCommons.isAfterFinancialLimitDate;
import static mx.com.necsus.util.Catalogs.retrieveCatalogMap;
import static mx.com.necsus.util.Catalogs.retrieveLimitPlanCode;
import static mx.com.necsus.util.Catalogs.retrievePlanCatalog;
import static mx.com.necsus.util.NoticesSender.sendRecord2NoticesTopic;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static mx.com.necsus.util.Strings.EMPTY_STRING;
import static mx.com.necsus.util.Strings.LOG_PAYLOAD_WITH_TWO;
import static mx.com.necsus.util.Time.now;
import static mx.com.necsus.util.TraceHeaders.DEFAULT_LANG;
import static mx.com.necsus.util.TracePayloads.RESPONSE_PAYLOAD;
import static org.apache.logging.log4j.LogManager.getLogger;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import mx.com.necsus.domain.AccountOwnerUpdateOne;
import mx.com.necsus.domain.AccountRetrieveOne;
import mx.com.necsus.domain.AccountUpdateOne;
import mx.com.necsus.domain.BuyerAssociationRetrieveOne;
import mx.com.necsus.domain.CatalogItem;
import mx.com.necsus.domain.Notice;
import mx.com.necsus.domain.Plan;
import mx.com.necsus.domain.catalog.BuyerAssociationStatus;
import mx.com.necsus.persistence.domain.Account;
import mx.com.necsus.persistence.domain.BuyerAssociation;

import io.undertow.server.HttpServerExchange;
import org.apache.hadoop.hbase.client.Put;
import org.apache.logging.log4j.Logger;

/**
 * Helper class for operations with the account.
 */
public final class Accounts {

  /**
   * Logger for the helper.
   */
  private static final Logger LOGGER = getLogger(Accounts.class);

  /**
   * Initial fiscal year size.
   */
  private static final Integer INITIAL_FISCAL_YEARS_SIZE = 3;

  /**
   * Minimum years to adjust.
   */
  private static final int MINIMUM_YEARS_2_ADJUST = 2;

  /**
   * Private explicit default constructor for security.
   */
  private Accounts() {
  }

  /**
   * Validate if plan received is valid apply for the flow.
   *
   * @param exchange Instance of the type {@link HttpServerExchange} for the response's flushing.
   * @param account Instance of the type {@link Account} with values from HBase.
   * @param accountUpdateOne Instance of the type {@link AccountUpdateOne} with values from user.
   *
   * @return Returns <strong>false</strong> if the cases matches, <strong>true</strong> in any other case.
   */
  public static boolean isInvalidPlan(final HttpServerExchange exchange,
                                      final Account            account,
                                      final AccountUpdateOne   accountUpdateOne) {

    final var currentCertificationType = findCertificationType(
                                           account
                                             .getCurrentCertificationType());

    final var updateCertificationType = findCertificationType(
                                          retrieveCertificationTypeByPlan(
                                            accountUpdateOne
                                              .getD1()));

    final var currentLimit = retrieveLimitPlanCode(
                               account
                                 .getCertificationPlan()
                                 .getD1());

    final var updateLimit = retrieveLimitPlanCode(
                              accountUpdateOne
                                .getD1()
                                .getD1());

    LOGGER.info(
      "Validating plan, currentCertificationType '{}' updateCertificationType '{}' currentLImit '{}' updateLimit '{}'",
      currentCertificationType,
      updateCertificationType,
      currentLimit,
      updateLimit);

    if (STD == currentCertificationType &&
        (STD == updateCertificationType ||
         ADV == updateCertificationType)) {

      return false;

    }

    if (ADV == currentCertificationType &&
        ADV == updateCertificationType &&
        currentLimit < updateLimit) {

      return false;

    }

    LOGGER.info(
      LOG_PAYLOAD_WITH_TWO,
      RESPONSE_PAYLOAD,
      CONFLICT,
      accountUpdateOne);

    exchange
      .setStatusCode(
        CONFLICT)
      .endExchange();

    return true;

  }

  /**
   * Validate if is it required to delete the block 1 and block 4 from section 3.
   *
   * @param account Instance of the type {@link Account} with values from HBase.
   * @param accountUpdateOne Instance of the type {@link AccountUpdateOne} with values from user.
   *
   * @return Returns <strong>false</strong> if the cases matches, <strong>true</strong> in any other case.
   */
  public static boolean isRequiredDeleteB1AndB4FromS3(final Account          account,
                                                      final AccountUpdateOne accountUpdateOne) {

    final var currentCertificationType = findCertificationType(
                                           account
                                             .getCurrentCertificationType());

    final var updateCertificationType = findCertificationType(
                                          account
                                            .getUpdateCertificationType());

    final var currentLimit = retrieveLimitPlanCode(
                               account
                                 .getCertificationPlan()
                                 .getD1());

    final var updateLimit = retrieveLimitPlanCode(
                              accountUpdateOne
                                .getD1()
                                .getD1());

    if (STD == currentCertificationType &&
        ADV == updateCertificationType) {

      return true;

    }

    if (ADV == currentCertificationType &&
        STD == updateCertificationType) {

      return true;

    }

    return
      (ADV == currentCertificationType &&
       ADV == updateCertificationType) &&
      (updateLimit < currentLimit);

  }

  /**
   * Fill the {@link Put} with the values of the <strong>section 5</strong> for a new certification.
   *
   * @param put Instance of the type {@link Put} to which the value will added.
   * @param signUpDate Account's sign up date time.
   */
  public static void createFiscalYears(final Put           put,
                                       final ZonedDateTime signUpDate) {

    final var initialFiscalYear = isAfterFinancialLimitDate(signUpDate)
                                    ? signUpDate.getYear() - YEARS_TO_REDUCE_AFTER_LIMIT
                                    : signUpDate.getYear() - YEARS_TO_REDUCE_BEFORE_LIMIT;

    LOGGER.info(
      "Create S5B1D1 signUpDate '{}' initialFiscalYear '{}'",
      signUpDate,
      initialFiscalYear);

    putValue(
      put,
      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
      S5_B1_D1,
      INITIAL_FISCAL_YEARS_SIZE);

    range(
        FIRST_ELEMENT_INDEX,
        INITIAL_FISCAL_YEARS_SIZE)
      .forEach(
        index -> putValue(
                   put,
                   ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                   qualifierNameOneIndex(
                     S5_B1_D2,
                     index),
                   initialFiscalYear - index));

  }

  /**
   * Fill the {@link Put} with the values of the <strong>section 5</strong> for a renewal.
   *
   * @param put Instance of the type {@link Put} to which the value will add.
   * @param renewalDate Account's renewal date time.
   * @param account Instance of the type {@link Account} with values from HBase.
   */
  public static void adjustFiscalYears4Renewal(final Put           put,
                                               final ZonedDateTime renewalDate,
                                               final Account       account) {

    LOGGER.info(
      "Adjust S5B1D1 renewalDate '{}' fiscalYearsSize '{}' lastFiscalYear '{}'",
      renewalDate,
      account
        .getFiscalYearsSize(),
      account
        .getLastFiscalYear());

    var years2Adjust = now().getYear() - renewalDate.getYear();

    if (MINIMUM_YEARS_2_ADJUST <= years2Adjust) {

      if (isAfterFinancialLimitDate(renewalDate)) {

        years2Adjust -= YEARS_TO_REDUCE_AFTER_LIMIT;

      } else {

        years2Adjust -= YEARS_TO_REDUCE_BEFORE_LIMIT;

      }

      putValue(
        put,
        ACCOUNTS_COLUMN_FAMILY_DEFAULT,
        S5_B1_D1,
        account
          .getPostCertificationVersion(),
        account
          .getFiscalYearsSize() + years2Adjust);

      range(
          FIRST_ELEMENT_INDEX,
          years2Adjust)
        .forEach(
          index -> putValue(
                     put,
                     ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                     qualifierNameOneIndex(
                       S5_B1_D2,
                       account
                         .getFiscalYearsSize() + index),
                     account
                       .getPostCertificationVersion(),
                     account
                       .getLastFiscalYear() + index + 1));

    }

  }

  /**
   * Merge the account <strong>privacy</strong> of the account.
   *
   * @param accountUpdate Instance of the type {@link AccountUpdateOne} with the data to put.
   * @param accountRetrieve Instance of the type {@link AccountRetrieveOne} with the data to put.
   */
  public static void mergeAccountPrivacy(final AccountUpdateOne accountUpdate,
                                         final AccountRetrieveOne accountRetrieve) {

    accountUpdate
      .setD2(
        retrieveValue(
          accountUpdate.getD2(),
          accountRetrieve.getD8()));
  }

  /**
   * Merge the account owner of current the account.
   *
   * @param accountOwnerUpdated Instance of the type {@link AccountOwnerUpdateOne} with the data to put.
   * @param accountOwnerRetrieved Instance of the type {@link AccountOwnerUpdateOne} with the data to put.
   */
  public static void mergeAccountOwner(final AccountOwnerUpdateOne accountOwnerUpdated,
                                       final AccountOwnerUpdateOne accountOwnerRetrieved) {

    accountOwnerUpdated
      .setD1(
        retrieveValue(
          accountOwnerUpdated.getD1(),
          accountOwnerRetrieved.getD1()))
      .setD2(
        retrieveValue(
          accountOwnerUpdated.getD2(),
          accountOwnerRetrieved.getD2()))
      .setD3(
        retrieveValue(
          accountOwnerUpdated.getD3(),
          accountOwnerRetrieved.getD3()))
      .setD4(
        retrieveValue(
          accountOwnerUpdated.getD4(),
          accountOwnerRetrieved.getD4()))
      .setD5(
        retrieveValue(
          accountOwnerUpdated.getD5(),
          accountOwnerRetrieved.getD5()))
      .setD6(
        retrieveValue(
          accountOwnerUpdated.getD6(),
          accountOwnerRetrieved.getD6()))
      .setD7(
        retrieveValue(
          accountOwnerUpdated.getD7(),
          accountOwnerRetrieved.getD7()));
  }

  /**
   * Retrieve all the information of each of accounts.
   *
   * @param lang Language used in the application.
   *
   * @return A {@link Map} with the parameters for the template.
   */
  public static Map<String, Object> retrieveAccountsParametersMap(final String lang) {

    final var parametersMap = new HashMap<String, Object>();

    switch (findLanguage(lang)) {

      case ES:

          parametersMap
            .put(
              "accountListSub",
              JASPER_REPORTS_TEMPLATES
                .getInstance()
                .get(
                  ACCOUNT_SUBREPORT_ES));

          break;

      case EN:

        parametersMap
          .put(
            "accountListSub",
            JASPER_REPORTS_TEMPLATES
              .getInstance()
              .get(
                ACCOUNT_SUBREPORT_EN));

        break;

      default:

        throw
          new RuntimeException(
            format(
              "Invalid language '%s'",
              lang));

    }

    return parametersMap;

  }

  /**
   * Validate if the plan is the latest.
   *
   * @param plan Instance of the type {@link CatalogItem} with the plan code.
   *
   * @return Returns <strong>true</strong> if the plan code belongs to the latest plan, <strong>false</strong>
   * in other case.
   */
  public static Boolean isLatestPlan(final CatalogItem plan) {

    return
      plan
        .getD1()
        .equalsIgnoreCase(
          retrievePlanCatalog(
              DEFAULT_LANG)
            .stream()
            .max(
              comparing(
                Plan::getD4))
            .orElse(
              new Plan()
                .setD1(
                  EMPTY_STRING))
            .getD1());

  }

  /**
   * Validate if the user is not allowed to retrieve accounts for stage.
   *
   * @param roleCode Role unique identifier.
   *
   * @return Returns <strong>true</strong> if the user is not allowed to retrieve, <strong>false</strong> in other case.
   */
  public static boolean isNotUserAllowed(final String roleCode) {

    return
      MANAGER      != findUserRole(roleCode) &&
      CONTROL_DESK != findUserRole(roleCode);

  }

  /**
   * Validate if the user is not allowed to update the account validation.
   *
   * @param roleCode Role unique identifier.
   * @param userId User's unique identifier.
   * @param accountId Account's unique identifier.
   *
   * @return Returns <strong>true</strong> if the user is not allowed to update, <strong>false</strong> in other case.
   */
  public static boolean isNotUserAllowed4BuyerAssociation(final String roleCode,
                                                          final String userId,
                                                          final String accountId) {

    if (isBlankString(
         userId)){

         return true;

    }

    return
      (ACCT_OWNER !=
        findUserRole(
          roleCode) ||
      !accountId
        .equalsIgnoreCase(
          userId));

  }

  /**
   * Retrieve merge value for a {@link CatalogItem}
   *
   * @param domain Value from the client.
   * @param persistence Value from the HBase.
   *
   * @return Instance of the type {@link CatalogItem} with the value merged.
   */
  private static CatalogItem retrieveValue(final CatalogItem domain,
                                          final CatalogItem persistence) {

    if (null == domain ||
        null == domain
                  .getD1()) {

      return persistence;

    }

    return domain;

  }

  /**
   * Retrieve merge value for a {@link String}
   *
   * @param domain Value from the client.
   * @param persistence Value from the HBase.
   *
   * @return Instance of the type {@link String} with the value merged.
   */
  private static String retrieveValue(final String domain,
                                     final String persistence) {

    return
      null == domain
        ? persistence
        : domain;

  }

  /**
   * Retrieve all buyers' association.
   *
   * @param accountId Account's unique identifier.
   * @param pageOffset The row key to starting to collect the result set.
   * @param pageSize The number of items to return.
   * @param lang Language used in the application.
   *
   * @return The instance of {@link AccountRetrieveOne}.
   */
  public static AccountRetrieveOne retrieveBuyersAssociation(final String  accountId,
                                                             final Integer pageOffset,
                                                             final Integer pageSize,
                                                             final String  lang) {

    final var buyers = retrieveBuyers();

    final var associations = retrieveAssociations(
                               accountId,
                               lang);

    final var buyerAssociationStatus = retrieveCatalogMap(
                                         BUYER_ASSOCIATION_STATUS,
                                         lang);

    return
      new AccountRetrieveOne()
        .setD18(
          buyers
          .stream()
          .map(
            (buyer) -> {

              final var association = associations
                                        .stream()
                                        .filter(
                                          item -> item
                                                    .getRowKey()
                                                    .contains(
                                                      buyer
                                                        .getRowKey()))
                                        .findFirst()
                                        .orElse(
                                          new BuyerAssociation());

              return
                new BuyerAssociationRetrieveOne()
                  .setD0(
                    encode(
                      buyer
                        .getRowKey()))
                  .setD1(
                    buyer
                      .getBusinessName())
                  .setD2(
                    association
                      .getAssociationDate())
                  .setD3(
                    findBuyerAssociationStatus(
                        association
                          .getAssociationStatusCode()) == BuyerAssociationStatus.NULL
                      ? new CatalogItem()
                          .setD1(
                            UNASSOCIATED.name())
                          .setD2(
                            buyerAssociationStatus
                              .get(
                                UNASSOCIATED.name()))
                      : new CatalogItem()
                          .setD1(
                            association
                              .getAssociationStatusCode())
                          .setD2(
                            buyerAssociationStatus
                              .get(
                                association
                                  .getAssociationStatusCode())));

            })
          .distinct()
          .skip(
            (long )pageOffset * pageSize)
          .limit(
            pageSize)
          .collect(
            toList()));

  }

  /**
   * Send record to notices for buyer association.
   *
   * @param accountId Account's unique identifier.
   * @param accountUpdateOne Instance of the type {@link AccountUpdateOne}.
   */
  public static void sendRecord2Notices4BuyerAssociation(final String accountId,
                                                         final AccountUpdateOne accountUpdateOne) {

    final var userBuyers = retrieveUsersAssociated4Buyer(
                                      decode(
                                        accountUpdateOne
                                          .getD4()
                                          .getD0()));

    userBuyers
      .forEach(userId -> sendRecord2NoticesTopic(
                           BUYER_ASSOCIATION_UPDATING.name(),
                           new Notice()
                             .setAccountId(
                               accountId)
                             .setAssociationStatusCode(
                               accountUpdateOne
                                 .getD4()
                                 .getD1()
                                 .getD1())
                             .setBuyerId(
                               decode(
                                 accountUpdateOne
                                   .getD4()
                                   .getD0()))
                             .setUserId(
                               userId)));

  }

}
