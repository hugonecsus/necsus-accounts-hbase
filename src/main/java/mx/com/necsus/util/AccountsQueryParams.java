package mx.com.necsus.util;

import static java.lang.Boolean.parseBoolean;
import static mx.com.necsus.domain.catalog.SearchValues.STD_C1;
import static mx.com.necsus.persistence.SearchValuesMapper.findSearchValue;
import static mx.com.necsus.persistence.SuggestedValuesMapper.findSuggestedValue;
import static mx.com.necsus.persistence.TypesHandler.JOIN_DELIMITER;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static mx.com.necsus.util.Strings.EMPTY_STRING;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_XLS;

import java.util.Deque;
import java.util.List;

import mx.com.necsus.domain.catalog.SearchValues;
import mx.com.necsus.domain.catalog.SuggestedValues;

/**
 * Query params available for the <strong>Accounts</strong> microservice.
 */
public final class AccountsQueryParams {

  /**
   * Certification stage query param.
   */
  public static final String STAGE = "st";

  /**
   * Account completed step query param.
   */
  public static final String COMPLETED = "c";

  /**
   * Search query param.
   */
  public static final String SEARCH = "s";

  /**
   * Scope or ambit query param.
   */
  public static final String SCOPE = "a";

  /**
   * Notes query param.
   */
  public static final String NOTES = "n";

  /**
   * Update account owner query param.
   */
  public static final String UPDATE_OWNER = "u";

  /**
   * Path variable for the <strong>payment's unique identifier</strong>.
   */
  public static final String PAYMENT_ID = "paymentId";

  /**
   * Form field name for the <strong>payment's file</strong>.
   */
  public static final String PAYMENT_FILE = "d1";

  /**
   * Form field name for the <strong>payment's capture date</strong>.
   */
  public static final String PAYMENT_CAPTURE_DATE = "d2";

  /**
   * Form field name for the <strong>payment's capture reference</strong>.
   */
  public static final String PAYMENT_CAPTURE_REFERENCE = "d3";

  /**
   * Query parameter for the <strong>p</strong> flag.
   */
  public static final String PLAN_CODE = "p";

  /**
   * Search visits query param.
   */
  public static final String SEARCH_VISITS = "sv";

  /**
   * Suggested query param.
   */
  public static final String SUGGESTED = "sg";

  /**
   * Account comparison query param.
   */
  public static final String ACCOUNT_COMPARISON = "sc";

  /**
   * Alternative flow query param.
   */
  public static final String ALTERNATIVE_FLOW = "f";

  /**
   * First position of a {@link String}.
   */
  public static final int FIRST_POSITION = 0;

  /**
   * Second position of a {@link String}.
   */
  public static final int SECOND_POSITION = 1;

  /**
   * FOUR position of a {@link String}.
   */
  public static final int FOUR_POSITION = 3;

  /**
   * Regular expression for the special characters.
   */
  private static final String SPECIAL_CHARACTERS_REGEX = "[¡!¿?#$%/()=^<>;:,|{}\\[\\]]*";

  /**
   * Size one of a {@link List}.
   */
  public static final int SIZE_ONE = 1;

  /**
   * Buyers query param.
   */
  public static final String BUYERS = "b";

  /**
   * Private explicit default constructor for security.
   */
  private AccountsQueryParams(){
  }

  /**
   * Validate the <strong>search</strong> query param.
   *
   * @param searchDeque {@link Deque} with the query param values.
   *
   * @return Return <strong>true</strong> if the last value does not match the validations,
   * <strong>false</strong> in other case.
   */
  public static boolean isInvalidSearchDeque(final Deque<String> searchDeque) {

    return
      searchDeque
        .stream()
        .anyMatch(
          search -> isBlankString(search) ||
                    !search.contains(JOIN_DELIMITER));

  }

  /**
   * Validate the <strong>search criteria</strong> semantic.
   *
   * @param searchDeque {@link Deque} with the query param values.
   *
   * @return Return <strong>true</strong> if the last value does not match the validations,
   * <strong>false</strong> in other case.
   */
  public static boolean isInvalidSearchValueSemantic(final Deque<String> searchDeque) {

    final var firstSearchSplit = searchDeque
                                   .getFirst()
                                   .split(JOIN_DELIMITER);

    final var firstSearchValue = firstSearchSplit[SECOND_POSITION];

    if (SearchValues.NULL == findSearchValue(firstSearchValue)) {

      return true;

    }

    final var prefixFirstSearchValue = firstSearchValue
                                         .substring(
                                           FIRST_POSITION,
                                           FOUR_POSITION);

    if (prefixFirstSearchValue
          .equalsIgnoreCase(
            STD_C1
              .name()
              .substring(
                FIRST_POSITION,
                FOUR_POSITION))) {

      return
         SIZE_ONE != searchDeque.size();

    }

    return
      searchDeque
        .stream()
        .anyMatch(
          (search) -> {

            final var searchSplit = search
                                      .split(JOIN_DELIMITER);

            final var searchValue = searchSplit[SECOND_POSITION];

            final var prefixSearchValue = searchValue
                                            .substring(
                                              FIRST_POSITION,
                                              FOUR_POSITION);

            return
              SearchValues.NULL == findSearchValue(searchValue) ||
              !prefixFirstSearchValue
                .equalsIgnoreCase(prefixSearchValue);

          });

  }

  /**
   * Method to sanitize keyword.
   *
   * @param keyword Keyword to sanitize.
   *
   * @return The sanitized keyword.
   */
  public static String sanitizeKeyword(final String keyword) {

   return
     keyword
       .replaceAll(
         SPECIAL_CHARACTERS_REGEX,
         EMPTY_STRING)
       .trim();

  }

  /**
   * Validate the <strong>search visits</strong> query param.
   *
   * @param searchDeque {@link Deque} with the string to search.
   *
   * @return Return <strong>true</strong> if pass the validation <strong>false</strong> in other case.
   */
  public static Boolean isInvalidSearchVisitDeque(final Deque<String> searchDeque) {

    if (null == searchDeque ||
        searchDeque.getLast().isBlank()) {

      return true;

    }

    return false;

  }

  /**
   * Validate the <strong>suggested</strong> query param.
   *
   * @param suggestedDeque {@link Deque} with the value.
   *
   * @return Return <strong>true</strong> if the last value does not match the validations,
   * <strong>false</strong> in other case.
   */
  public static boolean isInvalidSuggestedDeque(final Deque<String> suggestedDeque) {

    final var suggested = suggestedDeque
                            .getLast();

    return
      isBlankString(suggested) ||
      !suggested
        .contains(JOIN_DELIMITER);

  }

  /**
   * Validate the <strong>suggested criteria</strong> semantic.
   *
   * @param suggestedDeque {@link Deque} with the value.
   *
   * @return Return <strong>true</strong> if the last value does not match the validations,
   * <strong>false</strong> in other case.
   */
  public static boolean isInvalidSuggestedValueSemantic(final Deque<String> suggestedDeque) {

    final var suggestedSplit = suggestedDeque
                                 .getLast()
                                 .split(JOIN_DELIMITER);

    return
      SuggestedValues.NULL == findSuggestedValue(
                                suggestedSplit[SECOND_POSITION]);

  }

  /**
   * Validate content type for return file excel.
   *
   * @param accept {@link String} with the value.
   *
   * @return Return <strong>true</strong> if the last value does not match the validations,
   * <strong>false</strong> in other case.
   */
  public static boolean isValidFileExcel(final String accept) {

    if (accept
          .startsWith(
            APPLICATION_XLS)) {

     return true;

    }

     return false;

  }

  /**
   * Validate the <strong>buyers</strong> query param.
   *
   * @param buyers {@link String} with the value.
   *
   * @return Return <strong>true</strong> if all syntax validation are passed in other case <strong>false</strong>.
   */

  public static boolean isInvalidBuyer(final String buyers) {

    return
      buyers.isBlank() ||
        !parseBoolean(buyers);

  }

}
