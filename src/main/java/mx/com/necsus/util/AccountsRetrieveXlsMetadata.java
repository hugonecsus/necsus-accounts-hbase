package mx.com.necsus.util;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static mx.com.necsus.config.JasperReportsTemplates.JASPER_REPORTS_TEMPLATES;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNTS_COMPARISON_EN;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNTS_COMPARISON_ES;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNTS_LIST_EN;
import static mx.com.necsus.domain.catalog.ReportTemplates.ACCOUNTS_LIST_ES;
import static mx.com.necsus.persistence.SearchesMapper.retrieveAccount4XlsMetadata;
import static mx.com.necsus.persistence.LanguagesMapper.findLanguage;
import static mx.com.necsus.persistence.SearchesMapper.retrieveAccountsMetadata;
import static mx.com.necsus.util.AccessCrypto.decode;
import static mx.com.necsus.util.Accounts.retrieveAccountsParametersMap;
import static mx.com.necsus.util.Files.XLS_FILE_TYPE;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static net.sf.jasperreports.engine.JasperFillManager.fillReport;
import java.io.ByteArrayOutputStream;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.undertow.server.HttpServerExchange;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

import mx.com.necsus.persistence.domain.AccountsComparison;
import mx.com.necsus.persistence.domain.ComparisonAccounts;
import mx.com.necsus.persistence.domain.FilePersistence;
import mx.com.necsus.persistence.domain.AccountMetadata;

/**
 * Helper final class with commons methods.
 */
public class AccountsRetrieveXlsMetadata {

  /**
   * Private default constructor.
   */
  private AccountsRetrieveXlsMetadata() {
  }

  /**
   * Retrieve the <strong>content and extension</strong> of the report in excel.
   *
   * @param exchange Instance of the type {@link HttpServerExchange}.
   * @param searchCriteria The search criteria and keyword to search matches.
   *
   * @return Instance of the type {@link FilePersistence} with the content and extension.
   */
  public static FilePersistence retrieveXlsMetadata(final HttpServerExchange       exchange,
                                                    final Map<String,List<String>> searchCriteria) {

    final var accountList = new ComparisonAccounts()
                              .setAccountList(
                                retrieveAccountsMetadata(
                                  exchange,
                                  searchCriteria));

    final var lang = getHeader(
                       exchange,
                       X_LANG_CODE);

   switch (findLanguage(lang)) {

    case ES:

      return
        retrieveFilePersistence4Report(
          JASPER_REPORTS_TEMPLATES
            .getInstance()
            .get(
              ACCOUNTS_LIST_ES),
          retrieveAccountsParametersMap(lang),
          new JRBeanCollectionDataSource(
            List.of(
              accountList)));

    case EN:

      return
        retrieveFilePersistence4Report(
          JASPER_REPORTS_TEMPLATES
            .getInstance()
            .get(
              ACCOUNTS_LIST_EN),
          retrieveAccountsParametersMap(lang),
          new JRBeanCollectionDataSource(
            List.of(
              accountList)));

     default:

      throw
        new RuntimeException(
          format(
            "Invalid language '%s'",
            lang));

    }

  }

  /**
   * Retrieve the <strong>content and extension</strong> of the report in excel comparison.
   *
   * @param accounts Account's unique identifier.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return Instance of the type {@link FilePersistence} with the content and extension.
   */
  public static FilePersistence retrieveXlsMetadataComparison(final Deque<String> accounts,
                                                              final String timeZone,
                                                              final String lang) {

   switch (findLanguage(lang)) {

    case ES:

      return
        retrieveFilePersistenceReport(
          JASPER_REPORTS_TEMPLATES
            .getInstance()
            .get(
              ACCOUNTS_COMPARISON_ES),
          retrieveAccountsComparisonParametersMap(
            accounts,
            timeZone,
            lang));

    case EN:

      return
        retrieveFilePersistenceReport(
          JASPER_REPORTS_TEMPLATES
            .getInstance()
            .get(
              ACCOUNTS_COMPARISON_EN),
          retrieveAccountsComparisonParametersMap(
            accounts,
            timeZone,
            lang));

     default:

      throw
        new RuntimeException(
          format(
            "Invalid language '%s'",
            lang));

    }

  }

  /**
   * Assigns the items in the list to each account.
   *
   * @param accountsList contains all account information.
   * @return Instance of the type {@link AccountsComparison}.
   */
  public static ComparisonAccounts assignAccount(List<AccountMetadata> accountsList) {

    final var comparisonAccounts= new ComparisonAccounts();

     comparisonAccounts
       .setAccountOne(
         accountsList.get(0));

     if (accountsList.size() == 2) {

       comparisonAccounts
         .setAccountTwo(
           accountsList.get(1));

     }

     if (accountsList.size() == 3) {

       comparisonAccounts
         .setAccountTwo(
           accountsList.get(1));
       comparisonAccounts
         .setAccountThree(
           accountsList.get(2));
     }


    return
      comparisonAccounts;

  }

  /**
   * Retrieve the {@link FilePersistence} with the report exported to XLS.
   *
   * @param fileName File name of the compiled template.
   * @param parameters {@link Map} with the parameters.
   *
   * @return Instance of the type {@link FilePersistence} with the report exported to XLS.
   */
  private static FilePersistence retrieveFilePersistenceReport(final String fileName,
                                                               final Map<String, Object> parameters) {

    return
      retrieveFilePersistence4Report(
        fileName,
        parameters,
        new JREmptyDataSource());

  }

  /**
   * Retrieve the {@link FilePersistence} with the report exported to XLS.
   *
   * @param fileName File name of the compiled template.
   * @param parameters {@link Map} with the parameters.
   * @param dataSource {@link JRDataSource} with the values for the detail band.
   *
   * @return Instance of the type {@link FilePersistence} with the report exported to XLS.
   */
  private static FilePersistence retrieveFilePersistence4Report(final String              fileName,
                                                                final Map<String, Object> parameters,
                                                                final JRDataSource        dataSource) {

    try (final var byteArrayOutputStream = new ByteArrayOutputStream()) {

      final var jrXlsExporter = new JRXlsExporter();

      jrXlsExporter
        .setExporterInput(
          new SimpleExporterInput(
            fillReport(
              fileName,
              parameters,
              dataSource)));
      jrXlsExporter
        .setExporterOutput(
          new SimpleOutputStreamExporterOutput(
            byteArrayOutputStream));
      jrXlsExporter
        .setConfiguration(
          configurationReportXls());
      jrXlsExporter.exportReport();

    return
      new FilePersistence()
        .setContent(
          byteArrayOutputStream
            .toByteArray())
        .setExtension(XLS_FILE_TYPE);

    } catch (final Exception exc) {

      throw
        new RuntimeException(exc);
      }

  }

  /**
   * Retrieve configuration for the construction of archive xls.
   *
   * @return Instance of the type {@link SimpleXlsReportConfiguration}.
   */
  private static SimpleXlsReportConfiguration configurationReportXls() {

    final var simpleXlsReportConfiguration = new SimpleXlsReportConfiguration();

      simpleXlsReportConfiguration
        .setOnePagePerSheet(
          FALSE);
      simpleXlsReportConfiguration
        .setWhitePageBackground(
          FALSE);
      simpleXlsReportConfiguration
        .setRemoveEmptySpaceBetweenRows(
          TRUE);
      simpleXlsReportConfiguration
        .setRemoveEmptySpaceBetweenColumns(
          TRUE);
      simpleXlsReportConfiguration
        .setDetectCellType(
          FALSE);
      simpleXlsReportConfiguration
        .setFontSizeFixEnabled(
          FALSE);

    return
      simpleXlsReportConfiguration;

  }

  /**
   * Retrieve all the information of each of accounts the Deque.
   *
   * @param accounts contains the accounts.
   * @param timeZone Session's time zone.
   * @param lang Language used in the application.
   *
   * @return A {@link Map} with the parameters for the template.
   */
  public static  Map<String, Object> retrieveAccountsComparisonParametersMap(final Deque<String> accounts,
                                                                             final String timeZone,
                                                                             final String lang){

    final var parametersMap = new HashMap<String, Object>();

    parametersMap
      .put(
        "comparisonAccounts",
        assignAccount(
          accounts
            .stream()
            .map(
              accountId -> retrieveAccount4XlsMetadata(
                             decode(
                               accountId),
                             timeZone,
                             lang))
            .collect(
              toList())));

    return
      parametersMap;

  }

}
