package mx.com.necsus.util;

import io.undertow.util.HttpString;

/**
 * Header params available for the <strong>Accounts</strong> microservice.
 */
public class AccountsHeaders {

  /**
   * Header name for the error code from upstream server.
   */
  public static final String X_OWNER_VERIFYING = "X-Owner-Verifying";

  /**
   * {@link HttpString} of the error code from upstream server.
   */
  public static final HttpString X_OWNER_VERIFYING_HTTPSTRING  = new HttpString(X_OWNER_VERIFYING);

  /**
   * Header name for the error code from upstream server.
   */
  public static final String X_TOTAL_SIZE = "X-Total-Size";

  /**
   * {@link HttpString} of the error code from upstream server.
   */
  public static final HttpString X_TOTAL_SIZE_HTTPSTRING  = new HttpString(X_TOTAL_SIZE);

  /**
   * Private explicit default constructor for security.
   */
  private AccountsHeaders() {
  }
}
