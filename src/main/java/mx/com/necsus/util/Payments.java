package mx.com.necsus.util;

import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.persistence.AccountsMapper.retrieveOriginCode;
import static mx.com.necsus.persistence.PaymentsMapper.retrievePaymentsApproverId;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.util.Catalogs.isNotExistingPlanCode;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static org.apache.logging.log4j.LogManager.getLogger;

import org.apache.logging.log4j.Logger;

/**
 * Helper final class with helper methods for payments.
 */
public final class Payments {

  /**
   * Logger for the helper.
   */
  private static final Logger LOGGER = getLogger(Payments.class);

  /**
   * Template for online payment service.
   */
  public static final String TEMPLATE= "<bancoazteca><tipoOperacion>ecommerce3D</tipoOperacion><correoComprador>%s</correoComprador><idSesion>11111111</idSesion><idTransaccion>%s</idTransaccion><afiliacion>7952739</afiliacion><monto>%s</monto><ipComprador>187.140.144.31</ipComprador><navegador>Mozilla/5.0(Windows_NT_10.0;_Win64;_x64;_rv:76.0)_Gecko/20100101_Firefox/76.0</navegador><sku>%s</sku></bancoazteca>";

  /**
   * Private explicit default constructor for security.
   */
  private Payments() {
  }

  /**
   * Validate if the user is not allowed to update the payment validation.
   *
   * @param roleCode Role unique identifier.
   * @param userId User's unique identifier.
   * @param accountId Account's unique identifier.
   *
   * @return Returns <strong>true</strong> if the user is not allowed to update, <strong>false</strong> in other case.
   */
  public static boolean isNotUserAllowed(final String roleCode,
                                         final String userId,
                                         final String accountId) {

    if (isBlankString(userId)){

      return true;

    }

    if (ACCT_OWNER == findUserRole(roleCode) &&
        accountId.equalsIgnoreCase(userId)) {

      return false;

    }

    if (CONTROL_DESK == findUserRole(roleCode) &&
        userId
          .equalsIgnoreCase(
            retrievePaymentsApproverId(accountId))) {

      return false;

    }

    return
     MANAGER != findUserRole(roleCode);

  }

  /**
   * Search the <strong>planCode</strong> to determine if is part of the catalog <strong>PLANS</strong>.
   *
   * @param planCode Catalog item's unique code for the plan to search.
   * @param accountId Account's unique identifier.
   *
   * @return Return <strong>true</strong> if the search <strong>return null</strong>, in other case
   * return <strong>false</strong>.
   */
  public static boolean isNotExistingPlan(final String accountId,
                                          final String planCode) {

    LOGGER.info(
      "Find plan '{}'",
      planCode);

    return
      isNotExistingPlanCode(
        planCode,
        retrieveOriginCode(accountId));

  }

}
