package mx.com.necsus.util;

import static mx.com.necsus.domain.catalog.PaymentStatus.FAIL;
import static mx.com.necsus.domain.catalog.PaymentStatus.NULL;
import static mx.com.necsus.domain.catalog.PaymentStatus.SUCCESS;
import static mx.com.necsus.domain.catalog.PaymentStatus.TO_CHECK;
import static mx.com.necsus.persistence.PaymentStatusMapper.findPaymentStatus;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_CAPTURE_DATE;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_CAPTURE_REFERENCE;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_FILE;
import static mx.com.necsus.util.Files.extension;
import static mx.com.necsus.util.Files.isInvalidExtension;
import static mx.com.necsus.util.RequestBody.hasNotMatches;
import static mx.com.necsus.util.RequestBody.isBlankCatalogItem;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static org.apache.logging.log4j.LogManager.getLogger;

import io.undertow.server.handlers.form.FormData;
import mx.com.necsus.domain.PaymentUpdateOne;

import org.apache.logging.log4j.Logger;

/**
 * Helper final class with the validations of the <strong>syntax</strong> and <strong>semantic</strong> for the
 * request body content.
 */
public class PaymentsRequestBody {

  /**
   * Logger for the helper.
   */
  private static final Logger LOGGER = getLogger(PaymentsRequestBody.class);

  /**
   * Regular expression for capture date.
   */
  private static final String CAPTURE_DATE_REGEX = "^[w]{0}|((3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4})$";

  /**
   * Regular expression for capture reference.
   */
  private static final String CAPTURE_REFERENCE_REGEX = "^[A-Za-z0-9]{1,30}$";

  /**
   * Regular expression for the note message.
   */
  private static final String NOTE_MESSAGE_REGEX = "^.{1,300}$";

  /**
   * Private explicit default constructor for security.
   */
  private PaymentsRequestBody() {
  }

  /**
   * Syntax validations for the content of <strong>formData</strong>.
   *
   * @param formData Instance of the type {@link FormData} with the fields to validate.
   *
   * @return Return <strong>true</strong> if all syntax validations are passed.
   */
  public static boolean isInvalidPaymentUpdateFormDataSyntax(final FormData formData) {

    final var file = formData.getLast(PAYMENT_FILE);

    final var captureDate = formData.getLast(PAYMENT_CAPTURE_DATE);

    final var captureReference = formData.getLast(PAYMENT_CAPTURE_REFERENCE);

    if (file == null ||
        !file.isFileItem() ||
        captureDate == null ||
        captureDate.getValue().trim().isBlank() ||
        captureReference == null ||
        captureReference.getValue().trim().isBlank()) {

      return true;

    }

    return false;

  }

  /**
   * Semantic validations for the content of <strong>formData</strong>.
   *
   * @param formData Instance of the type {@link FormData} with the fields to validate.
   *
   * @return Return <strong>true</strong> if all semantic validations are passed.
   */
  public static boolean isInvalidPaymentUpdateFormDataSemantic(final FormData formData) {

    if (isInvalidExtension(
          extension(
            formData
              .getLast(
                PAYMENT_FILE)
              .getFileName()))) {

      LOGGER.info(
        "Invalid extension for d1");

      return true;

    }

    if (hasNotMatches(
          formData
            .getLast(
              PAYMENT_CAPTURE_DATE)
            .getValue()
            .trim(),
          CAPTURE_DATE_REGEX,
          "Invalid content for d2") ||
        hasNotMatches(
          formData
            .getLast(
              PAYMENT_CAPTURE_REFERENCE)
            .getValue()
            .trim(),
          CAPTURE_REFERENCE_REGEX,
          "Invalid content for d3")) {

      return true;

    }

    return false;

  }

  /**
   * Syntax validations for the content of <strong>PaymentUpdateOne</strong>.
   *
   * @param paymentUpdate Instance of type {@link PaymentUpdateOne}.
   *
   * @return Return <strong>true</strong> if all syntax validations are passed.
   */
  public static boolean isInvalidPaymentSyntax(final PaymentUpdateOne paymentUpdate){

    if (paymentUpdate == null){

      return true;

    }

    if (isBlankCatalogItem(
          paymentUpdate.getS1(),
          "Required s1")){

      return true;

    }

    return
      isNotBlankString(paymentUpdate.getN2()) &&
      hasNotMatches(
        paymentUpdate.getN2(),
        NOTE_MESSAGE_REGEX);

  }

  /**
   * Semantic validations for the content of <strong>paymentUpdate</strong>.
   *
   * @param paymentUpdate Instance of the type {@link PaymentUpdateOne}.
   * @param roleCode Role unique identifier.
   *
   * @return Return <strong>true</strong> if all semantic validations are passed.
   */
  public static boolean isInvalidPaymentSemantic(final PaymentUpdateOne paymentUpdate,
                                                 final String           roleCode){

    if (NULL == findPaymentStatus(
                  paymentUpdate
                    .getS1())){

      LOGGER.info("Not existing status");

      return true;

    }

    switch (findUserRole(roleCode)) {

      case MANAGER:
      case CONTROL_DESK:

        if (TO_CHECK == findPaymentStatus(paymentUpdate.getS1())) {

          return true;

        }

        if (FAIL == findPaymentStatus(paymentUpdate.getS1()) &&
          isBlankString(paymentUpdate.getN2())) {

          return true;

        }

        return
          SUCCESS == findPaymentStatus(paymentUpdate.getS1()) &&
          isNotBlankString(paymentUpdate.getN2());

      case ACCT_OWNER:

        if ((TO_CHECK == findPaymentStatus(paymentUpdate.getS1()) ||
             FAIL == findPaymentStatus(paymentUpdate.getS1())) &&
            isBlankString(paymentUpdate.getN2())) {

          return true;

        }

        return
          SUCCESS == findPaymentStatus(paymentUpdate.getS1());

      default:

        return true;

    }

  }

}
