package mx.com.necsus.handler;

import static com.jsoniter.output.JsonStream.serialize;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.persistence.SearchVisitsMapper.retrieveAccounts4SearchVisits;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.util.AccountsQueryParams.SEARCH_VISITS;
import static mx.com.necsus.util.AccountsQueryParams.isInvalidSearchVisitDeque;
import static mx.com.necsus.util.ResponseBody.QUERY_PARAM;
import static mx.com.necsus.util.ResponseBody.createForbiddenResponse;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createOkResponse;
import static mx.com.necsus.util.ResponseBody.createRequiredResponse;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TIME_ZONE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.X_USER_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

/**
 * GetAccountsSearchVisitsHandler, a particular handler to retrieve all accounts by matches.
 */
public class GetAccountsSearchVisitsHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetAccountsSearchVisitsHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 8;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 16;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 16;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetAccountsSearchVisitsHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var userId = exchange
                                 .getRequestHeaders()
                                 .get(X_USER_ID)
                                 .getLast();

            final var roleCode = exchange
                                   .getRequestHeaders()
                                   .get(X_ROLE_CODE)
                                   .getLast();

            final var timeZone = exchange
                                   .getRequestHeaders()
                                   .get(X_TIME_ZONE)
                                   .getLast();

            final var lang = exchange
                               .getRequestHeaders()
                               .get(X_LANG_CODE)
                               .getLast();

            final var searchDeque = exchange
                                      .getQueryParameters()
                                      .get(SEARCH_VISITS);

            final var searchKeyword = null == searchDeque
                                        ? ""
                                        : searchDeque
                                            .getLast();

            LOGGER.info(
              "{} {} '{}' {} '{}' {} '{}' {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              X_USER_ID,
              userId,
              X_ROLE_CODE,
              roleCode,
              SEARCH_VISITS,
              searchDeque,
              X_TIME_ZONE,
              timeZone,
              X_LANG_CODE,
              lang);

            if (MANAGER != findUserRole(roleCode) &&
                CONTROL_DESK != findUserRole(roleCode)) {

              createForbiddenResponse(exchange);

              return ;

            }

            if (isInvalidSearchVisitDeque(searchDeque)) {

              createRequiredResponse(
                exchange,
                QUERY_PARAM,
                SEARCH_VISITS,
                searchDeque);

              return ;

            }

            createOkResponse(
              exchange,
              serialize(
                retrieveAccounts4SearchVisits(
                  searchKeyword,
                  timeZone,
                  lang)));


          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
