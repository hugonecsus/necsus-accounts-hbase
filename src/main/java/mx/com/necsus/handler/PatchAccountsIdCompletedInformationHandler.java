package mx.com.necsus.handler;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_INFORMATION;
import static mx.com.necsus.domain.catalog.AccountStatus.INFORMATION;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.FileCodes.DMFB;
import static mx.com.necsus.domain.catalog.InformationStatus.ADJUSTMENT;
import static mx.com.necsus.domain.catalog.InformationStatus.TO_CHECK;
import static mx.com.necsus.domain.catalog.NoticeTypes.ACCOUNT_INFORMATION;
import static mx.com.necsus.persistence.AccountsMapper.completeInformation;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAccount4CompletedInformation;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D22;
import static mx.com.necsus.persistence.CertificationQualifiers.CERTIFICATION_D23;
import static mx.com.necsus.persistence.CertificationTypesMapper.findCertificationType;
import static mx.com.necsus.persistence.DisclaimerQualifiers.DISCLAIMER_S1;
import static mx.com.necsus.persistence.DisclaimerQualifiers.DISCLAIMER_S2;
import static mx.com.necsus.persistence.InformationStatusMapper.findInformationStatus;
import static mx.com.necsus.persistence.PostCertificationStatusMapper.findPostCertificationStatus;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_S1_B3;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.util.AccountsCommons.deleteFile;
import static mx.com.necsus.util.AccountsStrings.LOG_COMPLETE;
import static mx.com.necsus.util.NoticesSender.sendRecord2NoticesTopic;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createNoContentResponse;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.Notice;
import mx.com.necsus.domain.catalog.DisclaimerStatus;
import mx.com.necsus.domain.catalog.VisitStatus;

import org.apache.logging.log4j.Logger;

/**
 * Handler to process the partial update of one <strong>Account</strong>.
 */
public class PatchAccountsIdCompletedInformationHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchAccountsIdCompletedInformationHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public PatchAccountsIdCompletedInformationHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            final var traceId = retrieveTraceId(exchange);

            put(X_TRACE_ID, traceId);

            defaultResponseContentType(exchange);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var now = nowEpochMilli();

            final var put = newPut(accountId);

            final var account = retrieveAccount4CompletedInformation(accountId);

            var informationStatus = TO_CHECK.name();

            switch (findPostCertificationStatus(
                      account
                        .getPostCertificationStatus())) {

              case UPDATE_INITIATED:

                informationStatus = ADJUSTMENT.name();

                if (null == account
                              .getS3S1B3()) {

                  putValue(
                    put,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S3_S1_B3,
                    account
                      .getPostCertificationVersion(),
                    ADJUSTMENT.name());

                }

                break;

              case UPGRADE_INITIATED:

                informationStatus = ADJUSTMENT.name();

                deleteFile(
                  put,
                  DMFB.name());

                putValue(
                  put,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  DISCLAIMER_S1,
                  DisclaimerStatus.NULL.name());

                putValue(
                  put,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  DISCLAIMER_S2,
                  now);

                if (STD == findCertificationType(
                             account
                               .getCurrentCertificationType())) {

                  putValue(
                    put,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D22,
                    VisitStatus.NULL.name());

                  putValue(
                    put,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    CERTIFICATION_D23,
                    now);

                }

                if (null == account
                              .getS3S1B3()) {

                  putValue(
                    put,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S3_S1_B3,
                    account
                      .getPostCertificationVersion(),
                    ADJUSTMENT.name());

                }

                break;

              case RENEWAL_INITIATED:

                informationStatus = ADJUSTMENT.name();

                deleteFile(
                  put,
                  DMFB.name());

                putValue(
                  put,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  DISCLAIMER_S1,
                  DisclaimerStatus.NULL.name());

                putValue(
                  put,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  DISCLAIMER_S2,
                  now);

                putValue(
                  put,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  CERTIFICATION_D22,
                  VisitStatus.NULL.name());

                putValue(
                  put,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  CERTIFICATION_D23,
                  now);

                if (null == account
                              .getS3S1B3()) {

                  putValue(
                    put,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S3_S1_B3,
                    account
                      .getPostCertificationVersion(),
                    ADJUSTMENT.name());

                }

                break;

              default:

                break ;

            }

            account
              .setRowKey(
                accountId)
              .setStatusCode(
                CHECKING_INFORMATION.name())
              .setStatusDate(
                now)
              .setInformationStatusCode(
                informationStatus)
              .setInformationStatusDate(
                now);

            LOGGER.info(
              LOG_COMPLETE,
              INFORMATION,
              account);

            completeInformation(
              put,
              account);

            sendRecord2NoticesTopic(
              ACCOUNT_INFORMATION.name(),
              new Notice()
                .setId(
                  traceId)
                .setAccountId(
                  accountId));

            createNoContentResponse(exchange);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
