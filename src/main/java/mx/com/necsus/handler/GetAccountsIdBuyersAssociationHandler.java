package mx.com.necsus.handler;

import static com.jsoniter.output.JsonStream.serialize;
import static java.lang.Boolean.parseBoolean;
import static java.lang.Integer.parseInt;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.persistence.TypesHandler.FIRST_ELEMENT_INDEX;
import static mx.com.necsus.util.AccountsQueryParams.BUYERS;
import static mx.com.necsus.util.AccountsStrings.LOG_REQUEST_BUYERS_ASSOCIATED_HANDLER_GET;
import static mx.com.necsus.util.Accounts.retrieveBuyersAssociation;
import static mx.com.necsus.util.ResponseBody.QUERY_PARAM;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSemanticResponse;
import static mx.com.necsus.util.ResponseBody.createOkResponse;
import static mx.com.necsus.util.ResponseBody.createRequiredResponse;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_OFFSET;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_SIZE;
import static mx.com.necsus.util.TraceHeaders.X_TIME_ZONE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import org.apache.logging.log4j.Logger;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 * Handler to process the retrieve all the buyers associates of one <strong>Account</strong>.
 */
public class GetAccountsIdBuyersAssociationHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetAccountsIdBuyersAssociationHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Maximum pagination size value.
   */
  private static final int MAX_PAGINATE_SIZE = 250;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetAccountsIdBuyersAssociationHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var lang = getHeader(
                               exchange,
                               X_LANG_CODE);

            final var timeZone = getHeader(
                                   exchange,
                                   X_TIME_ZONE);

            final var xPageOffset = getHeader(
                                     exchange,
                                     X_PAGE_OFFSET);

            final var xPageSize = getHeader(
                                   exchange,
                                   X_PAGE_SIZE);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var buyers = getQueryParam(
                                 exchange,
                                 BUYERS);

            LOGGER.info(
              LOG_REQUEST_BUYERS_ASSOCIATED_HANDLER_GET,
              REQUEST_PAYLOAD,
              X_LANG_CODE,
              lang,
              X_TIME_ZONE,
              timeZone,
              X_PAGE_OFFSET,
              xPageOffset,
              X_PAGE_SIZE,
              xPageSize,
              ACCOUNT_ID,
              accountId,
              BUYERS,
              buyers);

            final var pageOffset = parseInt(xPageOffset);
            final var pageSize = parseInt(xPageSize);

            if (pageOffset < FIRST_ELEMENT_INDEX) {

              createInvalidSemanticResponse(
                exchange,
                xPageOffset);

              return ;

            }

            if (pageSize < FIRST_ELEMENT_INDEX + 1 || pageSize > MAX_PAGINATE_SIZE) {

              createInvalidSemanticResponse(
                exchange,
                xPageSize);

              return ;

            }

            if (!parseBoolean(
                 buyers)){

                 createRequiredResponse(
                   exchange,
                   QUERY_PARAM,
                   BUYERS,
                   buyers);

              return ;

            }

            createOkResponse(
              exchange,
              serialize(
                retrieveBuyersAssociation(
                  accountId,
                  pageOffset,
                  pageSize,
                  lang)));

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
