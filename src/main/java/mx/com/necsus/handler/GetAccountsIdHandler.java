package mx.com.necsus.handler;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.persistence.AccountsMapper.isNotExistingAccount;
import static mx.com.necsus.persistence.AccountsMapper.retrieveByAccountId;
import static mx.com.necsus.util.AccessQueryParams.ACCOUNT_OWNER_CHANGE_EMAIL_TOKEN;
import static mx.com.necsus.util.AccountsQueryParams.NOTES;
import static mx.com.necsus.util.AccountsQueryParams.PLAN_CODE;
import static mx.com.necsus.util.AccountsQueryParams.BUYERS;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createNotFoundEntityResponse;
import static mx.com.necsus.util.ResponseBody.createOkResponse;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

/**
 * Handler to process the retrieve of one <strong>Account</strong>.
 */
public class GetAccountsIdHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetAccountsIdHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Instance variable for the {@link GetAccountsIdPaymentHandler}.
   */
  private final HttpHandler getAccountsIdPayLoadHandler;

  /**
   * Instance variable for the {@link GetAccountsIdNotesHandler}.
   */
  private final HttpHandler getAccountsNotesHandler;

  /**
   * Instance variable for the {@link GetAccountsIdAccountOwnerHandler}.
   */
  private final HttpHandler getAccountsIdAccountOwnerHandler;

  /**
   * Instance variable for the {@link GetAccountsIdBuyersAssociationHandler}.
   */
  private final HttpHandler getAccountsIdBuyersAssociatedHandler;

  /**
   * Explicit default constructor.
   */
  public GetAccountsIdHandler() {

    this.getAccountsIdPayLoadHandler = new GetAccountsIdPaymentHandler();

    this.getAccountsNotesHandler = new GetAccountsIdNotesHandler();

    this.getAccountsIdAccountOwnerHandler = new GetAccountsIdAccountOwnerHandler();

    this.getAccountsIdBuyersAssociatedHandler = new GetAccountsIdBuyersAssociationHandler();

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var accountId = exchange
                                    .getQueryParameters()
                                    .get(ACCOUNT_ID)
                                    .getLast();

            final var pDeque = exchange
                                 .getQueryParameters()
                                 .get(PLAN_CODE);

            final var notesDeque = exchange
                                     .getQueryParameters()
                                     .get(NOTES);

            final var token = getQueryParam(
                                exchange,
                                ACCOUNT_OWNER_CHANGE_EMAIL_TOKEN);

            final var roleCode = getHeader(
                                  exchange,
                                  X_ROLE_CODE);

            final var lang = getHeader(
                              exchange,
                              X_LANG_CODE);

            final var buyers = getQueryParam(
                                 exchange,
                                 BUYERS);

            LOGGER.info(
              "{} {} '{}' {} '{}' {} '{}' {} '{}' {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              ACCOUNT_ID,
              accountId,
              PLAN_CODE,
              pDeque,
              NOTES,
              notesDeque,
              ACCOUNT_OWNER_CHANGE_EMAIL_TOKEN,
              token,
              X_ROLE_CODE,
              roleCode,
              BUYERS,
              buyers);

            if (isNotExistingAccount(accountId)) {

              createNotFoundEntityResponse(
                exchange,
                ACCOUNT_ID,
                accountId);

              return ;

            }

            if (null != notesDeque) {

              getAccountsNotesHandler.handleRequest(exchange);

              return ;

            }

            if (null != pDeque) {

              getAccountsIdPayLoadHandler.handleRequest(exchange);

              return ;

            }

            if (isNotBlankString(token)) {

              getAccountsIdAccountOwnerHandler.handleRequest(exchange);

              return ;

            }

            if (isNotBlankString(buyers)) {

              getAccountsIdBuyersAssociatedHandler.handleRequest(exchange);

              return ;

            }

            createOkResponse(
              exchange,
              retrieveByAccountId(
                  lang,
                  roleCode,
                  accountId)
                .toString());

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
