package mx.com.necsus.handler;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_PAYMENT;
import static mx.com.necsus.domain.catalog.AccountStatus.PLAN;
import static mx.com.necsus.persistence.AccountsMapper.deleteB1AndB4FromS3;
import static mx.com.necsus.persistence.AccountsMapper.fillSections4NewCertification;
import static mx.com.necsus.persistence.AccountsMapper.resetMissionCompleteAndVisitStatus;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAccount4Completed;
import static mx.com.necsus.persistence.AccountsMapper.retrieveCertificationTypeByPlan;
import static mx.com.necsus.persistence.AccountsMapper.updateAccount4CompletePlan;
import static mx.com.necsus.persistence.PaymentsMapper.confirmPayments;
import static mx.com.necsus.persistence.PostCertificationStatusMapper.findPostCertificationStatus;
import static mx.com.necsus.persistence.TypesHandler.NO_VERSION;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.util.Accounts.isInvalidPlan;
import static mx.com.necsus.util.Accounts.isRequiredDeleteB1AndB4FromS3;
import static mx.com.necsus.util.AccountsCommons.assignControlDesk4Payment;
import static mx.com.necsus.util.AccountsRequestBody.isInvalidAccountUpdateSemantic;
import static mx.com.necsus.util.AccountsRequestBody.isInvalidAccountUpdateSyntax;
import static mx.com.necsus.util.AccountsStrings.LOG_ACCOUNT_RETRIEVED;
import static mx.com.necsus.util.RequestBody.deserializeBody;
import static mx.com.necsus.util.ResponseBody.createForbiddenResponse;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSemanticResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSyntaxResponse;
import static mx.com.necsus.util.ResponseBody.createNoContentResponse;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.AccountUpdateOne;

import org.apache.logging.log4j.Logger;

/**
 * Handler to process the partial update of one <strong>Account</strong>.
 */
public class PatchAccountsIdCompletedPlanHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchAccountsIdCompletedPlanHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public PatchAccountsIdCompletedPlanHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    if (exchange.isInIoThread()) {

      exchange
        .dispatch(
          this);

      return;

    }

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var accountUpdate = deserializeBody(
                                        exchange,
                                        AccountUpdateOne.class);

            if (null == accountUpdate) {

              return ;

            }

            if (isInvalidAccountUpdateSyntax(accountUpdate)) {

              createInvalidSyntaxResponse(
                exchange,
                accountUpdate);

              return ;

            }

            if (isInvalidAccountUpdateSemantic(accountId, accountUpdate)) {

              createInvalidSemanticResponse(
                exchange,
                accountUpdate);

              return ;

            }

            final var now = nowEpochMilli();

            final var account = retrieveAccount4Completed(accountId);

            LOGGER.info(
             LOG_ACCOUNT_RETRIEVED,
              account);

            final var put = newPut(accountId);

            switch (findPostCertificationStatus(
                      account
                        .getPostCertificationStatus())) {

              case UPDATE_INITIATED:
              case UPDATE_CANCELED:
              case UPDATE_FINALIZED:
              case UPGRADE_CANCELED:
              case UPGRADE_FINALIZED:
              case RENEWAL_FINALIZED:

                createForbiddenResponse(
                  exchange);

                return ;

              case UPGRADE_INITIATED:

                if (isInvalidPlan(
                      exchange,
                      account,
                      accountUpdate)) {

                  return ;

                }

                break ;

              case RENEWAL_INITIATED:

                account
                  .setUpdateCertificationType(
                    retrieveCertificationTypeByPlan(
                      accountUpdate
                        .getD1()));

                if (isRequiredDeleteB1AndB4FromS3(
                      account,
                      accountUpdate)) {

                  deleteB1AndB4FromS3(
                    accountId,
                    account,
                    put);

                }

                resetMissionCompleteAndVisitStatus(
                  put,
                  account);

                break ;

              default:

                account
                  .setPostCertificationVersion(
                    NO_VERSION);

                fillSections4NewCertification(
                  put,
                  accountId);

            }

            LOGGER.info(
              "Complete {} '{}'",
              PLAN,
              account
                .setRowKey(
                  accountId)
                .setStatusCode(
                  CHECKING_PAYMENT.name())
                .setStatusDate(
                  now)
                .setCurrentCertificationType(
                  retrieveCertificationTypeByPlan(
                    accountUpdate.getD1()))
                .setCertificationPlan(
                  accountUpdate
                    .getD1()));

            confirmPayments(
              put,
              account.getRowKey());

            assignControlDesk4Payment(
              put,
              account
                .getRowKey());

            updateAccount4CompletePlan(
              put,
              account);

            createNoContentResponse(exchange);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
