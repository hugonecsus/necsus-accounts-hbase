package mx.com.necsus.handler;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.format;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.NoticeTypes.ACCOUNT_OWNER_UPDATING;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAccountOwnerByAccountId;
import static mx.com.necsus.persistence.AccountsMapper.updateAccountOwner;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.util.Accounts.mergeAccountOwner;
import static mx.com.necsus.util.AccountsHeaders.X_OWNER_VERIFYING_HTTPSTRING;
import static mx.com.necsus.util.AccountsQueryParams.UPDATE_OWNER;
import static mx.com.necsus.util.AccountsRequestBody.isInvalidAccountUpdated;
import static mx.com.necsus.util.NoticesSender.sendRecord2NoticesTopic;
import static mx.com.necsus.util.RequestBody.deserializeBody;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.ResponseBody.QUERY_PARAM;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSyntaxResponse;
import static mx.com.necsus.util.ResponseBody.createNoContentResponse;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.AccountUpdateOne;
import mx.com.necsus.domain.Notice;

import org.apache.logging.log4j.Logger;

/**
 * Handler to update a contact person of one <strong>Account</strong>.
 */
public class PatchAccountIdAccountOwnerHandler implements HttpHandler {
  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchAccountIdAccountOwnerHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    if (exchange.isInIoThread()) {

      exchange
        .dispatch(
          this);

      return;

    }

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            final var traceId = retrieveTraceId(exchange);

            put(X_TRACE_ID, traceId);

            defaultResponseContentType(exchange);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var lang = getHeader(
                                exchange,
                                X_LANG_CODE);

            final var isUpdated = parseBoolean(getQueryParam(
                                                  exchange,
                                                  UPDATE_OWNER));

            LOGGER.info(
              "{} {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              ACCOUNT_ID,
              accountId,
              UPDATE_OWNER,
              isUpdated);

            if (!isUpdated) {

              createInvalidResponse(
                exchange,
                QUERY_PARAM,
                UPDATE_OWNER,
                false);

              return ;

            }

            final var accountUpdateOne = deserializeBody(
                                          exchange,
                                          AccountUpdateOne.class);

            if (null == accountUpdateOne) {

              return ;

            }

            if (isInvalidAccountUpdated(accountUpdateOne)) {

              createInvalidSyntaxResponse(
                exchange,
                accountUpdateOne);

              return ;

            }

            final var accountOwnerUpdated = accountUpdateOne.getD3();

            final var accountOwnerRetrieved = retrieveAccountOwnerByAccountId(
                                                accountId,
                                                lang);

            if (isNotBlankString(
                  accountOwnerUpdated
                    .getD8()) &&
                !accountOwnerRetrieved
                  .getD8()
                  .equalsIgnoreCase(
                    accountOwnerUpdated
                      .getD8())) {

              sendRecord2NoticesTopic(
                ACCOUNT_OWNER_UPDATING.name(),
                new Notice()
                  .setId(
                    traceId)
                  .setAccountId(
                    format(
                      "%s,%s",
                      accountId,
                      accountOwnerUpdated
                        .getD8())));

              exchange
                .getResponseHeaders()
                .put(
                  X_OWNER_VERIFYING_HTTPSTRING,
                  "true");

            }

            mergeAccountOwner(
              accountOwnerUpdated,
              accountOwnerRetrieved);

            LOGGER.info(
              "Merged '{}'",
              accountOwnerUpdated);

            final var put = newPut(accountId);

            updateAccountOwner(
              put,
              accountOwnerUpdated);

            createNoContentResponse(exchange);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }
}
