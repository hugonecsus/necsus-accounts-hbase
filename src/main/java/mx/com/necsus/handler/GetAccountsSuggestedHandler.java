package mx.com.necsus.handler;

import static com.jsoniter.output.JsonStream.serialize;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_USR;
import static mx.com.necsus.persistence.SuggestedMapper.retrieveSuggested;
import static mx.com.necsus.persistence.TypesHandler.JOIN_DELIMITER;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.persistence.UsersMapper.retrieveBuyerRfcByUserId;
import static mx.com.necsus.util.AccountsQueryParams.FIRST_POSITION;
import static mx.com.necsus.util.AccountsQueryParams.SECOND_POSITION;
import static mx.com.necsus.util.AccountsQueryParams.SUGGESTED;
import static mx.com.necsus.util.AccountsQueryParams.isInvalidSuggestedDeque;
import static mx.com.necsus.util.AccountsQueryParams.isInvalidSuggestedValueSemantic;
import static mx.com.necsus.util.AccountsQueryParams.sanitizeKeyword;
import static mx.com.necsus.util.ResponseBody.QUERY_PARAM;
import static mx.com.necsus.util.ResponseBody.createForbiddenResponse;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSemanticResponse;
import static mx.com.necsus.util.ResponseBody.createOkResponse;
import static mx.com.necsus.util.ResponseBody.createRequiredResponse;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.X_USER_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

/**
 * GetAccountsSuggestedHandler, a particular handler to retrieve all suggested.
 */
public class GetAccountsSuggestedHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetAccountsSuggestedHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 8;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 16;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 16;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetAccountsSuggestedHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var userId = exchange
                                 .getRequestHeaders()
                                 .get(X_USER_ID)
                                 .getLast();

            final var roleCode = exchange
                                   .getRequestHeaders()
                                   .get(X_ROLE_CODE)
                                  .getLast();

            final var suggestedDeque = exchange
                                         .getQueryParameters()
                                         .get(SUGGESTED);

            final var lang = exchange
                               .getRequestHeaders()
                               .get(X_LANG_CODE)
                               .getLast();

            LOGGER.info(
              "{} {} '{}' {} '{}' {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              X_USER_ID,
              userId,
              X_ROLE_CODE,
              roleCode,
              SUGGESTED,
              suggestedDeque,
              X_LANG_CODE,
              lang);

            if (BYR_OWNER != findUserRole(roleCode) &&
                BYR_USR != findUserRole(roleCode)) {

              createForbiddenResponse(exchange);

              return ;

            }

            if (isInvalidSuggestedDeque(suggestedDeque)) {

              createRequiredResponse(
                exchange,
                QUERY_PARAM,
                SUGGESTED,
                suggestedDeque);

              return ;

            }

            if (isInvalidSuggestedValueSemantic(suggestedDeque)) {

              createInvalidSemanticResponse(
                exchange,
                suggestedDeque);

              return ;

            }

            final var suggestedSplit = suggestedDeque
                                         .getLast()
                                         .split(
                                           JOIN_DELIMITER);

            final var buyerId =  retrieveBuyerRfcByUserId(
                                   getHeader(
                                     exchange,
                                     X_USER_ID),
                                   getHeader(
                                     exchange,
                                     X_ROLE_CODE));

            createOkResponse(
              exchange,
              serialize(
                retrieveSuggested(
                  sanitizeKeyword(
                    suggestedSplit[FIRST_POSITION]),
                  suggestedSplit[SECOND_POSITION],
                  buyerId,
                  lang)));

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
