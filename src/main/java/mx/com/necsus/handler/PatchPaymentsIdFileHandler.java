package mx.com.necsus.handler;

import static io.undertow.server.handlers.form.FormDataParser.FORM_DATA;
import static io.undertow.util.Headers.CONTENT_TYPE_STRING;
import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static io.undertow.util.StatusCodes.FORBIDDEN;
import static io.undertow.util.StatusCodes.UNPROCESSABLE_ENTITY;
import static java.lang.Boolean.FALSE;
import static java.lang.Integer.valueOf;
import static java.nio.file.Files.readAllBytes;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.PaymentStatus.TO_CHECK;
import static mx.com.necsus.domain.catalog.UserRoles.ACCT_OWNER;
import static mx.com.necsus.persistence.PaymentsMapper.updatePaymentFile;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_CAPTURE_DATE;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_CAPTURE_REFERENCE;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_FILE;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_ID;
import static mx.com.necsus.util.Files.extension;
import static mx.com.necsus.util.PaymentsRequestBody.isInvalidPaymentUpdateFormDataSemantic;
import static mx.com.necsus.util.PaymentsRequestBody.isInvalidPaymentUpdateFormDataSyntax;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createNoContentResponse;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static mx.com.necsus.util.TraceHeaders.MULTI_PART_FORM_DATA;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.X_USER_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.isInvalidContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TracePayloads.RESPONSE_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.EagerFormParsingHandler;
import mx.com.necsus.persistence.domain.Payment;

import org.apache.logging.log4j.Logger;

/**
 * Handler to process the partial update of one <strong>Payment</strong>.
 */
public class PatchPaymentsIdFileHandler extends EagerFormParsingHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchPaymentsIdFileHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 32;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 64;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 64;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public PatchPaymentsIdFileHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    if (exchange.isInIoThread()) {

      exchange
        .dispatch(
          this);

      return;

    }

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            var result = "";

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var contentType = getHeader(
                                      exchange,
                                      CONTENT_TYPE_STRING);

            if (isInvalidContentType(
                  exchange,
                  contentType,
                  MULTI_PART_FORM_DATA)) {

              return ;

            }

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var userId = getHeader(
                                 exchange,
                                 X_USER_ID);

            final var roleCode = getHeader(
                                   exchange,
                                   X_ROLE_CODE);

            if (ACCT_OWNER != findUserRole(roleCode) &&
                !accountId.equalsIgnoreCase(userId)) {

              result = "The operation is not allowed for the signed user.";

              LOGGER.info(
                "{} '{}' '{}' '{}' '{}'",
                RESPONSE_PAYLOAD,
                FORBIDDEN,
                userId,
                roleCode,
                result);

              exchange
                .setStatusCode(
                  FORBIDDEN)
                .getResponseSender()
                .send(
                  result);

              return ;

            }

            final var formData = exchange.getAttachment(FORM_DATA);

            LOGGER.info(
                "{} '{}'",
                REQUEST_PAYLOAD,
                formData);

            if (isInvalidPaymentUpdateFormDataSyntax(formData)) {

              result = "Invalid form data, please read the OpenAPI";

              LOGGER.info(
                "{} '{}' '{}' '{}'",
                RESPONSE_PAYLOAD,
                BAD_REQUEST,
                formData,
                result);

              exchange
                .setStatusCode(
                  BAD_REQUEST)
                .getResponseSender()
                .send(
                  result);

              return ;

            }

            if (isInvalidPaymentUpdateFormDataSemantic(formData)) {

              result = "Unprocessable form data, please read the OpenAPI";

              LOGGER.info(
                "{} '{}' '{}' '{}'",
                RESPONSE_PAYLOAD,
                UNPROCESSABLE_ENTITY,
                formData,
                result);

              exchange
                .setStatusCode(
                  UNPROCESSABLE_ENTITY)
                .getResponseSender()
                .send(
                  result);

              return ;

            }

            LOGGER.info(
              "{} '{}'",
              REQUEST_PAYLOAD,
              formData);

            final var fileFormValue = formData.getLast(PAYMENT_FILE);

            final var fileItem = fileFormValue.getFileItem();

            final var now = nowEpochMilli();

            final var payment = new Payment()
                                  .setD0(
                                    valueOf(
                                      getQueryParam(
                                        exchange,
                                        PAYMENT_ID)))
                                  .setD2(
                                    formData
                                      .getLast(PAYMENT_CAPTURE_DATE)
                                      .getValue())
                                  .setD3(
                                    formData
                                      .getLast(PAYMENT_CAPTURE_REFERENCE)
                                      .getValue())
                                  .setD4(
                                    FALSE)
                                  .setD5(
                                    FALSE)
                                  .setF1(
                                    readAllBytes(fileItem.getFile()))
                                  .setF2(
                                    extension(fileFormValue.getFileName()))
                                  .setF3(
                                    fileItem
                                      .getFileSize())
                                  .setF4(
                                    now)
                                  .setS1(
                                    TO_CHECK.name())
                                  .setS2(
                                    now);

            LOGGER.info(
              "Updating '{}' '{}'",
              accountId,
              payment);

            updatePaymentFile(
              accountId,
              payment);

            createNoContentResponse(exchange);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
