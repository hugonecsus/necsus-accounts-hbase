package mx.com.necsus.handler;

import static java.lang.Integer.parseInt;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.NoticeTypes.NOTE;
import static mx.com.necsus.domain.catalog.NotificationStatus.UNREAD;
import static mx.com.necsus.domain.catalog.NotificationTypes.PAYMENT;
import static mx.com.necsus.domain.catalog.UserRoles.CONTROL_DESK;
import static mx.com.necsus.domain.catalog.UserRoles.MANAGER;
import static mx.com.necsus.persistence.PaymentsMapper.isNotExistingPayment;
import static mx.com.necsus.persistence.PaymentsMapper.updatePaymentValidation;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_ID;
import static mx.com.necsus.util.Catalogs.retrieveNotificationTitle;
import static mx.com.necsus.util.NoticesSender.sendRecord2NoticesTopic;
import static mx.com.necsus.util.NotificationsSender.sendRecord2NotificationsTopic;
import static mx.com.necsus.util.Payments.isNotUserAllowed;
import static mx.com.necsus.util.PaymentsRequestBody.isInvalidPaymentSemantic;
import static mx.com.necsus.util.PaymentsRequestBody.isInvalidPaymentSyntax;
import static mx.com.necsus.util.RequestBody.deserializeBody;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.ResponseBody.createForbiddenResponse;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSemanticResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSyntaxResponse;
import static mx.com.necsus.util.ResponseBody.createNoContentResponse;
import static mx.com.necsus.util.ResponseBody.createNotFoundEntityResponse;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.X_USER_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.Notice;
import mx.com.necsus.domain.PaymentUpdateOne;
import mx.com.necsus.persistence.domain.Notification;

import org.apache.logging.log4j.Logger;

/**
 * Handler to process the partial update of one <strong>Payment</strong>.
 */
public class PatchPaymentsIdValidationHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchPaymentsIdValidationHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public PatchPaymentsIdValidationHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    if (exchange.isInIoThread()) {

      exchange
        .dispatch(
          this);

      return;

    }

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            final var traceId = retrieveTraceId(exchange);

            put(X_TRACE_ID, traceId);

            defaultResponseContentType(exchange);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var paymentId = getQueryParam(
                                    exchange,
                                    PAYMENT_ID);

            final var xUserId = getHeader(
                                  exchange,
                                  X_USER_ID);

            final var xRoleCode = getHeader(
                                   exchange,
                                   X_ROLE_CODE);

            LOGGER.info(
              "{} {} '{}' {} '{}' {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              ACCOUNT_ID,
              accountId,
              PAYMENT_ID,
              paymentId,
              X_USER_ID,
              xUserId,
              X_ROLE_CODE,
              xRoleCode);

            if (isNotUserAllowed(
                  xRoleCode,
                  xUserId,
                  accountId)) {

              createForbiddenResponse(
                exchange);

              return ;

            }

            final var paymentUpdate = deserializeBody(
                                        exchange,
                                        PaymentUpdateOne.class);

            if (null == paymentUpdate) {

              return ;

            }

            if (isNotExistingPayment(
                  accountId,
                  parseInt(
                    paymentId))) {

              createNotFoundEntityResponse(
                exchange,
                PAYMENT_ID,
                paymentId);

              return ;

            }

            if (isInvalidPaymentSyntax(paymentUpdate)) {

              createInvalidSyntaxResponse(
                exchange,
                paymentUpdate);

              return ;

            }

            if (isInvalidPaymentSemantic(
                  paymentUpdate,
                  xRoleCode)) {

              createInvalidSemanticResponse(
                exchange,
                paymentUpdate);

              return ;

            }

            updatePaymentValidation(
              accountId,
              parseInt(
                paymentId),
              paymentUpdate,
              xRoleCode);

            if (isNotBlankString(
                  paymentUpdate.getN2())) {

              sendRecord2NotificationsTopic(
                accountId,
                new Notification()
                  .setRoleCode(
                    xRoleCode)
                  .setTypeCode(
                    PAYMENT.name())
                  .setStatusCode(
                    UNREAD.name())
                  .setCreationDate(
                    nowEpochMilli())
                  .setTitle(
                    retrieveNotificationTitle(
                      accountId,
                      xRoleCode))
                  .setMessage(
                    paymentUpdate.getN2()),
                traceId);

              if (CONTROL_DESK == findUserRole(xRoleCode) ||
                  MANAGER == findUserRole(xRoleCode)) {

                sendRecord2NoticesTopic(
                  NOTE.name(),
                  new Notice()
                    .setId(
                      traceId)
                    .setAccountId(
                      accountId)
                    .setUserId(
                      accountId));

              }

            }

            createNoContentResponse(exchange);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
