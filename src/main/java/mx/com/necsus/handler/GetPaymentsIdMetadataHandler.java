package mx.com.necsus.handler;

import static java.lang.Integer.parseInt;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.persistence.PaymentsMapper.retrievePaymentFileMetadata;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_ID;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createNotFoundEntityResponse;
import static mx.com.necsus.util.ResponseBody.createOkResponse;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.CatalogItem;
import mx.com.necsus.domain.FileMetadata;
import mx.com.necsus.domain.PaymentRetrieveOne;

import org.apache.logging.log4j.Logger;

/**
 * Handler to process the retrieve metadata of one <strong>Payment</strong>.
 */
public class GetPaymentsIdMetadataHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetPaymentsIdMetadataHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetPaymentsIdMetadataHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var paymentId = exchange
                                    .getQueryParameters()
                                    .get(PAYMENT_ID)
                                    .getLast();

            final var accountId = exchange
                                    .getQueryParameters()
                                    .get(ACCOUNT_ID)
                                    .getLast();

            LOGGER.info(
              "{} {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              PAYMENT_ID,
              paymentId,
              ACCOUNT_ID,
              accountId);

            final var payment = retrievePaymentFileMetadata(
                                  accountId,
                                  parseInt(paymentId));

            if (null == payment) {

              createNotFoundEntityResponse(
                exchange,
                PAYMENT_ID,
                paymentId);

              return ;

            }

            createOkResponse(
              exchange,
              new PaymentRetrieveOne()
                .setF(
                  new FileMetadata()
                    .setD2(
                      payment.getF2())
                    .setD3(
                      payment.getF3()))
                .setS1(
                  new CatalogItem()
                    .setD1(
                      payment.getS1()))
                .toString());

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }
}
