package mx.com.necsus.handler;

import static io.undertow.util.Headers.CONTENT_TYPE;
import static io.undertow.util.StatusCodes.OK;
import static java.nio.ByteBuffer.wrap;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_USR;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.util.AccountsQueryParams.ACCOUNT_COMPARISON;
import static mx.com.necsus.util.AccountsRetrieveXlsMetadata.retrieveXlsMetadataComparison;
import static mx.com.necsus.util.ResponseBody.createForbiddenResponse;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createNotFoundEntityResponse;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TIME_ZONE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveContentTypeFromExtension;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TracePayloads.RESPONSE_PAYLOAD;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

/**
 * Handler for retrieve of comparison <strong>Accounts</strong>.
 */
public class GetAccountsComparisonHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetAccountsComparisonHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 8;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 16;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 16;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetAccountsComparisonHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var roleCode = getHeader(
                                   exchange,
                                   X_ROLE_CODE);

            final var timeZone = getHeader(
                                   exchange,
                                   X_TIME_ZONE);

            final var lang = getHeader(
                               exchange,
                               X_LANG_CODE);

            final var comparisonDeque = exchange
                                          .getQueryParameters()
                                          .get(ACCOUNT_COMPARISON);

            LOGGER.info(
              "{} {} '{}' {} '{}' {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              X_ROLE_CODE,
              roleCode,
              X_TIME_ZONE,
              timeZone,
              X_LANG_CODE,
              lang,
              ACCOUNT_COMPARISON,
              comparisonDeque);

            if (BYR_OWNER != findUserRole(roleCode) &&
                BYR_USR != findUserRole(roleCode)) {

              createForbiddenResponse(exchange);

              return ;

            }

            final var persistenceFile = retrieveXlsMetadataComparison(
                                          comparisonDeque,
                                          timeZone,
                                          lang);

            if (null == persistenceFile) {

              createNotFoundEntityResponse(
                exchange,
                ACCOUNT_COMPARISON,
                comparisonDeque
                  .getLast());

            return ;

           }

            final var contentType =   retrieveContentTypeFromExtension(
                                        persistenceFile
                                          .getExtension());

            LOGGER.info(
                "{} '{}' {} '{}' '{}'",
                RESPONSE_PAYLOAD,
                OK,
                CONTENT_TYPE,
                contentType,
                persistenceFile);

              exchange
                .getResponseHeaders()
                  .put(
                    CONTENT_TYPE,
                    contentType);

              exchange
                .setStatusCode(
                  OK)
                .getResponseSender()
                  .send(
                   wrap(
                     persistenceFile
                       .getContent()));

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
