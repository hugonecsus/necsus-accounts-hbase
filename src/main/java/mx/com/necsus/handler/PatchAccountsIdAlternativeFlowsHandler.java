package mx.com.necsus.handler;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.AccountStatus.CERTIFIED;
import static mx.com.necsus.domain.catalog.AccountStatus.INFORMATION;
import static mx.com.necsus.domain.catalog.AccountStatus.PLAN;
import static mx.com.necsus.domain.catalog.CertificationStatus.CHANGE_PLAN;
import static mx.com.necsus.domain.catalog.CertificationStatus.RENEWAL;
import static mx.com.necsus.domain.catalog.CertificationStatus.UPDATE;
import static mx.com.necsus.domain.catalog.CertificationStatus.UPGRADE;
import static mx.com.necsus.domain.catalog.PostCertificationStatus.RENEWAL_INITIATED;
import static mx.com.necsus.domain.catalog.PostCertificationStatus.UPDATE_INITIATED;
import static mx.com.necsus.domain.catalog.PostCertificationStatus.UPGRADE_INITIATED;
import static mx.com.necsus.persistence.AccountsMapper.retrieveCertificationStatus;
import static mx.com.necsus.persistence.AccountsMapper.retrievePostCertificationStatus2Cancel;
import static mx.com.necsus.persistence.AccountsMapper.updatePostCertificationStatus;
import static mx.com.necsus.persistence.AlternativeFlowsMapper.findAlternativeFlow;
import static mx.com.necsus.persistence.PaymentsMapper.deleteAllPayments;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.util.AccountsCommons.retrievePostCertificationVersion;
import static mx.com.necsus.util.AccountsQueryParams.ALTERNATIVE_FLOW;
import static mx.com.necsus.util.ResponseBody.QUERY_PARAM;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidResponse;
import static mx.com.necsus.util.ResponseBody.createNoContentResponse;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.persistence.domain.Account;

import org.apache.logging.log4j.Logger;

/**
 * Handler to process the partial update for <strong>alternative flows</strong>.
 */
public class PatchAccountsIdAlternativeFlowsHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchAccountsIdAlternativeFlowsHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Value of the initial post certification version.
   */
  private static final long INITIAL_POST_CERTIFICATION_VERSION = 1;

  /**
   * Explicit default constructor.
   */
  public PatchAccountsIdAlternativeFlowsHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var alternativeFlow = getQueryParam(
                                          exchange,
                                          ALTERNATIVE_FLOW);

            LOGGER.info(
              "{} {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              ACCOUNT_ID,
              accountId,
              ALTERNATIVE_FLOW,
              alternativeFlow);

            final var put = newPut(
                              accountId);

            final var now = nowEpochMilli();

            final var account = new Account()
                                  .setStatusDate(
                                    now)
                                  .setPostCertificationDate(
                                    now)
                                  .setCertificationStatusDate(
                                    now);

            var postCertificationVersion = retrievePostCertificationVersion(accountId);

            switch (findAlternativeFlow(alternativeFlow)) {

              case UPDATE:

                account
                  .setStatusCode(
                    INFORMATION.name())
                  .setPostCertificationStatus(
                    UPDATE_INITIATED.name())
                  .setPostCertificationVersion(
                    null == postCertificationVersion
                      ? INITIAL_POST_CERTIFICATION_VERSION
                      : ++postCertificationVersion)
                  .setCertificationStatusCode(
                    UPDATE.name());

                break ;

              case UPGRADE:

                account
                  .setStatusCode(
                    PLAN.name())
                  .setPostCertificationStatus(
                    UPGRADE_INITIATED.name())
                  .setPostCertificationVersion(
                    null == postCertificationVersion
                      ? INITIAL_POST_CERTIFICATION_VERSION
                      : ++postCertificationVersion)
                  .setCertificationStatusCode(
                    UPGRADE.name());

                break ;

              case CANCEL:

                account
                  .setStatusCode(
                    CERTIFIED.name())
                  .setPostCertificationStatus(
                    retrievePostCertificationStatus2Cancel(
                      accountId))
                  .setCertificationStatusCode(
                    retrieveCertificationStatus(
                      accountId));

                break ;

              case RENEWAL:

                account
                  .setStatusCode(
                    PLAN.name())
                  .setPostCertificationStatus(
                    RENEWAL_INITIATED.name())
                  .setPostCertificationVersion(
                    null == postCertificationVersion
                      ? INITIAL_POST_CERTIFICATION_VERSION
                      : ++postCertificationVersion)
                  .setCertificationStatusCode(
                    RENEWAL.name());

                deleteAllPayments(
                  put);

                break ;

              case PLAN:

                account
                  .setStatusCode(
                    PLAN.name())
                  .setCertificationStatusCode(
                    CHANGE_PLAN.name());

                deleteAllPayments(
                  put);

                break ;

              default:

                createInvalidResponse(
                  exchange,
                  QUERY_PARAM,
                  ALTERNATIVE_FLOW,
                  alternativeFlow);

                return ;

            }

            LOGGER.info(
              "Alternative flow '{}' for '{}'",
              alternativeFlow,
              account);

            updatePostCertificationStatus(
              put,
              account);

            createNoContentResponse(exchange);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
