package mx.com.necsus.handler;

import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.util.AccountsQueryParams.ACCOUNT_COMPARISON;
import static mx.com.necsus.util.AccountsQueryParams.SEARCH;
import static mx.com.necsus.util.AccountsQueryParams.SEARCH_VISITS;
import static mx.com.necsus.util.AccountsQueryParams.STAGE;
import static mx.com.necsus.util.AccountsQueryParams.SUGGESTED;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TracePayloads.RESPONSE_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

;

/**
 * Handler to process the retrieve of all <strong>Accounts</strong>.
 */
public class GetAccountsHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetAccountsHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Instance variable for the {@link GetAccountsStageHandler}.
   */
  private final HttpHandler getAccountsStageHandler;

  /**
   * Instance variable for the {@link GetAccountsSuggestedHandler}.
   */
  private final HttpHandler getAccountSuggestedHandler;

  /**
   * Instance variable for the {@link GetAccountsSearchHandler}.
   */
  private final HttpHandler getAccountsSearchHandler;

  /**
   * Instance variable for the {@link GetAccountsSearchVisitsHandler}.
   */
  private final HttpHandler getAccountsSearchVisitsHandler;

  /**
   * Instance variable for the {@link GetAccountsComparisonHandler}.
   */
  private final HttpHandler getAccountsComparisonHandler;

  /**
   * Explicit default constructor.
   */
  public GetAccountsHandler() {

    this.getAccountsStageHandler = new GetAccountsStageHandler();

    this.getAccountSuggestedHandler = new GetAccountsSuggestedHandler();

    this.getAccountsSearchHandler = new GetAccountsSearchHandler();

    this.getAccountsSearchVisitsHandler = new GetAccountsSearchVisitsHandler();

    this.getAccountsComparisonHandler = new GetAccountsComparisonHandler();

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var stage = getQueryParam(
                                exchange,
                                STAGE);

            final var suggested = getQueryParam(
                                    exchange,
                                    SUGGESTED);

            final var search = getQueryParam(
                                 exchange,
                                 SEARCH);

            final var searchVisits = getQueryParam(
                                       exchange,
                                       SEARCH_VISITS);

            final var accountComparison = getQueryParam(
                                             exchange,
                                             ACCOUNT_COMPARISON);

            LOGGER.info(
              "{} {} '{}' {} '{}' {} '{}' {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              STAGE,
              stage,
              SUGGESTED,
              suggested,
              SEARCH,
              search,
              SEARCH_VISITS,
              searchVisits,
              ACCOUNT_COMPARISON,
              accountComparison);

            if (isNotBlankString(stage)) {

              getAccountsStageHandler.handleRequest(exchange);

              return ;

            }

            if (isNotBlankString(suggested)) {

              getAccountSuggestedHandler.handleRequest(exchange);

              return ;

            }

            if (isNotBlankString(search)) {

              getAccountsSearchHandler.handleRequest(exchange);

              return ;

            }

            if (isNotBlankString(searchVisits)) {

              getAccountsSearchVisitsHandler.handleRequest(exchange);

              return ;

            }

            if (isNotBlankString(accountComparison)) {

              getAccountsComparisonHandler.handleRequest(exchange);

              return ;

            }

            final var result = "Almost one query param must be specified, please read the OpenAPI";

            LOGGER.info(
              "{} '{}' '{}'",
              RESPONSE_PAYLOAD,
              BAD_REQUEST,
              result);

            exchange
              .setStatusCode(
                BAD_REQUEST)
              .getResponseSender()
              .send(
                result);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
