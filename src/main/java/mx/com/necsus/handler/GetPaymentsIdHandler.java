package mx.com.necsus.handler;

import static io.undertow.util.Headers.ACCEPT;
import static io.undertow.util.Headers.ACCEPT_STRING;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_ID;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.ResponseBody.HEADER;
import static mx.com.necsus.util.ResponseBody.PATH_VARIABLE;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidResponse;
import static mx.com.necsus.util.ResponseBody.createRequiredResponse;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_JSON;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

/**
 * Handler to process the retrieve of one <strong>Payment</strong>.
 */
public class GetPaymentsIdHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetPaymentsIdHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Instance variable for the {@link GetPaymentsIdMetadataHandler}.
   */
  private final HttpHandler getPaymentsIdMetadataHandler;

  /**
   * Instance variable for the {@link GetPaymentsIdFileHandler}.
   */
  private final HttpHandler getPaymentsIdFileHandler;

  /**
   * Explicit default constructor.
   */
  public GetPaymentsIdHandler() {

    this.getPaymentsIdMetadataHandler = new GetPaymentsIdMetadataHandler();

    this.getPaymentsIdFileHandler = new GetPaymentsIdFileHandler();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var paymentIdDeque = exchange
                                         .getQueryParameters()
                                         .get(PAYMENT_ID);

            final var acceptDeque = exchange
                                      .getRequestHeaders()
                                      .get(ACCEPT);

            LOGGER.info(
              "{} {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              PAYMENT_ID,
              paymentIdDeque,
              ACCEPT,
              acceptDeque);

            final var paymentId = null == paymentIdDeque
                                    ? ""
                                    : paymentIdDeque
                                        .getLast()
                                        .trim();

            if (isBlankString(paymentId)) {

              createRequiredResponse(
                exchange,
                PATH_VARIABLE,
                PAYMENT_ID,
                paymentId);

              return ;

            }

            if (!paymentId.matches("^[0-4]+$")) {

              createInvalidResponse(
                exchange,
                PATH_VARIABLE,
                PAYMENT_ID,
                paymentId);

              return ;

            }

            final var accept = null == acceptDeque
                                      ? ""
                                      : acceptDeque
                                          .getLast();

            if (isNotBlankString(accept)) {

              if (accept.startsWith(APPLICATION_JSON)) {

                getPaymentsIdMetadataHandler.handleRequest(exchange);

                return ;

              }

              getPaymentsIdFileHandler.handleRequest(exchange);

              return ;

            }

            createRequiredResponse(
              exchange,
              HEADER,
              ACCEPT_STRING,
              accept);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
