package mx.com.necsus.handler;

import static io.undertow.util.StatusCodes.BAD_REQUEST;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.persistence.AccountsMapper.isNotExistingAccount;
import static mx.com.necsus.util.AccountsQueryParams.ALTERNATIVE_FLOW;
import static mx.com.necsus.util.AccountsQueryParams.COMPLETED;
import static mx.com.necsus.util.AccountsQueryParams.SCOPE;
import static mx.com.necsus.util.AccountsQueryParams.UPDATE_OWNER;
import static mx.com.necsus.util.AccountsQueryParams.BUYERS;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createNotFoundEntityResponse;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TracePayloads.RESPONSE_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

/**
 * Handler to process the partial update of one <strong>Account</strong>.
 */
public class PatchAccountsIdHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchAccountsIdHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 8;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Instance variable for the {@link PatchAccountsIdCompletedHandler}.
   */
  private final HttpHandler patchAccountsIdCompletedHandler;

  /**
   * Instance variable for the {@link PatchAccountsIdAlternativeFlowsHandler}
   */
  private final HttpHandler patchAccountsIdPostCertificationFlowsHandler;

  /**
   * Instance variable for the {@link PatchAccountsIdPrivacyHandler}
   */
  private final HttpHandler patchAccountsIdPrivacyHandler;

  /**
   * Instance variable for the {@link PatchAccountIdAccountOwnerHandler}
   */
  private final HttpHandler patchAccountIdAccountOwnerHandler;


  /**
   * Instance variable for the {@link PatchAccountsIdBuyerAssociationHandler}
   */

  private final HttpHandler patchAccountsIdBuyerAssociationHandler;

  /**
   * Explicit default constructor.
   */
  public PatchAccountsIdHandler() {

    this.patchAccountsIdCompletedHandler = new PatchAccountsIdCompletedHandler();

    this.patchAccountsIdPostCertificationFlowsHandler = new PatchAccountsIdAlternativeFlowsHandler();

    this.patchAccountsIdPrivacyHandler = new PatchAccountsIdPrivacyHandler();

    this.patchAccountIdAccountOwnerHandler = new PatchAccountIdAccountOwnerHandler();

    this.patchAccountsIdBuyerAssociationHandler = new PatchAccountsIdBuyerAssociationHandler();

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var completed = getQueryParam(
                                    exchange,
                                    COMPLETED);

            final var postCertification = getQueryParam(
                                            exchange,
              ALTERNATIVE_FLOW);

            final var scope = getQueryParam(
                                exchange,
                                SCOPE);

            final var updated = getQueryParam(
                                  exchange,
                                  UPDATE_OWNER);

            final var buyers = getQueryParam(
                                 exchange,
                                 BUYERS);

            LOGGER.info(
              "{} {} '{}' {} '{}' {} '{}' {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              ACCOUNT_ID,
              accountId,
              COMPLETED,
              completed,
              ALTERNATIVE_FLOW,
              postCertification,
              SCOPE,
              scope,
              BUYERS,
              buyers);

            if (isNotExistingAccount(accountId)) {

              createNotFoundEntityResponse(
                exchange,
                ACCOUNT_ID,
                accountId);

              return ;

            }

            if (isNotBlankString(completed)) {

              patchAccountsIdCompletedHandler.handleRequest(exchange);

              return ;

            }

            if (isNotBlankString(postCertification)) {

              patchAccountsIdPostCertificationFlowsHandler.handleRequest(exchange);

              return ;

            }

            if (isNotBlankString(scope)) {

              patchAccountsIdPrivacyHandler.handleRequest(exchange);

              return ;

            }

            if (isNotBlankString(updated)) {

              patchAccountIdAccountOwnerHandler.handleRequest(exchange);

              return ;

            }

            if (isNotBlankString(buyers)) {
              patchAccountsIdBuyerAssociationHandler.handleRequest(exchange);
              return ;
            }

            final var result = "Almost one query param must be specified, please read the OpenAPI";

            LOGGER.info(
              "{} '{}' '{}'",
              RESPONSE_PAYLOAD,
              BAD_REQUEST,
              result);

            exchange
              .setStatusCode(
                BAD_REQUEST)
              .getResponseSender()
              .send(
                result);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
