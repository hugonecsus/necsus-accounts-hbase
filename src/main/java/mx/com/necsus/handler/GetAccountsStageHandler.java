package mx.com.necsus.handler;

import static com.jsoniter.output.JsonStream.serialize;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.Stages.NULL;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAccounts4CertifiedStage;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAccounts4DisclaimerStage;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAccounts4InformationStage;
import static mx.com.necsus.persistence.MatchesMapper.retrieveAccounts4MatchesStage;
import static mx.com.necsus.persistence.PaymentsMapper.retrieveAccounts4PaymentStage;
import static mx.com.necsus.persistence.StagesMapper.findStage;
import static mx.com.necsus.persistence.VisitsMapper.retrieveAccounts4VisitStage;
import static mx.com.necsus.util.Accounts.isNotUserAllowed;
import static mx.com.necsus.util.AccountsQueryParams.STAGE;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static mx.com.necsus.util.ResponseBody.QUERY_PARAM;
import static mx.com.necsus.util.ResponseBody.createForbiddenResponse;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidResponse;
import static mx.com.necsus.util.ResponseBody.createOkResponse;
import static mx.com.necsus.util.ResponseBody.createRequiredResponse;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TIME_ZONE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.X_USER_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.AccountRetrieveAll;

import org.apache.logging.log4j.Logger;

/**
 * GetAccountsStageHandler, a particular handler to retrieve all accounts.
 */
public class GetAccountsStageHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetAccountsStageHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 8;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 16;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 16;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetAccountsStageHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var userId = getHeader(
                                 exchange,
                                 X_USER_ID);

            final var roleCode = getHeader(
                                   exchange,
                                   X_ROLE_CODE);

            final var timeZone = getHeader(
                                   exchange,
                                   X_TIME_ZONE);

            final var lang = getHeader(
                               exchange,
                               X_LANG_CODE);

            final var stage = getQueryParam(
                                exchange,
                                STAGE);

            LOGGER.info(
              "{} {} '{}' {} '{}' {} '{}' {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              X_USER_ID,
              userId,
              X_ROLE_CODE,
              roleCode,
              X_TIME_ZONE,
              timeZone,
              X_LANG_CODE,
              lang,
              STAGE,
              stage);

            if (isNotUserAllowed(roleCode)) {

              createForbiddenResponse(exchange);

              return ;

            }

            if (isBlankString(stage) ||
                NULL == findStage(stage)) {

              createRequiredResponse(
                exchange,
                QUERY_PARAM,
                STAGE,
                stage);

              return ;

            }

            List<AccountRetrieveAll> result = null;

            switch (findStage(stage)) {

              case PAYMENTS_GRAY:
              case PAYMENTS_RED:
              case PAYMENTS_GREEN:

                result = retrieveAccounts4PaymentStage(
                           userId,
                           stage,
                           timeZone,
                           lang,
                           roleCode);

                break ;

              case INFORMATIONS_GRAY:
              case INFORMATIONS_RED:
              case INFORMATIONS_YELLOW:
              case INFORMATIONS_GREEN:

                result = retrieveAccounts4InformationStage(
                           userId,
                           stage,
                           timeZone,
                           lang,
                           roleCode);

                break ;

              case MATCHES_GRAY:
              case MATCHES_YELLOW:
              case MATCHES_GREEN:

                result = retrieveAccounts4MatchesStage(
                           userId,
                           stage,
                           timeZone,
                           lang,
                           roleCode);

                break ;

              case VISITS_GRAY:
              case VISITS_RED:
              case VISITS_YELLOW:
              case VISITS_GREEN:

                result = retrieveAccounts4VisitStage(
                           userId,
                           stage,
                           timeZone,
                           lang,
                           roleCode);

                break ;

              case DISCLAIMERS_GRAY:
              case DISCLAIMERS_RED:
              case DISCLAIMERS_GREEN:

                result = retrieveAccounts4DisclaimerStage(
                           userId,
                           stage,
                           timeZone,
                           lang,
                           roleCode);

                break ;

              case CERTIFICATES:

                result = retrieveAccounts4CertifiedStage(
                            timeZone,
                            lang);

                break ;

              default:

                createInvalidResponse(
                  exchange,
                  QUERY_PARAM,
                  STAGE,
                  stage);

                return ;

            }

            LOGGER.info(
              "Found '{}' accounts",
              result.size());

            createOkResponse(
              exchange,
              serialize(
                result));

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
