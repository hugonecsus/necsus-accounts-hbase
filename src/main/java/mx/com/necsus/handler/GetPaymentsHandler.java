package mx.com.necsus.handler;

import static com.jsoniter.output.JsonStream.serialize;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.persistence.AccountsMapper.isNotExistingAccount;
import static mx.com.necsus.persistence.PaymentsMapper.retrieveAllPayments;
import static mx.com.necsus.util.Payments.isNotUserAllowed;
import static mx.com.necsus.util.ResponseBody.createForbiddenResponse;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createNotFoundEntityResponse;
import static mx.com.necsus.util.ResponseBody.createOkResponse;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TIME_ZONE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.X_USER_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

/**
 * Handler to process the retrieve of all <strong>Payments</strong>.
 */
public class GetPaymentsHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetPaymentsHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetPaymentsHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var accountId = exchange
                                    .getQueryParameters()
                                    .get(ACCOUNT_ID)
                                    .getLast();

            final var timeZone = exchange
                                   .getRequestHeaders()
                                   .get(X_TIME_ZONE)
                                   .getLast();

            final var lang = exchange
                               .getRequestHeaders()
                               .get(X_LANG_CODE)
                               .getLast();

            LOGGER.info(
              "{} {} '{}' {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              ACCOUNT_ID,
              accountId,
              X_TIME_ZONE,
              timeZone,
              X_LANG_CODE,
              lang);

            if (isNotExistingAccount(accountId)) {

              createNotFoundEntityResponse(
                exchange,
                ACCOUNT_ID,
                accountId);

              return ;

            }

            final var userId = exchange
                                 .getRequestHeaders()
                                 .get(X_USER_ID)
                                 .getLast();

            final var roleCode = exchange
                                   .getRequestHeaders()
                                   .get(X_ROLE_CODE)
                                   .getLast();

            if (isNotUserAllowed(
                  roleCode,
                  userId,
                  accountId)) {

              createForbiddenResponse(
                exchange);

              return ;

            }

            createOkResponse(
              exchange,
              serialize(
                retrieveAllPayments(
                  accountId,
                  roleCode,
                  timeZone,
                  lang)));

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
