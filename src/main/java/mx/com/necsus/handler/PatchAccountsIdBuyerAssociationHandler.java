package mx.com.necsus.handler;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.persistence.BuyersAssociationMapper.updateBuyerAssociation;
import static mx.com.necsus.util.Accounts.isNotUserAllowed4BuyerAssociation;
import static mx.com.necsus.util.Accounts.sendRecord2Notices4BuyerAssociation;
import static mx.com.necsus.util.AccountsQueryParams.BUYERS;
import static mx.com.necsus.util.AccountsRequestBody.isInvalidAccountAssociationUpdateSemantic;
import static mx.com.necsus.util.AccountsRequestBody.isInvalidAccountAssociationUpdateSyntax;
import static mx.com.necsus.util.AccountsStrings.LOG_REQUEST_BUYERS_ASSOCIATED_HANDLER_PATCH;
import static mx.com.necsus.util.RequestBody.deserializeBody;
import static mx.com.necsus.util.ResponseBody.createForbiddenResponse;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSemanticResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSyntaxResponse;
import static mx.com.necsus.util.ResponseBody.createNoContentResponse;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_USER_ID;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

import mx.com.necsus.domain.AccountUpdateOne;

import org.apache.logging.log4j.Logger;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 * Handler to process the update the buyers associates of one <strong>Account</strong>.
 */
public class PatchAccountsIdBuyerAssociationHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchAccountsIdBuyerAssociationHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new ThreadPoolExecutor.CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public PatchAccountsIdBuyerAssociationHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var xUserId = getHeader(
                                  exchange,
                                  X_USER_ID);

            final var xRoleCode = getHeader(
                                    exchange,
                                    X_ROLE_CODE);

            final var buyers = getQueryParam(
                                 exchange,
                                 BUYERS);

            LOGGER.info(
              LOG_REQUEST_BUYERS_ASSOCIATED_HANDLER_PATCH,
              REQUEST_PAYLOAD,
              ACCOUNT_ID,
              accountId,
              X_USER_ID,
              xUserId,
              X_ROLE_CODE,
              xRoleCode,
              BUYERS,
              buyers);

            if (isNotUserAllowed4BuyerAssociation(
                 xRoleCode,
                 xUserId,
                 accountId)) {

                 createForbiddenResponse(
                   exchange);

              return ;

            }

            final var accountUpdateOne = deserializeBody(
                                           exchange,
                                           AccountUpdateOne.class);

            if (null == accountUpdateOne) {

              return ;

            }

            if (isInvalidAccountAssociationUpdateSyntax(
                 accountUpdateOne
                   .getD4())){

                 createInvalidSyntaxResponse(
                   exchange,
                   accountUpdateOne);

              return ;

            }

            if (isInvalidAccountAssociationUpdateSemantic(
                 accountUpdateOne
                   .getD4())){

                 createInvalidSemanticResponse(
                   exchange,
                   accountUpdateOne);

              return;

            }

            sendRecord2Notices4BuyerAssociation(
              accountId,
              accountUpdateOne);

            updateBuyerAssociation(
              accountId,
              accountUpdateOne
                .getD4());

            createNoContentResponse(
              exchange);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}