package mx.com.necsus.handler;

import static io.undertow.server.handlers.form.FormParserFactory.builder;
import static io.undertow.util.Headers.CONTENT_TYPE;
import static io.undertow.util.Headers.CONTENT_TYPE_STRING;
import static io.undertow.util.StatusCodes.UNSUPPORTED_MEDIA_TYPE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_ID;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static mx.com.necsus.util.RequestBody.isNotBlankString;
import static mx.com.necsus.util.ResponseBody.PATH_VARIABLE;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidResponse;
import static mx.com.necsus.util.ResponseBody.createRequiredResponse;
import static mx.com.necsus.util.TraceHeaders.APPLICATION_JSON;
import static mx.com.necsus.util.TraceHeaders.MULTI_PART_FORM_DATA;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TracePayloads.RESPONSE_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.EagerFormParsingHandler;
import io.undertow.server.handlers.form.MultiPartParserDefinition;
import org.apache.logging.log4j.Logger;

/**
 * Handler to process the partial update of one <strong>Payment</strong>.
 */
public class PatchPaymentsIdHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchPaymentsIdHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 8;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 16;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 16;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Instance variable for the {@link PatchPaymentsIdFileHandler}.
   */
  private final HttpHandler patchPaymentsIdFileHandler;

  /**
   * Instance variable for the {@link PatchPaymentsIdValidationHandler}.
   */
  private final HttpHandler patchPaymentsIdValidationHandler;

  /**
   * Explicit default constructor.
   */
  public PatchPaymentsIdHandler() {

    this.patchPaymentsIdFileHandler = new EagerFormParsingHandler(
                                          builder()
                                            .addParser(
                                              new MultiPartParserDefinition())
                                            .build())
                                        .setNext(new PatchPaymentsIdFileHandler());

    this.patchPaymentsIdValidationHandler = new PatchPaymentsIdValidationHandler();

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var paymentId = getQueryParam(
                                    exchange,
                                    PAYMENT_ID);

            final var contentType = getHeader(
                                      exchange,
                                      CONTENT_TYPE_STRING);

            LOGGER.info(
              "{} {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              PAYMENT_ID,
              paymentId,
              CONTENT_TYPE,
              contentType);

            if (isBlankString(paymentId)) {

              createRequiredResponse(
                exchange,
                PATH_VARIABLE,
                PAYMENT_ID,
                paymentId);

              return ;

            }

            if (!paymentId.matches("^[0-9]+$")) {

              createInvalidResponse(
                exchange,
                PATH_VARIABLE,
                PAYMENT_ID,
                paymentId);

              return ;

            }

            if (isNotBlankString(contentType)) {

              if (contentType.startsWith(APPLICATION_JSON)) {

                patchPaymentsIdValidationHandler.handleRequest(exchange);

                return ;

              }

              if (contentType.startsWith(MULTI_PART_FORM_DATA)) {

                patchPaymentsIdFileHandler.handleRequest(exchange);

                return ;

              }

            }

            final var result = "The content type is unsupported";

            LOGGER.info(
              "{} '{}' '{}' '{}'",
              RESPONSE_PAYLOAD,
              UNSUPPORTED_MEDIA_TYPE,
              contentType,
              result);

            exchange
              .setStatusCode(
                UNSUPPORTED_MEDIA_TYPE)
              .endExchange();

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
