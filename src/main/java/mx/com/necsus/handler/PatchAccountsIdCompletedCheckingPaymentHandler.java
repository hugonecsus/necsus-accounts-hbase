package mx.com.necsus.handler;

import static java.lang.Boolean.FALSE;
import static java.lang.String.format;
import static java.time.LocalDateTime.parse;
import static java.time.ZoneOffset.UTC;
import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.UUID.randomUUID;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_PAYMENT;
import static mx.com.necsus.domain.catalog.AccountStatus.PAYMENT_VALIDATED;
import static mx.com.necsus.domain.catalog.CertificationTypes.STD;
import static mx.com.necsus.domain.catalog.FileCodes.S1B3FA;
import static mx.com.necsus.domain.catalog.FileCodes.S1B5FA;
import static mx.com.necsus.domain.catalog.FileCodes.S1B5FB;
import static mx.com.necsus.domain.catalog.FileCodes.S1B5FC;
import static mx.com.necsus.domain.catalog.LegalEntities.LEGAL;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAccount4Completed;
import static mx.com.necsus.persistence.AccountsMapper.updateAccount4CompletePaymentValidated;
import static mx.com.necsus.persistence.AccountsTable.ACCOUNTS_COLUMN_FAMILY_DEFAULT;
import static mx.com.necsus.persistence.CertificationTypesMapper.findCertificationType;
import static mx.com.necsus.persistence.LegalEntitiesMapper.findLegalEntity;
import static mx.com.necsus.persistence.PaymentQualifiers.PY_D6;
import static mx.com.necsus.persistence.PostCertificationStatusMapper.findPostCertificationStatus;
import static mx.com.necsus.persistence.Section1Qualifiers.S1_D1;
import static mx.com.necsus.persistence.Section3Qualifiers.S3_D1;
import static mx.com.necsus.persistence.Section8Qualifiers.S8_D1;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.persistence.TypesHandler.putValue;
import static mx.com.necsus.util.Accounts.adjustFiscalYears4Renewal;
import static mx.com.necsus.util.Accounts.createFiscalYears;
import static mx.com.necsus.util.Accounts.isLatestPlan;
import static mx.com.necsus.util.AccountsCommons.deleteFile;
import static mx.com.necsus.util.AccountsStrings.LOG_ACCOUNT_RETRIEVED;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createNoContentResponse;
import static mx.com.necsus.util.Time.now;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

/**
 * Handler to process the partial update of one <strong>Account</strong>.
 */
public class PatchAccountsIdCompletedCheckingPaymentHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchAccountsIdCompletedCheckingPaymentHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Expire time in months for the first time.
   */
  private static final long EXPIRE_TIME_IN_MONTHS_FIRST_TIME = 13;

  /**
   * Expire time in months for renewals.
   */
  private static final long EXPIRE_TIME_IN_MONTHS_RENEWAL = 12;

  /**
   * Payment capture date pattern.
   */
  private static final String PAYMENT_CAPTURE_DATE_PATTERN = "%s 00:00:00";

  /**
   * {@link DateTimeFormatter} for the payment capture date.
   */
  private static final DateTimeFormatter  PAYMENT_CAPTURE_DATE_FORMATTER = ofPattern("dd/MM/yyyy HH:mm:ss");

  /**
   * Explicit default constructor.
   */
  public PatchAccountsIdCompletedCheckingPaymentHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var now = now();

            final var nowEpocMilli = now
                                       .toInstant()
                                       .toEpochMilli();

            final var account = retrieveAccount4Completed(accountId);

            LOGGER.info(
              LOG_ACCOUNT_RETRIEVED,
              account);

            final var put = newPut(accountId);

            switch (findPostCertificationStatus(
                      account
                        .getPostCertificationStatus())) {

              case UPGRADE_INITIATED:

                if (STD == findCertificationType(
                             account
                               .getCurrentCertificationType())) {

                  if (LEGAL == findLegalEntity(
                                 account
                                   .getLegalEntityCode())) {

                    putValue(
                      put,
                      ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                      S1_D1,
                      account
                        .getPostCertificationVersion(),
                      FALSE);

                  }

                  putValue(
                    put,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S3_D1,
                    account
                      .getPostCertificationVersion(),
                    FALSE);

                  putValue(
                    put,
                    ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                    S8_D1,
                    account
                      .getPostCertificationVersion(),
                    FALSE);

                }

                break ;

              case RENEWAL_INITIATED:

                putValue(
                  put,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  PY_D6,
                  account
                    .getPaymentCaptureDate());

                putValue(
                  put,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  S1_D1,
                  account
                    .getPostCertificationVersion(),
                  FALSE);

                if (LEGAL == findLegalEntity(
                               account
                                 .getLegalEntityCode())) {

                  deleteFile(
                    put,
                    S1B5FA.name(),
                    account
                      .getPostCertificationVersion());

                  deleteFile(
                    put,
                    S1B5FB.name(),
                    account
                      .getPostCertificationVersion());

                }

                deleteFile(
                  put,
                  S1B3FA.name(),
                  account
                    .getPostCertificationVersion());

                deleteFile(
                  put,
                  S1B5FC.name(),
                  account
                    .getPostCertificationVersion());

                adjustFiscalYears4Renewal(
                  put,
                  now,
                  account);

                account
                  .setCertificationId(
                    randomUUID()
                      .toString())
                  .setExpireDate(
                    parse(
                      format(
                        PAYMENT_CAPTURE_DATE_PATTERN,
                        account
                          .getPaymentCaptureDate()),
                      PAYMENT_CAPTURE_DATE_FORMATTER)
                      .atZone(
                        UTC)
                      .plusMonths(
                        EXPIRE_TIME_IN_MONTHS_RENEWAL)
                      .toInstant()
                      .toEpochMilli());

                break ;

              default:

                putValue(
                  put,
                  ACCOUNTS_COLUMN_FAMILY_DEFAULT,
                  PY_D6,
                  account
                    .getPaymentCaptureDate());

                createFiscalYears(
                  put,
                  account
                    .getSignUpDate());

                account
                  .setCertificationId(
                    randomUUID()
                      .toString())
                  .setExpireDate(
                    parse(
                      format(
                        PAYMENT_CAPTURE_DATE_PATTERN,
                        account
                          .getPaymentCaptureDate()),
                      PAYMENT_CAPTURE_DATE_FORMATTER)
                      .atZone(
                        UTC)
                      .plusMonths(
                        EXPIRE_TIME_IN_MONTHS_FIRST_TIME)
                      .toInstant()
                      .toEpochMilli());

            }

            LOGGER.info(
              "Complete {} '{}'",
              CHECKING_PAYMENT,
              account
                .setRowKey(
                  accountId)
                .setStatusCode(
                  PAYMENT_VALIDATED.name())
                .setStatusDate(
                  nowEpocMilli)
                .setIsLatestPlan(
                  isLatestPlan(
                    account
                      .getCertificationPlan())));

            updateAccount4CompletePaymentValidated(
              put,
              account);

            createNoContentResponse(exchange);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
