package mx.com.necsus.handler;

import static com.jsoniter.output.JsonStream.serialize;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.NotesValues.NULL;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAllDisclaimerNotes;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAllInformationNotes;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAllMatchesNotes;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAllPaymentNotes;
import static mx.com.necsus.persistence.NotesValuesMapper.findNoteValue;
import static mx.com.necsus.util.AccountsQueryParams.NOTES;
import static mx.com.necsus.util.ResponseBody.QUERY_PARAM;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidResponse;
import static mx.com.necsus.util.ResponseBody.createOkResponse;
import static mx.com.necsus.util.ResponseBody.createRequiredResponse;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TIME_ZONE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.AccountRetrieveOne;
import mx.com.necsus.domain.Note;

import org.apache.logging.log4j.Logger;

/**
 * Handler to the retrieve all notes of one <strong>Account</strong>.
 */
public class GetAccountsIdNotesHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetAccountsIdNotesHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetAccountsIdNotesHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var accountId = exchange
                                    .getQueryParameters()
                                    .get(ACCOUNT_ID)
                                    .getLast();

            final var notes = exchange
                                .getQueryParameters()
                                .get(NOTES)
                                .getLast();

            final var timeZone = exchange
                                   .getRequestHeaders()
                                   .get(X_TIME_ZONE)
                                   .getLast();

            final var lang = exchange
                               .getRequestHeaders()
                               .get(X_LANG_CODE)
                               .getLast();

            LOGGER.info(
              "{} {} '{}' {} '{}' {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              ACCOUNT_ID,
              accountId,
              NOTES,
              notes,
              X_TIME_ZONE,
              timeZone,
              X_LANG_CODE,
              lang);

            if (NULL == findNoteValue(notes)) {

              createRequiredResponse(
                exchange,
                QUERY_PARAM,
                NOTES,
                notes);

              return ;

            }

            List<Note> result = null;

            switch (findNoteValue(notes)) {

              case PAYMENT:

                result = retrieveAllPaymentNotes(
                           accountId,
                            timeZone,
                            lang);

                break;

              case INFORMATION:

                result = retrieveAllInformationNotes(
                           accountId,
                           timeZone,
                           lang);

                break;

              case MATCHES:

                result = retrieveAllMatchesNotes(
                           accountId,
                           timeZone,
                           lang);

                break;

              case DISCLAIMER:

               result = retrieveAllDisclaimerNotes(
                          accountId,
                          timeZone,
                          lang);

                break;

              default:

                createInvalidResponse(
                  exchange,
                  QUERY_PARAM,
                  NOTES,
                  notes);

                return ;

            }

            LOGGER.info(
              "Note size '{}'",
              result.size());

            createOkResponse(
              exchange,
              serialize(
                new AccountRetrieveOne()
                  .setD5(
                   result)));

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });
  }

}
