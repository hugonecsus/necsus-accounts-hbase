package mx.com.necsus.handler;

import static java.lang.Boolean.parseBoolean;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.persistence.AccountsMapper.retrieveAccountPrivacy;
import static mx.com.necsus.persistence.AccountsMapper.updateAccountPrivacy;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.util.Accounts.mergeAccountPrivacy;
import static mx.com.necsus.util.AccountsQueryParams.SCOPE;
import static mx.com.necsus.util.AccountsRequestBody.isInvalidAccountPrivacySyntax;
import static mx.com.necsus.util.RequestBody.deserializeBody;
import static mx.com.necsus.util.RequestBody.isBlankCatalogItem;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSyntaxResponse;
import static mx.com.necsus.util.ResponseBody.createNoContentResponse;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.AccountUpdateOne;

import org.apache.logging.log4j.Logger;

/**
 * Handler to process the privacy and associated update of one <strong>Account</strong>.
 */
public class PatchAccountsIdPrivacyHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchAccountsIdPrivacyHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                        CORE_POOL_SIZE,
                                                        MAXIMUM_POOL_SIZE,
                                                        KEEP_ALIVE_TIME,
                                                        MILLISECONDS,
                                                        new LinkedBlockingQueue<>(
                                                          BLOCKING_QUEUE_CAPACITY),
                                                        new ThreadPoolExecutor.CallerRunsPolicy());

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    if (exchange.isInIoThread()) {

      exchange
        .dispatch(
          this);

      return;

    }

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var lang = getHeader(
                              exchange,
                              X_LANG_CODE);

            final var hasPublicScope = parseBoolean(getQueryParam(
                                                exchange,
                                                SCOPE));

            LOGGER.info(
              "{} {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              ACCOUNT_ID,
              accountId,
              SCOPE,
              hasPublicScope);

            final var accountUpdate = deserializeBody(
                                        exchange,
                                        AccountUpdateOne.class);

            if (null == accountUpdate) {

              return ;

            }

            final var accountRetrieve = retrieveAccountPrivacy(
                                          lang,
                                          accountId);

            if (!hasPublicScope &&
                isBlankCatalogItem(
                  accountRetrieve
                    .getD8()) &&
                isInvalidAccountPrivacySyntax(
                  accountUpdate)) {

              createInvalidSyntaxResponse(
                exchange,
                accountUpdate);

              return ;

            }

            mergeAccountPrivacy(
              accountUpdate,
              accountRetrieve);

            LOGGER.info(
              "Merged '{}' with scope '{}'",
              accountUpdate,
              hasPublicScope);

            final var put = newPut(
                              accountId);

            updateAccountPrivacy(
              put,
              hasPublicScope,
              accountUpdate);

            createNoContentResponse(exchange);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }
}
