package mx.com.necsus.handler;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.domain.catalog.AccountStatus.CERTIFIED;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_DISCLAIMER;
import static mx.com.necsus.domain.catalog.AccountStatus.CHECKING_PAYMENT;
import static mx.com.necsus.domain.catalog.AccountStatus.DISCLAIMER;
import static mx.com.necsus.domain.catalog.AccountStatus.INFORMATION;
import static mx.com.necsus.domain.catalog.CertificationMatchesStatus.MATCHES_VALIDATED;
import static mx.com.necsus.domain.catalog.CertificationStatus.ACTIVE;
import static mx.com.necsus.domain.catalog.CertificationVisitStatus.VISIT_VALIDATED;
import static mx.com.necsus.domain.catalog.NoticeTypes.ACCOUNT_CERTIFIED;
import static mx.com.necsus.domain.catalog.PostCertificationStatus.RENEWAL_FINALIZED;
import static mx.com.necsus.persistence.AccountsMapper.updateAccount4Completed;
import static mx.com.necsus.persistence.CertificationsMapper.retrieveSectionsAndMatchesAndVisitStatus;
import static mx.com.necsus.persistence.CompletedValuesMapper.findCompletedValue;
import static mx.com.necsus.persistence.PaymentsMapper.confirmPayments;
import static mx.com.necsus.persistence.PostCertificationStatusMapper.findPostCertificationStatus;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.util.AccountsCommons.assignControlDesk4Disclaimer;
import static mx.com.necsus.util.AccountsCommons.executeActions4ValidationsCompleted;
import static mx.com.necsus.util.AccountsCommons.retrievePostCertificationStatus;
import static mx.com.necsus.util.AccountsCommons.retrievePostCertificationVersion;
import static mx.com.necsus.util.AccountsQueryParams.COMPLETED;
import static mx.com.necsus.util.CertificationsCommons.areValidationsCompleted;
import static mx.com.necsus.util.NoticesSender.sendRecord2NoticesTopic;
import static mx.com.necsus.util.ResponseBody.QUERY_PARAM;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidResponse;
import static mx.com.necsus.util.ResponseBody.createNoContentResponse;
import static mx.com.necsus.util.Time.nowEpochMilli;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.Notice;
import mx.com.necsus.domain.catalog.PostCertificationStatus;
import mx.com.necsus.persistence.domain.Account;

import org.apache.logging.log4j.Logger;

/**
 * Handler to process the partial update of one <strong>Account</strong>.
 */
public class PatchAccountsIdCompletedHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(PatchAccountsIdCompletedHandler.class);

  /**
   * Default value for post certification flow version.
   */
  private static final long DEFAULT_POST_CERTIFICATION_FLOW_VERSION = 0;

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Instance variable for the {@link PatchAccountsIdCompletedPlanHandler}.
   */
  private final HttpHandler patchAccountsIdCompletedPlanHandler;

  /**
   * Instance variable for the {@link PatchAccountsIdCompletedCheckingPaymentHandler}
   */
  private final HttpHandler patchAccountsIdCompletedCheckingPaymentHandler;

  /**
   * Instance variable for the {@link PatchAccountsIdCompletedInformationHandler}
   */
  private final HttpHandler patchAccountsIdCompletedInformationHandler;

  /**
   * Explicit default constructor.
   */
  public PatchAccountsIdCompletedHandler() {

    this.patchAccountsIdCompletedPlanHandler = new PatchAccountsIdCompletedPlanHandler();

    this.patchAccountsIdCompletedCheckingPaymentHandler = new PatchAccountsIdCompletedCheckingPaymentHandler();

    this.patchAccountsIdCompletedInformationHandler = new PatchAccountsIdCompletedInformationHandler();

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            final var traceId = retrieveTraceId(exchange);

            put(X_TRACE_ID, traceId);

            defaultResponseContentType(exchange);

            final var accountId = getQueryParam(
                                    exchange,
                                    ACCOUNT_ID);

            final var completed = getQueryParam(
                                    exchange,
                                    COMPLETED);

            LOGGER.info(
              "{} {} '{}' {} '{}'",
              REQUEST_PAYLOAD,
              ACCOUNT_ID,
              accountId,
              COMPLETED,
              completed);

            final var put = newPut(accountId);

            final var nowEpochMilli = nowEpochMilli();

            final var account = new Account()
                                  .setStatusDate(
                                    nowEpochMilli);

            switch (findCompletedValue(completed)) {

              case PLAN:

                patchAccountsIdCompletedPlanHandler.handleRequest(exchange);

                return ;

              case PAYMENT:

                account
                  .setStatusCode(
                    CHECKING_PAYMENT.name());

                confirmPayments(
                  put,
                  accountId);

                break ;

              case CHECKING_PAYMENT:

                patchAccountsIdCompletedCheckingPaymentHandler.handleRequest(exchange);

                return ;

              case PAYMENT_VALIDATED:

                account
                  .setStatusCode(
                    INFORMATION.name());

                break;

              case INFORMATION:

                patchAccountsIdCompletedInformationHandler.handleRequest(exchange);

                return ;

              case VISIT_VALIDATED:

                account
                  .setVisitStatusCode(
                    VISIT_VALIDATED.name())
                  .setVisitStatusDate(
                    account
                      .getStatusDate());

                if (areValidationsCompleted(
                      retrieveSectionsAndMatchesAndVisitStatus(
                          accountId)
                        .setCtD22(
                          account
                            .getVisitStatusCode()),
                      put)) {

                  executeActions4ValidationsCompleted(
                    accountId,
                    traceId);

                }

                break;

              case MATCHES_VALIDATED:

                account
                  .setMatchesStatusCode(
                    MATCHES_VALIDATED.name())
                  .setMatchesStatusDate(
                    account
                      .getStatusDate());

                if (areValidationsCompleted(
                      retrieveSectionsAndMatchesAndVisitStatus(
                          accountId)
                        .setCtD6(
                          account
                            .getMatchesStatusCode()),
                      put)) {

                  executeActions4ValidationsCompleted(
                    accountId,
                    traceId);

                }

                break ;

              case INFORMATION_VALIDATED:

                account
                  .setStatusCode(
                    DISCLAIMER.name());

                break ;

              case DISCLAIMER:

                account
                  .setStatusCode(
                    CHECKING_DISCLAIMER.name());

                assignControlDesk4Disclaimer(
                  put,
                  accountId);

                break ;

              case DISCLAIMER_REJECTED:

                account
                  .setStatusCode(
                    CHECKING_DISCLAIMER.name());

                break;

              case CHECKING_DISCLAIMER:

                final var postCertificationStatus = findPostCertificationStatus(
                                                      retrievePostCertificationStatus(accountId));

                if (PostCertificationStatus.NULL == postCertificationStatus ||
                    RENEWAL_FINALIZED == postCertificationStatus) {

                  account
                    .setEvaluationDate(
                      nowEpochMilli);

                }

                account
                  .setStatusCode(
                    CERTIFIED.name())
                  .setCertificationCreationDate(
                    nowEpochMilli)
                  .setCertificationStatusCode(
                    ACTIVE.name())
                  .setCertificationStatusDate(
                    nowEpochMilli)
                  .setPostCertificationVersion(
                    null == retrievePostCertificationVersion(accountId)
                      ? DEFAULT_POST_CERTIFICATION_FLOW_VERSION
                      : null)
                  .setIsSearchable(
                    TRUE)
                  .setCertificationCongrats(
                    TRUE);

                sendRecord2NoticesTopic(
                  ACCOUNT_CERTIFIED.name(),
                  new Notice()
                    .setId(
                      traceId)
                    .setAccountId(
                      accountId));

                break;

              case CERTIFIED:

                account
                  .setCertificationCongrats(
                    FALSE);

                break;

              default:

                createInvalidResponse(
                  exchange,
                  QUERY_PARAM,
                  COMPLETED,
                  completed);

                return ;

            }

            LOGGER.info(
              "Complete '{}' for '{}'",
              completed,
              account);

            updateAccount4Completed(
              put,
              account);

            createNoContentResponse(exchange);

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
