package mx.com.necsus.handler;

import static com.jsoniter.output.JsonStream.serialize;
import static io.undertow.util.Headers.ACCEPT;
import static io.undertow.util.Headers.ACCEPT_STRING;
import static io.undertow.util.Headers.CONTENT_TYPE;
import static io.undertow.util.StatusCodes.OK;
import static java.lang.Integer.parseInt;
import static java.nio.ByteBuffer.wrap;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_OWNER;
import static mx.com.necsus.domain.catalog.UserRoles.BYR_USR;
import static mx.com.necsus.persistence.SearchesMapper.retrieveAccounts4Search;
import static mx.com.necsus.persistence.SearchesMapper.retrieveAccountsMetadata;
import static mx.com.necsus.persistence.TypesHandler.FIRST_ELEMENT_INDEX;
import static mx.com.necsus.persistence.TypesHandler.JOIN_DELIMITER;
import static mx.com.necsus.persistence.UserRolesMapper.findUserRole;
import static mx.com.necsus.util.AccountsQueryParams.FIRST_POSITION;
import static mx.com.necsus.util.AccountsQueryParams.SEARCH;
import static mx.com.necsus.util.AccountsQueryParams.SECOND_POSITION;
import static mx.com.necsus.util.AccountsQueryParams.isInvalidSearchDeque;
import static mx.com.necsus.util.AccountsQueryParams.isInvalidSearchValueSemantic;
import static mx.com.necsus.util.AccountsQueryParams.isValidFileExcel;
import static mx.com.necsus.util.AccountsQueryParams.sanitizeKeyword;
import static mx.com.necsus.util.AccountsRetrieveXlsMetadata.retrieveXlsMetadata;
import static mx.com.necsus.util.AccountsStrings.LOG_GET_ACCOUNTS_SEARCH_HANDLER;
import static mx.com.necsus.util.AccountsStrings.LOG_SIZE_ACCOUNTS_FOUND;
import static mx.com.necsus.util.ResponseBody.QUERY_PARAM;
import static mx.com.necsus.util.ResponseBody.createForbiddenResponse;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSemanticResponse;
import static mx.com.necsus.util.ResponseBody.createOkResponse;
import static mx.com.necsus.util.ResponseBody.createRequiredResponse;
import static mx.com.necsus.util.Strings.LOG_PAYLOAD_WITH_FOUR;
import static mx.com.necsus.util.TraceHeaders.X_LANG_CODE;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_OFFSET;
import static mx.com.necsus.util.TraceHeaders.X_PAGE_SIZE;
import static mx.com.necsus.util.TraceHeaders.X_ROLE_CODE;
import static mx.com.necsus.util.TraceHeaders.X_TIME_ZONE;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.X_USER_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.retrieveContentTypeFromExtension;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TracePayloads.RESPONSE_PAYLOAD;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.ArrayDeque;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

/**
 * GetAccountsSearchHandler, a particular handler to retrieve all accounts by matches.
 */
public class GetAccountsSearchHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetAccountsSearchHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 8;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 16;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 16;

  /**
   * Search advanced value split pattern.
   */
  private static final String SPLIT_PATTERN = "&s=";

  /**
   * Maximum pagination size value.
   */
  private static final int MAX_PAGINATE_SIZE = 250;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetAccountsSearchHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var userId = getHeader(
                                 exchange,
                                 X_USER_ID);

            final var roleCode = getHeader(
                                   exchange,
                                   X_ROLE_CODE);

            final var timeZone = getHeader(
                                   exchange,
                                   X_TIME_ZONE);

            final var lang = getHeader(
                               exchange,
                               X_LANG_CODE);

            final var xPageOffset = getHeader(
                                      exchange,
                                      X_PAGE_OFFSET);

            final var xPageSize = getHeader(
                                    exchange,
                                    X_PAGE_SIZE);

            final var accept = getHeader(
                                 exchange,
                                 ACCEPT_STRING);

            final var searchDeque = exchange
                                      .getQueryParameters()
                                      .get(SEARCH);

            LOGGER.info(
              LOG_GET_ACCOUNTS_SEARCH_HANDLER,
              REQUEST_PAYLOAD,
              X_USER_ID,
              userId,
              X_ROLE_CODE,
              roleCode,
              SEARCH,
              searchDeque,
              X_TIME_ZONE,
              timeZone,
              X_PAGE_OFFSET,
              xPageOffset,
              X_PAGE_SIZE,
              xPageSize,
              X_LANG_CODE,
              lang,
              ACCEPT,
              accept);

            final var pageOffset = parseInt(xPageOffset);

            final var pageSize = parseInt(xPageSize);

            if (pageOffset < FIRST_ELEMENT_INDEX) {

              createInvalidSemanticResponse(
                exchange,
                xPageOffset);

              return ;

            }

            if (pageSize < FIRST_ELEMENT_INDEX + 1 ||
                pageSize > MAX_PAGINATE_SIZE) {

              createInvalidSemanticResponse(
                exchange,
                xPageSize);

              return ;

            }

            if (BYR_OWNER != findUserRole(roleCode) &&
                BYR_USR != findUserRole(roleCode)) {

              createForbiddenResponse(exchange);

              return ;

            }

            if (isInvalidSearchDeque(searchDeque)) {

              createRequiredResponse(
                exchange,
                QUERY_PARAM,
                SEARCH,
                searchDeque);

              return ;

            }

            final var searchDequeList = searchDeque
                                          .stream()
                                          .map(
                                            value -> List.of(value.split(SPLIT_PATTERN)))
                                          .flatMap(List::stream)
                                          .collect(toCollection(ArrayDeque::new));

            if (isInvalidSearchValueSemantic(searchDequeList)) {

              createInvalidSemanticResponse(
                exchange,
                searchDequeList);

              return ;

            }

            final var searchMap = searchDequeList
                                    .stream()
                                    .map(
                                      search -> search.split(JOIN_DELIMITER))
                                    .collect(
                                      groupingBy(
                                        searchValue -> searchValue[SECOND_POSITION],
                                        mapping(
                                          keyword -> sanitizeKeyword(keyword[FIRST_POSITION]),
                                          toList())));

            if (isValidFileExcel(accept)) {

              final var accountList = retrieveAccountsMetadata(
                                        exchange,
                                        searchMap);

              LOGGER.info(
                LOG_SIZE_ACCOUNTS_FOUND,
                accountList.size());

              final var persistenceFile = retrieveXlsMetadata(
                                            exchange,
                                            searchMap);

              final var contentType = retrieveContentTypeFromExtension(persistenceFile.getExtension());

              LOGGER.info(
                LOG_PAYLOAD_WITH_FOUR,
                RESPONSE_PAYLOAD,
                OK,
                CONTENT_TYPE,
                contentType,
                persistenceFile);

              exchange
                .getResponseHeaders()
                .put(
                  CONTENT_TYPE,
                  contentType);

              exchange
                .setStatusCode(OK)
                .getResponseSender()
                .send(wrap(persistenceFile.getContent()));

            } else {

              final var result = retrieveAccounts4Search(
                                   exchange,
                                   searchMap);

              LOGGER.info(
                LOG_SIZE_ACCOUNTS_FOUND,
                result.size());

              createOkResponse(
                exchange,
                serialize(result));

            }

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
