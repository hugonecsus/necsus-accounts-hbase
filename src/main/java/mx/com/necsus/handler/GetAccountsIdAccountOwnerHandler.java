package mx.com.necsus.handler;

import static com.jsoniter.JsonIterator.deserialize;
import static io.undertow.util.Headers.LOCATION;
import static io.undertow.util.StatusCodes.MOVED_PERMANENTLY;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.config.ApplicationProperties.APPLICATION_PROPERTIES;
import static mx.com.necsus.domain.catalog.NoticeTypes.ACCOUNT_OWNER_VERIFIED;
import static mx.com.necsus.persistence.AccountsMapper.retrieveEmailAccountOwner;
import static mx.com.necsus.persistence.AccountsMapper.updateEmailAccountOwner;
import static mx.com.necsus.persistence.TypesHandler.JOIN_DELIMITER;
import static mx.com.necsus.persistence.TypesHandler.newPut;
import static mx.com.necsus.util.AccessCrypto.decrypt;
import static mx.com.necsus.util.AccessQueryParams.ACCOUNT_OWNER_CHANGE_EMAIL_TOKEN;
import static mx.com.necsus.util.AccountsRequestBody.isInvalidNotice;
import static mx.com.necsus.util.NoticesSender.sendRecord2NoticesTopic;
import static mx.com.necsus.util.RequestBody.isBlankString;
import static mx.com.necsus.util.ResponseBody.QUERY_PARAM;
import static mx.com.necsus.util.ResponseBody.createForbiddenResponse;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createInvalidSyntaxResponse;
import static mx.com.necsus.util.ResponseBody.createRequiredResponse;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.REQUEST_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static mx.com.necsus.util.TraceQueryParams.getQueryParam;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import mx.com.necsus.domain.Notice;

import org.apache.logging.log4j.Logger;

/**
 * Handler to get the account owner token to confirm the updated <strong>Account</strong>.
 */
public class GetAccountsIdAccountOwnerHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetAccountsIdAccountOwnerHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 8;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 16;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 16;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetAccountsIdAccountOwnerHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
      () -> {

        try {

          final var traceId = retrieveTraceId(exchange);

          put(X_TRACE_ID, traceId);

          defaultResponseContentType(exchange);

          final var accountId = getQueryParam(
                                  exchange,
                                  ACCOUNT_ID);

          final var token = getQueryParam(
                              exchange,
                              ACCOUNT_OWNER_CHANGE_EMAIL_TOKEN);

          LOGGER.info(
            "{} {} '{}' {} '{}'",
            REQUEST_PAYLOAD,
            ACCOUNT_ID,
            accountId,
            ACCOUNT_OWNER_CHANGE_EMAIL_TOKEN,
            token);

          if (isBlankString(token)) {

            createRequiredResponse(
              exchange,
              QUERY_PARAM,
              ACCOUNT_OWNER_CHANGE_EMAIL_TOKEN,
              token);

            return ;

          }

          final var notice = deserialize(
                                decrypt(token),
                                Notice.class);

          if (null == notice) {

            return ;

          }

          if (isInvalidNotice(notice)) {

            createInvalidSyntaxResponse(
              exchange,
              notice);

            return ;

          }

          if (!accountId
                .equalsIgnoreCase(
                  notice
                    .getAccountId()
                    .split(JOIN_DELIMITER)[0])) {

            createForbiddenResponse(exchange);

            return ;

          }

          final var updatedEmail = notice
                                      .getAccountId()
                                      .split(JOIN_DELIMITER)[1];

          if (updatedEmail
                .equalsIgnoreCase(
                  retrieveEmailAccountOwner(
                    accountId))) {

            LOGGER.info(
              "The email is the same as '{}'",
              updatedEmail);

            exchange
              .setStatusCode(
                MOVED_PERMANENTLY)
              .getResponseHeaders()
              .put(
                LOCATION,
                APPLICATION_PROPERTIES
                  .getInstance()
                  .getProperty(
                    "signin.url"));

            exchange
              .endExchange();

            return ;

          }

          final var put = newPut(accountId);

          updateEmailAccountOwner(
            put,
            updatedEmail);

          sendRecord2NoticesTopic(
            ACCOUNT_OWNER_VERIFIED.name(),
            new Notice()
              .setId(
                traceId)
              .setAccountId(
                accountId));

          exchange
            .setStatusCode(MOVED_PERMANENTLY)
            .getResponseHeaders()
            .put(LOCATION,
              APPLICATION_PROPERTIES
                .getInstance()
                .getProperty(
                  "signin.url"));

          exchange
            .endExchange();

        } catch (final Exception exc) {

          createInternalServerErrorResponse(
            exchange,
            exc);

        }

      });
  }

}
