package mx.com.necsus.handler;

import static io.undertow.util.Headers.ACCEPT_STRING;
import static io.undertow.util.Headers.CONTENT_TYPE;
import static io.undertow.util.StatusCodes.OK;
import static java.lang.Integer.parseInt;
import static java.nio.ByteBuffer.wrap;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mx.com.necsus.persistence.PaymentsMapper.retrievePaymentFileContent;
import static mx.com.necsus.util.AccountsQueryParams.PAYMENT_ID;
import static mx.com.necsus.util.ResponseBody.createInternalServerErrorResponse;
import static mx.com.necsus.util.ResponseBody.createNotFoundEntityResponse;
import static mx.com.necsus.util.TraceHeaders.X_TRACE_ID;
import static mx.com.necsus.util.TraceHeaders.defaultResponseContentType;
import static mx.com.necsus.util.TraceHeaders.getHeader;
import static mx.com.necsus.util.TraceHeaders.isInvalidAccept;
import static mx.com.necsus.util.TraceHeaders.retrieveContentTypeFromExtension;
import static mx.com.necsus.util.TraceHeaders.retrieveTraceId;
import static mx.com.necsus.util.TracePayloads.RESPONSE_PAYLOAD;
import static mx.com.necsus.util.TraceQueryParams.ACCOUNT_ID;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.apache.logging.log4j.ThreadContext.put;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.apache.logging.log4j.Logger;

/**
 * Handler to process the retrieve file of one <strong>Payment</strong>.
 */
public class GetPaymentsIdFileHandler implements HttpHandler {

  /**
   * Logger for the handler.
   */
  private static final Logger LOGGER = getLogger(GetPaymentsIdFileHandler.class);

  /**
   * Initial size of the thread pool.
   */
  private static final int CORE_POOL_SIZE = 16;

  /**
   * Maximum size of the thread pool.
   */
  private static final int MAXIMUM_POOL_SIZE = 32;

  /**
   * Time in seconds to keep threads.
   */
  private static final int KEEP_ALIVE_TIME = 200;

  /**
   * Size of the blocking queue.
   */
  private static final int BLOCKING_QUEUE_CAPACITY = 32;

  /**
   * Thread pool for the handler.
   */
  private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
                                                    CORE_POOL_SIZE,
                                                    MAXIMUM_POOL_SIZE,
                                                    KEEP_ALIVE_TIME,
                                                    MILLISECONDS,
                                                    new LinkedBlockingQueue<>(
                                                      BLOCKING_QUEUE_CAPACITY),
                                                    new CallerRunsPolicy());

  /**
   * Explicit default constructor.
   */
  public GetPaymentsIdFileHandler() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleRequest(final HttpServerExchange exchange) throws Exception {

    exchange
      .dispatch(
        EXECUTOR,
        () -> {

          try {

            put(X_TRACE_ID, retrieveTraceId(exchange));

            defaultResponseContentType(exchange);

            final var accept = getHeader(
                                 exchange,
                                 ACCEPT_STRING);

            if (isInvalidAccept(
                  exchange,
                  accept)) {

              return ;

            }

            final var paymentId = exchange
                                    .getQueryParameters()
                                    .get(PAYMENT_ID)
                                    .getLast();

            final var payment = retrievePaymentFileContent(exchange
                                                             .getQueryParameters()
                                                             .get(ACCOUNT_ID)
                                                             .getLast(),
                                                           parseInt(paymentId));

            if (null == payment) {

              createNotFoundEntityResponse(
                exchange,
                PAYMENT_ID,
                paymentId);

              return ;

            }

            final var contentType = retrieveContentTypeFromExtension(
                                      payment
                                        .getF2());

            LOGGER.info(
              "{} '{}' {} '{}' '{}'",
              RESPONSE_PAYLOAD,
              OK,
              CONTENT_TYPE,
              contentType,
              payment);

            exchange
              .getResponseHeaders()
              .put(
                CONTENT_TYPE,
                contentType);

            exchange
              .setStatusCode(
                OK)
              .getResponseSender()
              .send(
                wrap(
                  payment.getF1()));

          } catch (final Exception exc) {

            createInternalServerErrorResponse(
              exchange,
              exc);

          }

        });

  }

}
