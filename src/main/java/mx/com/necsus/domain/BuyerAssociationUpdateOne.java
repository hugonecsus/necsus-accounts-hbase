package mx.com.necsus.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


/**
 * Domain for update one account.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class BuyerAssociationUpdateOne {

  /**
   * Buyer's unique identifier.
   *
   * @param d0 Buyer's unique identifier.
   * @return Buyer's unique identifier.
   */
  private String d0;

  /**
   * Buyer association status code.
   *
   * @param d1 Buyer association status code.
   * @return Buyer association status code.
   */
  private CatalogItem d1;

  /**
   * Explicit default constructor
   */
  public BuyerAssociationUpdateOne() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
