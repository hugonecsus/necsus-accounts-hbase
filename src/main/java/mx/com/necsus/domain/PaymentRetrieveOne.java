package mx.com.necsus.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for retrieve one payment.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class PaymentRetrieveOne {

  /**
   * FilePersistence metadata.
   *
   * @param f FilePersistence metadata.
   * @return FilePersistence metadata.
   */
  private FileMetadata f;

  /**
   * Payment's status.
   *
   * @param s1 Payment's status.
   * @return Payment's status.
   */
  private CatalogItem s1;

  /**
   * Explicit default constructor
   */
  public PaymentRetrieveOne() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
