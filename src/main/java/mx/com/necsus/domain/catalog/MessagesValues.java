package mx.com.necsus.domain.catalog;

/**
 * Listing the available values for the <strong>messages</strong> query param.
 */
public enum MessagesValues {

  /**
   * Value for <strong>not found</strong> value.
   */
  NULL,

  /**
   * All value.
   */
  ALL,

  /**
   * Payment value.
   */
  PAYMENT,

  /**
   * Information value.
   */
  INFORMATION,

  /**
   * Disclaimer value.
   */
  DISCLAIMER,

  /**
   * Matches value.
   */
  MATCHES

}
