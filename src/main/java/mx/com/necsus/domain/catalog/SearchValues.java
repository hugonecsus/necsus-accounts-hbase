package mx.com.necsus.domain.catalog;

/**
 * Listing the available values for the <strong>search</strong> query param.
 */
public enum SearchValues {

  /**
   * Value for <strong>not found</strong> value.
   */
  NULL,

  /**
   * Value for the standard search by products.
   */
  STD_C1,

  /**
   * Value for standard search by business name, RFC and TIN.
   */
  STD_C2,

  /**
   * Value for the advanced search by has public reach.
   */
  ADV_C1,

  /**
   * Value for the advanced search by certification date (start date/end date).
   */
  ADV_C2,

  /**
   * Value for the advanced search by expire date (start date/end date).
   */
  ADV_C3,

  /**
   * Value for the advanced search by products.
   */
  ADV_C4,

  /**
   * Value for advanced search by country.
   */
  ADV_C5,

  /**
   * Value for advanced search by federal entity.
   */
  ADV_C6,

  /**
   * Value for advanced search by size of the company.
   */
  ADV_C7,

  /**
   * Value for advanced search by legal entity.
   */
  ADV_C8,

  /**
   * Value for advanced search by names.
   */
  ADV_C9,

  /**
   * Value for advanced search by first surname.
   */
  ADV_C10,

  /**
   * Value for advanced search by second surname.
   */
  ADV_C11

}
