package mx.com.necsus.domain.catalog;

/**
 * Listing the available values for the <strong>completed</strong> query param.
 */
public enum CompletedValues {

  /**
   * Value for <strong>not found</strong> value.
   */
  NULL,

  /**
   * The account requires a plan.
   */
  PLAN,

  /**
   * The account requires modify payment ticket.
   */
  PAYMENT,

  /**
   * Validating payment ticket.
   */
  CHECKING_PAYMENT,

  /**
   * Payment validated.
   */
  PAYMENT_VALIDATED,

  /**
   * Receiving all the account information.
   */
  INFORMATION,

  /**
   * Visit validated.
   */
  VISIT_VALIDATED,

  /**
   * Matches validated.
   */
  MATCHES_VALIDATED,

  /**
   * Information validated.
   */
  INFORMATION_VALIDATED,

  /**
   * Receiving the signed disclaimer.
   */
  DISCLAIMER,

  /**
   * The disclaimer was validated but rejected.
   */
  DISCLAIMER_REJECTED,

  /**
   * Validating the signed disclaimer.
   */
  CHECKING_DISCLAIMER,

  /**
   * Account certified.
   */
  CERTIFIED

}
