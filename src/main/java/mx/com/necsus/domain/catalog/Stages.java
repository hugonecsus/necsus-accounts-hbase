package mx.com.necsus.domain.catalog;

/**
 * Listing the available values for the <strong>stages</strong> query param.
 */
public enum Stages {

  /**
   * Value for <strong>not found</strong> stage.
   */
  NULL,

  /**
   * Payment gray stage.
   */
  PAYMENTS_GRAY,

  /**
   * Payment red stage.
   */
  PAYMENTS_RED,

  /**
   * Payment green stage.
   */
  PAYMENTS_GREEN,

  /**
   * Information gray stage.
   */
  INFORMATIONS_GRAY,

  /**
   * Information red stage.
   */
  INFORMATIONS_RED,

  /**
   * Information yellow stage.
   */
  INFORMATIONS_YELLOW,

  /**
   * Information green stage.
   */
  INFORMATIONS_GREEN,

  /**
   * Matches gray stage.
   */
  MATCHES_GRAY,

  /**
   * Matches yellow stage.
   */
  MATCHES_YELLOW,

  /**
   * Matches green stage.
   */
  MATCHES_GREEN,

  /**
   * Visit gray stage.
   */
  VISITS_GRAY,

  /**
   * Visits red stage.
   */
  VISITS_RED,

  /**
   * Visits yellow stage.
   */
  VISITS_YELLOW,

  /**
   * Visit green stage
   */
  VISITS_GREEN,

  /**
   * Disclaimer gray stage.
   */
  DISCLAIMERS_GRAY,

  /**
   * Disclaimer red stage.
   */
  DISCLAIMERS_RED,

  /**
   * Disclaimer green stage.
   */
  DISCLAIMERS_GREEN,

  /**
   * Certified stage.
   */
  CERTIFICATES
}
