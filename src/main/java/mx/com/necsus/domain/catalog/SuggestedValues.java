package mx.com.necsus.domain.catalog;

/**
 * Listing the available values for the <strong>suggested</strong> query param.
 */
public enum SuggestedValues {

  /**
   * Value for <strong>not found</strong> value.
   */
  NULL,

  /**
   * Value for the standard search by products.
   */
  STD_C1,

  /**
   * Value for standard search by business name, RFC and TIN.
   */
  STD_C2

}
