package mx.com.necsus.domain.catalog;

/**
 * Listing the available values for the <strong>notes</strong> query param.
 */
public enum NotesValues {

  /**
   * Value for <strong>not found</strong> value.
   */
  NULL,

  /**
   * Payment value.
   */
  PAYMENT,

  /**
   * Information value.
   */
  INFORMATION,

  /**
   * Matches value.
   */
  MATCHES,

  /**
   * Disclaimer value.
   */
  DISCLAIMER

}
