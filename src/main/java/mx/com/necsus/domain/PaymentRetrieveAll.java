package mx.com.necsus.domain;

import static com.jsoniter.output.JsonStream.serialize;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for retrieve all payments.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class PaymentRetrieveAll {

  /**
   * Explicit default constructor
   */
  public PaymentRetrieveAll() {
  }

  /**
   * Payment's unique identifier.
   *
   * @param d0 Payment's unique identifier.
   * @return Payment's unique identifier.
   */
  private Integer d0;

  /**
   * Payment's capture date.
   *
   * @param d2 Payment's capture date.
   * @return Payment's capture date.
   */
  private String d2;

  /**
   * Payment's reference date.
   *
   * @param d3 Payment's reference date.
   * @return Payment's reference date.
   */
  private String d3;

  /**
   * Payment's status.
   *
   * @param s1 Payment's status.
   * @return Payment's status.
   */
  private CatalogItem s1;

  /**
   * Payment's notes.
   *
   * @param n Payment's notes.
   * @return Payment's notes.
   */
  private List<Note> n;

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
