package mx.com.necsus.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for retrieve all accounts.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class AccountRetrieveAll {

  /**
   * Account unique identifier.
   *
   * @param d0 Account unique identifier.
   * @return Account unique identifier.
   */
  private String d0;

  /**
   * Account status code.
   *
   * @param d1 Account status code.
   * @return Account status code.
   */
  private CatalogItem d1;

  /**
   * Certification unique identifier.
   *
   * @param d2 Certification unique identifier.
   * @return Certification unique identifier.
   */
  private String d2;

  /**
   * Certification type code.
   *
   * @param d3 Certification type code.
   * @return Certification type code.
   */
  private CatalogItem d3;

  /**
   * Certification plan code.
   *
   * @param d4 Certification plan code.
   * @return Certification plan code.
   */
  private CatalogItem d4;

  /**
   * Certification creation date.
   *
   * @param d5 Certification creation date.
   * @return Certification creation date.
   */
  private String d5;

  /**
   * Certification expire date.
   *
   * @param d6 Certification expire date.
   * @return Certification expire date.
   */
  private String d6;

  /**
   * Certification score operation total.
   *
   * @param d7 Certification score operation total.
   * @return Certification score operation total.
   */
  private Float d7;

  /**
   * Certification score commercial total.
   *
   * @param d8 Certification score commercial total.
   * @return Certification score commercial total.
   */
  private Float d8;

  /**
   * Certification score ethics and integrity total.
   *
   * @param d9 Certification score ethics and integrity total.
   * @return Certification score ethics and integrity total.
   */
  private Float d9;

  /**
   * Certification score legal total.
   *
   * @param d10 Certification score legal total.
   * @return Certification score legal total.
   */
  private Float d10;

  /**
   * Certification score economic performance total.
   *
   * @param d11 Certification score economic performance total.
   * @return Certification score economic performance total.
   */
  private Float d11;

  /**
   * Certification score environment total.
   *
   * @param d12 Certification score environment total.
   * @return Certification score environment total.
   */
  private Float d12;

  /**
   * Certification score health total.
   *
   * @param d13 Certification score health total.
   * @return Certification score health total.
   */
  private Float d13;

  /**
   * Certification score financial total.
   *
   * @param d14 Certification score financial total.
   * @return Certification score financial total.
   */
  private Float d14;

  /**
   * Certification score quality total.
   *
   * @param d15 Certification score quality total.
   * @return Certification score quality total.
   */
  private Float d15;

  /**
   * Certification score fiscal total.
   *
   * @param d16 Certification score fiscal total.
   * @return Certification score fiscal total.
   */
  private Float d16;

  /**
   * Certification score total.
   *
   * @param d17 Certification score total.
   * @return Certification score total.
   */
  private Float d17;

  /**
   * Business name.
   *
   * @param d18 Business name.
   * @return Business name.
   */
  private String d18;

  /**
   * Origin code
   *
   * @param d19 Origin code
   * @return Origin code
   */
  private CatalogItem d19;

  /**
   * RFC.
   *
   * @param d20 RFC.
   * @return RFC.
   */
  private String d20;

  /**
   * TIN.
   *
   * @param d21 TIN.
   * @return TIN.
   */
  private String d21;

  /**
   * Economic activity code.
   *
   * @param d22 Economic activity code.
   * @return  Economic activity code.
   */
  private CatalogItem d22;

  /**
   * Country code.
   *
   * @param d23 Country code.
   * @return Country code.
   */
  private CatalogItem d23;

  /**
   * Company size.
   *
   * @param d24 Company size.
   * @return Company size.
   */
  private CatalogItem d24;

  /**
   * Total employees the company has.
   *
   * @param d25 Total employees the company has.
   * @return Total employees the company has.
   */
  private Integer d25;

  /**
   * Has quality certificate.
   *
   * @param d26 Has quality certificate.
   * @return Has quality certificate.
   */
  private Boolean d26;

  /**
   * Has health and safety certificate.
   *
   * @param d27 Has health and safety certificate
   * @return Has health and safety certificate
   */
  private Boolean d27;

  /**
   * Has environmental certificate.
   *
   * @param d28 Has environmental certificate.
   * @return Has environmental certificate.
   */
  private Boolean d28;

  /**
   * Visit folio.
   *
   * @param d30 Visit folio.
   * @return Visit folio.
   */
  private String d30;

  /**
   * Visit status.
   *
   * @param d31 Visit status.
   * @return Visit status.
   */
  private CatalogItem d31;

  /**
   * Visit SLA.
   *
   * @param d32 Visit SLA..
   * @return Visit SLA..
   */
  private Integer d32;

  /**
   * Visit date.
   *
   * @param d33 Visit date.
   * @return Visit date.
   */
  private String d33;

  /**
   * Legal entity code.
   *
   * @param d34 Legal entity code.
   * @return Legal entity code.
   */
  private CatalogItem d34;

  /**
   * Certification status code.
   *
   * @param d35 Certification status code.
   * @return Certification status code.
   */
  private CatalogItem d35;

  /**
   * Confirmation date.
   *
   * @param d36 Confirmation date.
   * @return Confirmation date.
   */
  private String d36;

  /**
   * Assigned control desk names.
   *
   * @param d37 Assigned control desk names.
   * @return Assigned control desk names.
   */
  private String d37;

  /**
   * Assigned control desk first surname.
   *
   * @param d38 Assigned control desk first surname.
   * @return Assigned control desk first surname.
   */
  private String d38;

  /**
   * Assigned control desk second surname.
   *
   * @param d39 Assigned control desk second surname.
   * @return Assigned control desk second surname.
   */
  private String d39;

  /**
   * Explicit default constructor
   */
  public AccountRetrieveAll() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
