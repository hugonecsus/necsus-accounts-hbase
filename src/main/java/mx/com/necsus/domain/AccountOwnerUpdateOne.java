package mx.com.necsus.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for update one contact person.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class AccountOwnerUpdateOne {

  /**
   * Names (from sign up).
   *
   * @param d1 Names (from sign up).
   * @return Names (from sign up)s.
   */
  private String d1;

  /**
   * First surname (from sign up).
   *
   * @param d2 First surname.
   * @return First surname.
   */
  private String d2;

  /**
   * Second surname (from sign up).
   *
   * @param d3 Second surname (from sign up).
   * @return Second surname (from sign up).
   */
  private String d3;

  /**
   * Position (from sign up).
   *
   * @param d4 Position (from sign up).
   * @return Position (from sign up).
   */
  private String d4;

  /**
   * Mobile phone country calling code (from sign up).
   *
   * @param d5 Mobile phone country calling code (from sign up).
   * @return Mobile phone country calling code (from sign up).
   */
  private CatalogItem d5;

  /**
   * Mobile phone (from sign up).
   *
   * @param d6 Mobile phone (from sign up).
   * @return Mobile phone (from sign up).
   */
  private String d6;

  /**
   * Land line (from sign up).
   *
   * @param d7 Land line (from sign up).
   * @return Land line (from sign up).
   */
  private String d7;

  /**
   * Email (from sign up).
   *
   * @param d8 Email (from sign up).
   * @return Email (from sign up).
   */
  private String d8;

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }
}
