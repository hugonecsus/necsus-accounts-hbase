package mx.com.necsus.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for update one payment.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class PaymentUpdateOne {

  /**
   * Payment status.
   *
   * @param s1 Payment status.
   * @return Payment status.
   */
  private CatalogItem s1;

  /**
   * Payment message note.
   *
   * @param n2 Payment message note..
   * @return Payment message note..
   */
  private String n2;

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
