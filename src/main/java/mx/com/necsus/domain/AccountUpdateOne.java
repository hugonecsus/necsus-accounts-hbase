package mx.com.necsus.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for update one account.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class AccountUpdateOne {

  /**
   * Certification plan.
   *
   * @param d1 Certification plan.
   * @return Certification plan.
   */
  private CatalogItem d1;

  /**
   * Invited by or associated with.
   *
   * @param d2 Invited by or associated with.
   * @return Invited by or associated with.
   */
  private CatalogItem d2;

  /**
   * Contact person.
   *
   * @param d3 Contact person.
   * @return Contact person.
   */
  private AccountOwnerUpdateOne d3;

  /**
   * Buyer associated to the account.
   *
   * @param d4 Buyer associated to the account.
   * @return Buyer associated to the account.
   */
  private BuyerAssociationUpdateOne d4;

  /**
   * Explicit default constructor
   */
  public AccountUpdateOne() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
