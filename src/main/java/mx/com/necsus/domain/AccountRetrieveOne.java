package mx.com.necsus.domain;

import static com.jsoniter.output.JsonStream.serialize;
import static mx.com.necsus.domain.catalog.FileCodes.S1B1FA;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for retrieve one account.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class AccountRetrieveOne {

  /**
   * Account status code.
   *
   * @param d1 Account status code.
   * @return Account status code.
   */
  private CatalogItem d1;

  /**
   * Certification unique identifier.
   *
   * @param d2 Certification unique identifier.
   * @return Certification unique identifier.
   */
  private String d2;

  /**
   * Certification type.
   *
   * @param d3 Certification type.
   * @return Certification type.
   */
  private CatalogItem d3;

  /**
   * Certification plan.
   *
   * @param d4 Certification plan.
   * @return Certification plan.
   */
  private CatalogItem d4;

  /**
   * Notes.
   *
   * @param d5 Notes.
   * @return Notes.
   */
  private List<Note> d5;

  /**
   * Certification congrats.
   *
   * @param d6 Certification congrats.
   * @return Certification congrats.
   */
  private Boolean d6;

  /**
   * Certification status code.
   *
   * @param d7 Certification status code.
   * @return Certification status code.
   */
  private CatalogItem d7;

  /**
   * Invited by or associated with.
   *
   * @param d8 Invited by or associated with.
   * @return Invited by or associated with.
   */
  private CatalogItem d8;

  /**
   * Has public or private scope.
   *
   * @param d9 Has public or private scope.
   * @return Has public or private scope.
   */
  private Boolean d9;

  /**
   * Account owner.
   *
   * @param d10 Account owner.
   * @return Account owner.
   */
  private AccountOwnerUpdateOne d10;

  /**
   * Is change plan available.
   *
   * @param d15 Is change plan available.
   * @return Is change plan available.
   */
  private Boolean d15;

  /**
   * Account stage.
   *
   * @param d16 Account stage.
   * @return Account stage.
   */
  private String d16;

  /**
   * Matches stage.
   *
   * @param d17 Matches stage.
   * @return Matches stage.
   */
  private String d17;

  /**
   * Buyers association.
   *
   * @param d18 Buyers association.
   * @return Buyers association.
   */
  private List<BuyerAssociationRetrieveOne> d18;

  /** Articles size.
   *
   * @param d19 Articles size.
   * @return Articles size.
   */
  private Integer d19;

  /**
   * Has buyer association.
   *
   * @param d20 Has buyer association.
   * @return Has buyer association.
   */
  private Boolean d20;

  /**
   * RFC file code.
   *
   * @param d21 RFC file code.
   * @return RFC file code.
   */
  private String d21 = S1B1FA.name();

  /**
   * Is renewal available.
   *
   * @param d22 Is renewal available.
   * @return Is renewal available.
   */
  private Boolean d22;

  /**
   * Origin code.
   *
   * @param d23 Origin code.
   * @return Origin code.
   */
  private CatalogItem d23;

  /**
   * Explicit default constructor
   */
  public AccountRetrieveOne() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
