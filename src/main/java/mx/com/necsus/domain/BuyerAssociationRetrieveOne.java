package mx.com.necsus.domain;

import static com.jsoniter.output.JsonStream.serialize;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Domain for a buyer association.
 */
@Getter
@Setter
@Accessors(
  chain  = true,
  fluent = false
)
public class BuyerAssociationRetrieveOne {

  /**
   * Buyer's unique identifier.
   *
   * @param d0 Buyer's unique identifier.
   * @return Buyer's unique identifier.
   */
  private String d0;

  /**
   * Buyer's business name.
   *
   * @param d1 Buyer's business name.
   * @return Buyer's business name.
   */
  private String d1;

  /**
   * Association's date.
   *
   * @param d2 Association's date.
   * @return Association's date.
   */
  private String d2;

  /**
   * Buyer association status code.
   *
   * @param d3 Buyer association status code.
   * @return Buyer association status code.
   */
  private CatalogItem d3;

  /**
   * Explicit default constructor
   */
  public BuyerAssociationRetrieveOne() {
  }

  /**
   * String in <em>JSON format</em> of the Java Bean.
   */
  @Override
  public String toString() {
    return serialize(this);
  }

}
