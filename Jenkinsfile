library 'jenkins-libraries'

pipeline {

  agent {

    label 'dev'

  }

  options {

    ansiColor('xterm')

  }

  stages {

    stage('build') {

      agent {

        docker {

          label 'dev'

          image constants.openJdk11Image()

          args constants.mavenDockerArgs()

        }

      }

      steps {

        slackStart(env.getEnvironment())

        script {

          env.CONTAINER_NAME = "accounts"

          env.ARTIFACT_ID = readMavenPom().getArtifactId()

          env.VERSION = readMavenPom().getVersion()

          env.IS_SNAPSHOT = env.VERSION.endsWith("-SNAPSHOT")

        }

        mvnw('-P dev',
             'clean test-compile')

      }

    }

    stage('verify') {

      when {

        branch 'develop'

      }

      steps {

        rmContainer("${env.CONTAINER_NAME}")

        withDockerContainer(image: constants.openJdk11Image(),
                            args: constants.mavenDockerArgsVerify()) {

          mvnw('-P ci',
               'clean verify sonar:sonar')

        }

      }

      post {

        success {

          junit(allowEmptyResults: true,
                testResults: 'target/failsafe-reports/**/*.xml')

        }

        always {

          rmImage("${env.ARTIFACT_ID}")

        }

      }

    }

    stage('deploy-snapshot') {

      when {

        branch 'develop'

        environment name: 'IS_SNAPSHOT', value: 'true'

      }

      agent {

        label 'master'

      }

      steps {

        script {

          deployServiceOnTarget("${env.CONTAINER_NAME}",
                                "${env.ARTIFACT_ID}",
                                "${env.VERSION}",
                                '-P dev',
                                'dev',
                                'env-vars-dev')

          deployServiceOnTarget("${env.CONTAINER_NAME}",
                                "${env.ARTIFACT_ID}",
                                "${env.VERSION}",
                                '-P qa',
                                'qa',
                                'env-vars-qa')

        }

      }

      post {

        always {

          rmImage("${env.ARTIFACT_ID}")

        }

      }

    }

    stage('deploy-release') {

      when {

        branch 'master'

      }

      agent {

        label 'master'

      }

      steps {

        script {

          deployServiceOnTarget("${env.CONTAINER_NAME}",
                                "${env.ARTIFACT_ID}",
                                "${env.VERSION}",
                                '-P dev',
                                'dev',
                                'env-vars-dev')

          deployServiceOnTarget("${env.CONTAINER_NAME}",
                                "${env.ARTIFACT_ID}",
                                "${env.VERSION}",
                                '-P qa',
                                'qa',
                                'env-vars-qa')

          deployServiceOnTarget("${env.CONTAINER_NAME}",
                                "${env.ARTIFACT_ID}",
                                "${env.VERSION}",
                                '',
                                'prod',
                                'env-vars-prod')

        }

      }

      post {

        always {

          rmImage("${env.ARTIFACT_ID}")

        }

      }

    }

  }

  post {

    success {

      slackSuccess(env.getEnvironment())

    }

    failure {

      slackFailure(env.getEnvironment())

    }

  }

}
