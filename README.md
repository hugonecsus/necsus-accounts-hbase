# NECSUS - Accounts

Micro service for the accounts in NECSUS.

## Account Status

| Code                  | Role         | Description                                 |
| --------------------- | ------------ | ------------------------------------------- |
| SIGN_UP               |              | The account was created but not confirmed   |
| PLAN                  | ACCT_OWNER   | The account requires a plan                 |
| PAYMENT               | ACCT_OWNER   | The account requires modify payment tickets |
| CHECKING_PAYMENT      | CONTROL_DESK | Validating payment tickets                  |
| PAYMENT_VALIDATED     | ACCT_OWNER   | Payment validated                           |
| INFORMATION           | ACCT_OWNER   | Receiving all the account information       |
| CHECKING_INFORMATION  | CONTROL_DESK | Validating all the account information      |
| INFORMATION_VALIDATED | ACCT_OWNER   | Information validated                       |
| DISCLAIMER            | ACCT_OWNER   | Receiving the signed disclaimer             |
| DISCLAIMER_REJECTED   | ACCT_OWNER   | The disclaimer was validated but rejected   |
| CHECKING_DISCLAIMER   | CONTROL_DESK | Validating the signed disclaimer            |
| CERTIFIED             | CONTROL_DESK | The account is certified                    |

## Certification Status

| Code       | Description                     |
| ---------- | ------------------------------- |
| ACTIVE     | The certification is active     |
| INACTIVE   | The certification is inactive   |
| UPDATING   | The certification is updating   |
| UPGRADING  | The certification is upgrading  |
| RENEWING   | The certification is renewing   |

## Payment Status

| Code     | Description                     |
| -------- | ------------------------------- |
| TO_CHECK | The payment requires validation |
| FAIL     | The payment was rejected        |
| SUCCESS  | The payment was accepted        |

## Information Status

| Code       | Description                         |
| ---------- | ----------------------------------- |
| TO_CHECK   | The information requires validation |
| FAIL       | The information was rejected        |
| ADJUSTMENT | The information was corrected       |
| SUCCESS    | The information was accepted        |

## Requirements

  - Docker 20

## Contributing

### Git config

```bash
$ git config --local "user.name" "myusernameatgitlab"
```

```bash
$ git config --local "user.email" "myemail@linko.mx"
```

## Building

### Setup

First setup with the following steps:

#### GitLab

Generate your GitLab personal access token following [GitLab personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) and then execute the commands:

```bash
$ echo "export GITLAB_USERNAME=myusernameatgitlab" >> ~/.profile && \
  source ~/.profile
```

```bash
$ echo "export GITLAB_PERSONAL_ACCESS_TOKEN=mypersonalaccesstokenatgitlab" >> ~/.profile && \
  source ~/.profile
```

#### Log directory

```bash
$ sudo mkdir -p /var/log/necsus && \
  sudo chown 2222:2222 /var/log/necsus
```

### Integration Tests

The development cycle should consist in the creation of **integration test cases**. Use the below command to execute them:

```bash
$ docker run \
    -u $(id -u):$(grep -w docker /etc/group | awk -F\: '{print $3}') \
    --net=host \
    --rm \
    -w $(pwd) \
    -e GITLAB_USERNAME=$GITLAB_USERNAME \
    -e GITLAB_PERSONAL_ACCESS_TOKEN=$GITLAB_PERSONAL_ACCESS_TOKEN \
    -v /etc/group:/etc/group:ro \
    -v /etc/passwd:/etc/passwd:ro \
    -v $(pwd):$(pwd) \
    -v ${HOME}/.m2:${HOME}/.m2 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    azul/zulu-openjdk-alpine:11.0.12 \
    ./mvnw -Djansi.force=true -ntp -s .maven-settings -P local -U clean verify
```

