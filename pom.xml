<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation=" http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

  <modelVersion>4.0.0</modelVersion>
  <groupId>mx.com.necsus</groupId>
  <artifactId>necsus-accounts</artifactId>
  <version>1.50.0-SNAPSHOT</version>
  <packaging>jar</packaging>

  <name>NECSUS - Accounts</name>
  <description>Micro service to the accounts in NECSUS</description>

  <scm>
    <developerConnection>scm:git:https://gitlab.com/necsus/necsus-accounts.git</developerConnection>
    <tag>HEAD</tag>
  </scm>

  <properties>
    <java.version>11</java.version>
    <maven>3.9.1</maven>

    <main.class>mx.com.necsus.NecsusAccountsApplication</main.class>

    <project.build.environment>prod</project.build.environment>
    <project.build.resourcesDirectory>${basedir}/src/main/resources</project.build.resourcesDirectory>
    <project.build.dockerResourcesDirectory>${basedir}/src/main/docker</project.build.dockerResourcesDirectory>
    <project.build.testResourcesDirectory>${basedir}/src/test/resources</project.build.testResourcesDirectory>
    <project.build.filterDirectory>${basedir}/src/main/filters</project.build.filterDirectory>

    <project.build.logDirectory>./var/log/necsus</project.build.logDirectory>

    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.build.jdk.source>${java.version}</project.build.jdk.source>
    <project.build.jdk.target>${java.version}</project.build.jdk.target>

    <project.build.ut.skip>false</project.build.ut.skip>

    <properties.maven.plugin.version>1.0.0</properties.maven.plugin.version>
    <editorconfig.maven.plugin.version>0.1.3</editorconfig.maven.plugin.version>
    <maven.resources.plugin.version>3.2.0</maven.resources.plugin.version>
    <lombok.maven.plugin.version>1.18.20.0</lombok.maven.plugin.version>
    <maven.compiler.plugin.version>3.11.0</maven.compiler.plugin.version>
    <maven.surefire.plugin.version>3.0.0</maven.surefire.plugin.version>
    <maven.failsafe.plugin.version>3.0.0</maven.failsafe.plugin.version>
    <maven.shade.plugin.version>3.2.4</maven.shade.plugin.version>
    <build.helper.maven.plugin.version>3.3.0</build.helper.maven.plugin.version>
    <maven.release.plugin.version>3.0.0</maven.release.plugin.version>
    <docker.maven.plugin.version>0.42.0</docker.maven.plugin.version>

    <poi.version>3.14</poi.version>

    <lombok.version>1.18.22</lombok.version>

    <necsus.test.rules.version>1.0.0</necsus.test.rules.version>
    <necsus.catalogs.db.version>1.27.0-SNAPSHOT</necsus.catalogs.db.version>
    <zookeeper.version>3.5.6</zookeeper.version>
    <linkomx.hbase.pseudo.distributed.version>2.2.7</linkomx.hbase.pseudo.distributed.version>
    <cp.kafka.version>5.3.0</cp.kafka.version>

    <docker.image.alias>accounts</docker.image.alias>
    <docker.image.registry>registry.gitlab.com/necsus</docker.image.registry>
    <docker.image.name>${project.artifactId}:${project.version}</docker.image.name>
    <docker.image.build.dockerfiledir>${project.build.outputDirectory}</docker.image.build.dockerfiledir>
  </properties>

  <dependencies>
    <dependency>
      <groupId>mx.com.necsus</groupId>
      <artifactId>necsus-db</artifactId>
      <version>1.52.0</version>
      <exclusions>
        <exclusion>
          <groupId>redis.clients</groupId>
          <artifactId>jedis</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>mx.com.necsus</groupId>
      <artifactId>necsus-access</artifactId>
      <version>1.16.0</version>
    </dependency>
    <dependency>
      <groupId>org.apache.poi</groupId>
      <artifactId>poi</artifactId>
      <version>${poi.version}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.poi</groupId>
      <artifactId>poi-ooxml</artifactId>
      <version>${poi.version}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.poi</groupId>
      <artifactId>poi-ooxml-schemas</artifactId>
      <version>${poi.version}</version>
    </dependency>

    <!-- Provided -->
    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>${lombok.version}</version>
      <scope>provided</scope>
    </dependency>

    <!-- Test -->
    <dependency>
      <groupId>mx.com.necsus</groupId>
      <artifactId>necsus-test-rules</artifactId>
      <version>${necsus.test.rules.version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <repositories>
    <repository>
      <id>gitlab-maven</id>
      <name>GitLab Maven</name>
      <url>https://gitlab.com/api/v4/groups/8641547/-/packages/maven</url>
    </repository>
  </repositories>

  <build>
    <finalName>${project.artifactId}</finalName>

    <resources>
      <resource>
        <directory>${project.build.resourcesDirectory}</directory>
        <filtering>true</filtering>
      </resource>
      <resource>
        <directory>${project.build.dockerResourcesDirectory}</directory>
        <filtering>true</filtering>
      </resource>
    </resources>
    <testResources>
      <testResource>
        <directory>${project.build.testResourcesDirectory}</directory>
        <filtering>true</filtering>
        <excludes>
          <exclude>**/*.pdf</exclude>
          <exclude>**/*.txt</exclude>
        </excludes>
      </testResource>
    </testResources>
    <filters>
      <filter>${project.build.filterDirectory}/${project.build.environment}.properties</filter>
    </filters>

    <!-- Defining the plugin stack -->
    <plugins>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>properties-maven-plugin</artifactId>
        <version>${properties.maven.plugin.version}</version>
        <executions>
          <execution>
            <phase>initialize</phase>
            <goals>
              <goal>read-project-properties</goal>
            </goals>
            <configuration>
              <files>
                <file>${project.build.filterDirectory}/${project.build.environment}.properties</file>
              </files>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.ec4j.maven</groupId>
        <artifactId>editorconfig-maven-plugin</artifactId>
        <version>${editorconfig.maven.plugin.version}</version>
        <executions>
          <execution>
            <phase>initialize</phase>
            <goals>
              <goal>format</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <excludes>
            <exclude>mvnw*</exclude>
            <exclude>.mvn/**</exclude>
            <exclude>**/*.swp</exclude>
          </excludes>
          <includes>
            <include>**</include>
          </includes>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-resources-plugin</artifactId>
        <version>${maven.resources.plugin.version}</version>
        <configuration>
          <useDefaultDelimiters>false</useDefaultDelimiters>
          <delimiters>
            <delimiter>${*}</delimiter>
          </delimiters>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok-maven-plugin</artifactId>
        <version>${lombok.maven.plugin.version}</version>
        <executions>
          <execution>
            <phase>generate-sources</phase>
            <goals>
              <goal>delombok</goal>
            </goals>
            <configuration>
              <verbose>true</verbose>
              <addOutputDirectory>false</addOutputDirectory>
              <sourceDirectory>src/main/java</sourceDirectory>
              <outputDirectory>${project.build.directory}/lombok</outputDirectory>
              <formatPreferences>
                <pretty />
              </formatPreferences>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>${maven.compiler.plugin.version}</version>
        <configuration>
          <source>${project.build.jdk.source}</source>
          <target>${project.build.jdk.target}</target>
          <encoding>${project.build.sourceEncoding}</encoding>
          <failOnWarning>true</failOnWarning>
          <compilerArgs>
            <arg>-Werror</arg>
            <arg>-Xdoclint:all</arg>
            <arg>-Xlint:all,-processing</arg>
          </compilerArgs>
          <showWarnings>true</showWarnings>
          <showDeprecation>true</showDeprecation>
        </configuration>
      </plugin>
<!--      <plugin>-->
<!--        <groupId>org.apache.maven.plugins</groupId>-->
<!--        <artifactId>maven-surefire-plugin</artifactId>-->
<!--        <version>${maven.surefire.plugin.version}</version>-->
<!--        <configuration>-->
<!--          <skip>${project.build.ut.skip}</skip>-->
<!--        </configuration>-->
<!--      </plugin>-->
<!--      <plugin>-->
<!--        <groupId>org.apache.maven.plugins</groupId>-->
<!--        <artifactId>maven-failsafe-plugin</artifactId>-->
<!--        <version>${maven.failsafe.plugin.version}</version>-->
<!--        <configuration>-->
<!--          <encoding>${project.build.sourceEncoding}</encoding>-->
<!--          <redirectTestOutputToFile>true</redirectTestOutputToFile>-->
<!--          <environmentVariables>-->
<!--            <ACCESS_PASSWORD>${access.password}</ACCESS_PASSWORD>-->
<!--            <MONGO_NECSUSDB_USERNAME>${mongo.necsusdb.username}</MONGO_NECSUSDB_USERNAME>-->
<!--            <MONGO_NECSUSDB_PASSWORD>${mongo.necsusdb.password}</MONGO_NECSUSDB_PASSWORD>-->
<!--          </environmentVariables>-->
<!--        </configuration>-->
<!--        <executions>-->
<!--          <execution>-->
<!--            <goals>-->
<!--              <goal>integration-test</goal>-->
<!--              <goal>verify</goal>-->
<!--            </goals>-->
<!--          </execution>-->
<!--        </executions>-->
<!--      </plugin>-->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>${maven.shade.plugin.version}</version>
        <configuration>
          <createDependencyReducedPom>false</createDependencyReducedPom>
          <transformers>
            <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
              <mainClass>${main.class}</mainClass>
              <manifestEntries>
                <Multi-Release>true</Multi-Release>
              </manifestEntries>
            </transformer>
            <transformer implementation="org.apache.maven.plugins.shade.resource.DontIncludeResourceTransformer">
              <resources>
                <resource>Dockerfile</resource>
              </resources>
            </transformer>
          </transformers>
        </configuration>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
<!--      <plugin>-->
<!--        <groupId>org.codehaus.mojo</groupId>-->
<!--        <artifactId>build-helper-maven-plugin</artifactId>-->
<!--        <version>${build.helper.maven.plugin.version}</version>-->
<!--      </plugin>-->
<!--      <plugin>-->
<!--        <groupId>org.apache.maven.plugins</groupId>-->
<!--        <artifactId>maven-release-plugin</artifactId>-->
<!--        <version>${maven.release.plugin.version}</version>-->
<!--        <configuration>-->
<!--          <preparationGoals>release:clean</preparationGoals>-->
<!--          <autoResolveSnapshots>dependencies</autoResolveSnapshots>-->
<!--          <username>${env.GITLAB_USERNAME}</username>-->
<!--          <password>${env.GITLAB_PERSONAL_ACCESS_TOKEN}</password>-->
<!--          <scmCommentPrefix>version(app):</scmCommentPrefix>-->
<!--          <scmReleaseCommitComment>@{prefix} @{releaseLabel}</scmReleaseCommitComment>-->
<!--          <scmDevelopmentCommitComment>@{prefix} ${parsedVersion.majorVersion}.${parsedVersion.nextMinorVersion}.0-SNAPSHOT</scmDevelopmentCommitComment>-->
<!--          <tagNameFormat>@{project.version}</tagNameFormat>-->
<!--          <developmentVersion>${parsedVersion.majorVersion}.${parsedVersion.nextMinorVersion}.0-SNAPSHOT</developmentVersion>-->
<!--        </configuration>-->
<!--      </plugin>-->
<!--      <plugin>-->
<!--        <groupId>io.fabric8</groupId>-->
<!--        <artifactId>docker-maven-plugin</artifactId>-->
<!--        <version>${docker.maven.plugin.version}</version>-->
<!--        <configuration>-->
<!--          <verbose>true</verbose>-->
<!--          <images>-->
<!--            <image>-->
<!--              <registry>${docker.image.registry}</registry>-->
<!--              <name>${docker.image.name}</name>-->
<!--              <build>-->
<!--                <dockerFileDir>${docker.image.build.dockerfiledir}</dockerFileDir>-->
<!--                <assembly>-->
<!--                  <name>artifact</name>-->
<!--                  <descriptorRef>artifact</descriptorRef>-->
<!--                </assembly>-->
<!--              </build>-->
<!--            </image>-->
<!--          </images>-->
<!--        </configuration>-->
<!--      </plugin>-->
    </plugins>
  </build>
  <profiles>
    <profile>
      <id>local</id>

      <properties>
        <project.build.environment>local</project.build.environment>
        <wait.time.default>720000</wait.time.default>
      </properties>

      <build>
        <plugins>
<!--          <plugin>-->
<!--            <groupId>io.fabric8</groupId>-->
<!--            <artifactId>docker-maven-plugin</artifactId>-->
<!--            <version>${docker.maven.plugin.version}</version>-->
<!--            <executions>-->
<!--              <execution>-->
<!--                <id>start-containers</id>-->
<!--                <phase>pre-integration-test</phase>-->
<!--                <goals>-->
<!--                  <goal>start</goal>-->
<!--                </goals>-->
<!--                <configuration>-->
<!--                  <verbose>true</verbose>-->
<!--                  <imagePullPolicy>Always</imagePullPolicy>-->
<!--                  <images>-->
<!--                    <image>-->
<!--                      <name>${docker.image.registry}/necsus-catalogs-db:${necsus.catalogs.db.version}</name>-->
<!--                      <run>-->
<!--                        <net>host</net>-->
<!--                        <wait>-->
<!--                          <time>${wait.time.default}</time>-->
<!--                          <log>(?s)port: 3306.*</log>-->
<!--                        </wait>-->
<!--                      </run>-->
<!--                    </image>-->
<!--                    <image>-->
<!--                      <name>zookeeper:${zookeeper.version}</name>-->
<!--                      <run>-->
<!--                        <net>host</net>-->
<!--                        <wait>-->
<!--                          <time>${wait.time.default}</time>-->
<!--                          <log>(?s)binding to port .*:2181.*</log>-->
<!--                        </wait>-->
<!--                      </run>-->
<!--                    </image>-->
<!--                    <image>-->
<!--                      <name>registry.gitlab.com/linkomx/linkomx-hbase-pseudo-distributed:${linkomx.hbase.pseudo.distributed.version}</name>-->
<!--                      <run>-->
<!--                        <net>host</net>-->
<!--                        <extraHosts>-->
<!--                          <host>zk-1:127.0.0.1</host>-->
<!--                          <host>zk-2:127.0.0.1</host>-->
<!--                          <host>zk-3:127.0.0.1</host>-->
<!--                          <host>${env.HOSTNAME}:127.0.0.1</host>-->
<!--                        </extraHosts>-->
<!--                        <wait>-->
<!--                          <time>${wait.time.default}</time>-->
<!--                          <log>(?s)HMaster: Master has completed initialization.*</log>-->
<!--                        </wait>-->
<!--                      </run>-->
<!--                    </image>-->
<!--                    <image>-->
<!--                      <name>confluentinc/cp-kafka:${cp.kafka.version}</name>-->
<!--                      <run>-->
<!--                        <net>host</net>-->
<!--                        <env>-->
<!--                          <KAFKA_ZOOKEEPER_CONNECT>localhost:2181</KAFKA_ZOOKEEPER_CONNECT>-->
<!--                          <KAFKA_ADVERTISED_LISTENERS>PLAINTEXT://localhost:9092</KAFKA_ADVERTISED_LISTENERS>-->
<!--                          <KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR>1</KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR>-->
<!--                        </env>-->
<!--                        <wait>-->
<!--                          <time>${wait.time.default}</time>-->
<!--                          <log>(?s)KafkaServer.*started.*</log>-->
<!--                        </wait>-->
<!--                      </run>-->
<!--                    </image>-->
<!--                    <image>-->
<!--                      <alias>topic-notifications</alias>-->
<!--                      <name>confluentinc/cp-kafka:${cp.kafka.version}</name>-->
<!--                      <run>-->
<!--                        <net>host</net>-->
<!--                        <cmd>kafka-topics &#45;&#45;create &#45;&#45;topic notifications &#45;&#45;partitions 1 &#45;&#45;replication-factor 1 &#45;&#45;zookeeper localhost:2181</cmd>-->
<!--                        <wait>-->
<!--                          <time>4000</time>-->
<!--                        </wait>-->
<!--                      </run>-->
<!--                    </image>-->
<!--                  </images>-->
<!--                </configuration>-->
<!--              </execution>-->
<!--              <execution>-->
<!--                <id>start-service</id>-->
<!--                <phase>pre-integration-test</phase>-->
<!--                <goals>-->
<!--                  <goal>build</goal>-->
<!--                  <goal>start</goal>-->
<!--                </goals>-->
<!--                <configuration>-->
<!--                  <verbose>true</verbose>-->
<!--                  <images>-->
<!--                    <image>-->
<!--                      <registry>${docker.image.registry}</registry>-->
<!--                      <name>${docker.image.name}</name>-->
<!--                      <build>-->
<!--                        <dockerFileDir>${docker.image.build.dockerfiledir}</dockerFileDir>-->
<!--                        <assembly>-->
<!--                          <name>artifact</name>-->
<!--                          <descriptorRef>artifact</descriptorRef>-->
<!--                        </assembly>-->
<!--                      </build>-->
<!--                      <run>-->
<!--                        <log>-->
<!--                          <prefix>%z</prefix>-->
<!--                          <date>DEFAULT</date>-->
<!--                          <file>${project.build.directory}/${docker.image.alias}.log</file>-->
<!--                        </log>-->
<!--                        <net>host</net>-->
<!--                        <env>-->
<!--                          <ACCESS_PASSWORD>${access.password}</ACCESS_PASSWORD>-->
<!--                          <MYSQL_CATALOGSDB_USERNAME>${mysql.catalogsdb.username}</MYSQL_CATALOGSDB_USERNAME>-->
<!--                          <MYSQL_CATALOGSDB_PASSWORD>${mysql.catalogsdb.password}</MYSQL_CATALOGSDB_PASSWORD>-->
<!--                          <MONGO_NECSUSDB_USERNAME>${mongo.necsusdb.username}</MONGO_NECSUSDB_USERNAME>-->
<!--                          <MONGO_NECSUSDB_PASSWORD>${mongo.necsusdb.password}</MONGO_NECSUSDB_PASSWORD>-->
<!--                        </env>-->
<!--                        <volumes>-->
<!--                          <bind>-->
<!--                            <volume>${project.build.logDirectory}:${project.build.logDirectory}</volume>-->
<!--                          </bind>-->
<!--                        </volumes>-->
<!--                        <wait>-->
<!--                          <time>8000</time>-->
<!--                        </wait>-->
<!--                      </run>-->
<!--                    </image>-->
<!--                  </images>-->
<!--                </configuration>-->
<!--              </execution>-->
<!--              <execution>-->
<!--                <id>stop-containers</id>-->
<!--                <phase>post-integration-test</phase>-->
<!--                <goals>-->
<!--                  <goal>stop</goal>-->
<!--                </goals>-->
<!--              </execution>-->
<!--            </executions>-->
<!--          </plugin>-->
        </plugins>
      </build>
    </profile>

    <profile>
      <id>ci</id>

      <properties>
        <project.build.environment>ci</project.build.environment>
      </properties>

      <build>
        <plugins>
          <plugin>
            <groupId>io.fabric8</groupId>
            <artifactId>docker-maven-plugin</artifactId>
            <version>${docker.maven.plugin.version}</version>
            <configuration>
              <verbose>true</verbose>
              <images>
                <image>
                  <alias>${docker.image.alias}</alias>
                  <registry>${docker.image.registry}</registry>
                  <name>${docker.image.name}</name>
                  <build>
                    <dockerFileDir>${docker.image.build.dockerfiledir}</dockerFileDir>
                    <assembly>
                      <name>artifact</name>
                      <descriptorRef>artifact</descriptorRef>
                    </assembly>
                  </build>
                  <run>
                    <containerNamePattern>%a</containerNamePattern>
                    <log>
                      <prefix>%z</prefix>
                      <date>DEFAULT</date>
                      <file>${project.build.directory}/${docker.image.alias}.log</file>
                    </log>
                    <network>
                      <mode>custom</mode>
                      <name>necsus</name>
                    </network>
                    <env>
                      <ACCESS_PASSWORD>${access.password}</ACCESS_PASSWORD>
                      <MYSQL_CATALOGSDB_USERNAME>${mysql.catalogsdb.username}</MYSQL_CATALOGSDB_USERNAME>
                      <MYSQL_CATALOGSDB_PASSWORD>${mysql.catalogsdb.password}</MYSQL_CATALOGSDB_PASSWORD>
                      <MONGO_NECSUSDB_USERNAME>${mongo.necsusdb.username}</MONGO_NECSUSDB_USERNAME>
                      <MONGO_NECSUSDB_PASSWORD>${mongo.necsusdb.password}</MONGO_NECSUSDB_PASSWORD>
                    </env>
                    <volumes>
                      <bind>
                        <volume>${project.build.logDirectory}:${project.build.logDirectory}</volume>
                      </bind>
                    </volumes>
                    <wait>
                      <time>4000</time>
                    </wait>
                  </run>
                </image>
              </images>
            </configuration>
            <executions>
              <execution>
                <id>start-container</id>
                <phase>pre-integration-test</phase>
                <goals>
                  <goal>build</goal>
                  <goal>start</goal>
                </goals>
              </execution>
              <execution>
                <id>stop-container</id>
                <phase>post-integration-test</phase>
                <goals>
                  <goal>stop</goal>
                  <goal>remove</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>

    <profile>
      <id>dev</id>

      <properties>
        <project.build.environment>dev</project.build.environment>
      </properties>
    </profile>

    <profile>
      <id>qa</id>

      <properties>
        <project.build.environment>qa</project.build.environment>
      </properties>
    </profile>

    <profile>
      <id>prod</id>

      <properties>
        <project.build.environment>prod</project.build.environment>
      </properties>
    </profile>
  </profiles>

</project>
